# Csic Programming Language

## Overview
Csic is an experimental dependently typed programming language. Influenced by Haskell, Agda, and Idris, it explores areas such as type universe polymorphism, linear type polymorphism, and effect handling.

## Features

### Core
* Dependently typed lambda calculus
* Propositional equality (without Axiom K)
* Inductive definitions (with dependent pattern matching)
* Structural recursion (with termination checking)
* General recursion (opt-in per-definition, disables compile-time unfolding)
* Type classes (as implicit records with instance inference)
* Explicit prenex universe level polymorphism with cumulativity (EPULP)
* Explicit prenex multiplicity polymorphism with cumulativity (QTT-style)

### Front-end Syntax Sugar
* Do-notation and inline bind #-notation
* Record definitions (with construction, update, and field projection syntax)
* Type class and instance definitions (with base classes and default methods)
* Effect ability definitions and ability constraint syntax

## Getting Started

### Setup Development Environment
1. Install the [Haskell Stack Toolchain](https://docs.haskellstack.org/en/stable/README/):
> curl -sSL https://get.haskellstack.org/ | sh
2. Build and run compiler tests:
> stack test

## Project Structure
* [doc/](doc/) - project documentation
* [src/Csic/](src/Csic/) - compiler code
    * [Core/](src/Csic/Core/) - type-checking kernel
    * [Front/](src/Csic/Front/) - front-end parsing, desugaring, and checking
    * [Runtime/](src/Csic/Runtime/) - runtime execution and external effect handling
    * [Util/](src/Csic/Util/) - language-independent utilities
* [src/stdlib/](src/stdlib/) - Csic standard library
* [test/functional](test/functional/) - Csic examples testing compiler functionality
* [test/Reference](test/Reference/) - Haskell reference implementations for comparison
* [test/Unit](test/Unit/) - unit tests for core and front-end code