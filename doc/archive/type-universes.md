I think I've arrived at a conclusion about the type universe approaches I've looked at so far.

In the last meetup, we discussed there are three main trade-offs at stake:
* **Expressivity** - how general can your definitions be with respect to universes?
* **Usability** - how much does a user need to think about universes?
* **Performance** (type-checking) - is your computer reduced to a smoldering blob of precious metals?

All of these are important, but that is the order that I prioritize. With that in mind, the winning approach (to me) can probably be called something silly like "explicit prenex universe level polymorphism with cumulativity."

Revisiting some of the examples we've used so far, I'm expecting the user to write something like this:
```
id : `A@u -> A
id x = x

Functor : (Type@u -> Type@v) -> Type
Functor F = class
  map : (`A@u -> `B@u) -> F A -> F B

Applicative : (F: Type@u -> Type@v) {Functor F} -> Type
Applicative F = class
  pure : `A@u -> F A
  seqA : F (`A@u -> `B@u) -> F A -> F B

Traversable : (Type@u -> Type@v) -> Type
Traversable F = class
  traverse : {Applicative@{v=w} `G} -> (`A@u -> G `B@u) -> F A -> G (F B)
```

The compiler elaborates these into something like this:
```
id@{u} : {A: Type@u} -> A -> A
id@{u} x = x

Functor@{u,v} : (Type@u -> Type@v) -> Type@(u+1|v)
Functor@{u,v} F = class
  map : {A: Type@u} -> {B: Type@u} -> (A -> `B) -> F A -> F B

Applicative@{u,v} : (F: Type@u -> Type@v) {Functor@{u=u,v=v} F} -> Type@(u+1|v)
Applicative@{u,v} F = class
  pure : {A: Type@u} -> A -> F A
  seqA : {A: Type@u} -> {B: Type@u} -> F (A -> B) -> F A -> F B

Traversable@{u,v,w} : (Type@u -> Type@v) -> Type@(u+1|v+1|w+1)
Traversable@{u,v,w} F = class
  traverse : {G: Type@(u|v) -> Type@w} {Applicative@{u=u|v,v=w} G} -> {A: Type@u} -> {B: Type@u} ->
    (A -> G B) -> F A -> G (F B)
```

Notes:
* The user-supplied level names (e.g. `u`, `v`, etc.) and `0` (the smallest level) are *required* to form a level **basis** for the module (set of mutual definitions)
* It is an error if any constraints are found between basis levels (other than `0 ≤ x`)
* It is an error if any level metavariables are unconstrained (relative to the basis)
* `a|b` is syntax for `∃c | a≤c ∧ b≤c` - this is not the same as `max` as we discussed in the meetup, but I think I've worked out a decent solution for interpreting this expression/constraint algorithmically

So how does this approach fair with the three trade-offs?
* **Expressivity**. I believe this approach is close to optimal, relying heavily on cumulativity. It is not quite as expressive as Agda-style first-class (e.g. non-prenex) level polymorphism, which for example, can push the `w` of `Traversable` onto `traverse`. However, that extra power comes at the cost of usability, introducing the complexities of omega universes. Additionally, the `w` level actually does appear in the `Traversable` type (it's just inferred) and I think that relation is probably more intuitive than `Omega` anyway. The `w` in the final `Type@(u+1|v+1|w+1)` of `Traversable` indicates there is an indepedent universe lower-bound outside of the constraints from `F`. Also, with cumulativity, `w` grows as needed across multiple uses of `traverse`. In practice I don't think we lose a lot of generality here.
* **Usability**. When writing general code (e.g. libraries) you need to think about universes quite a bit. Otherwise, inference will work decently well - most user code will be instantiating levels to `0` automatically. Personally, I think this is a good compromise.  Since constraints are all relative to what the library writer has thought about (hopefully carefully), the user experience of universe inconsistencies is probably a good deal improved (less reverse-engineering). But it still probably sucks, sometimes.
* **Performance**. This is pretty decent. Because the programmer identifies the level basis, the compiler can derive reasonable substitutions for any level metavariables (this relies on cumulativity to retain generality). After a module is universe-checked, all its local constraints are eliminated by these substitutions. As such, there is no explosion of metavariables and constraints as seen by implicit approaches. Level metavariables are still generated for each level parameter of each definition reference, but this is no worse in scaling than any other implicit parameter technique (e.g. more or less linear or small-polynomial with code size).

So while this approach is not the best in any of the three dimensions, it's a decent compromise across all three. I'm curious about thoughts on additional trade-offs. Also, if you're curious about the algorithm details (e.g. the Bellman-Ford adaptation for local stratification and such) - let me know.