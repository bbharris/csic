12/23/22:
- "dummy namespace" modules to group projects
- use as name prefix for project defs? or separate namespace doc tag? render optionally?
- constructors automatically namespaced by def?

12/1/22:
- no projects/workspaces:
  - search: global head modules < module dep view < local module

11/23/22:
- informal workspaces?
  - collection of working set CModBaseId - but what if moved/removed from "project" and going back to old branch?
- alternative: projects as metadata
  - attach module commits to project ID

11/15/22:
- merge def/pkg => module for "minimal deps"
- merge kernel/doc => simpler, little value in separation (if docs are version-locked anyway)
- generalize doc key/values => extensible
  - support export as doc
  - module/def links in doc
- support mutual ind/rec syntax
- add inductive kind
- support abstract defs (elimination and inline-ability)
- branches per module?
- byte-level bin format (for compress?)

drop substitutions - statically link everything?
  -- postulates back to ffi
  -- text based marker implementation, e.g.
      "runtime:open-2348a2349823842890349823"
      "python-3:pip:pygame-1.3:pygame.display.flip"
      "python-3:pip:pygame-1.3:pygame.event.wait"
      "python-3:pip:pygame-1.3:pygame.event.Event.pos[optional-attr]"

-- add data type display name to constructor lookup

DB V1 => V2
-- multi-constructor ConstRef
  -- NO, subsumed by general compression
-- multi-constructor Sort (special for omega index, etc.)
  -- NO, subsumed by general compression
-- more space-efficient flat format
  -- Bit-packed like original flat library?
    -- NO, subsumed by general compression (and negatively impacts delta compression)
  -- drop extensibility in favour of migration
    -- YES
  -- zero-overhead record types (no newtype/data distinction)
    -- YES
  -- collapse single-field nested constructor hierarchies
    -- NO, subsumed by zero-overhead records
  -- remove type-sort annotations?
    -- YES, dynamically construct sorts at type-check time
  -- remove lambda param type?
    -- NO, needed to preserve local inferencing
  -- remove let from kernel?
    -- NO, needed for indirect eval while type-checking
-- tune compression
  -- remove checked delta compression
    -- NO, saves more than 2x space
  -- gzip => zlib
    -- YES, less overhead
  -- dynamically compress only if smaller?
    -- YES, less overhead
  -- tune compress/decompress parameters
    -- NO, doesn't make a difference
