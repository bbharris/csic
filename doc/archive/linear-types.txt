Nonlinear universes subtype linear universes:
  Type[0] <: LinearType[0]

Can only introduce linear types with assumptions.
Everything else is inferered transitively:
- tuples
  - projections slightly complex sum/mul (for case analysis)
- adts
  - dependent case analysis issue (solution => nondependent case)
  - enforce linear closures after first linear ctor param (last param special case)
  - dissallow linear inductive params and indices
- functions: A -> B %-> A (linear closure)

Linear unit type (useful tag type for internal implementations):
assume UnitL:   LinearType
assume acquire: Unit -> UnitL
assume release: UnitL -> Unit

Old thoughts...

https://en.wikipedia.org/wiki/Uniqueness_type

"A unique type is very similar to a linear type, to the point that the terms are often used interchangeably, but there is in fact a distinction: actual linear typing allows a non-linear value to be typecast to a linear form, while still retaining multiple references to it. Uniqueness guarantees that a value has no other references to it, while linearity guarantees that no more references can be made to a value."

We want linear types?

strcat: @(str, str) tuple(str, str, str)

Data structures - if any constructor contains linear type, whole type is linear

What can be linear?
  Type Universe (explicit mark)
  Function type (explicit mark)
  ADT (implicit)

Externs cannot be linear

Reference parameters for input-output convenience
  Used only as reference parameters (0+ times)

  strcat: @(&str, &str) str

  Support destructured references?
    Propogates references to linear constuctor fields

Non-linear types are garbage-collected
