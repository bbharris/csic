First the interpreter has an expression predicate `isComp x` defined recursively as:
```
isComp (#(...)) = True
isComp (\v -> ...) = False
isComp x = any isComp (subExpressions x)
```
Note this is very "pseudo-cody" because I'm mixing the Haskell syntax of the interpreter with interpreted code syntax for brevity.

The interpreter also has a utility function:
```
makeComp x = if isComp(x) then x else ('pure x)
```

Finally, there are three main rewrite rules for the translation (most other cases just recurse over sub-terms):
1. `#(f r)` ===> `bind (makeComp f) (\fv -> bind (makeComp r) (\rv -> fv rv))`
    This rule says if we are executing an application, execute the function, then argument, and apply the results.

2. `(f r)` (given that `isComp f || isComp r`) ===> `bind (makeComp f) (\fv -> bind (makeComp r) (\rv -> 'pure (fv rv)))`
    This rule says if an application contains computation sub-terms, execute the function, then argument, and apply the results.
    Crucially, also wrap the result with 'pure. This differs from rule (1) that assumes the final result is a computation.

3. `case a of (p[i] -> b[i])` (given that `isComp a || any isComp b`) ===> `bind (makeComp a) (\av -> case av of (p[i] -> makeComp b[i]))`
    This rule says if any sub-terms of a pattern match are computations, execute the discriminator first.
    Then ensure each branch is a computation (e.g. wrap with `'pure` as needed).

So overall, the algorithm is essentially just sequencing computation sub-terms with `bind` and wrapping pure parts with `'pure`.  The trickiest part was figuring out the difference between rule (1) and (2) was needed.  Otherwise, I think it flows fairly intuitively from what the user programmer expects.

You can also mix and match combinators and regular binds with the # notation, but you have to be a bit careful.
For example you can say `#(if cond then !print "test" else 'pure 'unit)` or `if cond then #print "test" else 'unit`.
But `if cond then #print "test" else 'pure 'unit` is a type-error, since the else will be translated to `'pure ('pure 'unit)`.
Of course, a type-checker should catch these errors.