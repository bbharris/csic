-- code stored in files (git version control)
-- files are purely for human organization, compiler infers mutual groups across all files
-- export names with syntax prefix

file1.csic:
*foo = ... bar

file2.scic:
*bar = ... foo

-- import other projects by repo+commit or local directory, plus optional namespace qualifier

import https://bitbucket.org/bbharris/csic:64bbca4cf9cf511e5ac29bdbaf0d3c5c1e2414a4 as csic
import ./local-lib1 as lib
import ./local-lib2 -- brings all names into scope

import +"url-with-non-local-hash" -- start with all (including macros and instances)
import -"url-with-non-local-hash" -- start with nothing
  +(".*", "file-regex".csic, name, Additive Int, Negatable _, ++, >>=) {-additions-}
  -(".*", "file-regex".csic, name, Additive Int, Negatable _, ++, >>=) {-removals-}
  {name => name1, ++ => --, ".*" => "gl_$1"} {-renames-}

example:
import -"https://bitbucket.org/bbharris/csic/src/1519d896f6c2379e449871d4e012bbae18cde765/src/stdlib"
  +"arithmetic".csic {".*" => "math_$1"}

-- literals are short-hand to construct any data type?
literal extern "string_to_nat" : string -> option nat

-- operators are short-hand syntax for a named function in the module containing the type of the LHS
operator + left 10 "add"

foo = 1 + 1 -- nat.add 1 1

-- imperative style (effect handlers + let bindings)
*fibonacci x =
  if int.leq x 0 then 1 else
    y = int.add x (fibonacci (int.sub x 1))
    y


-- ast
data Term
  = Var VarName
  | App Term Term
  | Lam [(Pattern, Term)]
  | Con CtorName
  | Tup [(FieldName, Term)]
  | Prj Term FieldName

data Pattern
  = Wild VarName
  | CPat VarName CtorName [Pattern]
  | TPat VarName [(FieldName, Pattern)]

-- lambda syntaxes:
  \{pat1 -> term1, pat2 -> term2, ...}
  \pat1 -> term1
  ... \
    pat1 -> term1
    pat2 -> term2
    ...

-- let binding/sequencing syntax:
  (\pat2 -> term2) term1
  (pat2 = term1, term2)
  ...
    pat2 = term1
    term2
  case term1 of {pat2 -> term2, pat3 -> term3, ...}
  case term1 of
    pat2 -> term2
    pat3 -> term3
    ...

-- data types:
map f = \
  'nil -> 'nil
  'cons h t -> 'cons (f h) (map f t)

-- with tuples:
map ?a ?b (f : a -> b): list a -> list b = \
  'nil -> 'nil
  'cons x -> 'cons {head = f x.head, tail = map f x.tail}

point1 = {x = 1, y = 2}
point2 = point1 {y = 3} -- record update via application?


-- more ideas
- compile-time code generation? auto-detect changed files?
- literals as comptime string parsers?
- type-class methods in global namespace similar to constructors/operators?
- transform effect handlers into lambdas and extensible sums
- operators and methods as global constants - better namespacing? how to pattern match operators?


-- syntax:
-- (x = %(comp), y) === (\ky -> \{'return x -> 'return (ky x), 'bind op k -> 'bind op (ky . k)}) (\x -> y) comp
-- ^op a b c        === %('bind ('op a b c) id)
-- $ref             === ^refLoad ref
-- ref <- val       === ^refStore ref val
-- arr[i]           === ^arrayIndex arr i