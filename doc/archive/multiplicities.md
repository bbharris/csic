Those are basically the steps, except its not unification because of the ⊆ partial order.
The ⊆ partial order is necessary for function sub-multiplicities, but also just to check usages in a term.
If we unified then we couldn't use a ω-parameter 0 or 1 time, for example.

The general form of my multiplicity constraint is:
  a1*a2*... + b1*b2*... + ... ⊆ c1*c2*... + d1*d2*... + ...

Each side is normalized to sum-of-products form, with a deterministic ordering and with all the direct semiring simplications applied.
I even reduce equalties A = B to A ⊆ B and B ⊆ A, because the algorithm needs to handle the general case where an equality emerges from independent constraints.

In this general form, it seems to me all we can do is shrink the lower and upper bound of each metavariable.
When the bounds meet we can eliminate a metavariable by substitution.
When they cross we have an inconsistency.

To do that I try to reduce each general constraint to one of two forms:
A ⊆ x, where x is a metavariable and A contains no metavariables, then we can directly update the lower bound for x
x ⊆ A, where x is a metavariable and A contains no metavariables, then we can directly update the upper bound for x

To give an example of some of the heuristics I apply to simplify constraints:
1+x ⊆ 1  ==>  x ⊆ 0 (which implies x = 0, because 0 is a least element)
ω ⊆ 1+x  ==>  1 ⊆ x (doesn't allow us to substitute yet, unless we already have x ⊆ 1)

There are many heuristics we can apply because the semiring is small, but a general polynomial solver would be far superior.
I don't know if one exists though (my guess is currently negative).

--------
12/12/24

I have been trying to find a representative example where multiplicity inequalities are useful, but most of the cases seem to be a bit complex. As I've been experimenting with different strategies though, one example keeps coming up - my Foldable typeclass. This example is nice in that it doesn't involve data types/church-encodings at least. It also doesn't really matter that it's a typeclass - we can just look at it as a set of function types and default implementations that assume one to derive the others (which is where things get interesting).

Leaving off multiplicities for now, the typeclass has 3 methods, borrowed from Haskell:

  foldl : (`B -> `A -> B) -> B -> F A -> B
  foldr : (`A -> `B -> B) -> F A -> B -> B
  foldMap : {Monoid `B} -> (`A -> B) -> F A -> B

At a minimum an instance only needs to implement one of the above and we can derive the other two. Haskell does this with arbitrary mutual recursion in method defaults, but I chose to make the default dependencies explicit (and easily termination-checked), like this:

  {foldl} =>
    foldr   f = foldl (\acc v => f v . acc) id
    foldMap f = foldl (\acc v => f v <> acc) empty

This says we can derive foldr and foldMap automatically if an instance provides foldl. This is all just syntax sugar - these are really just top-level functions like foldr_from_foldl, etc. that take the foldl as a parameter. Also note these defaults may not be particularly efficient - if the language is strict then foldl often optimizes to a for-loop while this foldr allocates N thunks. Efficiency aside, I find the parallel structure between the default foldr and foldMap really elegant (it probably demonstrates some monoidal property of id and compose (.), but that's for category theorists to talk about).

If that makes sense so far, let's look at adding multiplicies. First, let's see if the default implementations count-check without any annotations or polymorphism (assuming no annotation means ω).

It's a bit hard to parse foldr so let's add the inferred type annotations:

    foldr f = foldl `A=A `B=(B %?-> B) (\acc v => f v . acc) (id `A=B)

The accumulator here is a function B %?-> B, but what is that missing count? With only equality constraints it would have to match id, so B %1-> B. However, since f v uses its next parameter ω times, the composition f v . acc results in B %ω-> B. I can't think of a way for this example to count-check without a sub-count relation 1 ⊆ ω. But this sub-count relation came from checking convertibility between two pi-types B %1-> B and B %ω-> B. Does that imply the need to generate arbitrary count ⊆-constraints for function type convertibility?

When I extended Foldable further with multiplicity polymorphism, the issue persists. My current solution has types:
  foldl : (`B@ %(q+1)-> `A@ %(p+1)-> B) -> B %(q+1)-> F A %(r+1)-> B
  foldr : (`A@ %(p+1)-> `B@ %(q+1)-> B) -> F A %(r+1)-> B %(q+1)-> B

And the default foldr only count-checks because 1 ⊆ q+1 (in addition to other ⊆-identities that resolve out of the composition f v . acc), following the same reasoning as the unrestricted version. This is now more general though because it captures a linearly foldable structure, or an unrestricted one, or both simultaneoulsly (depending on an instance's choice of count polymorphism or monomorphism).