
I'm thinking through why primitive recursion is insufficient and how well-founded recursion helps. I might be piecing together some ideas.

Firstly, if we have some function that we can prove terminates in some finite number of steps (fuel), but we don't know what that number is - primitive recursion seems useless.  My initial naive thought was to guess at the number, and then retry with a larger number if it's not enough - but this doesn't work at all - we would need an upper bound for the number of retries, or the number of retries of retries, etc.

How well-founded recursion helps with this problem does not seem intuitive, but I am seeing some clues.
Using ordinal numbers as a concrete example seems to help at least.

Ordinal numbers can be defined in Agda like this:
```
data Ord : Set where
  zero : Ord
  succ : Ord -> ord
  limit : (f : Nat -> Ord) -> Monotone f -> Ord
```

I don't think the monotone condition on `f` is relevant for this discussion of well-founded recursion but it is necessary for defining ordinal operators in a way that makes sense?

Now, consider some function defined using well-founded recursion over an `Ord`:
```
someOrdFunc : (x : Ord) -> ...
someOrdFunc ...
someOrdFunc (limit f f_mono) = let i = ... in ... someOrdFunc (f i) ...
```
I've left out all the details of the function - the only part I care about is the `limit` case and its recursive call.
This is the part that needs to pass Agda's well-founded termination check, and it does (assuming this is the only recursive call in this clause). The reason for termination is that `f i` is structurally smaller than `limit f f_mono` - because `f` had to be constructed before `limit f f_mono`.

The interesting part is that `i` can be any natural number - it can be computed however `someOrdFunc` would like to, and there is no limit on that? The limit is on the result of `f i` being structurally smaller, which is all we need. That means `f i` could compute its result ordinal by a fold over `i` (or anything else) - but I think `i` can increase the number of algorithm steps arbitrarily (but finitely)?

From this example I'm seeing that well-founded recursion allows a function to compute how much fuel it needs "along the way" instead of "upfront".  We know the final amount of fuel it uses is finite, but we can't determine the bound without running the function itself. Is this why well-founded recursion is more powerful/general than primitive recursion?

UPDATE:

Here's a summary of how we can build up from the simple binary tree to infinite ones.  At each step, we can encode the tree in (at least) two forms - the first encoding branches with regular data types (and nested positivity) and the second using a higher-order function (like `Ord` above).

Here's the simple familiar case (with less familiar higher-order encoding):
```
data BinaryTree1 : Set where
  Leaf : BinaryTree1
  Node : (left : BinaryTree1) -> (right : BinaryTree1) -> BinaryTree1

data BinaryTree2 : Set where
  Leaf : BinaryTree2
  Node : (f : Bool -> BinaryTree2) -> BinaryTree2
```
These are isomorphic and "primitively recursive" - we can recurse over the structure to compute an upper bound for recursion.  This property relies on `Bool` being finite.

Next we can extend this to arbitrary (but still finite) N-ary trees:
```
data NaryTree1 : Set where
  Leaf : NaryTree1
  Node : (n : Nat) -> (branches : Vec n NaryTree1) -> NaryTree1

data NaryTree2 : Set where
  Leaf : NaryTree2
  Node : (n : Nat) -> (f : Finite n -> NaryTree2) -> NaryTree2
```
Again these are isomorphic and primitively recursive, similarly because `Finite n` is... well, finite. :smile:

And finally, generalizing to infinitary trees:
```
data InfinitaryTree1 : Set where
  Leaf : InfinitaryTree1
  Node : (branches : Stream InfinitaryTree1) -> InfinitaryTree1

data InfinitaryTree2 : Set where
  Leaf : InfinitaryTree2
  Node : (f : Nat -> InfinitaryTree2) -> InfinitaryTree2
```
Now these are isomorphic, but not primitively recursive, because `Nat` is infinite.  The `Stream` encoding may be less familiar - this is a co-inductive type that allows extracting a finite list prefix from an infinite stream of values - that is isomorphic to a function from `Nat` to the element type. The `Stream` encoding is potentially less efficient, since we need always need to unpack the `Stream` linearly (it is like an iterative generator), while the function form can be optimized for particular streams (e.g. a constant function for a constant stream).

The conclusion I have for recursing over these infinitary trees is that we cannot compute a size or maximum fuel, but as long as we only traverse a finite subset of branches at each `Node` in whatever recursion scheme we may have, we are still guaranteed to terminate.
