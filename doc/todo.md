# TODO

## Core

- [x] feature: catch loop/exhaustion errors in IO layers, monadic eval not good for unify backtracking
- [x] feature: basic termination checking
- [x] feature: basic positivity checking
- [x] feature: exhaustion checking
- [x] feature: type universes
- [x] refactor: continuation-style mutual checking to improve front-end type information?
- [x] refactor: separate constructor refs to avoid storing implicit data type parameters? not worth complexity
    - branch: exper/constructor-constants
- [x] feature: multiplicities
    - branch: exper/multi-poly-qtt-3
- [x] feature: generalize parameterized declaration syntax
    - branch: exper/dry-decl-syntax
- [x] refactor: parameterize decls by metavariables, check to Void
    - branch: exper/core-pending-decls
- [x] refactor: parameterize level and counts by metavariables, check to Void
    - branch: exper/core-unified-metas
- [x] optimize: solveInstance should not indexTCtx every variable (N^2)
- [x] optimize: cache isInstanceVar in TCtx
- [x] optimize: introduce IName, convert CEnv to HashMap Int CDecl
- [x] optimize: replace all Data.Map with Data.HashMap (faster lookup) or Data.IntMap (faster cons)
- [x] optimize: share more sub-terms in instantiateTerm from multis and levels
- [x] optimize: LevelId and MultiId back to Int (names only in decl)
    - branch: exper/optimize-level-id
- [x] refactor: try relude again, e.g. way better Map interfaces... nope, custom interfaces instead
    - branch: exper/optimize-relude
- [x] optimize: general constraint dependency graph (blocking metavariable dependencies)
    - branch: exper/optimize-constraints
- [x] optimize: wrap let-binds in Sized, delay shiftTerm to access
    - branch: exper/optimize-let-binds
- [x] optimize: ListSet, ListMap wrappers for safety (test perf)
- [x] optimize: never use HashMap.size and HashSet.size - O(N)
- [x] optimize: use arrays for substituting level/multi metavariables? NO! monadic approach slow and sucks
    - branch: exper/optimize-io-monad
- [ ] optimize: utility Nat-indexed context list/sequence wrapper (generalized Vector)
- [ ] optimize: rework TCtx for faster indexing and Multi composition
- [ ] refactor: utility De-Bruijn index/level wrappers
- [ ] feature: inductive index (or general?) equality rewrites/substitutions (avoid Axiom K?)
- [ ] feature: type pattern matching (or fully generally, lambdas too?)
- [ ] feature: advanced termination checking (multiple calls, mutual recursion, lexical ordering)
- [ ] feature: advanced positivity checking (nested, with eval)
- [ ] feature: revisit primitive declaration lookup and checks (especially for type-check eval)

## Front-End

- [x] feature: record construction/update
- [x] refactor: cleanup frontend desugaring passes?
- [x] refactor: convert from parsec to megaparsec+parser-combinators
- [x] refactor: resolve macro operators and build parser per expression
- [x] feature: literal type-classes (e.g. fromStr, fromNat)?
- [x] feature: recursion ann syntax: rec => general, guarded inferred (multi-index)
- [x] refactor: cleanup instance desugar type pass and self-refs
- [x] refactor: typed annotations
- [x] feature: let/do pattern match syntax
        - branch: exper/general-pattern-syntax
- [ ] feature: revisit mutual blocks (auto-detect module?, prepend mutual parameters?)
- [ ] feature: import remote repo+commit (or just rely on git submodules?)
- [ ] feature: import regex renames or namespace prefixes?
- [ ] feature: linear ref/in-out parameter syntax sugar?
- [ ] feature: indexed inductive type syntax (desugar to equalities?)
- [ ] feature: keep all symbols in env but mark hidden/renamed for error messages
- [ ] feature: even more general pattern syntax (e.g. macros/operators, lists)?
- [ ] feature: real macros as front-end AST transformers? (needs termToExpr util as well?)

## Back-End

- [x] feature: interpreter (re-uses type-check eval, wrapped with effect handling)
- [ ] feature: potential targets: Chez Scheme, Racket, Closure, SBCL, GLSL, SPIRV, C?

## Testing

- [x] revive functional tests
- [x] revive benchmark tests
- [ ] revive generated tests

## Tooling

- [ ] FFI generator
- [ ] cache type-checked modules
- [ ] language server
- [ ] structural editor?
