# Pattern-Matching Lambdas

## Introduction
In Haskell, GHC has an extension called [`LambdaCase`](https://ghc.gitlab.haskell.org/ghc/doc/users_guide/exts/lambda_case.html) that enables short-hand syntax for a lambda followed by immediate pattern-match of the parameter.

Borrowing from the extension docs, expressions written as:
```
\case { p1 -> e1; ...; pN -> eN }
```
are translated to:
```
\freshName -> case freshName of { p1 -> e1; ...; pN -> eN }
```

In Csic, we take a different approach: the `\case` syntax is a primitive term former of the Csic Core AST, and all other lambda and case variations are derived from this.

Firstly, note that Csic uses slightly different punctuation for `\case` expressions:
```
\case ( p1 => e1, ..., pN => eN )
```

Now can we show the variations of syntax that desugar to `\case` expressions:

- Traditional `case` expressions are applied `\case` expressions:
    `case arg of ( p1 => e1, ..., pN => eN )` ==> `(\case ( p1 => e1, ..., pN => eN )) arg`
- Traditional lambdas are `\case` expressions with a single branch:
    `\p1 => e1` ==> `\case ( p1 => e1 )`
- Let-bindings are applied `\case` expressions with a single branch:
    `(p1 = arg, e1)` ==> `(\case ( p1 => e1 )) arg`

As you can see, `\case` expressions generalize a great deal of user-facing syntax. It is also interesting that `\case` expressions generalize lambdas in the same way that `case` expressions generalize let-bindings.

## Motivation
The primary motivation for taking this approach was to reduce the number of term formers in the Csic Core AST. This approach significantly reduces the amount of code in the Csic type-checking kernel, because in many situations, all of the above variations of `\case` expressions are handled uniformly.

There are however, situations where the variations are distinguished. Most prominently, during type-checking, applied `\case` expressions infer the type of the argument first, and then check the `\case` expression with a pi-type constructed from the argument's type (and result type as a fresh metavariable). This contrasts from normal function application, which infers the type of the function and then checks the argument against the function's parameter type.

These special-cases are not difficult to implement however, and can usually be seen as optimizations to aid type-inference, as opposed to major differences in the core typing rules.
