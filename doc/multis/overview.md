# Multiplicity Polymorphism Overview

This feature extends [Quantitative Type Theory (QTT)](https://bentnib.org/quantitative-type-theory.html) with polymorphism on top-level declarations.

## Terminology

- **runtime** - term evaluation occurring after type-checking, potentially after compilation to a different target language
- **erasable** - terms that are not used at runtime and thus, as an optimization, *can* be discarded when translating to a runtime target language
- **multiplicity** - the number of times a function's parameter is used at runtime each time the function's result is used at runtime
- **count** - synonym for multiplicity

## Count Semiring

As in QTT, we start with an algebraic semiring defined as follows.

### Literals

Counts have three possible literal values:

- `0` - used zero times at runtime (e.g. erasable)
- `1` - used exactly once at runtime (e.g. linear)
- `ω` - used any number of times at runtime (e.g. unrestricted)

### Operators

Count literals are equipped with two binary operators `+` and `*`, defined as follows:
```
+ | 0 1 ω       * | 0 1 ω
--|------       --|------
0 | 0 1 ω       0 | 0 0 0
1 | 1 ω ω       1 | 0 1 ω
ω | ω ω ω       ω | 0 ω ω
```

These operators have the following properties:

- `+` is associative, commutative, with identity element `0` and absorbing element `ω`
- `*` is associative, commutative, with identity element `1` and absorbing element `0`
- `*` is also idempotent (e.g. `x * x = x`) and distributive over addition

## Count Expressions

### Syntax

**Count expressions** provide concrete syntax for the count semiring, and extend it with variables and metavariables.
A count expression `<count>` can be one of:

- Literal: `0`, `1`, `ω`
- Composition: `<count> + <count>`, `<count> * <count>`
- Grouping: `(<count>)` (as standard, `*` has higher precedence than `+` and both are left associative)
- Variable: `p`, `q`, `r`, ...
- Metavariable: `$1`, `$2`, `$3`, ... (abstract syntax, the user never writes these)
- Metavariable hole: `_` (concrete syntax that generates a fresh abstract metavariable)

Note that all of the algebraic properties of the count semiring still hold with these extensions.

### Substitution

With variables added to the count semiring, we gain a notion of **count substitution** which replaces a set of variables and/or metavariables in a count expression with other count expressions. Count substitutions are expressed in a dictionary-like notation mapping variable and metavariable names to arbitrary count expressions. For example:

- `{}` is the empty/identity substitution
- `{p = m, $1 = n}` maps variable `p` to count `m` and metavariable `$1` to count `n`

We notate the application of a substitution to a count expression as `m s` where `m` is the count expression and `s` is the count substitution. For example:

- `(1 + p){p = ω} = 1 + ω = ω`
- `(ω * $2){$2 = q} = ω * q`

Additionally, we identify several special sets to help restrict the domain of substitutions:

- **AllVars** - is the set of all variable (and metavariable) names
- **LitSubs(V)** - is the set of all substitutions that map a set of variable names `V` to the set of count literals

### Equivalence

Two count expressions `m` and `n` are **equivalent**, notated as `m ≅ n`, if every possible substitution applied to `m` and `n` that maps variables to count literals, yields the same count literal. Formally, `m ≅ n` iff `∀(s ∈ LitSubs(AllVars)). m s = n s`.

### Normal Form

*Conjecture*: Every count expression can be algorithmically simplified to a **Count Normal Form (CNF)**, such that all equivalent count expressions simplify to the same CNF.

The CNF-algorithm is somewhat complex and I have only verified correctness up to 3 count variables via exhaustive testing. However, a few examples can show what kind of identities it employs:

- `CNF(1 + ω*p) = 1 + p` (easy to verify)
- `CNF(p + q + p*q) = p + q` (somewhat harder)

The full algorithm can be found in the `multiNormalize` function in [src/Csic/Core/Syntax/Multi.hs](/src/Csic/Core/Syntax/Multi.hs).

### Convertibility

Two count expressions `m` and `n` are **convertible**, notated as `m ⊆ n`, if being used `m` times can be treated as being used `n` times. Formally, `m ⊆ n` iff `∀(s ∈ LitSubs(AllVars)). m s ≤ n s`, where `≤` is a relation on count literals defined by:
```
≤ | 0 1 ω
--|------
0 | T F T
1 | F T T
ω | F F T
```
The properties of the `≤` relation are:

- reflexive, transitive, antisymmetric
- weakly connected since neither `0 ≤ 1` nor `1 ≤ 0`
- `ω` is a top element

Thus the `≤` relation forms a partial order.

*Conjecture*: Like CNF, the generalized `⊆` relation is also decidable in polynomial time. My algorithm (verified up to 3 variables) can be found in the `multiConvertible` function in [src/Csic/Core/Syntax/Multi.hs](/src/Csic/Core/Syntax/Multi.hs).

## Count Annotations

This section describes how the above algebra interacts with the concrete syntax of Csic. For an overview of the general Csic syntax, see [test/functional/syntax.csic](/test/functional/syntax.csic).

### Function Parameters

Every pi-type parameter in the Csic Core AST is annotated with a count expression. The general concrete syntax for parameters is `(x : A %m)`, where `x` is a name, `A` is a type, and `m` is a count expression. Additionally, there are several short-hand notations:

- `A %m` is the same as `(_ : A %m)`, which means the parameter is anonymous (cannot be referenced)
- `(x : A %+m)` is the same as `(x : A %(1 + m))` (this count expression is very common)
- `(x : A)` means the count takes on a default value, depending on context:
    - For function definition parameters, the default count is `_` (a fresh count metavariable) since these counts should be inferrable from the function implementation.
    - For data type, class, and instance parameters, the default count is `0` since these parameters are usually types or erasable indices.
    - For constructor parameters, the default count is `1` since constructor parameters inherit the count of the whole value that is pattern-matched (via multiplication, see below constraint generation rules).
    - In all other contexts (e.g. normal pi-type terms), the default count is `ω` so that unrestricted syntax is the cleanest.

### Top-Level Declarations

In the above algebra, there isn't a meaningful distinction between count variables and metavariables. However, their purpose and quantification are very different:

- Count metavariables are placeholder counts introduced by the user and/or type-checker that will eventually be substituted for a count expression containing no metavariables. Thus a complete type-checked and count-checked declaration does not contain count metavariables.
- Count variables, on the other hand, reference the **count parameters** of a top-level declaration. These parameters are introduced implicitly as they are referenced. In other words, the set of distinct count variable names referenced in a top-level declaration's type and implementation are the count parameters of the declaration.

Because count parameters are introduced implicitly, there is no syntax needed to quantify over them. However, it is important to keep in mind that these parameters are in a prenex-position over the entire declaration, and therefore in scope for all type parameters, the result type, and implementation.

As an example:
```
someDef (x: Nat %p) : Bool = f x (Nat %p -> Bool %q -> Nat)
```
The declaration `someDef` has two count parameters `p` and `q`. Both references to `p` refer to the same count parameter, even though they annotate independent pi-type parameters.

### Declaration References

When any top-level declaration is referenced by another, we instantiate the count parameters of the referenced declaration. By default, this is done implicitly by constructing a count substitution from the referenced declaration's count parameter names to fresh count metavariables in the referencing declaration's type-checking environment.

However, because ambiguities may arise, we have syntax to specify this instantiation as well (partially or completely).
Using the `someDef` example above, the most general syntax is `someDef%{p=m,q=n}`, where `m` and `n` are count expressions (which may contain metavariables or introduce and refer to count parameters in the referencing declaration). Note the `{p=m,q=n}` part is in fact a [count substitution](#substitution).
There are a couple of short-hand notations:

- `someDef%{p=m}` is the same as `someDef%{p=m,q=_}` - any missing parameters default to fresh metavariables
- `someDef%{p,q=n}` is the same as `someDef%{p=p,q=n}` - a standalone name substitutes a count parameter of the same name (but again, now in the scope of the referencing declaration)

Given a declaration reference's count substitution, instantiation proceeds by applying the substitution to all count expressions in the declaration (both type and implementation). After instantiation, the referenced declaration's type and implementation can now be safely inspected as needed during the rest of type-checking, because any counts encountered are now in the scope of the referencing type-check environment.

## Count Constraints

A **count constraint** relates two count expressions either by equivalence `≅` or by convertibility `⊆` defined above.

### Generation

During type-checking, the compiler generates count constraints to be solved in a deferred count-checking step. Constraints are generated in several situations:

- When checking two pi-types for equality (`(x : A %m) -> B` =? `(y : C %n) -> D`):
    - **generate** `m ≅ n`, ensuring the parameters are count-equivalent
- When checking two pi-types for convertibility (`(x : A %m) -> B` ⊆? `(y : C %n) -> D`):
    - **generate** `m ⊆ n`, ensuring the parameters are count-convertible
- When checking two declaration references for equality (`a%s` =? `b%t`):
    - The two declaration names `a` and `b` must be equal (otherwise unification fails)
    - For each count parameter name `k` in `s` (and `t`), **generate** `s[k] ≅ t[k]`
    - This ensure that equal declaration references instantiate count-equivalent types and implementations
- When checking a [pattern-matching lambda](/doc/features/pattern-matching-lambdas.md) (`\case (p1 => e1, ..., pN => eN)` :? `(x : A %m) -> B`):
    - For each branch `p_i => e_i` in `p1 => eN` through `pN => eN`:
        - Generate a fresh count metavariable `$i` for the *descriptive* count of `x` in this branch
        - **generate** `$i ⊆ m`, ensuring branch descriptive count is convertible to the prescriptive count
        - Initialize `$i` to `0` to begin tracking all the uses of `x` in this branch
        - Accumulate into `$i` all usages of `x` in `e_i` according to the QTT rules (TODO: details needed?)
        - If `p_i` is a constructor pattern (destructing the parameter):
            - add `1` to `$i`, since destructing a parameter uses it
            - For each constructor parameter `c` in the pattern:
                - Compute the *prescriptive* count for `c` as `m * c_m`, where `c_m` is the prescriptive count of the parameter in the constructor's type (e.g. default `1`)
                - Apply the same usage counting procedures for `c` as we did for `x`

### Solving

After type-checking and unification are complete (e.g. all term metavariables are resolved), we are left with a set of count constraints that were generated during those steps. We solve these all at once in a post-step, referred to as **count-checking**. This makes it trivial to optionally disable count-checking - we can simply discard the constraints and assume every parameter was annotated with `ω`. Incidentally, this mirrors what we do when "type-in-type" is enabled, which discards universe level constraints that are generated and deferred to an analogous level-checking step.  Count-checking and level-checking (the order of which is irrelevant) are the final steps taken before a top-level declaration is added to the checked declaration environment for future declarations to reference.

#### Algorithm

The current algorithm for count-checking is quite powerful, but with a major caveat: it only supports count inference for declarations with 3 or fewer count parameters. This limitation may be relaxed in the future if better algorithms are developed. In the meantime, declarations with 4 or more count parameters may need additional count annotations. So far we have not encountered a practical use case for more than 3, and 3 is already a rarity.

The reason for this limitation is actually the source of the algorithm's power: we employ brute force to solve count metavariables. At its core, the algorithm is fairly straightforward:

1. For each count metavariable `$k` contained in the set of input count constraints:
    - Initialize the set of potential solutions for `$k`, containing all count expressions over the count parameters of the declaration being checked
    - When the number of count parameters is 3, this initial set contains 6561 CNF count expressions
2. For each count constraint `m ≅ n` or `m ⊆ n` in the set of unsolved constraints:
    - Generate the set of all count substitutions for the potential solutions of the count metavariables in `CNF(m)` and `CNF(n)`
    - If this count substitution set is reasonably small (a command-line-controllable parameter):
        - Filter the list of substitutions to the ones that are valid - this is polynomial-time decidable from the above conjectures of [Normal Form](#normal-form) and [Convertibility](#convertibility)
        - If the filtered list is empty, report a constraint error and fail
        - Otherwise, use the filtered list to narrow the potential solutions of each impacted metavariable
        - For each impacted metavariable:
            - If its solution set is empty, report a constraint error and fail
            - If its solution set contains one element OR a lower-bound element that is convertible to all others in the set, substitute the count metavariable for this count expression in all remaining constraints
            - Otherwise, the metavariable is not yet solvable, just continue with the narrowed solution set
        - Remove the constraint from the constraint set
3. Repeat step 2 until no constraints remain (success) or no constraints are solved in that step (error)
4. Any metavariables that were not narrowed can be substituted with `ω` (or alternatively, report ambiguity)
5. Verify all metavariables were solved and double-check their solutions solve the original input constraints

This algorithm does provide a decidable solution (if you set the limit parameters to infinite), but is very much exponential. In practice, however, with 3 or fewer count parameters it seems to behave quite well.

#### Future Work

One potential research avenue might use the fact that this semiring is a [closed semiring](https://cs.stackexchange.com/questions/54616/solving-systems-of-linear-equations-over-semirings), for which there exist polynomial-time linear equation solvers. While this avenue is hopeful for equivalence constraints, it is unclear if these solvers could be adapted to handle convertibility constraints.

**Theorem**: The count semiring defined above is a closed semiring.

**Proof**: With `a* = 1 + a`, the equation `a* = 1 + a × a*` holds, because:
```
a* =? 1 + a × a*        | goal
a* =? 1 + a × (1 + a)   | substitution of a*
a* =? 1 + a + a × a     | distributivity of multiplication over addition
a* =? 1 + a + a         | idempotency of multiplication
a* =? 1 + a × (1 + 1)   | distributivity of multiplication over addition
a* =? 1 + a × ω         | definition of addition
a* =? 1 + a             | case analysis on a (same lemma as the example in the Normal Form section above)
a* =? a*                | substitution of a*
                        | refl
```
Additionally, `a* = 1 + a* × a` holds because multiplication is commutative.
∎