# Multiplicity Polymorphism Example

This document is a deep-dive example for the count-inference algorithm documented [here](/doc/multis/overview.md).
The example covers the `either` utility function present in most functional languages. This example is nice in that it has a motivation for count polymorphism, uses the lambda-pattern-matching construct, and doesn't use type-classes (which are complex and don't impact the algorithm in an essential way).

## Motivation

With Linear Haskell, we can have:
```
either :: (a %p-> c) -> (b %p-> c) -> Either a b %p-> c
either f g = \case {Left x -> f x; Right y -> g y}
```
This Haskell `either` is count polymorphic over `{1,ω}`, since Linear Haskell doesn't have `0`.
If we try to extend this into QTT naively, it immediately fails, since this function does not support `p=0` (it always uses the `Either` value at runtime).

One solution with QTT polymorphism is to "bump" *usage* of the `p` count parameter into the `{1,ω}` space by adding `1` everywhere.
Therefore the Csic count-polymorphic `either` looks like this (I've removed level-polymorphism syntax for simpler comparison):
```
either : (A %+p-> C) -> (B %+p-> C) -> Either A B %+p-> C
either f g = \case ('left x => f x, 'right y => g y)
```
Here `%+p` is short-hand for `%(1+p)` - a.k.a the increment operator (and also the star-operator of the QTT closed semiring).

This `either` now expresses the same two use cases as the Haskell version, but as we will see, the count inference algorithm now has to handle more interesting constraints.

## Input Code

To demonstrate the algorithm, let's annotate the input code with count metavariables for each parameter:
```
either : (A %+p-> C) %$0-> (B %+p-> C) %$1-> Either A B %$2-> C
either f g = \case ('left x => f x, 'right y => g y)
```
I've removed the explicit `1+p` annotation from the final parameter to give the algorithm more work - it should be able to infer parameter counts in descriptive positions, so we now have three count metavariables `$0`, `$1`, and `$2`.

## Usage Tree

The first stage of the algorithm, as described in the overview, generates count constraints in several situations. The only case used here, which is by far the most complex, is the pattern-matching-lambda case. For this case, the compiler internally generates a "usage tree" that describes all the variable usages it found in the input code based on the QTT rules.

Here is a cleaned-up and annotated print-out of the usage tree generated for `either`:
```
(A: _) %0-> _
  (C: _) %0-> _
    (B: _) %0-> _
      (f: _) %$0-> _
        (g: _) %$1-> _
          [Split]
            (discL: _) %$2-> _   -- automatic discriminator parameter, e.g. discL@('left x)
              {discL} += 1       -- from destructing discL
              (x: _) %$2-> _
                {f} += 1         -- from calling f
                {x} += 1+p       -- from applying f x, where f has a prescriptive parameter count of 1+p
            (discR: _) %$2-> _   -- automatic discriminator parameter, e.g. discR@('right y)
              {discR} += 1       -- from destructing discR
              (y: _) %$2-> _
                {g} += 1         -- from calling g
                {y} += 1+p       -- from applying g y, where g has a prescriptive parameter count of 1+p
```
This tree shows the count-checking context of each variable usage. Each parameter binds a node (with the parameter name and *prescriptive* parameter count displayed afterwards). Each curly brace node is a variable usage that refers to a parameter in the context and accumulates into the descriptive count of that parameter. For example `{f} += 1` adds `1` to the descriptive count of the `f` parameter. The `[Split]` node represents a case split, which is used to merge usages from different branches.

It is also worth pointing out that the prescriptive counts for `x` and `y` are each calculated as `$2 * 1 = $2` from the rule of multiplying the discriminator's prescriptive count (`$2`) by each constructor parameter's prescriptive count (`1`), recalling that the default count for constructor parameters is `1`, e.g.:
```
'left  : A %1-> Either A B
'right : B %1-> Either A B
```

## Usage Constraints

For the next step, the algorithm generates the following constraints from the usage tree.
Each constraint is relating a descriptive (e.g. computed) count on the left side to a prescriptive (e.g. user-specified) count on the right side.
```
0 ⊆ 0      -- descriptive count for A parameter = 0 (e.g. it was NOT used)
0 ⊆ 0      -- descriptive count for C parameter = 0 (e.g. it was NOT used)
0 ⊆ 0      -- descriptive count for B parameter = 0 (e.g. it was NOT used)
$3 ⊆ $0    -- descriptive count for f parameter = $3 (generated metavariable to merge branch usages)
1 ⊆ $3     --   constrain the descriptive count for f by the 'left branch (e.g. it was used)
0 ⊆ $3     --   constrain the descriptive count for f by the 'right branch (e.g. it was NOT used)
$4 ⊆ $1    -- descriptive count for g parameter = $4 (generated metavariable to merge branch usages)
0 ⊆ $4     --    constrain the descriptive count for g by the 'left branch (e.g. it was NOT used)
1 ⊆ $4     --    constrain the descriptive count for g by the 'right branch (e.g. it was used)
1 ⊆ $2     -- descriptive count for discL parameter = 1 (e.g. it was used)
1 ⊆ $2     -- descriptive count for discR parameter = 1 (e.g. it was used)
1+p ⊆ $2   -- descriptive count for x parameter = 1+p (e.g. it was used 1+p times)
1+p ⊆ $2   -- descriptive count for y parameter = 1+p (e.g. it was used 1+p times)
```
Most of these constraints are straight-forward, but the merging cases for `f` and `g` are particularly interesting. It is clear that the placeholder `$3` and `$4` metavariables are not really necessary for this example, but they are needed when you start accumulating usage from case splits with other usages. For these scenarios, we need to merge the case split usage first before accumulating.

Here is a slightly abstract usage tree to demonstrate this:
```
[in some context with parameter x with prescriptive count $x]
              f (case ... of ...)
             /               / \
            /               /   \
          ...            'left  'right
          /               /       \
         /               /         \
   use x 1 time    use x 1 time   use x 1 time
```

In this scenario, it is completely incorrect to generate the independent constraints:
```
1 ⊆ $x
1 ⊆ $x
1 ⊆ $x
```

Instead we generate:
```
1 + $merge ⊆ $x
1 ⊆ $merge
1 ⊆ $merge
```
This way `$merge` is constrained to `1` and then `1 + 1 = ω ⊆ $x` constrains `$x` to `ω`, which is correct.
With this more general case in mind, the seemingly redundant `$3` and `$4` metavariables for the `either` example should hopefully make sense now.

## Constraint Solving

This is the final step of the algorithm, which currently employs a brute force approach to solve the metavariables in the above constraints so that each constraint is always true.

Because we only have one count parameter `p`, there are only **6** possible substitutions for each count metavariable:
```
0
1
ω
p
1+p
ω*p
```
All other count expressions are eliminated by `multiNormalize`.

Here is a cleaned-up and annotated debug-trace of the algorithm (note the constraint order differs from above but the order is irrelevant):
```
TRIVIAL: 0 ⊆ 0                    multiCovertible returns True, discard this constraint
TRIVIAL: 0 ⊆ 0                    multiCovertible returns True, discard this constraint
TRIVIAL: 0 ⊆ 0                    multiCovertible returns True, discard this constraint
FILTER: $3 ⊆ $0                   filter the 36 possibilities for [$3,$0] by this constraint, then discard it
  REFINE: [$3,$0]: 36 ==> 14          narrowed to 14 possibilities
FILTER: $4 ⊆ $1                   filter the 36 possibilities for [$4,$1] by this constraint, then discard it
  REFINE: [$4,$1]: 36 ==> 14          narrowed to 14 possibilities
FILTER: 1 ⊆ $4                    filter the 14 possibilities for [$4,$1] by this constraint, then discard it
  REFINE: [$4,$1]: 14 ==> 6           narrowed to 6 possibilities
FILTER: 0 ⊆ $4                    filter the 6 possibilities for [$4,$1] by this constraint, then discard it
  SUBST: [($4,ω),($1,ω)]              narrowed to 1 possibility, found substitutions
FILTER: 0 ⊆ $3                    filter the 14 possibilities for [$3,$0] by this constraint, then discard it
  REFINE: [$3,$0]: 14 ==> 6           narrowed to 6 possibilities
FILTER: 1 ⊆ $3                    filter the 6 possibilities for [$3,$0] by this constraint, then discard it
  SUBST: [($3,ω),($0,ω)]              narrowed to 1 possibility, found substitutions
FILTER: 1 ⊆ $2                    filter the 6 possibilities for [$2] by this constraint, then discard it
  REFINE: [$2]: 6 ==> 3               narrowed to 3 possibilities
FILTER: 1+p ⊆ $2                  filter the 3 possibilities for [$2] by this constraint, then discard it
  REFINE: [$2]: 3 ==> 2               narrowed to 2 possibilities
FILTER: 1 ⊆ $2                    filter the 2 possibilities for [$2] by this constraint, then discard it
  REFINE: [$2]: 2 ==> 2               no new information
FILTER: 1+p ⊆ $2                  filter the 2 possibilities for [$2] by this constraint, then discard it
  REFINE: [$2]: 2 ==> 2               no new information
```

At this point all metavariables are solved except `$2` which has the two possibilities `1+p` and `ω`. In this case, since `1+p ⊆ ω` the algorithm picks `1+p` for the solution. If such a "lower-bound" does not exist the algorithm will report an ambiguity, unless the metavariable's space was not narrowed at all, in which case it will use `ω` - this usually matches what the user wants.

This produces a final count substitution that solves the metavariables as:
```
  ($0,ω)
  ($1,ω)
  ($2,1+p)
  ($3,ω)
  ($4,ω)
```

Substituting, into our original declaration we get our final declaration:
```
either : (A %(1+p)-> C) %ω-> (B %(1+p)-> C) %ω-> Either A B %(1+p)-> C
either f g = \case ('left x => f x, 'right y => g y)
```
