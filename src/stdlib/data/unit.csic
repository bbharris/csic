import "../"
  "primitive/equality"
  "abstract/functional"
  "abstract/functor"
  "primitive/unit" public

-- | Utility for do-notation to enforce unit values on anonymous temporary values.
unitExpected : Unit -> Unit = id

-- | Discard a functorial result.
void {Functor@{U}%{p=1,q=p} `F} : F `A@ -> F Unit =
  map (const 'unit)

-- | Eta-equality of unit.
unitEtaEq %0 : (x: Unit) %1-> x ≡ 'unit =
  \'unit => refl
