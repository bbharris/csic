import "../"
  "abstract/equality"
  "data/nat"
  "data/either"
  "data/list"
  "effect-base/ability"

-- | Union of operations provided by an ability list.
record OpUnion (Fs: Abilities@{A,U}) (A: @A) =
  index : Nat
  value : indexAbility index Fs A

-- | Inject an instantiated operation into an operation union.
injectOp [@inline] {Member@{A,U} `F `Fs} (op %1: F `A) : OpUnion Fs A =
  case memberProof of
    'memberProof i p => 'opUnion i (subst (\G => G A) p op)

-- | Decompose an operation union into a operation of the head ability or the remaining union.
decompOp : OpUnion@{A,U} (`F :: `Fs) `A %1-> Either (F A) (OpUnion Fs A) = \case
  'opUnion i op => case i of
    'Z    => 'left op
    'S i' => 'right ('opUnion i' op)
