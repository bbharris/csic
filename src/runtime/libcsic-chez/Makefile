
chez-dir := /usr/lib/csv$(shell scheme --version 2>&1)/ta6le
chez-libs := $(chez-dir)/libkernel.a
chez-flags := --optimize-level 3

outdir := .libs
target := $(outdir)/libcsic-chez.so
boot-objs := $(outdir)/petite.o $(outdir)/scheme.o $(outdir)/main.o $(outdir)/runtime.o

srcdir := src/runtime/libcsic-chez
source := $(srcdir)/main.c
cflags := -I$(chez-dir) -std=c11 -Wall -Werror -O3 -fPIC
lflags := -L$(chez-dir) -lkernel -lz -llz4 -lncurses

.PHONY: build
build: $(target)
	@:

.PHONY: clean
clean:
	rm -f $(target) $(boot-objs)

# compile and link shared library
$(target): $(source) $(chez-libs) $(boot-objs)
	@echo "gcc $@"
	@mkdir -p $(@D)
	@gcc $(source) $(cflags) $(boot-objs) $(lflags) -shared -o $@

# convert chez boot file to object file
$(outdir)/%.o: $(chez-dir)/%.boot
	@echo "objcopy $@"
	@mkdir -p $(@D)
	@cp --no-preserve=mode,ownership $< $(@D)/$*.boot  # need temp copy for nice objcopy symbol names
	@cd $(@D) && objcopy -I binary -O default $*.boot $*.o
	@rm $(@D)/$*.boot

# convert custom boot source file to object file
$(outdir)/%.o: $(srcdir)/%.ss
	@mkdir -p $(@D)
	@echo "make-boot-file $@"
	@echo '(make-boot-file "$(outdir)/$*.boot" '"'"'("scheme") "$<")' | scheme -q $(chez-flags)
	@cd $(@D) && objcopy -I binary -O default $*.boot $*.o
	@rm $(@D)/$*.boot
