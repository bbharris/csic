(library (embed)
  (export wrapCompile wrapExecute)
  (import (chezscheme))

  (define utf8Tx (make-transcoder (utf-8-codec) (eol-style none) (error-handling-mode raise)))

  (define (error->bytevector ex)
    (string->bytevector (call-with-string-output-port (lambda (port) (display-condition ex port))) utf8Tx))

  (define (try-eval e)   (guard (ex [#t (error->bytevector ex)]) (eval e)))
  (define (try-call f x) (guard (ex [#t (error->bytevector ex)]) (f x)))

  (define (wrapCompile flags srcpath outpath) (try-eval `(begin
    (define en-build (fxbit-set? ,flags 0))
    (define en-cache (fxbit-set? ,flags 1))
    (define en-opt   (fxbit-set? ,flags 2))
    (define en-asm   (fxbit-set? ,flags 3))
    (define opt-lvl (if en-opt 3 2))
    (define asm-path (format "~a.asm" (path-root ,outpath)))
    (define asm-file (if en-asm (open-output-file asm-path '(buffered replace)) #f))
    (when en-build
      (parameterize ([optimize-level opt-lvl] [#%$assembly-output asm-file])
        (compile-library ,srcpath ,outpath)
      )
    )
    (define csic-entrypoint #f)
    (parameterize ([optimize-level opt-lvl] [#%$assembly-output asm-file])
      (load-library (if en-cache ,outpath ,srcpath))
    )
    (when en-asm
      (close-output-port asm-file)
    )
    (if (procedure? csic-entrypoint)
      csic-entrypoint
      (errorf 'wrapCompile "no csic-entrypoint defined in ~s" ,srcpath)
    )
  )))

  (define (wrapExecute f x) (try-call f x))
)
(import (embed))