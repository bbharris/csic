#include <scheme.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define register_boot(name) \
  do {\
    extern char _binary_##name##_boot_start[];\
    extern char _binary_##name##_boot_end[];\
    Sregister_boot_file_bytes(#name".boot", _binary_##name##_boot_start, \
                              _binary_##name##_boot_end - _binary_##name##_boot_start); \
  } while (0)

#define Ssym Sstring_to_symbol
#define Sstr Sstring

static void uhoh(void) {
  fprintf(stderr, "CRITICAL: Chez Scheme aborting program\n");
  fflush(stderr);
  exit(1);
}

static ptr wrapCompile;
static ptr wrapExecute;
static ptr rtOutputPortOpen;
static ptr rtOutputPortClose;

void chezInit(void) {
  Sscheme_init(uhoh);
  register_boot(petite);
  register_boot(scheme);
  register_boot(main);
  register_boot(runtime);
  Sbuild_heap(NULL, NULL);

  wrapCompile = Stop_level_value(Ssym("wrapCompile"));
  Slock_object(wrapCompile);
  wrapExecute = Stop_level_value(Ssym("wrapExecute"));
  Slock_object(wrapExecute);

  rtOutputPortOpen = Stop_level_value(Ssym("csic-output-port-open!"));
  Slock_object(rtOutputPortOpen);
  rtOutputPortClose = Stop_level_value(Ssym("csic-output-port-close!"));
  Slock_object(rtOutputPortClose);

  Scall1(Stop_level_value(Ssym("load-shared-object")), Sstr("libc.so.6"));
}

void chezQuit(void) {
  Sscheme_deinit();
}

static char *toCStr(ptr x) {
  if (Sbytevectorp(x)) {
    size_t n = Sbytevector_length(x);
    char *out = malloc(n - 1);
    memcpy(out, Sbytevector_data(x), n);
    out[n] = 0;
    return out;
  }
  return NULL;
}

ptr chezCompile(char **err, int flags, char const *srcpath, char const *outpath) {
  ptr res = Scall3(wrapCompile, Sfixnum(flags), Sstr(srcpath), Sstr(outpath));
  *err = toCStr(res);
  if (!*err) Slock_object(res);
  return res;
}

int chezExecute(char **err, ptr f, int n) {
  ptr res = Scall2(wrapExecute, f, Sfixnum(n));
  *err = toCStr(res);
  return Sfixnum_value(res);
}

void chezRtOutputPortOpen(int fd) {
  Scall1(rtOutputPortOpen, Sfixnum(fd));
}

void chezRtOutputPortClose(void) {
  Scall0(rtOutputPortClose);
}