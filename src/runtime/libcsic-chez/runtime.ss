(library (csic-runtime)
  (export
    csic-output-port-open! csic-output-port-close!
    csic-prim-Undefined
    csic-prim-Erased
    csic-prim-Coerce
    csic-prim-ShowAny
    csic-prim-NatZ
    csic-prim-NatS
    csic-prim-NatSUnpack
    csic-prim-NatIsZero
    csic-prim-NatAdd
    csic-prim-IntNonNeg
    csic-prim-IntNonNegUnpack
    csic-prim-IntNegMinus1
    csic-prim-IntNegMinus1Unpack
    csic-prim-StrPack
    csic-prim-StrUnpack
    csic-eff-Console_putStrLn
    csic-eff-Console_outputFD
    csic-eff-New_new
    csic-eff-Load_load
    csic-eff-Store_store
  )
  (import (chezscheme))

  ; output port management
  (define csic-output-port-ref
    (box (current-output-port))
  )
  (define (csic-output-port)
    (unbox csic-output-port-ref)
  )
  (define utf8Tx (make-transcoder (utf-8-codec) (eol-style none) (error-handling-mode raise)))
  (define (csic-output-port-open! fd)
    (set-box! csic-output-port-ref (open-fd-output-port fd (buffer-mode line) utf8Tx))
  )
  (define (csic-output-port-close!)
    (begin
      (close-port (csic-output-port))
      (set-box! csic-output-port-ref (current-output-port))
    )
  )

  ; list conversion between Csic and Scheme
  (define (csic->list x)
    (record-case x
      [(_nil) () '()]
      [(_cons) (h t) (cons h (csic->list t))]
      [else (errorf #f "invalid List ~a" x)]
    )
  )
  (define (list->csic x)
    (if (null? x)
      (list '_nil)
      (list '_cons (car x) (list->csic (cdr x)))
    )
  )

  ; primitives
  (define (csic-prim-Undefined) 'undefined)
  (define (csic-prim-Erased) 'erased)
  (define (csic-prim-Coerce x) x)
  (define (csic-prim-ShowAny x) (format "~a" x))

  (define (csic-prim-BoolTrue)  #t)
  (define (csic-prim-BoolFalse) #f)

  (define (csic-prim-NatZ)         0)
  (define (csic-prim-NatS x)       (fx+ 1 x))
  (define (csic-prim-NatSUnpack x) (fx- x 1))
  (define (csic-prim-NatIsZero x)  (fxzero? x))
  (define (csic-prim-NatAdd x y)   (fx+ x y))

  (define (csic-prim-IntNonNeg x) x)
  (define (csic-prim-IntNonNegUnpack x) x)

  (define (csic-prim-IntNegMinus1 x)       (fx- 0 x 1))
  (define (csic-prim-IntNegMinus1Unpack x) (fx- 0 x 1))

  (define (csic-prim-StrPack x) (list->string (csic->list x)))
  (define (csic-prim-StrUnpack x) (list->csic (string->list x)))

  ; effect operations
  (define (csic-eff-Console_putStrLn s) (begin (fprintf (csic-output-port) "~a\n" s) '(_unit)))
  (define (csic-eff-Console_outputFD) (port-file-descriptor (csic-output-port)))

  (define (csic-eff-New_new v) (box v))
  (define (csic-eff-Load_load r) (unbox r))
  (define (csic-eff-Store_store r v) (begin (set-box! r v) '(_unit)))
)
(import (only (csic-runtime) csic-output-port-open! csic-output-port-close!))
