#include <csic-c.h>
#include <stdio.h>

// effect operations

csic_Any csic_eff_Console_putStrLn(csic_Str s) {
  printf("%s\n", s);
  return &csic_unit;
}
csic_Int csic_eff_Console_outputFD(void) {
  return 1;
}

csic_Any csic_eff_New_new(csic_Any v) {
  void *r = csic_gc_alloc(sizeof(csic_Any));
  *(csic_Any *)r = v;
  return r;
}
csic_Any csic_eff_Load_load(csic_Any r) {
  return *(csic_Any *)r;
}
csic_Any csic_eff_Store_store(csic_Any r, csic_Any v) {
  *(csic_Any *)r = v;
  return &csic_unit;
}
