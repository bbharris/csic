#ifdef __linux__
#define _GNU_SOURCE // for MAP_ANONYMOUS
#include <sys/mman.h>
#endif
#include <csic-c.h>
#include <stdlib.h>

// garbage collected memory allocation

void *csic_gc_alloc(size_t size) {
  return malloc(size);
}
void *csic_gc_alloc_array(size_t count, size_t size) {
  return malloc(count * size);
}

typedef struct {
#ifdef __x86_64__
  struct __attribute__((packed, aligned(1))) {
    #define  MOV_R11 0xBB49 // movq <data>, %r11  // environment pointer
    uint16_t mov_r11;
    uint64_t mov_r11_data;
    #define  MOV_RAX 0xB848 // movq <data>, %rax  // function pointer
    uint16_t mov_rax;
    uint64_t mov_rax_data;
    #define  JMP_RAX 0xE0FF // jmp *%rax
    uint16_t jmp_rax;
  } tramp;
#else
    #error "not implemented"
#endif
} csic_closure;

static void *csic_gc_alloc_exec(size_t size) {
#ifdef __linux__
  return mmap(NULL, size, PROT_READ | PROT_WRITE | PROT_EXEC, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
#else
  #error "not implemented"
#endif
}

void *csic_gc_alloc_closure(void (*func)(void), void **env, size_t env_size) {
  size_t size = sizeof(csic_closure) + env_size;
  csic_closure *c = csic_gc_alloc_exec(size);
#ifdef __x86_64__
  c->tramp.mov_r11 = MOV_R11;
  c->tramp.mov_r11_data = (uint64_t)env;
  c->tramp.mov_rax = MOV_RAX;
  c->tramp.mov_rax_data = (uint64_t)func;
  c->tramp.jmp_rax = JMP_RAX;
#else
  #error "not implemented"
#endif
  *env = c + 1;
  return c;
}
