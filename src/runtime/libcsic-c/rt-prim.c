#include <csic-c.h>
#include <stdio.h>
#include <string.h>

/*
  TODO this will work later, but there is no primitive list type yet
       instead, we will use a tagged structure to represent lists matching the code generator
csic_Any csic_prim_ListNil(void) {
  return NULL;
}
csic_Any csic_prim_ListCons(csic_Any h, csic_Any t) {
  csic_Any r = csic_gc_alloc(sizeof(csic_Any) * 2);
  ((csic_Any *)r)[0] = h;
  ((csic_Any *)r)[1] = t;
  return r;
}

csic_Bool csic_prim_ListIsNil(csic_Any x) {
  return x == NULL;
}
csic_Any csic_prim_ListHead(csic_Any x) {
  return ((csic_Any *)x)[0];
}
csic_Any csic_prim_ListTail(csic_Any x) {
  return ((csic_Any *)x)[1];
}
*/
typedef struct {
  int tag;
  csic_Any head;
  csic_Any tail;
} csic_List;

csic_Any csic_prim_ListNil(void) {
  csic_List *r = csic_gc_alloc(sizeof(csic_List));
  r->tag = 0;
  return r;
}
csic_Any csic_prim_ListCons(csic_Any h, csic_Any t) {
  csic_List *r = csic_gc_alloc(sizeof(csic_List));
  r->tag = 1;
  r->head = h;
  r->tail = t;
  return r;
}

csic_Bool csic_prim_ListIsNil(csic_Any x) {
  return ((csic_List *)x)->tag == 0;
}
csic_Any csic_prim_ListHead(csic_Any x) {
  return ((csic_List *)x)->head;
}
csic_Any csic_prim_ListTail(csic_Any x) {
  return ((csic_List *)x)->tail;
}

// primitive operations

csic_Str csic_prim_ShowAny(csic_Any x) {
  int n = snprintf(NULL, 0, "%p", x);
  char *s = csic_gc_alloc_array(n + 1, sizeof(char));
  snprintf(s, n + 1, "%p", x);
  return s;
}

csic_Str csic_prim_StrPack(csic_Any x) {
  // input is a list of csic_Char
  csic_List *l = (csic_List *)x;
  size_t n = 0;
  for (csic_List *p = l; !csic_prim_ListIsNil(p); p = (csic_List *)csic_prim_ListTail(p)) {
    // TODO use <stdchar.h> to support UTF-32 characters to multi-byte UTF-8 encoding
    n++;
  }
  char *s = csic_gc_alloc_array(n + 1, sizeof(char));
  size_t i = 0;
  for (csic_List *p = l; !csic_prim_ListIsNil(p); p = (csic_List *)csic_prim_ListTail(p)) {
    s[i++] = csic_prim_CoerceTo(csic_Char, csic_prim_ListHead(p));
  }
  s[i] = '\0';
  return s;
}
csic_Any csic_prim_StrUnpack(csic_Str x) {
  // output is a list of csic_Char
  size_t n = strlen(x);
  // TODO allocate all the list cells in a single block
  csic_List const *l = csic_prim_ListNil();
  for (ptrdiff_t i = n; i >= 0; --i) {
    // TODO use <stdchar.h> to support UTF-32 characters to multi-byte UTF-8 encoding
    l = csic_prim_ListCons(csic_prim_CoerceTo(csic_Any, x[i]), l);
  }
  return l;
}