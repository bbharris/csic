// CSIC runtime library in C
#ifndef __CSIC_C
#define __CSIC_C
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

// utility macros

#define csic_unused __attribute__((unused))
#define csic_inline static inline

// primitive types

typedef bool        csic_Bool;
typedef ptrdiff_t   csic_Int;
typedef size_t      csic_Nat;
typedef uint32_t    csic_Char;
typedef char const *csic_Str;
typedef void const *csic_Any;

static const struct {
  int tag;
} csic_unit = {0};

// primitive operations

csic_inline csic_Any csic_prim_Undefined(void) {
  return NULL;
}

csic_inline csic_Any csic_prim_Erased(void) {
  return NULL;
}

#define csic_prim_CoerceTo(T, x) ((T)((size_t)x))

csic_Str csic_prim_ShowAny(csic_Any x);

csic_inline csic_Bool csic_prim_BoolTrue(void) {
  return true;
}
csic_inline csic_Bool csic_prim_BoolFalse(void) {
  return false;
}

csic_inline csic_Nat csic_prim_NatZ(void) {
  return 0;
}
csic_inline csic_Nat csic_prim_NatS(csic_Nat x) {
  return x + 1;
}
csic_inline csic_Nat csic_prim_NatSUnpack(csic_Nat x) {
  return x - 1;
}
csic_inline csic_Bool csic_prim_NatIsZero(csic_Nat x) {
  return x == 0;
}
csic_inline csic_Nat csic_prim_NatAdd(csic_Nat x, csic_Nat y) {
  return x + y;
}

csic_inline csic_Int csic_prim_IntNonNeg(csic_Nat x) {
  return (csic_Int)x;
}
csic_inline csic_Nat csic_prim_IntNonNegUnpack(csic_Int x) {
  return (csic_Nat)x;
}

csic_inline csic_Int csic_prim_IntNegMinus1(csic_Nat x) {
  return -((csic_Int)x) - 1;
}
csic_inline csic_Nat csic_prim_IntNegMinus1Unpack(csic_Int x) {
  return -(x + 1);
}

csic_Str csic_prim_StrPack(csic_Any x);
csic_Any csic_prim_StrUnpack(csic_Str x);

// effect operations

csic_Any csic_eff_Console_putStrLn(csic_Str s);
csic_Int csic_eff_Console_outputFD(void);

csic_Any csic_eff_New_new(csic_Any v);
csic_Any csic_eff_Load_load(csic_Any r);
csic_Any csic_eff_Store_store(csic_Any r, csic_Any v);

// garbage collected memory allocation

void *csic_gc_alloc(size_t size);
void *csic_gc_alloc_array(size_t count, size_t size);
void *csic_gc_alloc_closure(void (*func)(void), void **env, size_t env_size);

#ifdef __x86_64__
#define csic_closure_get_env() ({void *ctx; __asm__ __volatile__("movq %%r11, %0" : "=r" (ctx)); ctx;})
#else
#error "not implemented"
#endif

/*
  GC design ideas:
  - non-moving, precise (what about stack? conservative for stack/registers?)
  - bitmask-type: bitmask of non-pointer/pointer words
  - each unique bitmask-type allocated independently (dispatched statically)
    - fast-path - bump allocator of last-allocated block
    - slow-path - scan allocated bitmap of non-full blocks, allocate new block on failure
  - allocation blocks:
    - number of type-bitmap words
    - type bitmap
    - allocated bitmap (1-bit per object)
    - referenced bitmap (1-bit per object)
  - how to handle internal array pointers?
*/

#endif