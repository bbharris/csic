module Csic.Front.Import (processImports) where
import Csic.Util.Prelude
import Csic.Core.Syntax
import Csic.Core.Environment
import Csic.Front.Syntax

-- | Import declarations and macros into local scope.
processImports :: Namespace -> ImportMap -> [ModuleImports] -> FrontEnv -> CheckResult FrontEnv
processImports ns im xs env = flip execStateT env $
  mapM_ (\(ModuleImports _ url mods) -> mapM_ (importModule ns im url) mods) xs

type ImportM = StateT FrontEnv CheckResult

-- | Import an external module into the local module scope.
importModule :: Namespace -> ImportMap -> Text -> ModuleImport -> ImportM ()
importModule ns0 im url (ModuleImport loc rpath acc filters) = case lookup (url <> rpath) im of
  Nothing -> checkError loc $ "invalid import map " <> show (rpath, im)
  Just ns -> do
    env <- get
    -- Check each import filter references names exported by the module.
    forM_ filters $ \case
      ImportInclude l a -> checkFilterName l (QName ns a)
      ImportExclude l a -> checkFilterName l (QName ns a)
      ImportRename l a _ -> checkFilterName l (QName ns a)
    -- Import elements into the local scope.
    traverse_ importDecl (lookupModuleCDecls ns env)
    traverse_ importMacro (maybe [] toList $ lookup ns (frontMacros env))
  where
  importDecl :: (Name, CDecl) -> ImportM ()
  importDecl (k, d) = when (declAccess d == Public) $ case applyImportFilters k filters of
    Nothing -> pure ()
    Just s -> do
      let s' = QName ns0 s
      env <- get
      ok <- case lookupCDeclQ s' env of
        Just d' -> do
          unless (declName d == declName d') $ ambiguous loc s [declLoc d, declLoc d']
          pure (acc == Public)
        Nothing -> pure True
      let d' = d {declAccess = acc}
      when ok $ put env {frontCore = registerCDeclImport s' d' (frontCore env)}

  importMacro :: (Name, Macro QName) -> ImportM ()
  importMacro (k, m) = when (macroAccess m == Public) $ case applyImportFilters k filters of
    Nothing -> pure ()
    Just s -> do
      let s' = QName ns0 s
      env <- get
      ok <- case lookupEnv s' (frontMacros env) of
        Just m' -> do
          unless (macroName m == macroName m') $ ambiguous loc s [macroLoc m, macroLoc m']
          pure (acc == Public)
        Nothing -> pure True
      when ok $ put env {frontMacros = insertEnv s' (m {macroAccess = acc}) (frontMacros env)}

checkFilterName :: Loc -> QName -> ImportM ()
checkFilterName loc s = get >>= \env ->
  case (lookupCDeclQ s env, lookupEnv s (frontMacros env)) of
    (Just d, _) | declAccess d == Public -> pure ()
    (_, Just m) | macroAccess m == Public -> pure ()
    _ -> checkError loc $ "invalid import filter name " <> show s

applyImportFilters :: Name -> [ImportFilter] -> Maybe Name
applyImportFilters s = flip foldl' (Just s) $ \res x -> case x of
  ImportInclude _ a -> if s == a then Just a else res
  ImportExclude _ a -> if s == a then Nothing else res
  ImportRename _ a b -> if s == a then Just b else res
