module Csic.Front.Macro (processMacros, parseMacroExpr) where
import Csic.Util.Prelude
import Csic.Util.Parse
import Csic.Core.Syntax
import Csic.Core.Environment
import Csic.Front.Syntax
import Text.Megaparsec
import Control.Monad.Combinators.Expr
import qualified Data.Map.Strict as Map
import qualified Data.List as L

processMacros ::  Namespace -> [Macro Name] -> FrontEnv -> CheckResult FrontEnv
processMacros ns xs env = do
  macros <- execStateT (mapM_ (processMacro ns) xs) (frontMacros env)
  pure env {frontMacros = macros}

type MacroM = StateT MacroEnv CheckResult

processMacro :: Namespace -> Macro Name -> MacroM ()
processMacro ns m = do
  let s = macroName m
  let s' = QName ns s
  env <- get
  case lookupEnv s' env of
    Just m' -> ambiguous (macroLoc m) s [macroLoc m']
    Nothing -> pure ()
  put $ insertEnv s' (m {macroName = s'}) env

-- | Parse dynamic macro expression trees.
parseMacroExpr :: [MacroExprToken] -> CheckResult ExprExt
parseMacroExpr toks =
  first (mapParseErrorBundle prettyMacroToken ferror) $ parse (parser <* notFollowedBy parseAny) "" toks
  where
  ferror i = CErrTree nullLoc "macro parse error" . (:[]) . strErr (tokenLoc $ toks !! i)
  macros = L.nubBy (\a b -> macroName a == macroName b) $ mapMaybe (leftToMaybe . relevant . tokenInfo) toks
  fmacro res m = Map.insertWith (++) (macroPrec m) [(macroName m, macroImpl m)] res
  macrosByPrec = foldl' fmacro mempty macros
  table = map (map parseOp . snd) $ Map.toDescList macrosByPrec
  parser = makeExprParser parseExpr table
  parseOp (op, x) = case x of
    MacroPrefix sa e -> Prefix $ do
      l <- parseOp' op
      pure $ \a -> substMacro l [(sa, a)] e
    MacroInfix assoc sa sb e -> finfix assoc $ do
      _ <- parseOp' op
      pure $ \a b ->
        let la = getLoc a in
        let lb = getLoc b in
        let l' = la {locLength = locOffset lb + locLength lb - locOffset la} in
        substMacro l' [(sa, a), (sb, b)] e
      where
  finfix x = case x of
    InfixNone -> InfixN
    InfixLeft -> InfixL
    InfixRight -> InfixR
  parseAny = parseToken Just
  parseOp' op = parseToken $ \(LToken l (Irr x)) -> case x of Left m | macroName m == op -> Just l; _ -> Nothing
  parseExpr = parseToken $ \(LToken _ (Irr x)) -> case x of Right e -> Just e; _ -> Nothing
  parseToken :: (MacroExprToken -> Maybe a) -> P a
  parseToken f = token f mempty

type P = Parsec Void [MacroExprToken]

prettyMacroToken :: MacroExprToken -> ShowS
prettyMacroToken (LToken _ (Irr x)) = case x of
  Left m -> shows (qname $ macroName m)
  Right _ -> showString "expression"

substMacro :: Loc -> [(Name, ExprExt)] -> ExprExt -> ExprExt
substMacro l subs e = case unExpr e of
  EName s lvls -> fromMaybe (mkExpr l (EName s lvls)) (L.lookup s subs)
  ELambda ps b -> mkExpr l $ ELambda (map (fpat <$>) ps) (frec b)
  ECase dM bs  -> mkExpr l $ ECase (frec <$> dM) (map (\(EBranch ps b) -> EBranch (map (fpat <$>) ps) (frec b)) bs)
  x            -> mkExpr l (frec <$> x)
  where
  fpat p = p {applyLoc = l}
  frec = substMacro l subs