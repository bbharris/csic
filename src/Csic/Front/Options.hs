module Csic.Front.Options where
import Csic.Util.Prelude
import Csic.Core.Environment
import Options.Applicative

coreOptionsParser :: Parser CoreOptions
coreOptionsParser = CoreOptions False False
  <$> switch (long "levels-off"   <> help "Disable level checking and assume (Type : Type)")
  <*> switch (long "multis-off"   <> help "Disable multi checking and assume (x : %ω).")
  <*> switch (long "trace-levels" <> help "Debug trace level checking.")
  <*> switch (long "trace-multis" <> help "Debug trace multi checking.")
  <*> switch (long "trace-metas"  <> help "Debug trace all declarations after unification.")
  <*> switch (long "trace-okays"  <> help "Debug trace all declarations after all checks.")
  <*> option auto (long "fuel" <> metavar "N" <> value (coreFuelMax coreOptionsDefault) <>
                   help "Amount of steps before assuming an infinite loop.")
  <*> printOptionsParser

printOptionsParser :: Parser PrintOptions
printOptionsParser = PrintOptions (showLocStr printOptionsDefault)
  <$> option auto (long "print-indent" <> metavar "N" <> value (showIndent printOptionsDefault) <>
                   help "Pretty-printing starting indentation.")
  <*> switch (long "print-color"     <> help "Enable pretty-print colors.")
  <*> switch (long "print-locations" <> help "Enable pretty-print of error locations.")
  <*> switch (long "print-implicits" <> help "Enable pretty-print of implicit parameters.")
  <*> switch (long "print-levels"    <> help "Enable pretty-print of levels.")
  <*> switch (long "print-multis"    <> help "Enable pretty-print of multis.")
  <*> switch (long "print-details"   <> help "Enable pretty-print of error details.")
