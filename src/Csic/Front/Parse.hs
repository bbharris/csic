module Csic.Front.Parse (parseModule) where
import Csic.Util.Prelude
import Csic.Util.Parse
import Csic.Core.Syntax
import Csic.Core.Environment
import Csic.Front.Syntax
import Csic.Front.Lexer
import Text.Megaparsec
import qualified Data.List.NonEmpty as NE
import qualified Control.Monad.Combinators.NonEmpty as PNE
import qualified Data.Text as T
import qualified Text.Read as T
import qualified Data.Char as C

parseModule :: FilePath -> [EToken] -> CheckResult [Statement]
parseModule path toks = first (mapParseErrorBundle (shows . tokenInfo) ferror) $
  runParser (evalStateT (many (noIndent >> pStatement) <* endOfFile) (nullLoc, nullLoc)) path toks
  where
  ferror i = CErrTree nullLoc "parse error" . (:[]) . strErr (tokenLoc $ toks !! i)

type P = StateT (Loc, Loc) (Parsec Void [EToken])

pStatement :: P Statement
pStatement = withPos $ choice
  [ do
    beg <- getLocBeg
    pReserved "import"
    url <- pStrLiteral
    access <- pAccess Private
    mods <- pParensCommaOrBlock (pModuleImport access)
    loc <- getLocEnd beg
    pure $ SImport $ ModuleImports loc url mods
  , do
    beg <- getLocBeg
    pReserved "macro"
    prec <- pNatLiteral <* pReserved "("
    (op, f) <- choice
      [ do
        op <- pOperator
        sa <- pName
        pure (op, MacroPrefix sa)
      , do
        (sa, lp) <- pParensName
        op <- pOperator
        (sb, rp) <- pParensName
        let assoc = case (lp, rp) of (True, False) -> InfixLeft; (False, True) -> InfixRight; _ -> InfixNone
        pure (op, MacroInfix assoc sa sb)
      ]
    pReserved ")"
    access <- pAccess Public
    e <- pReserved "=" *> pTermOrIndentSeq
    -- let s = if sRaw == Name "" then Name ("_line_" <> pack (show (locLine loc))) else sRaw
    loc <- getLocEnd beg
    pure $ SMacro $ Macro loc access (Name op) (fromIntegral prec) (f e)
  , SDecl <$> pDecl
  ]

pDecl :: P (EDecl ExprExt)
pDecl = choice
  [ (\d -> d {edeclImpl = ESingle (edeclImpl d)}) <$> pDecl1
  , EDecl <$> (pReserved "mutual" *> getLocNext) <*> pure (Name "") <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiZero (eHole <$> getLocNext)
      <*> (pReserved "=" *> (EMutual <$> pParensCommaOrBlock pDecl1))
  , EDecl <$> (pReserved "class" *> getLocNext) <*> pName <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiZero (pResultType <|> pDefaultTypeUniv)
      <*> (pReserved "=" *> (EClass <$> pParensCommaOrBlock pMethod))
  , EDecl <$> (pReserved "instance" *> getLocNext) <*> option (Name "") pName <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiZero pResultType
      <*> (pReserved "=" *> (EInstance <$> pParensCommaOrBlock pInstanceDecl))
  , EDecl <$> (pReserved "ability" *> getLocNext) <*> pName <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiZero (eHole <$> getLocNext)
      <*> (pReserved "=" *> (EAbility <$> pParensCommaOrBlock pOperation))
  ]

pDecl1 :: P (EDeclMutual ExprExt)
pDecl1 = choice
  [ EDecl <$> (pReserved "axiom" *> getLocNext) <*> pName <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiAny pResultType
      <*> pure EAxiom
  , EDecl <$> getLocNext <*> pNameOrLine <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiWild (pResultType <|> (eHole <$> getLocNext))
      <*> option (EDef RecursionNone $ eHole nullLoc)
          (pReserved "=" *> (EDef <$> pRecursionKind <*> pTermOrIndentSeq))
  , EDecl <$> (pReserved "data" *> getLocNext) <*> pName <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiZero (pResultType <|> pDefaultTypeUniv)
      <*> (pReserved "=" *> (EData <$> pParensCommaOrBlock pConstructor))
  , EDecl <$> (pReserved "record" *> getLocNext) <*> pName <*> pAccess Public <*> pAnnotations
      <*> pParamsType emultiZero (pResultType <|> pDefaultTypeUniv)
      <*> (pReserved "=" *> (ERecord <$> pAccess Public <*> (concat <$> pParensCommaOrBlock (pParamFull emultiOne))))
  ]

pNameOrLine :: P Name
pNameOrLine = pName <|> do
  l <- getLocNext
  pReserved "_"
  pure $ Name ("_line_" <> pack (show (locLine l)))

pModuleImport :: Access -> P ModuleImport
pModuleImport acc = ModuleImport <$> getLocNext <*> pStrLiteral <*> pAccess acc <*> (concat <$> many pImportFilters)

pImportFilters :: P [ImportFilter]
pImportFilters = choice
  [ pReserved "#" >> pParens (pCommaSep1 (pRef ImportExclude))
  , pBraces (pCommaSep1 pRename)
  ]
  where
  pRef f = f <$> getLocNext <*> (pName <|> pCtorName <|> pOpName)
  pRename = choice [pRename' pName pName, pRename' pCtorName pCtorName, pRename' pOpName pOpName]
  pRename' p1 p2 = ImportRename <$> getLocNext <*> p1 <*> (pReserved "=>" *> p2)
  pOpName = Name <$> pOperator

pAccess :: Access -> P Access
pAccess def = option def (pReserved "private" $> Private <|> pReserved "public" $> Public)

pAnnotations :: P [Annotation]
pAnnotations = do
  anns <- option [] $ do
    i <- getOffset
    m <- pMultiFull
    unless (m == emultiZero) $ region (setErrorOffset i) $ fail $ "invalid erasure annotation " <> show m
    pure [AnnErased]
  (anns <>) <$> option [] (pBrackets (pCommaSep0 pAnnotation))

pAnnotation :: P Annotation
pAnnotation = do
  i <- getOffset
  pReserved "@"
  s <- ("Ann" <>) . fcap . unName <$> pName
  args <- many $ show <$> pNatLiteral <|> show <$> pStrLiteral
  let s' = T.unwords (s: (pack <$> args))
  case T.readMaybe (unpack s') of
    Just a -> pure a
    Nothing -> region (setErrorOffset i) $ fail $ "invalid annotation " <> show s
  where
  fcap s = case T.uncons s of
    Nothing -> s
    Just (c, s') -> T.cons (C.toUpper c) s'

pParensName :: P (Name, Bool)
pParensName = choice
  [ (,True) <$> pParens pName
  , (,False) <$> pName
  ]

pParamsType :: EMulti -> P ExprExt -> P (EParamsType ExprExt)
pParamsType m rt = eparamsType <$> pParamsFull m <*> rt

pParamsType' :: EMulti -> P ExprExt -> P ExprExt
pParamsType' m rt = ePiTypes <$> pParamsFull m <*> rt

pResultType :: P ExprExt
pResultType = pReserved ":" >> pTerm

pDefaultTypeUniv :: P ExprExt
pDefaultTypeUniv = pExpr (pure $ ETypeUniv [(ELevelHole, 0)])

pRecursionKind :: P RecursionKind
pRecursionKind = option RecursionNone $ choice
  [ pReserved "rec" $> RecursionGeneral
  ]

pConstructor :: P (ECtor ExprExt)
pConstructor = EDecl <$> getLocNext <*> pCtorName <*> pAccess Public <*> pAnnotations <*>
  pParamsType emultiOne (pure $ eHole nullLoc) <*> pure mempty

pMethod :: P (EMethod ExprExt)
pMethod = choice
  [ fmap EMethodType $ flip <$> (EDecl <$> getLocNext <*> pName <*> pAccess Public <*> pAnnotations) <*>
      pure (Const ()) <*> (Identity <$> pParamsType' emultiAny pResultType)
  , curry EMethodDefs <$> pBraces (pCommaSep0 $ (,) <$> getLocNext <*> pName) <*>
      (pReserved "=>" >> pParensCommaOrBlock pMethodImpl)
  ]

pMethodImpl :: P (EMethodImpl ExprExt)
pMethodImpl = EMethodImpl <$> getLocNext <*> pName <*> pAnnotations <*> pDefPatterns
  <*> (pReserved "=" *> pRecursionKind) <*> pTermOrIndentSeq

pInstanceDecl :: P (EInstanceDecl ExprExt)
pInstanceDecl = choice
  [ EInstMethod <$> pMethodImpl
  , EInstMutual <$> (pReserved "mutual" *> pDecl1)
  ]

pOperation :: P (ECtor ExprExt)
pOperation = EDecl <$> getLocNext <*> pName <*> pAccess Public <*> pAnnotations <*>
  pParamsType emultiOne (pReserved "=>" *> pTerm) <*> pure mempty

pDefPatterns :: P [EPattern]
pDefPatterns = many pArgPattern

-- ************************************************************************************************

pParamsFull :: EMulti -> P [EParam ExprExt]
pParamsFull mdef = concat <$> many (pParamParens mdef <|> (($ emultiAny) <$> pConstraintsNoMulti))

pParamParens :: EMulti -> P [EParam ExprExt]
pParamParens mdef = do
  let fmid = choice [Just <$> pMultiPrefix, pReserved ":" $> Nothing]
  (ss, mulPre) <- try (pReserved "(" *> ((,) <$> pParamNames <*> fmid))
  m <- case mulPre of
    Nothing -> pure mdef
    Just fm -> fm <$> pMultiSuffix <* pReserved ":"
  ps <- ($ m) <$> pParamSuffix ss
  pReserved ")"
  pure ps

pParamFull :: EMulti -> P [EParam ExprExt]
pParamFull mdef = do
  ss <- pParamNames
  m <- option mdef pMultiFull
  pReserved ":"
  ($ m) <$> pParamSuffix ss

pParamSuffix :: ParamNames -> P (EMulti -> [EParam ExprExt])
pParamSuffix (z NE.:| zs) = do
  t <- pTerm
  let t' = noImplicits t
  pure $ \m -> EParam z m t : map (\z' -> EParam z' m t') zs

-- ************************************************************************************************

type ParamNames = NonEmpty ParamInfo

pParamNames :: P ParamNames
pParamNames = PNE.some (uncurry . ParamInfo <$> getLocNext <*> pParamName)

pParamName :: P (Maybe Name, Name)
pParamName = (Nothing,) <$> pWildName <|> (\s -> (Just s, s)) <$> pImplicitName

-- ************************************************************************************************

pParamNoMulti :: P (EMulti -> [EParam ExprExt])
pParamNoMulti = (try (pReserved "(" *> pParamNames <* pReserved ":") >>= pParamSuffix) <* pReserved ")"

pConstraintsNoMulti :: P (EMulti -> [EParam ExprExt])
pConstraintsNoMulti = do
  cs <- pBraces $ pCommaSep1 $ do
    beg <- getLocBeg
    s <- option (Name "") $ try (pImplicitName <* pReserved ":")
    t <- pTermOrIndentSeq
    l <- getLocEnd beg
    pure $ \m -> EParam (implicitParam l s) m t
  pure $ \m -> map ($ m) cs

-- ************************************************************************************************

pTermOrIndentSeq :: P ExprExt
pTermOrIndentSeq = (same >> pTerm) <|> pSeqLet False

pSeqLet :: Bool -> P ExprExt
pSeqLet inline = do
  beg <- getLocBeg
  xs <- (if inline then pParens . pCommaSep1 else indentedBlock) $ do
    beg1 <- getLocBeg
    i <- getOffset
    x <- pTerm
    choice
      [ do
        y <- pReserved "=" >> if inline then pTerm else pTermOrIndentSeq
        (,y) <$> pPattern' i (exprPattern x)
      , do
        loc1 <- getLocEnd beg1
        pure (epatV loc1 (Name ""), x)
      ]
  case xs of
    [(EPat _ (Name "") EPatV, x)] -> pure x
    _ -> getLocEnd beg >>= \loc -> pure (mkExpr loc $ ESugar $ ESequence False xs)

pSeqDo :: P ExprExt
pSeqDo = pExpr $ do
  pReserved "do"
  inline <- option False (same $> True)
  fmap (ESugar . ESequence True) <$> (if inline then pParens . pCommaSep1 else indentedBlock) $ do
    beg1 <- getLocBeg
    i <- getOffset
    x <- pTerm
    choice
      [ do
        isBind <- (pReserved "=" $> False) <|> (pReserved "<-" $> True)
        y <- if inline then pTerm else pTermOrIndentSeq
        loc1 <- getLocEnd beg1
        let y' = if isBind then mkExpr loc1 (ESugar $ EInlineBind y) else y
        (,y') <$> pPattern' i (exprPattern x)
      , do
        loc1 <- getLocEnd beg1
        pure (epatV loc1 (Name ""), x)
      ]

pTerm :: P ExprExt
pTerm = choice
  [ do
    fps <- pParamNoMulti <|> pConstraintsNoMulti
    m <- option emultiAny pMultiFull <* pReserved "->"
    ePiTypes (fps m) <$> pTerm
  , do
    beg <- getLocBeg
    pt <- pMacroExpr >>= wrap beg
    option pt $ do
      m <- option emultiAny pMultiFull <* pReserved "->"
      rt <- pTerm
      l <- getLocEnd beg
      pure $ mkExpr l $ EPiType (EParam (nullParamInfo l) m pt) rt
  ]
  where
  wrap beg xs = case xs of
    [Right x] -> pure x
    _ -> getLocEnd beg >>= \loc -> pure (mkExpr loc $ ESugar $ EMacro xs)

pMacroExpr :: P [MacroExprElem ExprExt]
pMacroExpr = do
  pre <- many op
  t <- pReservedOrApps
  let tmp = pre ++ [Right t]
  option tmp $ (tmp ++) <$> ((:) <$> op <*> pMacroExpr)
  where
  op = Left <$> ((,) <$> getLocNext <*> (Name <$> pOperator))

pReservedOrApps :: P ExprExt
pReservedOrApps = choice
  [ pLambdas
  , pCase
  , pIf
  , pSeqDo
  , pExpr (pReserved "#" >> ESugar . EInlineBind <$> pTermOrIndentSeq)
  , pApp
  ]

pLambdas :: P ExprExt
pLambdas = pExpr $ do
  pReserved "\\"
  choice
    [ pReserved "case" >> ECase Nothing <$> pBranches
    , do
      EBranch ps b <- pBranch True
      pure $ ELambda ps b
    ]

pCase :: P ExprExt
pCase = do
  beg <- getLocBeg
  d <- pReserved "case" *> pTerm <* pReserved "of"
  bs <- pBranches
  l <- getLocEnd beg
  pure $ mkExpr l $ ECase (Just d) bs

pBranches :: P [EBranch ExprExt]
pBranches = pParensCommaOrBlock (pBranch False)

pBranch :: Bool -> P (EBranch ExprExt)
pBranch multiArg = EBranch <$> (pats <* pReserved "=>") <*> pTermOrIndentSeq
  where
  pats = if multiArg then many pArgPattern else choice
    [ pReserved "\\" >> many pArgPattern
    , (:[]) <$> pTermPattern
    ]

pArgPattern :: P EPattern
pArgPattern = pPattern pArg argPattern

pTermPattern :: P EPattern
pTermPattern = pPattern pTerm exprPattern

pPattern :: P e -> (e -> Maybe EPattern) -> P EPattern
pPattern p f = do
  i <- getOffset
  p >>= pPattern' i . f

pPattern' :: Int -> Maybe EPattern -> P EPattern
pPattern' i = \case
  Just r -> pure r
  Nothing -> region (setErrorOffset i) $ fail "invalid pattern"

argPattern :: (ApplyInfo, ExprExt) -> Maybe EPattern
argPattern (rz, arg) = do
  p <- exprPattern arg
  pure $ if isJust (applyImplicit rz) then p {epatInfo = rz} else p

exprPattern :: ExprExt -> Maybe EPattern
exprPattern e = case unExpr e of
  ESugar (EImplicit s Nothing _) -> pure $ EPat (ApplyInfo l (Just s)) s EPatV
  ESugar (EAsPat s r) -> do
    p <- exprPattern r
    pure $ p {epatName = s}
  ESugar (ERecCtor sc Nothing args) -> do
    args' <- forM args $ \(z, arg) -> (z,) <$> exprPattern arg
    pure $ EPat (nullApplyInfo l) (Name "") (EPatC sc $ EPatProj args')
  _ -> case first unExpr $ unfoldEApps e of
    (EName s _, []) -> case unpack (unName s) of
      '\'':_ -> pure $ EPat (nullApplyInfo l) (Name "") (EPatC s $ EPatArgs [])
      _ -> pure $ epatV l s
    (EName sc _, args) -> do
      args' <- mapM (argPattern . first (ApplyInfo l)) args
      pure $ EPat (nullApplyInfo l) (Name "") (EPatC sc $ EPatArgs args')
    _ -> Nothing
  where
  l = getLoc e

pWildName :: P Name
pWildName = pName <|> pReserved "_" $> Name ""

pIf :: P ExprExt
pIf = do
  beg <- getLocBeg
  pReserved "if"
  disc <- pTerm
  begT <- getLocBeg <* pReserved "then"
  a <- pTerm
  locT <- getLocEnd begT
  begF <- getLocBeg <* pReserved "else"
  b <- pTerm
  locF <- getLocEnd begF
  loc <- getLocEnd beg
  let pa = EPat (nullApplyInfo locT) (Name "") (EPatC (Name "'true") $ EPatArgs [])
  let pb = EPat (nullApplyInfo locF) (Name "") (EPatC (Name "'false") $ EPatArgs [])
  pure $ mkExpr loc $ ECase (Just disc) [EBranch [pa] a, EBranch [pb] b]

pApp :: P ExprExt
pApp = do
  beg <- getLocBeg
  f <- pAtom
  rs <- many $ do
    (z, r) <- pArg
    l <- getLocEnd beg
    pure (l, applyImplicit z, r)
  let res = foldl (\x (l, z, r) -> mkExpr l (EApply z x r)) f rs
  option res $ pExpr $ ESugar <$> (EEffect res <$> (pReserved "!" >> pBraces (pCommaSep0 pTerm)) <*> pPrenexArgs)

pArg :: P (ApplyInfo, ExprExt)
pArg = (,) <$> (ApplyInfo <$> getLocNext <*> optional pImplicitArgName) <*> pAtom

pAtom :: P ExprExt
pAtom = choice
  [ pExpr (EName <$> pWildName <*> pPrenexArgs)
  , pCtor
  , pExpr (pReserved "this" >> EName . addNamePrefix "this." <$> pProjName <*> pPrenexArgs)
  , pImplicit
  , pExpr (ESugar <$> (EAsPat <$> pAsPatternName <*> pAtom))
  , pExpr (ETypeUniv <$> pLevel)
  , pExpr (ELiteral <$> pConcreteLiteral)
  , pExpr (ESugar . ELitGen <$> pAbstractLiteral)
  , pSeqLet True
  , pExpr $ pBrackets $ ESugar . EList <$> pCommaSep0 pTerm
  ] >>= pProjections

pImplicit :: P ExprExt
pImplicit = pExpr $ choice
  [ ESugar <$> (EImplicit <$> pImplicitName <*> pure Nothing <*> pure emultiZero)
  , ESugar <$> (EImplicit <$> pImplicitMPoly <*> pure Nothing <*> pure emultiWild)
  , do
    (s, multiPoly) <- pImplicitType
    u <- option [(ELevelUser s, 0)] $ pBraces pLevelFull
    pure $ ESugar $ EImplicit s (Just u) (if multiPoly then emultiWild else emultiZero)
  ]

pPrenexArgs :: P EPrenexArgs
pPrenexArgs = choice
  [ (\lvls muls -> (lvls, fromMaybe mempty muls)) <$> pLevels <*> optional pMultis
  , (mempty,) <$> pMultis
  , pure mempty
  ]

pLevels :: P [(Name, ELevel)]
pLevels = do
  try (pReserved "@" >> pReserved "{")
  lvls <- pCommaSep1 $ do
    k <- pName
    (k,) <$> option [(ELevelUser k, 0)] (pReserved "=" >> pLevelFull)
  pReserved "}"
  pure lvls

pLevel :: P ELevel
pLevel = try (pReserved "@" >> notFollowedBy (pReserved "{")) >> choice
  [ (:[]) . (,0) <$> pLevelId
  , (:[]) . (ELevelZero,) . fromIntegral <$> pNatLiteral
  , pParens pLevelFull
  ]

pLevelFull :: P ELevel
pLevelFull =
  optional (fromIntegral <$> pNatLiteral) >>= \case
    Nothing -> sepEndBy1 flevel maxSep
    Just n -> ((ELevelZero, n):) <$> option [] (maxSep >> sepEndBy1 flevel maxSep)
  where
  maxSep = pOperatorSpecial "|"
  flevel = (,) <$> pLevelId <*> option 0 (pOperatorSpecial "+" >> fromIntegral <$> pNatLiteral)

pLevelId :: P ELevelId
pLevelId = choice
  [ ELevelUser <$> pName
  , ELevelHole <$ pReserved "_"
  ]

pMultis :: P [(Name, EMulti)]
pMultis = do
  try (pReserved "%" >> pReserved "{")
  xs <- pCommaSep1 $ do
    k <- pName
    (k,) <$> option (emultiUser k) (pReserved "=" >> pMultiExpr)
  pReserved "}"
  pure xs

pMultiFull :: P EMulti
pMultiFull = pMultiPrefix <*> pMultiSuffix

pMultiPrefix :: P (EMulti -> EMulti)
pMultiPrefix = choice
  [ try (pReserved "%" >> notFollowedBy (pReserved "{")) $> id
  , pReserved "%+" $> (([], MultiOne):)
  ]

pMultiSuffix :: P EMulti
pMultiSuffix = pMultiAtom <|> pParens pMultiExpr

pMultiExpr :: P EMulti
pMultiExpr = sepBy1 (pSepAndEndBy pMultiId (pOperatorSpecial "*") MultiOne pMultiLit) (pOperatorSpecial "+")

pMultiAtom :: P EMulti
pMultiAtom = choice
  [ (\k -> [([k], MultiOne)]) <$> pMultiId
  , (\n -> [([], n)]) <$> pMultiLit
  ]

pMultiId :: P EMultiId
pMultiId = choice
  [ EMultiUser <$> pName
  , EMultiHole <$ pReserved "_"
  ]

pMultiLit :: P MultiLit
pMultiLit = choice
  [ MultiAny <$ pReserved "ω"
  , do
    i <- getOffset
    pNatLiteral >>= \case
      0 -> pure MultiZero
      1 -> pure MultiOne
      _ -> region (setErrorOffset i) $ fail "invalid multiplicity literal"
  ]

pExpr :: P (Expr' ExprSugar ExprExt) -> P ExprExt
pExpr p = mkExpr <$> getLocNext <*> p

pCtor :: P ExprExt
pCtor = do
  beg <- getLocBeg
  c <- pCtorName
  x <- choice
    [ pBraces $ do
        initM <- optional (notFieldName *> pTerm <* pReserved "=>")
        ESugar . ERecCtor c initM <$> pCommaSep0 field
    , EName c <$> pPrenexArgs
    ]
  l <- getLocEnd beg
  pure $ mkExpr l x
  where
  notFieldName = notFollowedBy $ try (pName <* (pReserved "=" <|> pReserved "," <|> pReserved "}"))
  field = do
    l <- getLocNext
    s <- pName
    (BindInfo l s,) <$> option (mkExpr l (EName s mempty)) (pReserved "=" >> pTerm)

pProjections :: ExprExt -> P ExprExt
pProjections x = option x $ do
  s <- pProjName
  loc <- getLocEnd (LocBeg $ getLoc x)
  pProjections $ mkExpr loc $ ESugar $ EProject x s

-- ************************************************************************************************

pParensCommaOrBlock :: P a -> P [a]
pParensCommaOrBlock p = pParens (pCommaSep0 p) <|> indentedBlock p

pCommaSep0 :: P a -> P [a]
pCommaSep0 p = sepEndBy p (pReserved ",")

pCommaSep1 :: P a -> P [a]
pCommaSep1 p = sepEndBy1 p (pReserved ",")

pParens :: P a -> P a
pParens x = pReserved "(" *> x <* pReserved ")"

pBraces :: P a -> P a
pBraces x = pReserved "{" *> x <* pReserved "}"

pBrackets :: P a -> P a
pBrackets x = pReserved "[" *> x <* pReserved "]"

pSepAndEndBy :: P a -> P () -> b -> P b -> P ([a], b)
pSepAndEndBy pa sep b pb = choice
  [ ([],) <$> pb
  , (,) <$> sepBy1 pa sep <*> option b (sep >> pb)
  ]

-- ************************************************************************************************

pName :: P Name
pName = nextToken "identifier" $ \case ExplicitName v -> Just (Name v); _ -> Nothing

pAsPatternName :: P Name
pAsPatternName = nextToken "as-pattern" $ \case AsPatternName v -> Just (Name v); _ -> Nothing

pImplicitName :: P Name
pImplicitName = nextToken "implicit" $ \case ImplicitName v -> Just (Name v); _ -> Nothing

pImplicitType :: P (Name, Bool)
pImplicitType = nextToken "implicit" $ \case ImplicitType v m -> Just (Name v, m); _ -> Nothing

pImplicitMPoly :: P Name
pImplicitMPoly = nextToken "implicit" $ \case ImplicitMPoly v -> Just (Name v); _ -> Nothing

pImplicitArgName :: P Name
pImplicitArgName = nextToken "implicit-prefix" $ \case ImplicitPrefix v -> Just (Name v); _ -> Nothing

pCtorName :: P Name
pCtorName = nextToken "constructor" $ \case ConstructorName v -> Just (Name v); _ -> Nothing

pProjName :: P Name
pProjName = nextToken "projection" $ \case ProjectionName v -> Just (Name v); _ -> Nothing

pReserved :: Text -> P ()
pReserved s = nextToken ("'" <> s <> "'") $ \case Reserved v | v == s -> Just (); _ -> Nothing

pOperatorSpecial :: Text -> P ()
pOperatorSpecial s = nextToken ("'" <> s <> "'") $ \case CustomOp v | v == s -> Just (); _ -> Nothing

pOperator :: P Text
pOperator = nextToken "custom-operator" $ \case CustomOp v -> Just v; _ -> Nothing

pAbstractLiteral :: P Literal
pAbstractLiteral = nextToken "abstract-literal" $ \case AbstractLiteral v -> Just v; _ -> Nothing

pConcreteLiteral :: P Literal
pConcreteLiteral = nextToken "concrete-literal" $ \case ConcreteLiteral v -> Just v; _ -> Nothing

pNatLiteral :: P Natural
pNatLiteral = nextToken "natural-literal" $ \case AbstractLiteral (Nat v) -> Just v; _ -> Nothing

pStrLiteral :: P Text
pStrLiteral = nextToken "string-literal" $ \case AbstractLiteral (Str v) -> Just v; _ -> Nothing

-- ************************************************************************************************

nextToken :: Text -> (ETokenInfo -> Maybe a) -> P a
nextToken kind f = do
  ref <- getRefLoc
  (a, loc) <- token (fcheck ref) (singleton kind')
  modify $ second $ const loc
  pure a
  where
  fcheck ref tok = case f (tokenInfo tok) of
    Nothing -> Nothing
    Just a -> if sameOrIndented ref (tokenLoc tok) then Just (a, tokenLoc tok) else Nothing
  kind' = case unpack kind of
    h:ts -> Label (h NE.:| ts)
    _ -> EndOfInput

endOfFile :: P ()
endOfFile = satisfy (\t -> case tokenInfo t of EndOfFile -> True; _ -> False) >> eof

noIndent :: P ()
noIndent = getRefLoc >>= checkIndent "unexpected indentation" (\_ y -> locColumn y == 1)

-- | Ensure we are on the same line or greater column as the reference indentation.
sameOrIndented :: Loc -> Loc -> Bool
sameOrIndented x y = locLine x == locLine y || locColumn x < locColumn y

same :: P ()
same = getRefLoc >>= checkIndent "expected same line"
  (\x y -> locLine x == locLine y)

indented :: P ()
indented = getRefLoc >>= checkIndent "expected greater indentation"
  (\x y -> locLine x < locLine y && locColumn x < locColumn y)

indentedBlock :: P a -> P [a]
indentedBlock p = indented >> block (withPos p)

block :: P a -> P [a]
block p = do
  ref <- getLocNext
  some $ checkIndent "expected same indentation" (\x y -> locColumn x == locColumn y) ref >> p

withPos :: P a -> P a
withPos p = do
  old <- getRefLoc
  getLocNext >>= setRefLoc
  p <* setRefLoc old

checkIndent :: String -> (Loc -> Loc -> Bool) -> Loc -> P ()
checkIndent msg f ref = do
  loc <- getLocNext
  unless (f ref loc) $ fail $ msg <> ":\n" <>
    "  reference : Line " <> show (locLine ref) <> ", Column " <> show (locColumn ref) <> "\n" <>
    "  current   : Line " <> show (locLine loc) <> ", Column " <> show (locColumn loc)

newtype LocBeg = LocBeg Loc

getLocBeg :: P LocBeg
getLocBeg = LocBeg <$> getLocNext

getLocEnd :: LocBeg -> P Loc
getLocEnd (LocBeg l1) = do
  l2 <- gets snd
  pure l1 {locLength = max 1 (locOffset l2 + locLength l2 - locOffset l1)}

getRefLoc :: P Loc
getRefLoc = gets fst

setRefLoc :: Loc -> P ()
setRefLoc = modify . first . const

getLocNext :: P Loc
getLocNext = getInput >>= \case
  t:_ -> pure $ tokenLoc t
  [] -> pure nullLoc
