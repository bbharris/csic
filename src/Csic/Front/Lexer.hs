module Csic.Front.Lexer (EToken, ETokenInfo(..), tokenizeModule) where
import Csic.Util.Prelude
import Csic.Core.Syntax.Constant
import Csic.Front.Syntax
import Text.Megaparsec
import Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L
import qualified Data.Text as T
import Data.Char

-- ************************************************************************************************

type EToken = LToken ETokenInfo

data ETokenInfo
  = WhiteSpace Text
  | Reserved Text
  | AsPatternName Text
  | ExplicitName Text
  | ImplicitName Text
  | ImplicitType Text Bool
  | ImplicitMPoly Text
  | ImplicitPrefix Text
  | ProjectionName Text
  | ConstructorName Text
  | CustomOp Text
  | AbstractLiteral Literal
  | ConcreteLiteral Literal
  | EndOfFile
  deriving (Eq, Ord, Show)

tokenizeModule :: FilePath -> Text -> Either String [EToken]
tokenizeModule path code = first errorBundlePretty $ runParser (pTokens (Loc path 1 1 0 0) []) path code

-- ************************************************************************************************

type P = Parsec Void Text

pTokens :: Loc -> [EToken] -> P [EToken]
pTokens loc res = (eof $> reverse (LToken loc EndOfFile : res)) <|> do
  i <- getOffset
  ti <- pToken
  j <- getOffset
  let tloc = loc {locOffset = i, locLength = j - i}
  case ti of
    WhiteSpace s -> pTokens (updateLoc (locLine loc) (locColumn loc) (unpack s)) res
    _            -> pTokens (loc {locColumn = locColumn loc + locLength tloc}) (LToken tloc ti : res)
  where
  updateLoc line colm = \case
    c:rs -> if c == '\n' then updateLoc (line + 1) 1 rs else updateLoc line (colm + 1) rs
    [] -> loc {locLine = line, locColumn = colm}

pToken :: P ETokenInfo
pToken = choice
  [ whitespace
  , reservedOrExplicitName
  , implicitNameOrPrefix
  , constructorName
  , projectionName
  , numericLiteral
  , stringLiteral
  , reservedPunc
  , reservedOrCustomOp
  ]

-- ************************************************************************************************

whitespace :: P ETokenInfo
whitespace = fmap (WhiteSpace . fst) $ match $
  some (space1 <|> L.skipLineComment "--" <|> L.skipBlockCommentNested "{-" "-}")

reservedOrExplicitName :: P ETokenInfo
reservedOrExplicitName = reservedOrExplicitName' >>= \case
  ExplicitName s -> choice
    [ try (char '@' >> notFollowedBy (char '{')) $> AsPatternName s
    , pure $ ExplicitName s
    ]
  x -> pure x

reservedOrExplicitName' :: P ETokenInfo
reservedOrExplicitName' = do
  h <- satisfy isIdStart <?> "ident-start"
  t <- takeWhileP (Just "ident-letter") isIdLetter
  let s = T.cons h t
  pure $ if member s reserved then Reserved s else ExplicitName s
  where
  reserved :: HashSet Text
  reserved = fromList
    [ "import", "public", "private"
    , "mutual", "axiom", "data", "record", "class", "instance", "ability"
    , "case", "of", "if", "then", "else"
    , "macro", "do", "rec", "this"
    , "_", "ω"
    ]

isIdStart :: Char -> Bool
isIdStart c = isLetter c || c == '_'

isIdLetter :: Char -> Bool
isIdLetter c = isAlphaNum c || c == '_' || c == '\''

implicitNameOrPrefix :: P ETokenInfo
implicitNameOrPrefix = do
  i <- char '`' >> getOffset
  reservedOrExplicitName' >>= \case
    ExplicitName s -> choice
      [ char '@' >> ImplicitType s <$> option False (char '%' $> True)
      , char '%' $> ImplicitMPoly s
      , char '=' $> ImplicitPrefix s
      , pure $ ImplicitName s
      ]
    _ -> region (setErrorOffset i) $ fail "invalid implicit name"

constructorName :: P ETokenInfo
constructorName = char '\'' >> reservedOrExplicitName >>= \case
  ExplicitName s -> pure $ ConstructorName $ T.cons '\'' s
  _ -> fail "reserved constructor name"

projectionName :: P ETokenInfo
projectionName = try (char '.' >> reservedOrExplicitName) >>= \case
  ExplicitName s -> pure $ ProjectionName s
  _ -> fail "reserved projection name"

reservedPunc :: P ETokenInfo
reservedPunc = Reserved . T.singleton <$> satisfy isReservedPunc

isReservedPunc :: Char -> Bool
isReservedPunc c = c `elem` ("()[]{}," :: String)

reservedOrCustomOp :: P ETokenInfo
reservedOrCustomOp = do
  s <- takeWhile1P (Just "op-letter") isOpLetter
  pure $ if member s reserved then Reserved s else CustomOp s
  where
  reserved :: HashSet Text
  reserved = fromList
    [ "=", ":", "@", "\\", "->", "=>", "<-", "#", "!", "%", "%+"
    ]
  isOpLetter :: Char -> Bool
  isOpLetter c = c /= '`' && (isSymbol c || c `elem` (":!#$%&*+./<=>?@\\^|-~" :: String))
  -- isSymbol c || isPunctuation c && not (isReservedPunc c)
  -- opLetter = oneOf (":!#$%&*+./<=>?@\\^|-~" :: String)

numericLiteral :: P ETokenInfo
numericLiteral = do
  v <- choice
    [ string "0x" >> L.hexadecimal
    , string "0o" >> L.octal
    , string "0b" >> L.binary
    , L.decimal
    ]
  i <- getOffset
  suf <- takeWhileP (Just "numeric-suffix") isIdLetter
  case suf of
    "" -> pure $ AbstractLiteral $ Nat v
    "n" -> pure $ ConcreteLiteral $ Nat v
    "i" -> pure $ ConcreteLiteral $ Int (fromIntegral v)
    _ -> region (setErrorOffset i) $ fail "invalid natural literal suffix"

stringLiteral :: P ETokenInfo
stringLiteral = do
  i <- getOffset
  v <- pack <$> (char '"' >> manyTill L.charLiteral (char '"'))
  j <- getOffset
  suf <- takeWhileP (Just "string-suffix") isIdLetter
  case suf of
    "" -> pure $ AbstractLiteral $ Str v
    "s" -> pure $ ConcreteLiteral $ Str v
    "c" -> case T.uncons v of
      Just (c, "") -> pure $ ConcreteLiteral $ Char c
      _ -> region (setErrorOffset i) $ fail "invalid character literal"
    _ -> region (setErrorOffset j) $ fail "invalid string literal suffix"
