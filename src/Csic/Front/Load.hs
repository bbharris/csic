module Csic.Front.Load where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Front.Syntax
import Csic.Front.Lexer
import Csic.Front.Parse
import Csic.Front.Import
import Csic.Front.Macro
import Csic.Front.Check
import System.Directory
import System.FilePath
import qualified Data.ByteString as BS
import qualified Data.Text.Encoding as T

type LoadM = ExceptT [CheckError] (StateT FrontEnv IO)

loadModule :: Loc -> FilePath -> LoadM Namespace
loadModule loc rpath = do
  apath <- liftIO $ canonicalizePath rpath
  let ns = Namespace (pack apath)
  env0 <- get
  unless (member ns (frontMacros env0)) $ do
    put $ env0 {frontMacros = insert ns mempty (frontMacros env0)}
    decls <- loadModuleDecls loc ns
    get >>= liftEither . checkModule ns decls >>= put
  pure ns

loadModuleDecls :: Loc -> Namespace -> LoadM [EDecl ExprExt]
loadModuleDecls loc ns = do
  let apath = unpack (spaceStr ns)
  let bpath = takeDirectory apath
  xs <- parseModuleFile loc (apath <> ".csic")
  let imports = mapMaybe (\case SImport x -> Just x; _ -> Nothing) xs
  let macros = mapMaybe (\case SMacro x -> Just x; _ -> Nothing) xs
  let decls = mapMaybe (\case SDecl x -> Just x; _ -> Nothing) xs
  importMap <- foldl' union mempty <$> mapM (loadImports bpath) imports
  get
    >>= liftEither . processImports ns importMap imports
    >>= liftEither . processMacros ns macros
    >>= put
  pure decls

loadImports :: FilePath -> ModuleImports -> LoadM ImportMap
loadImports bpath (ModuleImports _ url mods) = fmap fromList $
  forM mods $ \m -> do
    let kpath = url <> modImpRelPath m in (kpath,) <$> loadModule (modImpLoc m) (bpath </> unpack kpath)

parseModuleFile :: Loc -> FilePath -> LoadM [Statement]
parseModuleFile loc path = do
  -- liftIO $ putStrLn $ "Parsing " ++ show rpath
  src <- handleAll (checkError loc . show) $ liftIO $ T.decodeUtf8 <$> BS.readFile path
  modify $ \st -> st {frontSrcMap = insert path src (frontSrcMap st)}
  ts <- liftEither $ first (\s -> [strErr nullLoc s]) $ tokenizeModule path src
  liftEither $ parseModule path ts