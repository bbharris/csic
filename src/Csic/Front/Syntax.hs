module Csic.Front.Syntax where
import Csic.Util.Prelude
import Csic.Core.Syntax
import Csic.Core.Environment
import qualified Data.List as L

-- ************************************************************************************************

data FrontEnv = FrontEnv
  { frontCore   :: CEnv
  , frontMacros :: MacroEnv
  , frontSrcMap :: HashMap FilePath Text
  , frontErrLog :: [[CheckError]]
  } deriving (Generic, NFData)

initFrontEnv :: CEnv -> FrontEnv
initFrontEnv e = FrontEnv e mempty mempty mempty

instance HasCEnv FrontEnv where
  getCEnv = frontCore
  mapCEnv f e = e {frontCore = f (frontCore e)}

-- ************************************************************************************************

data Statement
  = SImport ModuleImports
  | SMacro  (Macro Name)
  | SDecl   (EDecl ExprExt)

-- ************************************************************************************************
-- Imports

data ModuleImports = ModuleImports
  { importLoc     :: Loc
  , importUrl     :: Text
  , importModules :: [ModuleImport]
  }

data ModuleImport = ModuleImport
  { modImpLoc     :: Loc
  , modImpRelPath :: Text -- ^ Directory path relative to containing package URL.
  , modImpAccess  :: Access
  , modImpFilters :: [ImportFilter]
  }

type ImportMap = HashMap Text Namespace

data ImportFilter
  = ImportInclude Loc Name
  | ImportExclude Loc Name
  | ImportRename Loc Name Name

-- ************************************************************************************************
-- Macros

type MacroEnv = GlobalEnv (Macro QName)
type MacroModule = LocalScope (Macro QName)

data Macro s = Macro
  { macroLoc    :: Loc
  , macroAccess :: Access
  , macroName   :: s
  , macroPrec   :: Int
  , macroImpl   :: MacroImpl
  } deriving (Generic, NFData)

data MacroImpl
  = MacroPrefix Name ExprExt
  | MacroInfix InfixAssoc Name Name ExprExt
  deriving (Generic, NFData)

data InfixAssoc = InfixNone | InfixLeft | InfixRight
  deriving (Generic, NFData)

type MacroExprElem = Either (Loc, Name)
type MacroExprToken = LToken (Irr (Either (Macro QName) ExprExt))

data LToken t = LToken {tokenLoc :: Loc, tokenInfo :: t}
  deriving (Eq, Ord, Show)

-- ************************************************************************************************
-- Declarations

type EDecl = EDecl' EParamsType EImpl
type EDeclMutual = EDecl' EParamsType EImplMutual

data EDecl' fty fx e = EDecl
  { edeclLoc    :: Loc
  , edeclName   :: Name
  , edeclAccess :: Access
  , edeclAnns   :: [Annotation]
  , edeclType   :: fty e
  , edeclImpl   :: fx e
  } deriving (Functor, Foldable, Traversable, Generic, NFData)

newtype EParamsType e = EParamsType e
  deriving (Functor, Foldable, Traversable) deriving newtype NFData

data EImpl e
  = EMutual [EDeclMutual e]
  | ESingle (EImplMutual e)
  | EClass [EMethod e]
  | EInstance [EInstanceDecl e]
  | EAbility [ECtor e]
  deriving (Functor, Foldable, Traversable, Generic, NFData)

data EImplMutual e
  = EAxiom
  | EDef RecursionKind e
  | EData [ECtor e]
  | ERecord Access [EParam e]
  deriving (Functor, Foldable, Traversable, Generic, NFData)

type ECtor = EDecl' EParamsType (Const ())

data EMethod e
  = EMethodType (EMethodType e)
  | EMethodDefs (EMethodDefs e)
  deriving (Functor, Foldable, Traversable, Generic, NFData)

type EMethodType = EDecl' Identity (Const ())
type EMethodDefs e = ([(Loc, Name)], [EMethodImpl e])

data EInstanceDecl e
  = EInstMethod (EMethodImpl e)
  | EInstMutual (EDeclMutual e)
  deriving (Functor, Foldable, Traversable, Generic, NFData)

data EMethodImpl e = EMethodImpl
  { emethodLoc  :: Loc
  , emethodName :: Name
  , emethodAnns :: [Annotation]
  , emethodPats :: [EPattern]
  , emethodRec  :: RecursionKind
  , emethodBody :: e
  } deriving (Functor, Foldable, Traversable, Generic, NFData)

edeclHasAnnot :: Annotation -> EDecl e -> Bool
edeclHasAnnot s x = s `elem` edeclAnns x

emethodTypeParam :: EMethodType e -> EParam e
emethodTypeParam (EDecl l s _ anns ty _) =
  EParam (explicitParam l s) (if AnnErased `elem` anns then emultiZero else emultiOne) (runIdentity ty)

-- ************************************************************************************************
-- Expression wrappers

-- | Concrete syntax expression, parameterized by syntax sugar.
newtype Expr f = Expr {locExpr :: Located (Expr' f (Expr f))}

deriving instance Generic ExprExt
deriving newtype instance NFData ExprExt

instance HasLocation (Expr f) where
  getLoc = getLoc . locExpr

mkExpr :: Loc -> Expr' f (Expr f) -> Expr f
mkExpr l = Expr . locate l

unExpr :: Expr f -> Expr' f (Expr f)
unExpr = unLoc . locExpr

type ExprExt = Expr ExprSugar
type ExprCore = Expr (Const Void)

-- ************************************************************************************************
-- Expressions

data Expr' f e
  = EName Name EPrenexArgs              -- ^ Unresolved variable or declaration reference.
  | ETypeUniv ELevel
  | EDeclRef DeclName EPrenexArgs
  | ELiteral Literal
  | EApply (Maybe Name) e e             -- ^ Function application.
  | ELambda [EPattern] e                -- ^ Pattern matching lambda.
  | ECase (Maybe e) [EBranch e]         -- ^ Case split.
  | EPiType (EParam e) e                -- ^ Dependent function type.
  | ESugar (f e)
  deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

data ExprSugar e
  = EImplicit Name (Maybe ELevel) EMulti      -- ^ Implicit name introduction.
  | EMacro [MacroExprElem e]                  -- ^ Unparsed macro expression.
  | ELitGen Literal                           -- ^ Generic literal.
  | ERecCtor Name (Maybe e) [(BindInfo, e)]-- ^ Record construction/update.
  | EProject e Name                           -- ^ Record projection.
  | EAsPat Name e                             -- ^ As-pattern (e.g. s@p)
  | ESequence Bool [(EPattern, e)]            -- ^ Let/do sequence.
  | EInlineBind e                             -- ^ Inline monadic bind.
  | EList [e]                                 -- ^ List syntax sugar.
  | EEffect e [e] EPrenexArgs                 -- ^ Effect type syntax sugar.
  deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

-- | Branch of a pattern match.
data EBranch e = EBranch [EPattern] e
  deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

-- | Pattern to match against.
type EPattern = EPattern' ApplyInfo
data EPattern' z = EPat
  { epatInfo :: z
  , epatName :: Name
  , epatImpl :: EPatternImpl z
  } deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

data EPatternImpl z
  = EPatV
  | EPatC Name (EPatArgs z)
  deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

data EPatArgs z
  = EPatArgs [EPattern' z]
  | EPatProj [(BindInfo, EPattern' z)]
  deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

data EParam e = EParam
  { eparamInfo  :: ParamInfo
  , eparamMulti :: EMulti
  , eparamType  :: e
  } deriving (Eq, Show, Functor, Foldable, Traversable, Generic, NFData)

instance HasLocation (EParam e) where
  getLoc = paramLoc . eparamInfo

instance HasLocation EPattern where
  getLoc = applyLoc . epatInfo

eparamName :: EParam e -> Name
eparamName = paramName . eparamInfo

-- ************************************************************************************************
-- Prenex

type EPrenexArgs = ([(Name, ELevel)], [(Name, EMulti)])

type ELevel = [(ELevelId, Int)]
data ELevelId
  = ELevelHole
  | ELevelZero
  | ELevelUser Name
  deriving (Eq, Ord, Show, Generic, NFData)

elevelNat :: Int -> ELevel
elevelNat n = [(ELevelZero, n)]

elevelUser :: Name -> ELevel
elevelUser s = [(ELevelUser s, 0)]

type EMulti = [([EMultiId], MultiLit)]
data EMultiId
  = EMultiHole
  | EMultiUser Name
  deriving (Eq, Ord, Show, Generic, NFData)

emultiZero :: EMulti
emultiZero = [([], MultiZero)]

emultiOne :: EMulti
emultiOne = [([], MultiOne)]

emultiAny :: EMulti
emultiAny = [([], MultiAny)]

emultiWild :: EMulti
emultiWild = [([EMultiHole], MultiOne)]

emultiUser :: Name -> EMulti
emultiUser s = [([EMultiUser s], MultiOne)]

-- ************************************************************************************************
-- Utilities

substExprs :: Functor s => [(Name, Loc -> Expr s)] -> Expr s -> Expr s
substExprs subs = if null subs then id else frec
  where
  frec e = case unExpr e of
    EName s lvls -> maybe (mkExpr (getLoc e) (EName s lvls)) ($ getLoc e) (L.lookup s subs)
    x            -> mkExpr (getLoc e) (frec <$> x)

namesInExpr :: Foldable s => Expr s -> HashSet Name
namesInExpr e = case unExpr e of
  EName s _ -> singleton s
  x         -> foldMap namesInExpr x

makeCtorName :: Name -> Name
makeCtorName = addNamePrefix "'"

ePiTypes :: [EParam (Expr s)] -> Expr s -> Expr s
ePiTypes = flip $ foldr (\p rt -> mkExpr (getLoc p) $ EPiType p rt)

wrapImplicitNames :: [(Name, (Loc, Maybe ELevel, EMulti))] -> Expr s -> Expr s
wrapImplicitNames = ePiTypes . map (\(s, (l, uM, m)) ->
  EParam (ParamInfo l (Just s) s) m (maybe (eHole l) (mkExpr l . ETypeUniv) uM))

noImplicits :: ExprExt -> ExprExt
noImplicits e = mkExpr (getLoc e) $ case unExpr e of
  ESugar (EImplicit s _ _) -> EName s mempty
  x -> fmap noImplicits x

eLambda :: Loc -> [EPattern] -> Expr s -> Expr s
eLambda l ps b = mkExpr l $ case unExpr b of
  ELambda ps' b' -> ELambda (ps <> ps') b'
  _ -> ELambda ps b

epatV :: Loc -> Name -> EPattern
epatV l s = EPat (nullApplyInfo l) s EPatV

eLet :: EPattern -> Expr s -> Expr s -> Expr s
eLet p a b = eapplyEx (getLoc p) (eLambda (getLoc p) [p] b) [a]

-- | Extract the untyped parameters of a type.
epiParams :: Expr s -> ([EParam (Expr s)], Expr s)
epiParams x = case unExpr x of
  EPiType p rt -> first (p:) $ epiParams rt
  _ -> ([], x)

makeImplicit :: ParamInfo -> ParamInfo
makeImplicit z = z {paramImplicit = Just (paramName z)}

eapply :: Loc -> Expr s -> [(Maybe Name, Expr s)] -> Expr s
eapply l = foldl (\f (z, r) -> mkExpr l (EApply z f r))

eapplyEx :: Loc -> Expr s -> [Expr s] -> Expr s
eapplyEx l f = eapply l f . map (Nothing,)

eparamArg :: ParamInfo -> (Maybe Name, Expr s)
eparamArg z = (paramImplicit z, ename (paramLoc z) (paramName z))

edeclRefInferPrenex :: Loc -> DeclName -> Expr s
edeclRefInferPrenex l s = mkExpr l (EDeclRef s mempty)

edeclRefParamPrenex :: Loc -> LDecl -> Expr s
edeclRefParamPrenex l d = mkExpr l (EDeclRef (ldecl d declName) (ldecl d eprenexSelfMap))

eprenexSelfMap :: Decl fimpl t -> EPrenexArgs
eprenexSelfMap = bimap (map (\s -> (s, elevelUser s))) (map (\s -> (s, emultiUser s))) . declPrenex

eprenexInDecl :: EDecl ExprCore -> PrenexParams
eprenexInDecl d = bimap (L.sort . toList) (L.sort . toList) (fromAuto <> fromTerms)
  where
  fromTerms = foldMap eprenexInExpr d
  fromAuto = case edeclImpl d of
    EAbility {} -> (singleton (Name "A"), singleton (Name "p"))
    _ -> mempty

eprenexInExpr :: ExprCore -> (HashSet Name, HashSet Name)
eprenexInExpr e = foldMap eprenexInExpr (unExpr e) <> case unExpr e of
  EName _ pargs    -> bimap (foldMap (enamesInLevel . snd)) (foldMap (enamesInMulti . snd)) pargs
  EDeclRef _ pargs -> bimap (foldMap (enamesInLevel . snd)) (foldMap (enamesInMulti . snd)) pargs
  ETypeUniv u      -> (enamesInLevel u, mempty)
  EPiType p _      -> (mempty, enamesInMulti (eparamMulti p))
  _ -> mempty

enamesInLevel :: ELevel -> HashSet Name
enamesInLevel = foldMap ((\case ELevelUser s -> singleton s; _ -> mempty) . fst)

enamesInMulti :: EMulti -> HashSet Name
enamesInMulti = foldMap (foldMap (\case EMultiUser s -> singleton s; _ -> mempty) . fst)

ename :: Loc -> Name -> Expr s
ename l s = mkExpr l (EName s mempty)

eHole :: Loc -> Expr s
eHole l = ename l (Name "")

eparamsType :: [EParam (Expr s)] -> Expr s -> EParamsType (Expr s)
eparamsType ps rt = EParamsType $ ePiTypes ps (mkExpr l $ EApply Nothing (eHole l) rt)
  where
  l = getLoc rt

unfoldEApps :: Expr s -> (Expr s, [(Maybe Name, Expr s)])
unfoldEApps = frec []
  where
  frec rs x = case unExpr x of
    EApply z f r -> frec ((z, r):rs) f
    _ -> (x, rs)

ambiguous :: CheckErrorC m => Loc -> Name -> [Loc] -> m a
ambiguous loc s locs = checkErrorN nullLoc ("duplicate declarations of " <> show s) (map (`strErr` "") (loc:locs))

eparamPattern :: ParamInfo -> EPattern
eparamPattern z = EPat (paramApplyInfo z) (paramName z) EPatV

wrapLambdas :: forall k n. (KnownNat k, KnownNat n) => Vector k ParamInfo -> UTerm (k + n) -> UTerm n
wrapLambdas ps b = case sNat @k of
  Zero -> b
  Succ _ -> mkULam (vhead ps) (wrapLambdas (vtail ps) b)
