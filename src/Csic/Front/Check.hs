module Csic.Front.Check (checkModule) where
import Csic.Util.Prelude
import Csic.Core.Check
import Csic.Front.Syntax
import Csic.Front.Check.ExprExt
import Csic.Front.Check.Decl

-- | Check module declarations.
checkModule :: Namespace -> [EDecl ExprExt] -> FrontEnv -> CheckResult FrontEnv
checkModule ns = flip $ foldM (desugarAndCheckDecl ns)

-- | Desugar and type-check a block of mutual declarations.
desugarAndCheckDecl :: Namespace -> FrontEnv -> EDecl ExprExt -> CheckResult FrontEnv
desugarAndCheckDecl ns env d = case fcheck of
  Left err -> do
    unless isErrTest $ throwError err
    pure env {frontErrLog = err:frontErrLog env}
  Right cenv' -> do
    when isErrTest $ checkError (edeclLoc d) "expected type-check error"
    pure env {frontCore = cenv'}
  where
  isErrTest = edeclHasAnnot AnnError d
  fcheck = withError (CErrTree (edeclLoc d) ("while checking " <> show (edeclName d)) []:) $ do
    -- Desugar extended expressions to core expressions.
    d' <- mapM (desugarExprExtTop ns (frontMacros env) (getCEnv env)) d
    -- Check core expression declaration.
    runTypeCheck ns (getCEnv env) (eprenexInDecl d') (checkEDecl d')
