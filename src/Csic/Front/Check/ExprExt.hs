module Csic.Front.Check.ExprExt (desugarExprExtTop) where
import Csic.Util.Prelude
import Csic.Core.Check
import Csic.Front.Syntax
import Csic.Front.Check.Base
import Csic.Front.Macro
import qualified Data.List as L

-- ************************************************************************************************

-- | Desugar a top-level expression.
desugarExprExtTop :: Namespace -> MacroEnv -> CEnv -> ExprExt -> CheckResult ExprCore
desugarExprExtTop ns macros env t = do
  (t', st') <- runStateT (desugarExprExtWithBinds t) (DesugarState ns env macros [] 0 [])
  pure $ wrapImplicitNames (reverse $ stImplicits st') t'

data DesugarState = DesugarState
  { stNamespace :: Namespace
  , stCEnv      :: CEnv
  , stMacros    :: MacroEnv
  , stImplicits :: [(Name, (Loc, Maybe ELevel, EMulti))]
  , stNextTemp  :: Int
  , stBinds     :: [(EPattern, ExprCore)]
  }
instance HasCEnv DesugarState where
  getCEnv = stCEnv
  mapCEnv f e = e {stCEnv = mapCEnv f (stCEnv e)}

type DesugarExtM = StateT DesugarState CheckResult

lookupMacro :: Loc -> Name -> DesugarExtM (Macro QName)
lookupMacro l s = get >>= \st -> case lookupEnv (QName (stNamespace st) s) (stMacros st) of
  Just x -> pure x
  Nothing -> checkError l $ "unresolved macro " <> show s

lookupCtorInfo :: Loc -> Name -> DesugarExtM (DeclName, [ParamInfo])
lookupCtorInfo l s = do
  cd <- get >>= \st -> liftE $ lookupCDeclQE l (QName (stNamespace st) s)
  case declImpl cd of
    ICtor c -> do
      d <- liftE $ lookupCDeclE l (ctorDataTypeName c)
      case declImpl d of
        IData y -> pure (declName cd, drop (dataTypeArity y) (piParams $ declType cd))
        _ -> checkError l "invalid contructor reference"
    _ -> checkError l "invalid contructor reference"

-- ************************************************************************************************

desugarExprExt :: ExprExt -> DesugarExtM ExprCore
desugarExprExt e0 = case unExpr e0 of
  EName s pargs      -> fret $ pure $ EName s pargs
  ETypeUniv u        -> fret $ pure $ ETypeUniv u
  EDeclRef s pargs   -> fret $ pure $ EDeclRef s pargs
  ELiteral v         -> fret $ pure $ ELiteral v
  EApply z f r       -> fret $ EApply z <$> desugarExprExt f <*> desugarExprExt r
  ELambda ps b       -> fret $ ELambda ps <$> desugarExprExtWithBinds b
  ECase dM bs        -> fret $ ECase <$> mapM desugarExprExt dM
                                     <*> mapM (\(EBranch ps b) -> EBranch ps <$> desugarExprExtWithBinds b) bs
  EPiType p rt       -> fret $ EPiType <$> mapM desugarExprExt p <*> desugarExprExtWithBinds rt
  ESugar y           -> case y of
    EImplicit s uM m -> do
      st <- get
      when (isJust (L.lookup s $ stImplicits st)) $ checkError l "duplicate implicit name binding"
      put st {stImplicits = (s, (l, uM, m)):stImplicits st}
      fret $ pure $ EName s mempty
    EMacro es -> do
      toks <- forM es $ \case
        Left (lm, s) -> LToken lm . Irr . Left <$> lookupMacro lm s
        Right e -> pure $ LToken (getLoc e) (Irr $ Right e)
      liftEither (parseMacroExpr toks) >>= desugarExprExt
    ELitGen v -> do
      f <- getPrimExpr l "coerce"
      desugarExprExt $ eapplyEx l f [mkExpr l $ ELiteral v]
    ERecCtor sc srcM vs -> do
      (ctorName, cps) <- lookupCtorInfo l sc
      unless (length (L.nub (map (bindName . fst) vs)) == length vs) $ checkError l "duplicate field"
      forM_ vs $ \(z, _) -> unless (isJust (find ((bindName z ==) . paramName) cps)) $
        checkError l $ "invalid field name " <> show (bindName z)
      tmp <- nextTempName
      args <- forM cps $ \z -> case find ((paramName z ==) . bindName . fst) vs of
        Just (_, r) -> pure (paramImplicit z, r)
        Nothing -> case srcM of
          Just _ -> pure (paramImplicit z, mkExpr l $ ESugar $ EProject (ename l tmp) (paramName z))
          Nothing -> checkError l $ "missing field name " <> show (paramName z)
      let val = eapply l (edeclRefInferPrenex l ctorName) args
      let fseq src = [(epatV l tmp, src), (epatV l (Name ""), val)]
      desugarExprExt $ maybe val (mkExpr l . ESugar . ESequence False . fseq) srcM
    EProject v s -> do
      project <- getPrimExpr l "project"
      let x' = eapply l project [(Just (Name "label"), mkExpr l $ ELiteral (Str (unName s))), (Nothing, v)]
      desugarExprExt x'
    EAsPat {} -> checkError l "invalid as-pattern outside a pattern context"
    ESequence isDo xs0 -> handleBinds (frec xs0)
      where
      frec = \case
        [] -> checkError l "empty seqeuence"
        [(EPat z s p, a)] -> case p of
          EPatV | s == Name "" -> desugarExprExt a
          _ -> checkError (applyLoc z) "last element in sequence cannot be named"
        (p@(EPat _ s px), a0):bs -> do
          a <- case px of
            EPatV | s == Name "" -> do
              unless isDo $ checkError la "initial elements in let sequence must be named"
              unitExp <- getPrimExpr l "unitExpected"
              pure $ eapplyEx l unitExp [mkExpr la (ESugar $ EInlineBind a0)]
            _ -> pure a0
          case unExpr a of
            -- Optimization: avoid redundant let-expression when binding directly.
            ESugar (EInlineBind ab) -> do
              ab' <- desugarExprExt ab
              handleBinds (frec bs) >>= genBind (p, ab')
            _ -> eLet p <$> desugarExprExt a <*> handleBinds (frec bs)
          where
          la = getLoc p
    EInlineBind a -> do
      a' <- desugarExprExt a
      s <- nextTempName
      modify $ \st -> st {stBinds = (epatV l s, a'):stBinds st}
      pure $ ename l s
    EList vs -> do
      nil' <- getPrimExpr l "'nil"
      cons' <- getPrimExpr l "'cons"
      desugarExprExt $ foldr (\h ts -> eapplyEx l cons' [h, ts]) nil' vs
    EEffect res effs pargs -> do
      fsTy <- getPrimExpr' l "Abilities" (second (const mempty) pargs)
      mem <- getPrimExpr' l "Member" (second (const mempty) pargs)
      eff <- getPrimExpr' l "Effect" pargs
      let sf = Name "Fs"
      let effs' = zipWith (\e i -> EParam (implicitParam l (Name $ "mem" <> pack (show i)))
            emultiAny (eapplyEx l mem [e, ename l sf])) effs [0::Int ..]
      let ps = EParam (implicitParam l sf) emultiZero fsTy:effs'
      let rt = eapplyEx l eff [ename l sf, res]
      desugarExprExt (ePiTypes ps rt)
  where
  l = getLoc e0
  fret = fmap $ mkExpr l

-- ************************************************************************************************

desugarExprExtWithBinds :: ExprExt -> DesugarExtM ExprCore
desugarExprExtWithBinds e = handleBinds (desugarExprExt e)

handleBinds :: DesugarExtM ExprCore -> DesugarExtM ExprCore
handleBinds fx = do
  prev <- gets stBinds
  modify $ \st -> st {stBinds = []}
  x <- fx
  bs <- reverse <$> gets stBinds
  modify $ \st -> st {stBinds = prev}
  if null bs then pure x else foldrM genBind x bs

genBind :: (EPattern, Expr s) -> Expr s -> DesugarExtM (Expr s)
genBind (p, a) b = do
  f <- getPrimExpr (getLoc p) "bind"
  pure $ eapplyEx (getLoc p) f [a, eLambda (getLoc p) [p] b]

-- ************************************************************************************************

nextTempName :: DesugarExtM Name
nextTempName = do
  st <- get
  put st {stNextTemp = stNextTemp st + 1}
  pure $ Name $ "_temp" <> pack (show (stNextTemp st))