module Csic.Front.Check.Decl (checkEDecl) where
import Csic.Util.Prelude
import Csic.Core.Check
import Csic.Front.Syntax
import Csic.Front.Check.Base
import Csic.Front.Check.ExprCore
import qualified Data.List as L
import qualified Data.Text as T

-- ************************************************************************************************

checkEDecl :: EDecl ExprCore -> CheckM ()
checkEDecl d = case edeclImpl d of
  -- Check a set of mutual declarations.
  EMutual xs -> do
    -- (ps, _) <- splitParamsType (edeclType d)
    types <- mapM checkEDeclType xs
    forM_ (zip types xs) $ \(ty, x) -> checkEDeclImpl (edeclImpl x) ty
  -- Check a standalone declaration.
  ESingle x -> checkEDeclType d >>= checkEDeclImpl x
  EClass methods -> do
    (dty, dps) <- checkEDeclType d
    -- Partition method declarations into types and defaults.
    let fsplit = \case EMethodType v -> Left v; EMethodDefs v -> Right v
    let (methodTypes, methodDefaults) = partitionEithers $ map fsplit methods
    -- Construct class info structure from default method group names.
    let defGroups = map (bimap (map snd) (map emethodName)) methodDefaults
    let classInfo = TypeClassInfo [] [] defGroups
    -- Check the class record type.
    let fields = map emethodTypeParam methodTypes
    (fieldInfos, ctorType) <- checkRecordType (Just classInfo) l dty dps (declAccess dty) fields
    -- Check method projection wrappers.
    forM_ (zip methodTypes fieldInfos) $ \(m, (f, _)) ->
      deferTypeCheckWrap l ("method " <> show (edeclName m)) $
        checkProjection (declName dty) dps (declNameQ ctorType) (Just (edeclAccess m, edeclAnns m), f)
    -- Check method default functions.
    forM_ methodDefaults $ \(deps, defs) -> forM_ defs $ \def ->
      deferTypeCheckWrap l ("default " <> show (emethodName def)) $
        checkDefaultMethod (declName dty) dps fieldInfos (deps, defs) def
  EInstance xs0 -> do
    -- Extract class info from the instance type.
    info@(Sized dps) <- checkParamsType emptyECtx (edeclType d) >>= checkInstanceType l
    let atc = ptyExtra dps
    -- Generate instance name if none provided.
    s' <- if edeclName d /= Name "" then pure (edeclName d) else genInstanceName l info
    -- Reorder and generate instance methods from the provided methods and class defaults.
    xs <- genInstanceDecls l s' d info xs0
    -- Check each instance declaration.
    forM_ xs $ \(s, fcheck) ->
      deferTypeCheckWrap l ("instance declaration " <> show s) fcheck
    -- Check the instance record.
    deferTypeCheckWrap l ("instance record " <> show s') $ do
      -- Check the instance type with potentially generated name.
      dty <- checkUDeclType l s' (edeclAccess d) (AnnInline : edeclAnns d) (ptyWrap dps id id (ptyResult dps))
      -- Lookup the class constructor.
      classCtor <- lookupLDecl l (atcCtor atc)
      -- Extract the contstructor's class parameters from its type.
      let (ctorClassParams, _) = L.partition (isJust . paramImplicit) (ldecl classCtor declParamInfos)
      -- Construct implicit class arguments from the class arguments in the instance type.
      let classDecl = atcDecl atc
      let classArgs = implicifyClassArgs (zip (declParamInfos classDecl) ctorClassParams) (atcArgs atc)
      -- Construct explicit method arguments from the instance methods.
      methodArgs <- forM (classMethods $ atcInfo atc) $ \m -> do
        method <- lookupLDeclByName l (instMethodName s' $ methodName m)
        let methodRef = edeclRefParamPrenex l method
        Arg (nullApplyInfo l) multiHole <$> checkExpr (ptyCtx dps) (eapply l methodRef (ptyArgs dps))
      -- Wrap the fully-applied class constructor in the instance parameters.
      let f = mkCon (Irr l) $ DeclRef (atcCtor atc) (atcPrenex atc)
      let b = mkApps f (classArgs <> methodArgs)
      let t = wrapLambdas (ptyParams dps) b
      -- Check the completed instance record declaration.
      checkUDeclImpl l (declName dty) $ IDef $ Def (DefInstance $ declName classDecl) RecursionNone t
  EAbility ops -> do
    (dty, dps) <- genAbilityDecl d >>= checkEDeclType
    -- Construct ability datatype.
    mapM (genOpCtor >=> checkCtor dty dps) ops >>= checkDataType Nothing l dty
    -- Construct operation wrapper functions.
    dps' <- checkParamsType emptyECtx (edeclType d)
    forM_ ops $ \op ->
      deferTypeCheckWrap l ("operation " <> show (edeclName op)) $
        checkOpFunc (declName dty) dps' op
  where
  l = edeclLoc d

checkEDeclType :: EDecl' EParamsType f ExprCore -> CheckM (PDecl, Sized (ParamsType 0))
checkEDeclType (EDecl l s acc anns ty _) = do
  ps@(Sized x) <- checkParamsType emptyECtx ty
  (,ps) <$> checkUDeclType l s acc anns (ptyWrap x id id (ptyResult x))

checkEDeclImpl :: EImplMutual ExprCore -> (PDecl, Sized (ParamsType 0)) -> CheckM ()
checkEDeclImpl x (dty, dps@(Sized pty)) = case x of
  EAxiom -> checkUDeclImpl l (declName dty) IAxiom
  EDef recKind b -> do
    t <- checkExpr emptyECtx (eLambda l (ptyPatterns id pty) b)
    checkUDeclImpl l (declName dty) $ IDef $ Def DefNormal recKind t
  EData cs -> mapM (checkCtor dty dps) cs >>= checkDataType Nothing l dty
  ERecord ctorAcc fields -> do
    (fieldInfos, ctorType) <- checkRecordType Nothing l dty dps ctorAcc fields
    -- Check field projection instances.
    forM_ fieldInfos $ \(f, _) ->
      deferTypeCheckWrap l ("project " <> show (fieldName f)) $
        checkProjection (declName dty) dps (declNameQ ctorType) (Nothing, f)
  where
  l = declLoc dty

-- ************************************************************************************************

checkDataType :: Maybe TypeClassInfo -> Loc -> PDecl -> [PDecl] -> CheckM ()
checkDataType classInfo l dty ctors =
  checkUDeclImpl l (declName dty) $ IData $ DataTypeInfo classInfo 0 (map ((,0) . declName) ctors)

checkCtor :: PDecl -> Sized (ParamsType 0) -> ECtor ExprCore -> CheckM PDecl
checkCtor dataType (Sized dps) (EDecl l s acc anns ty _) = do
  Sized ps <- checkParamsType (ptyCtx dps) ty
  let rt = eapply l (edeclRefParamPrenex l $ Left dataType) (ptyArgs dps)
  ty' <- ptyWrap dps makeImplicit (const multiZero) . ptyWrap ps id id <$> checkExpr (ptyCtx ps) rt
  d <- checkUDeclType l s acc (declAnnots dataType <> anns) ty'
  checkUDeclImpl l (declName d) $ ICtor $ DataCtorInfo (declName dataType)
  pure d

-- ************************************************************************************************

checkRecordType :: Maybe TypeClassInfo -> Loc -> PDecl -> Sized (ParamsType 0) -> Access -> [EParam ExprCore] ->
  CheckM ([(FieldInfo, MethodInfo)], PDecl)
checkRecordType classInfo0 l dty dps@(Sized @k _) acc fields = do
  -- Check single constructor.
  let ctorName = makeCtorName $ Name $ uncapitalize $ unName (qname $ declNameQ dty)
  let ctor = EDecl l ctorName acc (declAnnots dty) (eparamsType fields (eHole l)) (Const ())
  ctorDecl <- checkCtor dty dps ctor
  -- Update class info with typed method info.
  let fieldInfos = reverse $ foldl' makeFieldInfo [] fields
  let fieldParamInfos = drop (natInt @k) $ nestedPiParams (declType ctorDecl)
  let methodInfos = zipWith makeMethodInfo fieldInfos fieldParamInfos
  let classInfo = (\x -> x {classMethods = methodInfos}) <$> classInfo0
  -- Check final data type.
  checkDataType classInfo l dty [ctorDecl]
  -- Check field type aliases.
  forM_ fieldInfos $ \f ->
    deferTypeCheckWrap l ("typeof " <> show (fieldName f)) $
      checkFieldType (declName dty) dps f
  pure (zip fieldInfos methodInfos, ctorDecl)

makeMethodInfo :: FieldInfo -> (ParamInfo, [ParamInfo]) -> MethodInfo
makeMethodInfo f (_, ps) = MethodInfo
  { methodName = fieldName f
  , methodErased = eparamMulti (fieldParam f) == emultiZero
  , methodDeps = map fieldName (fieldDeps f)
  , methodImps = takeWhile (isJust . paramImplicit) ps
  }

-- | Extract the untyped parameters of a type.
nestedPiParams :: KnownNat n => Term m n -> [(ParamInfo, [ParamInfo])]
nestedPiParams x = case unT x of
  Pit (Irr z) pt _ rt -> (z,piParams pt):nestedPiParams rt
  _ -> []

-- ************************************************************************************************

type FieldCtx = [(Name, ExprCore)]

lookupFieldCtx :: Loc -> String -> FieldCtx -> Name -> CheckM (Maybe Name, ExprCore)
lookupFieldCtx l msg depCtx s = case L.lookup s depCtx of
  Nothing -> checkError l $ "missing field dependency: " <> msg <> " => " <> show s
  Just v -> pure (Nothing, v)

-- ************************************************************************************************

data FieldInfo = FieldInfo
  { fieldParam    :: EParam ExprCore
  , fieldDeps     :: [FieldInfo]
  }

type FieldNameAndDeps = (Name, [Name])

fieldName :: FieldInfo -> Name
fieldName = eparamName . fieldParam

fieldNameAndDeps :: FieldInfo -> FieldNameAndDeps
fieldNameAndDeps f = (fieldName f, map fieldName (fieldDeps f))

makeFieldInfo :: [FieldInfo] -> EParam ExprCore -> [FieldInfo]
makeFieldInfo rprev p = FieldInfo p recDeps:rprev
  where
  -- Determine dependence on previous fields.
  namesInField = namesInExpr (eparamType p)
  directDeps = filter (\g -> member (fieldName g) namesInField) rprev
  recDeps = reverse $ filter (\g -> any (needDep g) directDeps) rprev
  needDep x y = fieldName x == fieldName y || any ((fieldName x ==) . fieldName) (fieldDeps y)

instFieldType' :: FieldCtx -> Loc -> Name -> [(Maybe Name, ExprCore)] -> [Name] -> CheckM ExprCore
instFieldType' depCtx l s dataArgs deps = do
  dft <- lookupLDeclByName l s
  depArgs <- forM deps $ lookupFieldCtx l (show s) depCtx
  pure $ eapply l (edeclRefParamPrenex l dft) (dataArgs <> depArgs)

instFieldType :: QName -> Sized (ParamsType 0) -> FieldCtx -> Loc -> FieldNameAndDeps -> CheckM ExprCore
instFieldType dataName (Sized dps) depCtx l (s, deps) =
  instFieldType' depCtx l (fieldTypeName (qname dataName) s) (ptyArgs dps) deps

checkFieldType :: DeclName -> Sized (ParamsType 0) -> FieldInfo -> CheckM ()
checkFieldType dataName (Sized dps) f = do
  -- Lookup the datatype declaration.
  dataType <- liftE $ lookupCDeclE l dataName
  -- Lookup dependency parameter types.
  (_, depParams) <- second reverse <$> foldM (makeDepParam dataType) ([], []) (fieldDeps f)
  -- Check type hole wrapped in the record type parameters (implicit).
  ty <- ptyWrap dps id (const multiAny) <$> checkExpr (ptyCtx dps) (ePiTypes depParams (eHole l))
  let s = fieldTypeName (qname $ declNameQ dataType) (fieldName f)
  dty <- checkUDeclType l s (declAccess dataType) (AnnErased : declAnnots dataType) ty
  -- Check implementation as the field type itself.
  let ps = ptyPatterns id dps <> map (eparamPattern . eparamInfo) depParams
  let b = eparamType $ fieldParam f
  t <- checkExpr emptyECtx (eLambda l ps b)
  checkUDeclImpl l (declName dty) $ IDef $ Def DefNormal RecursionNone t
  where
  l = getLoc $ fieldParam f
  makeDepParam dataType (depCtx, res) g = do
    pg <- EParam (explicitParam l (fieldName g)) emultiAny <$>
      instFieldType (declNameQ dataType) (Sized dps) depCtx l (fieldNameAndDeps g)
    pure ((fieldName g, ename l $ fieldName g):depCtx, pg:res)

-- ************************************************************************************************

checkProjection :: DeclName -> Sized (ParamsType 0) -> QName -> (Maybe (Access, [Annotation]), FieldInfo) -> CheckM ()
checkProjection dataName (Sized dps) ctorName (mi, fi) = do
  -- Lookup the datatype declaration.
  dataType <- liftE $ lookupCDeclE l dataName
  let (acc, anns0) = maybe (declAccess dataType, declAnnots dataType) (second (declAnnots dataType <>)) mi
  let anns = if eparamMulti (fieldParam fi) == emultiZero then AnnErased:anns0 else anns0
  -- Construct 'this' record type parameter.
  let thisParam = (if isMethod then implicitParam else explicitParam) l (Name "i_this")
  thisType <- checkExpr (ptyCtx dps) (eapply l (edeclRefParamPrenex l $ Right dataType) (ptyArgs dps))
  let thisVar = ename l $ paramName thisParam
  -- Construct the (dependent) field type.
  let depCtx = map (\g -> (fieldName g, makeFieldDepProj thisVar g)) (fieldDeps fi)
  fieldTypeE <- instFieldType (declNameQ dataType) (Sized dps) depCtx l (fieldNameAndDeps fi)
  fieldTypeU <- checkExpr (consECtx (paramName thisParam) (ptyCtx dps)) fieldTypeE
  -- Construct the full function type.
  (defKind, funcType) <- fmap (second $ ptyWrap dps makeImplicit id) $ if isMethod
    -- Method projection: (`dataParams: ...) -> (`this: DataType) -> FieldType
    then pure (DefNormal, mkPit (Irr thisParam) thisType (levelHole, multiAny) fieldTypeU)
    -- Normal projection: (`dataParams: ...) -> Project "fieldName" DataType `F=(\this => FieldType)
    else do
      projClass <- getPrimDecl l "Project"
      pure $ (DefInstance (declName projClass),) $ mkApps (declRefInferPrenex l projClass)
        [ Arg (ApplyInfo l Nothing) multiHole (mkCon (Irr l) $ Literal $ Str $ unName (fieldName fi))
        , Arg (ApplyInfo l Nothing) multiHole thisType
        , Arg (ApplyInfo l (Just $ Name "F")) multiHole (wrapLambdas (vsingleton thisParam) fieldTypeU)
        ]
  -- Check the projection type.
  let name = if isMethod
        then fieldName fi
        else Name $ "project__" <> unName (qname $ declNameQ dataType) <> "_" <> unName (fieldName fi)
  dty <- checkUDeclType l name acc (if isMethod then AnnInline:anns else anns) funcType
  -- Check the projection function.
  let dpsImpPats = ptyPatterns makeImplicit dps
  let projPatI = makeProjPat ctorName (eparamInfo $ fieldParam fi)
  let projPat = EPat (paramApplyInfo thisParam) (paramName thisParam) projPatI
  let projBody = ename l (fieldName fi)
  -- Wrap the projection function if needed.
  projTerm <- if isMethod
    then checkExpr emptyECtx (eLambda l (dpsImpPats <> [projPat]) projBody)
    else do
      projCtor <- getPrimExpr l "'project"
      checkExpr emptyECtx $ eLambda l dpsImpPats $ eapplyEx l projCtor [eLambda l [projPat] projBody]
  -- Check the projection implementation.
  checkUDeclImpl l (declName dty) $ IDef $ Def defKind RecursionNone projTerm
  where
  l = getLoc $ fieldParam fi
  isMethod = isJust mi
  makeFieldDepProj disc g = makeProjExpr l disc ctorName (eparamInfo $ fieldParam g)

makeProjExpr :: Loc -> ExprCore -> QName -> ParamInfo -> ExprCore
makeProjExpr l disc ctorName z =
  mkExpr l $ ECase (Just disc)
    [EBranch [EPat (nullApplyInfo l) (Name "") (makeProjPat ctorName z)] (ename l $ paramName z)]

makeProjPat :: QName -> ParamInfo -> EPatternImpl ApplyInfo
makeProjPat ctorName z = EPatC (qname ctorName) $ EPatProj [(paramBindInfo z, epatV (paramLoc z) (paramName z))]

-- ************************************************************************************************

checkDefaultMethod :: DeclName -> Sized (ParamsType 0) -> [(FieldInfo, MethodInfo)] ->
  EMethodDefs ExprCore -> EMethodImpl ExprCore -> CheckM ()
checkDefaultMethod dataName (Sized dps) fieldInfos (deps, allDefs) def = do
  dataType <- liftE $ lookupCDeclE lm dataName
  depFields <- mapM (\(l, s) -> (l,) . fst <$> findFieldInfo l s) deps
  let depFieldNames = map (fieldName . snd) depFields
  -- Construct default dependency parameters.
  depParams <- reverse <$> foldlM (makeDepParam dataType) [] depFields
  let depPatterns = map (eparamPattern . eparamInfo) depParams
  -- Construct the default method type.
  (depCtx, prevCtx) <- foldM (addPrevDep depFieldNames) (paramsDepCtx lm depParams, []) prevDefs
  (fi, mi) <- findFieldInfo lm sm
  rt <- instFieldType (declNameQ dataType) (Sized dps) depCtx lm (fieldNameAndDeps fi)
  ty <- ptyWrap dps id id <$> checkExpr (ptyCtx dps) (ePiTypes depParams rt)
  let anns1 = declAnnots dataType <> anns0
  let anns = if eparamMulti (fieldParam fi) == emultiZero then AnnErased:anns1 else anns1
  dty <- checkUDeclType lm (defaultMethodName depNames sm) (declAccess dataType) anns ty
  let recArgs = map eparamArg (vtoList $ ptyParams dps) <> map (eparamArg . eparamInfo) depParams
  let frecSub l = eapply l (ename l (qname $ declNameQ dty)) recArgs
  let b = substExprs ((sm, frecSub) : map (second const) prevCtx) b0
  t <- checkExpr emptyECtx (eLambda lm (ptyPatterns id dps <> depPatterns <> methodImpPats mi <> ps) b)
  checkUDeclImpl lm (declName dty) $ IDef $ Def DefNormal recKind t
  where
  EMethodImpl lm sm anns0 ps recKind b0 = def
  depNames = map snd deps
  prevDefs = takeWhile (\x -> sm /= emethodName x) allDefs
  addPrevDep depFieldNames (depCtx, prevCtx) x = do
    let sdef = defaultMethodName depNames (emethodName x)
    sub <- (emethodName x,) <$> instFieldType' depCtx lm sdef (ptyArgs dps) depFieldNames
    pure (sub : depCtx, sub : prevCtx)

  paramsDepCtx l = map (\p -> (eparamName p, ename l (eparamName p)))

  makeDepParam dataType prev (l, g) = do
    ty <- instFieldType (declNameQ dataType) (Sized dps) (paramsDepCtx lm prev) l (fieldNameAndDeps g)
    pure $ EParam (explicitParam l $ fieldName g) emultiAny ty : prev

  findFieldInfo l s =
    fromMaybeM (checkError l "invalid method name") $ find ((s ==) . fieldName . fst) fieldInfos

-- ************************************************************************************************

type InstanceInfo = ParamsType' AppliedTypeClass 0

checkInstanceType :: Loc -> Sized (ParamsType 0) -> CheckM (Sized InstanceInfo)
checkInstanceType l (Sized dps) = do
  unless (all (isJust . paramImplicit) $ ptyParams dps) $ checkError l "all instance parameters must be implicit"
  isAppliedTypeClass l (ptyResult dps) >>= \case
    Nothing -> checkError l "instance result type must have class head"
    Just atc -> pure $ Sized $ dps {ptyExtra = atc}

implicifyClassArgs :: [(ParamInfo, ParamInfo)] -> Args () n -> Args () n
implicifyClassArgs ps rs = case (ps, rs) of
  ((pz, cz):ps', r:rs') -> case applyImplicit (argInfo r) of
    Just s -> if paramImplicit pz == Just s
      then r {argInfo = (argInfo r) {applyImplicit = Just (paramName cz)}}:implicifyClassArgs ps' rs'
      else implicifyClassArgs ps' rs
    Nothing -> if isJust (paramImplicit pz)
      then implicifyClassArgs ps' rs
      else r {argInfo = (argInfo r) {applyImplicit = Just (paramName cz)}}:implicifyClassArgs ps' rs'
  _ -> rs

genInstanceName :: Loc -> Sized InstanceInfo -> CheckM Name
genInstanceName l (Sized dps) = case mapMaybe checkArg (atcArgs atc) of
  [] -> checkError l "no constant arguments to generate instance name"
  args -> do
    args' <- mapM (fmap (qname . declNameQ) . liftE . lookupCDeclE l) args
    pure $ defaultInstName (qname $ declNameQ $ atcDecl atc) args'
  where
  atc = ptyExtra dps
  checkArg x = case unfoldApps (argTerm x) of
    (Con _ (DeclRef s _), _)-> Just s
    _ -> Nothing

genInstanceDecls :: Loc -> Name -> EDecl ExprCore -> Sized InstanceInfo ->
  [EInstanceDecl ExprCore] -> CheckM [(Name, CheckM ())]
genInstanceDecls l instName instDecl (Sized dps) = genDefaults (classDefaults tci) . map genProvided
  where
  atc = ptyExtra dps
  tci = atcInfo atc

  genProvided = \case
    EInstMethod m -> genMethod $ flip fmap m $ \b0 mi -> do
      b <- substThisRefs instName b0
      checkExpr emptyECtx (eLambda l (ptyPatterns id dps <> methodImpPats mi <> emethodPats m) b)
    EInstMutual d0 -> (edeclName d0,) $ do
      d <- mapM (substThisRefs instName) d0
      checkEDeclType d >>= checkEDeclImpl (edeclImpl d)

  hasMethod xs s = any ((s ==) . fst) xs
  genDefaults groups methods = case groups of
    -- Check each default method group to see if dependencies are met.
    (deps, defs0):groups' -> if null defs
      -- If no methods can be resolved, check the next default group.
      then genDefaults groups' methods
      -- Instantiate default methods and restart search to use earliest defaults first when their dependencies are met.
      else genDefaults (classDefaults tci) (methods <> defs')
      where
      defs = if all (hasMethod methods) deps then filter (not . hasMethod methods) defs0 else []
      defs' = map (\s -> genMethod $ EMethodImpl l s [] [] RecursionNone (genDefault deps s)) defs
    -- Ensure all methods are implemented.
    [] -> do
      forM_ (classMethods tci) $ \m ->
        unless (hasMethod methods $ methodName m) $ checkError l $ "missing method " <> show (methodName m)
      pure methods

  genMethod m =
    (emethodName m, checkInstanceMethod instName (edeclAccess instDecl) (edeclAnns instDecl) (Sized dps) m)
  genDefault deps s _mi = do
    depArgs <- forM deps $ \sd -> do
      dep <- lookupLDeclByName l (instMethodName instName sd)
      let depRef = edeclRefParamPrenex l dep
      Arg (nullApplyInfo l) multiHole <$> checkExpr (ptyCtx dps) (eapply l depRef (ptyArgs dps))
    dd <- lookupLDeclByName l (defaultMethodName deps s)
    let defRef = mkDeclRef l (ldecl dd declName) (atcPrenex atc)
    let b = mkApps defRef (atcArgs atc <> depArgs)
    pure $ wrapLambdas (ptyParams dps) b

methodImpPats :: MethodInfo -> [EPattern]
methodImpPats = map fpat . methodImps
  where
  fpat z = EPat (paramApplyInfo z) (Name "") EPatV

substThisRefs :: Name -> ExprCore -> CheckM ExprCore
substThisRefs instName e = case unExpr e of
  EName s _ | T.isPrefixOf "this." (unName s) -> do
    d <- lookupLDeclByName l (instMethodName instName $ Name $ T.drop 5 $ unName s)
    pure $ edeclRefParamPrenex l d
  x -> mkExpr l <$> traverse (substThisRefs instName) x
  where
  l = getLoc e

checkInstanceMethod :: Name -> Access -> [Annotation] -> Sized InstanceInfo ->
  EMethodImpl (MethodInfo -> CheckM (UTerm 0)) -> CheckM ()
checkInstanceMethod instName acc anns0 (Sized dps) (EMethodImpl l s anns1 _ recKind fcheck) = do
  -- Lookup method type dependencies.
  mi <- fromMaybeM (checkError l "invalid method name") (find ((s ==) . methodName) (classMethods tci))
  let anns = if methodErased mi then AnnErased:anns0 <> anns1 else anns0 <> anns1
  depArgs <- forM (methodDeps mi) $ \sd -> do
    dep <- lookupLDeclByName l (instMethodName instName sd)
    let depRef = edeclRefParamPrenex l dep
    Arg (nullApplyInfo l) multiHole <$> checkExpr (ptyCtx dps) (eapply l depRef (ptyArgs dps))
  -- Check the method type.
  fty <- lookupLDeclByName l (fieldTypeName (qname dataName) s)
  let ftyRef = mkDeclRef l (ldecl fty declName) (atcPrenex atc)
  let ty = ptyWrap dps id id $ mkApps ftyRef (atcArgs atc <> depArgs)
  dty <- checkUDeclType l (instMethodName instName s) acc anns ty
  -- Check the method implementation.
  t <- fcheck mi
  checkUDeclImpl l (declName dty) $ IDef $ Def DefNormal recKind t
  where
  atc = ptyExtra dps
  tci = atcInfo atc
  dataName = declNameQ $ atcDecl atc

-- ************************************************************************************************

genAbilityDecl :: EDecl ExprCore -> CheckM (EDecl ExprCore)
genAbilityDecl d = do
  let l = edeclLoc d
  (ps, rt) <- splitParamsType (edeclType d)
  let resultTypeUniv = mkExpr l (ETypeUniv $ elevelUser (Name "A"))
  let resultP = EParam (explicitParam l opResultTypeName) emultiZero resultTypeUniv
  pure d {edeclType = eparamsType (ps <> [resultP]) rt}

genOpCtor :: ECtor ExprCore -> CheckM (ECtor ExprCore)
genOpCtor d = do
  let l = edeclLoc d
  (ps, rt) <- splitParamsType (edeclType d)
  eq <- getPrimExpr l "Equal"
  let rt' = eapplyEx l eq [ename l opResultTypeName, rt]
  let eqP = EParam (implicitParam l (Name "P")) emultiZero rt'
  pure d {edeclName = makeCtorName (edeclName d), edeclType = eparamsType (ps <> [eqP]) (eHole l)}

checkOpFunc :: DeclName -> Sized (ParamsType 0) -> ECtor ExprCore -> CheckM ()
checkOpFunc dataName (Sized dps) d = do
  dataType <- liftE $ lookupCDeclE l dataName
  -- Check op function type.
  let levelA = Name "A"
  let multiP = Name "p"
  mem <- getPrimExpr' l "Member" ([(levelA, elevelUser levelA)], mempty)
  eff <- getPrimExpr' l "Effect" (mempty, [(multiP, emultiUser multiP)])
  (opParams, resultType) <- splitParamsType (edeclType d)
  let
    ability = eapply l (edeclRefInferPrenex l dataName) (ptyArgs dps)
    abilities = Name "Fs"
    memType = eapplyEx l mem [ability, ename l abilities]
    extraParams =
      [ EParam (implicitParam l abilities) emultiZero (eHole l)
      , EParam (implicitParam l (Name "mem0")) emultiAny memType
      ]
    ty = ePiTypes (opParams <> extraParams) (eapplyEx l eff [ename l abilities, resultType])
  ty' <- ptyWrap dps makeImplicit id <$> checkExpr (ptyCtx dps) ty
  dty <- checkUDeclType l (edeclName d) (edeclAccess d) (declAnnots dataType <> edeclAnns d) ty'
  -- Check op function implementation.
  bindOp' <- getPrimExpr l "'bindOp"
  inject <- getPrimExpr l "injectOp"
  return' <- getPrimExpr l "'return"
  let opArgs = map makeOpArg opParams
  let pats = ptyPatterns makeImplicit dps <> map (eparamPattern . eparamInfo) (opParams <> extraParams)
  let op = eapply l (ename l $ makeCtorName (edeclName d)) opArgs
  let body = eapplyEx l bindOp' [eapplyEx l inject [op], return']
  t <- checkExpr emptyECtx (eLambda l pats body)
  checkUDeclImpl l (declName dty) $ IDef $ Def DefNormal RecursionNone t
  where
  l = edeclLoc d
  makeOpArg = eparamArg . eparamInfo

-- ************************************************************************************************

type ParamsType = ParamsType' SNat

data ParamsType' f n k = ParamsType
  { ptyCtx    :: ECtx (n + k)
  , ptyParams :: Vector k ParamInfo
  , ptyResult :: UTerm (n + k)
  , ptyWrap   :: TypeWrapper (n + k) n
  , ptyExtra  :: f (n + k)
  }

type TypeWrapper n1 n2 = (ParamInfo -> ParamInfo) -> (Multi () -> Multi ()) -> UTerm n1 -> UTerm n2

ptyArgs :: ParamsType' f n k -> [(Maybe Name, Expr s)]
ptyArgs = map eparamArg . vtoList . ptyParams

ptyPatterns :: (ParamInfo -> ParamInfo) -> ParamsType' f n k -> [EPattern]
ptyPatterns f = map (eparamPattern . f) . vtoList . ptyParams

checkParamsType :: forall n. KnownNat n => ECtx n -> EParamsType ExprCore -> CheckM (Sized (ParamsType n))
checkParamsType ctx0 (EParamsType e) = checkExpr ctx0 e >>= frec @0 ctx0 vempty (const $ const id)
  where
  frec :: KnownNat k => ECtx (n + k) -> Vector k ParamInfo -> TypeWrapper (n + k) n -> UTerm (n + k) ->
    CheckM (Sized (ParamsType n))
  frec ctx rinfo frt ty = case unT ty of
    Pit (Irr z) pt (u, m) rt -> frec (consECtx (paramName z) ctx) (vcons z rinfo)
      (\fz fm -> frt fz fm . mkPit (Irr (fz z)) pt (u, fm m)) rt
    App _ f _ rt | Met {} <- unT f -> pure $ Sized (ParamsType ctx (vreverse rinfo) rt frt sNat)
    _ -> checkError (getLoc ty) "invalid params type"

splitParamsType :: EParamsType ExprCore -> CheckM ([EParam ExprCore], ExprCore)
splitParamsType (EParamsType e) = case unExpr rt of
  EApply _ _ rt' -> pure (ps, rt')
  _ -> checkError (getLoc rt) "invalid params result type"
  where
  (ps, rt) = epiParams e

-- ************************************************************************************************

deferTypeCheckWrap :: Loc -> String -> CheckM () -> CheckM ()
deferTypeCheckWrap l s = deferTypeCheck . withError (CErrTree l ("while deferred checking " <> s) []:)

-- ************************************************************************************************

-- Generated names

fieldTypeName :: Name -> Name -> Name
fieldTypeName dataName s = Name $ "typeof__"  <> unName dataName <> "_" <> unName s

defaultMethodName :: [Name] -> Name -> Name
defaultMethodName deps s = Name $ "default__" <> unName s <> "_from_" <> T.intercalate "_" (map unName deps)

defaultInstName :: Name -> [Name] -> Name
defaultInstName className args = Name $ unName className <> "_" <> T.intercalate "_" (map unName args)

instMethodName :: Name -> Name -> Name
instMethodName instName s = Name $ "method__" <> unName instName <> "_" <> unName s

opResultTypeName :: Name
opResultTypeName = Name "Result"