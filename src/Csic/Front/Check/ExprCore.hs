module Csic.Front.Check.ExprCore (checkExpr, AppliedTypeClass(..), isAppliedTypeClass) where
import Csic.Util.Prelude
import Csic.Core.Check
import Csic.Front.Syntax
import Csic.Front.Check.Base
import qualified Data.List as L

-- ************************************************************************************************

checkExpr :: forall n. KnownNat n => ECtx n -> ExprCore -> CheckM (UTerm n)
checkExpr ctx e = case unExpr e of
  EName (Name "") _ -> pure $ termHole l
  EName s pargs -> case vfindIndex (== s) (fst ctx) of
    Just i -> do
      unless (pargs == mempty) $ checkError l "invalid prenex arguments on local variable"
      pure $ mkVar (Irr l) i
    Nothing -> do
      d <- lookupLDeclByName l s
      pargs' <- checkEPrenexArgs l d pargs
      pure $ mkCon (Irr l) $ DeclRef (ldecl d declName) pargs'
  ETypeUniv u -> do
    u' <- checkELevel l u
    pure $ mkCon (Irr l) $ TypeUniv u'
  EDeclRef s pargs -> do
    d <- lookupLDecl l s
    pargs' <- checkEPrenexArgs l d pargs
    pure $ mkCon (Irr l) $ DeclRef s pargs'
  ELiteral v -> pure $ mkCon (Irr l) $ Literal v
  EApply z f r -> do
    f' <- checkExpr ctx f
    r' <- checkExpr ctx r
    pure $ mkApp (Irr $ ApplyInfo l z) f' multiHole r'
  ELambda ps b -> checkELambda id ctx ps b
  ECase dM bs -> case dM of
    Just d -> mkUCas l <$> checkExpr ctx d <*> mapM (checkEBranch ctx l) bs
    Nothing -> do
      let z = explicitParam l (Name "")
      let pctx = consECtx (paramName z) ctx
      let d = mkVar (Irr l) (finite @0)
      mkULam z . mkUCas l d <$> mapM (checkEBranch pctx l) bs
  EPiType (EParam z m pt) rt -> do
    pt' <- checkExpr ctx pt
    isAppliedTypeClass l pt' >>= \case
      Just atc | isJust (paramImplicit z) -> do
        let iname s = addNamePrefix "i_" $ qname s
        let z' = if paramImplicit z == Just (Name "") then implicitParam l (iname $ declNameQ $ atcDecl atc) else z
        let classArgs' = explicitArgs (atcArgs atc)
        baseParams <- forM (classBases $ atcInfo atc) $ \(baseName, baseArgs) -> do
          baseDecl <- liftE $ lookupCDeclE l baseName
          let baseType = mkApps (declRefInferPrenex l baseDecl) (map (classArgs' !!) baseArgs)
          pure $ EParam (implicitParam l (iname $ declNameQ baseDecl)) emultiAny baseType
        let baseParams' = filter (\p -> eparamType p `notElem` snd ctx) baseParams
        desugarConstraints ctx baseParams' (EParam z' m pt) rt
      _ -> do
        when (paramImplicit z == Just (Name "")) $ checkError l "invalid anonymous implicit parameter"
        m' <- checkEMulti l m
        rt' <- checkExpr (consECtx (paramName z) ctx) rt
        pure $ mkPit (Irr z) pt' (levelHole, m') rt'
  ESugar (Const nope) -> absurd nope
  where
  l = getLoc e

-- ************************************************************************************************

checkELambda :: forall n. KnownNat n => (ExprCore -> ExprCore) -> ECtx n -> [EPattern] -> ExprCore -> CheckM (UTerm n)
checkELambda fwrap ctx ps b = case ps of
  [] -> checkExpr ctx (fwrap b)
  EPat z s0 p:ps' -> case p of
    EPatV -> do
      let s = s0
      let pctx = consECtx s ctx
      b' <- checkELambda fwrap pctx ps' b
      pure $ mkULam (applyParamInfo z) {paramName = s} b'
    EPatC {} -> do
      let s = if s0 == Name "" then Name ("_disc" <> pack (show (natInt @n))) else s0
      let pctx = consECtx s ctx
      let p' = EPat (nullApplyInfo l) (Name "") p
      let fwrap' x = mkExpr l (ECase (Just (mkExpr l $ EName s mempty)) [EBranch [p'] x])
      b' <- checkELambda (fwrap . fwrap') pctx ps' b
      pure $ mkULam (applyParamInfo z) {paramName = s} b'
    where
    l = applyLoc z

checkEBranch :: forall n. KnownNat n => ECtx n -> Loc -> EBranch ExprCore -> CheckM (Branch () n)
checkEBranch ctx l0 (EBranch ps b0) = case ps of
  [] -> checkError l0 "empty branch patterns"
  EPat z s p:ps' -> case p of
    EPatV -> do
      b' <- checkELambda id pctx ps' b0
      pure $ Branch (Irr $ BindInfo (applyLoc z) s) PatWild b'
    EPatC sc crs0 -> do
      cd <- lookupLDeclByName l sc
      (_, dti) <- ctorDataTypeInfo l cd
      let cps = drop (dataTypeArity dti) $ ldecl cd declParamInfos
      ctorArgs (ldecl cd declName) dti pctx vempty crs0 cps
      where
      ctorArgs :: forall k. KnownNat k =>
        DeclName -> DataTypeInfo -> ECtx (k + 1 + n) -> Vector k (Maybe (ParamInfo, EPattern)) ->
        EPatArgs ApplyInfo -> [ParamInfo] -> CheckM (Branch () n)
      ctorArgs ctorName dti bctx res crs = \case
        [] -> do
          case crs of
            EPatArgs rs -> do
              case rs of
                [] -> pure ()
                r:_ -> checkError (getLoc r) "trailing constructor argument"
            EPatProj rs -> do
              case rs of
                [] -> pure ()
                (rz, _):_ -> checkError (bindLoc rz) $ "invalid field projection " <> show (bindName rz)
          b'' <- checkELambda id bctx ps' b'
          pure $ Branch (Irr $ BindInfo (applyLoc z) s) (PatCtor ctorName (Irr (dti, fromList args))) b''
          where
          (args, b') = foldl' nestedPattern ([], b0) (vzip vindices res)
          nestedPattern (rs, b) (i, pM) = case pM of
            Nothing -> (rs, b)
            Just (pz, r) -> ((i, (pz, BindInfo (getLoc r) (epatName r))):rs, b1)
              where
              b1 = case epatImpl r of
                EPatV -> b
                EPatC {} -> eapplyEx lr f [ename lr (epatName r)]
                  where
                  f = mkExpr lr $ ELambda [EPat (nullApplyInfo lr) (Name "") (epatImpl r)] b
                  lr = getLoc r
        cp:cps -> do
          (pM, crs') <- case crs of
            EPatArgs rs -> case rs of
              [] -> do
                unless (isJust $ paramImplicit cp) $ checkError l "too few constructor arguments"
                pure (Nothing, EPatArgs [])
              r:rs' -> if doesApplyMatchParam (epatInfo r) cp
                then pure (Just (cp, addProjName r), EPatArgs rs')
                else if isJust (paramImplicit cp)
                  then pure (Nothing, EPatArgs rs)
                  else checkError (getLoc r) "invalid constructor argument"
            EPatProj rs -> do
              let (found, rs') = L.partition ((== paramName cp) . bindName . fst) rs
              (,EPatProj rs') <$> case found of
                [] -> pure Nothing
                [(_, r)] -> pure $ Just (cp, addProjName r)
                _:(rz, _):_ -> checkError (bindLoc rz) "duplicate field projection"
          let sp = maybe (Name "") (epatName . snd) pM
          ctorArgs ctorName dti (consECtx sp bctx) (vcons pM res) crs' cps
          where
          addProjName r = case epatImpl r of
            EPatV -> r
            EPatC {} -> if epatName r /= Name "" then r else
              r {epatName = Name $ "_proj" <> pack (show (natInt @(n + 1 + k)))}
    where
    l = applyLoc z
    pctx = consECtx s ctx

-- ************************************************************************************************

data AppliedTypeClass n = AppliedTypeClass
  { atcDecl   :: CDecl
  , atcInfo   :: TypeClassInfo
  , atcCtor   :: DeclName
  , atcPrenex :: PrenexArgs ()
  , atcArgs   :: Args () n
  }

isAppliedTypeClass :: Loc -> UTerm n -> CheckM (Maybe (AppliedTypeClass n))
isAppliedTypeClass l ty = case unfoldApps ty of
  (Con _ (DeclRef s pargs), args) -> expandClass s pargs args
  _ -> pure Nothing
  where
  expandClass s pargs args = lookupLDecl l s >>= \case
    Left _ -> pure Nothing
    Right d -> case declImpl d of
      IData (DataTypeInfo {dataClassInfo = Just z, dataCtorInfos = [(sc, _)]}) ->
        pure $ Just $ AppliedTypeClass d z sc pargs args
      IDef x -> case unfoldApps $ defImpl x of
        (Con _ (DeclRef s' pargs'), []) -> expandClass s' (applyPrenex pargs pargs') args
        _ -> pure Nothing
      _ -> pure Nothing
  applyPrenex :: PrenexArgs () -> PrenexArgs Void -> PrenexArgs ()
  applyPrenex (lvls, muls) = bimap
    (map $ \x -> let x' = vacuousLevel x in fromMaybe x' $ instantiateLevelArgs lvls x')
    (map $ \x -> let x' = vacuousMulti x in fromMaybe x' $ instantiateMultiArgs muls x')

desugarConstraints :: KnownNat n =>
  ECtx n -> [EParam (UTerm n)] -> EParam ExprCore -> ExprCore -> CheckM (UTerm n)
desugarConstraints ctx bps p rt = case bps of
  bp:bps' -> do
    let bps2 = map (shiftTerm <$>) bps'
    let bctx = consECtxConstraint (eparamName bp) (eparamType bp) ctx
    m' <- checkEMulti (getLoc bp) $ eparamMulti bp
    rt' <- desugarConstraints bctx bps2 p rt
    pure $ mkPit (Irr $ eparamInfo bp) (eparamType bp) (levelHole, m') rt'
  [] -> do
    pt' <- checkExpr ctx (eparamType p)
    m' <- checkEMulti (getLoc p) $ eparamMulti p
    rt' <- checkExpr (consECtxConstraint (eparamName p) pt' ctx) rt
    pure $ mkPit (Irr $ eparamInfo p) pt' (levelHole, m') rt'

-- ************************************************************************************************

checkEPrenexArgs :: Loc -> LDecl -> EPrenexArgs -> CheckM (PrenexArgs ())
checkEPrenexArgs l d (lvls, muls) = do
  let (plvls, pmuls) = ldecl d declPrenex
  -- Verify argument names are valid.
  forM_ lvls $ \(s, _) -> unless (s `elem` plvls) $ checkError l $ "invalid level arg " <> show s
  forM_ muls $ \(s, _) -> unless (s `elem` pmuls) $ checkError l $ "invalid multi arg " <> show s
  -- Re-order arguments to match parameters, creating meta-variables for missing arguments.
  lvls' <- forM plvls $ \s -> case L.lookup s lvls of
    Nothing -> case d of
      Left {} -> checkELevel l (elevelUser s)
      Right {} -> pure levelHole
    Just x -> checkELevel l x
  muls' <- forM pmuls $ \s -> case L.lookup s muls of
    Nothing -> case d of
      Left {} -> checkEMulti l (emultiUser s)
      Right {} -> pure multiHole
    Just x -> checkEMulti l x
  pure (lvls', muls')

checkELevel :: Loc -> ELevel -> CheckM (Level ())
checkELevel l xs = fmap levelNormalize $ forM xs $ \(k, n) -> case k of
  ELevelHole -> pure (levelIdMeta (), n)
  ELevelZero -> pure (levelIdZero, n)
  ELevelUser s -> do
    lvls <- gets (fst . penvPrenex)
    case L.elemIndex s lvls of
      Nothing -> checkError l $ "unregistered level name " <> show s
      Just i -> pure (levelIdUser i, n)

checkEMulti :: Loc -> EMulti -> CheckM (Multi ())
checkEMulti l xs = fmap multiNormalize $ forM xs $ \(ks, n) -> fmap (,n) $ forM ks $ \case
  EMultiHole -> pure (multiIdMeta ())
  EMultiUser s -> do
    muls <- gets (snd . penvPrenex)
    case L.elemIndex s muls of
      Nothing -> checkError l $ "unregistered multi name " <> show s
      Just i -> pure (multiIdUser i)
