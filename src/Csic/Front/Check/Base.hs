module Csic.Front.Check.Base where
import Csic.Util.Prelude
import Csic.Core.Check
import Csic.Front.Syntax

-- ************************************************************************************************

getPrimExpr :: (CEnvState u m) => Loc -> Text -> m (Expr s)
getPrimExpr l s = getPrimExpr' l s mempty

getPrimExpr' :: (CEnvState u m) => Loc -> Text -> EPrenexArgs -> m (Expr s)
getPrimExpr' l s args = do
  s' <- getPrimName l s
  pure $ mkExpr l (EDeclRef s' args)

getPrimName :: (CEnvState u m) => Loc -> Text -> m DeclName
getPrimName l s = declName <$> getPrimDecl l s

getPrimDecl :: (CEnvState u m) => Loc -> Text -> m CDecl
getPrimDecl l s = liftE $ lookupCDeclPrimE l (Name s)

getPrimTerm :: (CEnvState u m) => Loc -> Text -> m (UTerm k)
getPrimTerm l s = liftE $ lookupUTermPrimE l (Name s)

-- ************************************************************************************************

lookupLDeclByName :: Loc -> Name -> CheckM LDecl
lookupLDeclByName loc s = get >>= \env -> lookupLDeclQ loc (QName (penvSpace env) s)

-- ************************************************************************************************

checkUDeclType :: Loc -> Name -> Access -> [Annotation] -> UTerm 0 -> CheckM PDecl
checkUDeclType l s acc anns ty = do
  env <- get
  let s' = QName (penvSpace env) s
  case lookupCDeclQ s' env of
    Just d -> ambiguous l s [declLoc d]
    Nothing -> do
      i <- updatePEnv $ allocDeclName s'
      checkDecl $ UDeclType $ Decl l i acc anns (penvPrenex env) ty (Const ())
      lookupPDecl l i

checkUDeclImpl :: Loc -> DeclName -> Impl (UTerm 0) -> CheckM ()
checkUDeclImpl l s x = checkDecl (UDeclImpl l s x)

-- ************************************************************************************************

type ECtx n = (Vector n Name, [UTerm n])

emptyECtx :: ECtx 0
emptyECtx = (vempty, [])

consECtx :: KnownNat n => Name -> ECtx n -> ECtx (1 + n)
consECtx s (ss, cs) = (vcons s ss, map shiftTerm cs)

consECtxConstraint :: KnownNat n => Name -> UTerm n -> ECtx n -> ECtx (1 + n)
consECtxConstraint s pt (ss, cs) = (vcons s ss, map shiftTerm (pt:cs))
