{-# LANGUAGE AllowAmbiguousTypes #-}
module Csic.Util.Finite
( Finite, getFinite, absurdFinite, unsafeFinite, finite, weaken, strengthen, shift, shiftS, unshift, separateSum, mirror
) where
import Csic.Util.Base
import Csic.Util.Nat

newtype Finite (n :: Nat) = Finite {getFinite :: Int}
  deriving newtype (Eq, Ord, Show, NFData, Hashable)

absurdFinite :: Finite 0 -> a
absurdFinite _ = error "absurdFinite"
{-# INLINE absurdFinite #-}

unsafeFinite :: Int -> Finite n
unsafeFinite = Finite
{-# INLINE unsafeFinite #-}

finite :: forall i n. (KnownNat i, i <= n) => Finite (1 + n)
finite = useLEQ @i @n $ Finite (natInt @i)
{-# INLINE finite #-}

weaken :: forall k n. Finite n -> Finite (n + k)
weaken = coerce
{-# INLINE weaken #-}

strengthen :: forall k n. (KnownNat n) => Finite (n + k) -> Maybe (Finite n)
strengthen (Finite x) = if x < natInt @n then Just (Finite x) else Nothing
{-# INLINE strengthen #-}

shift :: forall k n. (KnownNat k) => Finite n -> Finite (k + n)
shift (Finite n) = Finite (natInt @k + n)
{-# INLINE shift #-}

shiftS :: SNat k -> Finite n -> Finite (k + n)
shiftS k (Finite n) = Finite (fromIntegral (fromSNat k) + n)
{-# INLINE shiftS #-}

unshift :: forall k n. (KnownNat k) => Finite (k + n) -> Maybe (Finite n)
unshift = rightToMaybe . separateSum
{-# INLINE unshift #-}

separateSum :: forall n m. (KnownNat n) => Finite (n + m) -> Either (Finite n) (Finite m)
separateSum (Finite x) =
  let n = natInt @n in
  if x < n then Left (Finite x) else Right (Finite $ x - n)
{-# INLINE separateSum #-}

mirror :: forall n. (KnownNat n) => Finite n -> Finite n
mirror (Finite x) = Finite (natInt @n - x - 1)
{-# INLINE mirror #-}
