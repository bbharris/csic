module Csic.Util.Text
( T.Text, T.unpack, T.pack, unpackL, uncapitalize
, showPairS, showListS, intercalateS, showTable, showPretty, strPretty
) where
import Prelude
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Char as C
import Data.Foldable
import Text.Show (showListWith)
import Text.Pretty.Simple

unpackL :: TL.Text -> String
unpackL = TL.unpack
{-# INLINE unpackL #-}

uncapitalize :: T.Text -> T.Text
uncapitalize s = maybe "" (\(h, t) -> T.cons (C.toLower h) t) (T.uncons s)

showPairS :: (a -> ShowS) -> (b -> ShowS) -> (a, b) -> ShowS
showPairS fa fb (a, b) = showParen True (fa a . showString "," . fb b)

showListS :: (a -> ShowS) ->  [a] -> ShowS
showListS = showListWith

intercalateS :: String -> [ShowS] -> ShowS
intercalateS mid = \case
  [] -> id
  x:xs -> foldl' (\f y -> f . showString mid . y) x xs

showTable :: Show a => String -> [a] -> String
showTable pre vs = (showString pre . showString ":" . foldl' (\f v -> f . showString "\n  " . shows v) id vs) ""


showPretty :: Show a => a -> String
showPretty = unpackL . pShowOpt defaultOutputOptionsNoColor
  { outputOptionsInitialIndent = 0
  , outputOptionsIndentAmount = 2
  -- , outputOptionsCompact = True -- quite ugly
  }

strPretty :: String -> String
strPretty = unpackL . pStringOpt defaultOutputOptionsNoColor
  { outputOptionsInitialIndent = 0
  , outputOptionsIndentAmount = 2
  }
