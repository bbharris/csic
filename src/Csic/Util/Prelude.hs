module Csic.Util.Prelude
( module Csic.Util.Base
, module Csic.Util.Irrelevant
, module Csic.Util.Nat
, module Csic.Util.Finite
, module Csic.Util.Vector
, module Csic.Util.Text
, module Csic.Util.Name
, module Csic.Util.Loc
, module Csic.Util.Constraint
, module Csic.Util.Container
, Sized(..)
, AllocN(..), emptyAllocN, allocate1, allocateN
, fromMaybeM, fromRightM
, powerSet
, insertHMap2, alterHMap2
, insertWithLO, unionWithLO
, todo
, trace, traceShow, traceIf
) where

import Csic.Util.Base
import Csic.Util.Irrelevant
import Csic.Util.Nat
import Csic.Util.Finite
import Csic.Util.Vector
import Csic.Util.Text
import Csic.Util.Name
import Csic.Util.Loc
import Csic.Util.Constraint
import Csic.Util.Container
import qualified Debug.Trace as Debug
import GHC.Stack (HasCallStack)

-- | Wrap a Nat-indexed value with an arbitrary size.
data Sized f where Sized :: KnownNat n => f n -> Sized f

data AllocN a = AllocN {allocNext :: Int, allocV :: a}
  deriving (Eq, Ord, Generic, NFData, Hashable, Functor, Foldable, Traversable)

emptyAllocN :: Monoid a => AllocN a
emptyAllocN = AllocN 0 mempty
{-# inline emptyAllocN #-}

allocate1 :: (Int -> k) -> AllocN a -> (k, AllocN a)
allocate1 f (AllocN next x) = (f next, AllocN (next + 1) x)
{-# inline allocate1 #-}

allocateN :: (Int -> k) -> Int -> AllocN a -> ([k], AllocN a)
allocateN f n (AllocN next x) = (map (f . (+next)) [0..n-1], AllocN (next + n) x)
{-# inline allocateN #-}

fromMaybeM :: Applicative m => m a -> Maybe a -> m a
fromMaybeM def = maybe def pure
{-# inline fromMaybeM #-}

fromRightM :: Applicative m => (e -> m a) -> Either e a -> m a
fromRightM def = either def pure
{-# inline fromRightM #-}

powerSet :: [a] -> [[a]]
powerSet = foldr (\x y -> fmap (x:) y <> y) [[]]

insertHMap2 :: (Hashable k1, Hashable k2) =>
  k1 -> k2 -> v -> HashMap k1 (HashMap k2 v) -> HashMap k1 (HashMap k2 v)
insertHMap2 = alterHMap2 const

alterHMap2 :: (Hashable k1, Hashable k2) =>
  (v -> v -> v) -> k1 -> k2 -> v -> HashMap k1 (HashMap k2 v) -> HashMap k1 (HashMap k2 v)
alterHMap2 f k1 k2 v = alter (Just . maybe (singleton (k2, v)) (alter (Just . maybe v (f v)) k2)) k1

todo :: HasCallStack => a
todo = undefined
{-# WARNING todo "'todo' remains in code" #-}

trace :: String -> a -> a
trace = Debug.trace
{-# WARNING trace "'trace' remains in code" #-}

traceShow :: Show a => a -> b -> b
traceShow = Debug.traceShow
{-# WARNING traceShow "'traceShow' remains in code" #-}

traceIf :: Bool -> String -> a -> a
traceIf cond s = if cond then Debug.trace s else id

insertWithLO :: Ord k => (v -> v -> v) -> (k, v) -> [(k, v)] -> [(k, v)]
insertWithLO f (k, v) = \case
  [] -> [(k, v)]
  (k', v') : xs -> case compare k k' of
    LT -> (k, v) : (k', v') : xs
    EQ -> (k, f v v') : xs
    GT -> (k', v') : insertWithLO f (k, v) xs

unionWithLO :: Ord k => (v -> v -> v) -> [(k, v)] -> [(k, v)] -> [(k, v)]
unionWithLO f xs ys = case (xs, ys) of
  ([], _) -> ys
  (_, []) -> xs
  ((k1, v1):xs', (k2, v2):ys') -> case compare k1 k2 of
    LT -> (k1, v1) : unionWithLO f xs' ys
    GT -> (k2, v2) : unionWithLO f xs ys'
    EQ -> (k1, f v1 v2) : unionWithLO f xs' ys'