module Csic.Util.Vector
( Vector, vempty, vcons, vsingleton, vtoList, vfromList, vwithList
, vhead, vtail, vindex, vadjust, vappend, vreverseAppend
, vsplit, vindices, vfindIndex, vfilterIndices, vreverse, vzip, vunzip
) where
import Csic.Util.Base
import Csic.Util.Nat
import Csic.Util.Finite
import qualified Data.List as L

newtype Vector (n :: Nat) a = Vector [a]
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)
  deriving newtype (NFData)

vempty :: Vector 0 a
vempty = Vector []
{-# INLINE vempty #-}

vcons :: a -> Vector n a -> Vector (1 + n) a
vcons a (Vector vs) = Vector (a:vs)
{-# INLINE vcons #-}

vsingleton :: a -> Vector 1 a
vsingleton a = Vector [a]
{-# INLINE vsingleton #-}

vtoList :: Vector n a -> [a]
vtoList (Vector vs) = vs
{-# INLINE vtoList #-}

vfromList :: forall n a. KnownNat n => [a] -> Maybe (Vector n a)
vfromList vs =
  let n = natInt @n in
  if L.length vs >= n then Just (Vector vs) else Nothing
{-# INLINE vfromList #-}

vwithList :: [a] -> (forall n. KnownNat n => Vector n a -> b) -> b
vwithList vs f = withSNat (fromIntegral $ L.length vs) $ \(_ :: SNat n) -> f (Vector @n vs)
{-# INLINE vwithList #-}

vhead :: Vector (1 + n) a -> a
vhead (Vector vs) = L.head vs
{-# INLINE vhead #-}

vtail :: Vector (1 + n) a -> Vector n a
vtail (Vector vs) = Vector (L.tail vs)
{-# INLINE vtail #-}

vreverse :: Vector n a -> Vector n a
vreverse (Vector vs) = Vector (L.reverse vs)
{-# INLINE vreverse #-}

vzip :: Vector n a -> Vector n b -> Vector n (a, b)
vzip (Vector vs) (Vector vs') = Vector (L.zip vs vs')
{-# INLINE vzip #-}

vunzip :: Vector n (a, b) -> (Vector n a, Vector n b)
vunzip (Vector vs) = bimap Vector Vector $ L.unzip vs
{-# INLINE vunzip #-}

vindex :: Vector n a -> Finite n -> a
vindex (Vector vs) i = vs !! getFinite i
{-# INLINE vindex #-}

vadjust :: (a -> a) -> Finite n -> Vector n a -> Vector n a
vadjust f i (Vector vs) =
  let (pre, suf) = splitAt (getFinite i) vs in
  Vector $ pre ++ (f (L.head suf) : L.tail suf)
{-# INLINE vadjust #-}

vappend :: Vector n a -> Vector m a -> Vector (n + m) a
vappend (Vector as) (Vector bs) = Vector (as <> bs)
{-# INLINE vappend #-}

vreverseAppend :: Vector n a -> Vector m a -> Vector (n + m) a
vreverseAppend (Vector as) (Vector bs) = Vector (reverse as <> bs)
{-# INLINE vreverseAppend #-}

vsplit :: forall i k n a. (KnownNat i, n ~ i + k) => Vector n a -> (Vector i a, Vector k a)
vsplit (Vector vs) = useAdd @n @i @k $ bimap Vector Vector $ L.splitAt (natInt @i) vs
{-# INLINE vsplit #-}

vindices :: forall n. KnownNat n => Vector n (Finite n)
vindices = Vector $ unsafeFinite <$> [0 .. natInt @n - 1]
{-# INLINE vindices #-}

vfindIndex :: (a -> Bool) -> Vector n a -> Maybe (Finite n)
vfindIndex f (Vector vs) = unsafeFinite <$> L.findIndex f vs
{-# INLINE vfindIndex #-}

vfilterIndices :: (a -> Bool) -> Vector n a -> [Finite n]
vfilterIndices f (Vector vs) = catMaybes $ zipWith (\i x -> if f x then Just (unsafeFinite i) else Nothing) [0..] vs
{-# INLINE vfilterIndices #-}
