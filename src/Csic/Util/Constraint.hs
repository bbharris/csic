module Csic.Util.Constraint where
import Csic.Util.Base
import Csic.Util.Loc

data ConstraintRelation = CEqual | CConv
  deriving (Eq, Ord)

instance Show ConstraintRelation where
  show CEqual = "="
  show CConv = "⊆"

data Constraint a = Constraint (Located a) ConstraintRelation (Located a)
  deriving (Eq, Ord, Functor, Foldable, Traversable)

constraintErrorMsg :: (Show a) => Constraint a -> [(Loc, String)]
constraintErrorMsg c = constraintErrorMsg' c c

constraintErrorMsg' :: (Show a, Show b) => Constraint a -> Constraint b -> [(Loc, String)]
constraintErrorMsg' c@(Constraint m1 _ m2) c' =
  [ (getLoc m1, "LHS: " <> show c <> "  ==>  " <> show c')
  , (getLoc m2, "RHS: " <> show c <> "  ==>  " <> show c')
  ]

instance Show a => Show (Constraint a) where
  show (Constraint u1 rel u2) = show u1 <> " " <> show rel <> " " <> show u2