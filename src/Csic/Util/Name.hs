module Csic.Util.Name where
import Csic.Util.Base
import Csic.Util.Text
import Csic.Util.Container

-- ************************************************************************************************

newtype IName = IName Int
  deriving newtype (Eq, Ord, Show, NFData, Hashable)

-- ************************************************************************************************


data QName = QName {qspace :: Namespace, qname :: Name}
  deriving (Eq, Ord, Generic, NFData, Hashable)

instance Show QName where
  show = show . qname

newtype Namespace = Namespace {spaceStr :: Text}
  deriving newtype (Eq, Ord, NFData, Hashable)

instance Show Namespace where
  show (Namespace s) = unpack s

newtype Name = Name {unName :: Text}
  deriving newtype (Eq, Ord, NFData, Hashable)
instance Show Name where
  show (Name s) = let s' = unpack s in case s' of '\'':_ -> s' ++ "'"; _ -> s'

addNamePrefix :: Text -> Name -> Name
addNamePrefix a (Name b) = Name $ a <> b

addNameSuffix ::  Name -> Text -> Name
addNameSuffix (Name a) b = Name $ a <> b

-- ************************************************************************************************

-- | Metavariable identifier.
newtype MetaId = MetaId {metaId :: Int}
  deriving newtype (Eq, Ord, NFData, Hashable)

instance Show MetaId where
  show (MetaId m) = "$" ++ show m

class (Show m, Eq m, Ord m, Hashable m) => IsMetaId m where
  toMetaId :: m -> MetaId
  fromMetaId :: MetaId -> m

instance IsMetaId MetaId where
  toMetaId = id
  fromMetaId = id

instance IsMetaId Void where
  toMetaId = absurd
  fromMetaId = error "Void MetaId"

instance IsMetaId () where
  toMetaId () = MetaId 0
  fromMetaId (MetaId i) = if i == 0 then () else error "non-zero () MetaId"

-- ************************************************************************************************

-- | Global environment.
type GlobalEnv d = HashMap Namespace (LocalScope d)

-- | Local scope of named declarations.
type LocalScope d = HashMap Name d

lookupEnv :: QName -> GlobalEnv d -> Maybe d
lookupEnv s e = lookup (qspace s) e >>= lookup (qname s)
{-# INLINE lookupEnv #-}

insertEnv :: QName -> d -> GlobalEnv d -> GlobalEnv d
insertEnv s d = alter (Just . insert (qname s) d . fromMaybe mempty) (qspace s)
{-# INLINE insertEnv #-}

adjustEnv :: (d -> d) -> QName -> GlobalEnv d -> GlobalEnv d
adjustEnv f s = alter (Just . adjust f (qname s) . fromMaybe mempty) (qspace s)
{-# INLINE adjustEnv #-}
