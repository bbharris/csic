{-# OPTIONS_GHC -Wno-redundant-constraints #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
module Csic.Util.Nat where
import Csic.Util.Base

natInt :: forall n. KnownNat n => Int
natInt = fromIntegral (natVal (Proxy @n))
{-# INLINE natInt #-}

useAdd :: forall (n :: Nat) (i :: Nat) (k :: Nat) a. (n ~ i + k) => a -> a
useAdd = id
{-# INLINE useAdd #-}

useLEQ :: forall (i :: Nat) (n :: Nat) a. (i <= n) => a -> a
useLEQ = id
{-# INLINE useLEQ #-}

useLT :: forall (i :: Nat) (n :: Nat) a. (i < n) => a -> a
useLT = id
{-# INLINE useLT #-}

useLE :: forall (i :: Nat) (n :: Nat) a. (i <= n) => a -> a
useLE = id
{-# INLINE useLE #-}