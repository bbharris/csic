module Csic.Util.Irrelevant where
import Csic.Util.Base

-- | Equality-irrelevant wrapper.
newtype Irr a = Irr {relevant :: a}
  deriving (Functor, Foldable, Traversable) deriving newtype (NFData)

instance Eq (Irr a) where
  _ == _ = True
  {-# INLINE (==) #-}

instance Ord (Irr a) where
  compare _ _ = EQ
  {-# INLINE compare #-}

instance Hashable (Irr a) where
  hashWithSalt _ _ = 0
  {-# INLINE hashWithSalt #-}

instance Show a => Show (Irr a) where
  show x = show (relevant x)
  {-# INLINE show #-}

data IrrTagged a b = IrrTagged {irrTag :: Irr a, unTag :: b}
  deriving (Generic, NFData, Functor, Foldable, Traversable)

instance Eq b => Eq (IrrTagged a b) where
  a == b = unTag a == unTag b
  {-# INLINE (==) #-}

instance Ord b => Ord (IrrTagged a b) where
  compare a b = compare (unTag a) (unTag b)
  {-# INLINE compare #-}

instance Hashable b => Hashable (IrrTagged a b) where
  hashWithSalt salt = hashWithSalt salt . unTag
  {-# INLINE hashWithSalt #-}

instance (Show a) => Show (IrrTagged a b) where
  show = show . irrTag
  {-# INLINE show #-}