module Csic.Util.Parse where
import Csic.Util.Base
import Csic.Util.Text
import Text.Megaparsec
import Text.Pretty.Simple (pStringNoColor)
import qualified Data.Set as Set
import qualified Data.List.NonEmpty as NE

mapParseErrorBundle :: (Token s -> ShowS) -> (Int -> String -> a) -> ParseErrorBundle s e -> [a]
mapParseErrorBundle showToken f (ParseErrorBundle errs _) =
  map (\e -> f (errorOffset e) (showParseError showToken e "")) (NE.toList errs)

showParseError :: forall s e. (Token s -> ShowS) -> ParseError s e -> ShowS
showParseError showToken = \case
  TrivialError _ uM ps ->
    maybe id (\u -> showString "unexpected " . showItem u . showString "\n") uM .
    if null ps then id else
      showString (unpackL $ pStringNoColor ((showString "expecting one of " .
        showListS showItem (Set.toAscList ps)) ""))
  FancyError _ xs -> case Set.toAscList xs of
    [x] -> showFancy x
    xs' -> showListS showFancy xs'
  where
  showItem :: ErrorItem (Token s) -> ShowS
  showItem = \case
    Tokens ts -> showListS showToken (NE.toList ts)
    Label s -> showString $ NE.toList s
    EndOfInput -> showString "end of input"
  showFancy :: ErrorFancy e -> ShowS
  showFancy = \case
    ErrorFail s -> showString s
    _ -> showString "unknown parse error"
