{-# LANGUAGE TypeFamilies #-}
module Csic.Util.Container
( IsSet(..), S.Set, HS.HashSet, IntSet, ListSet
, IsMap(..), M.Map, HM.HashMap, IntMap, ListMap
) where
import Csic.Util.Base
import qualified Data.Map.Strict as M
import qualified Data.HashMap.Strict as HM
import qualified Data.IntMap.Strict as IM
import qualified Data.Set as S
import qualified Data.HashSet as HS
import qualified Data.IntSet as IS
import qualified Data.List as L
import qualified Data.List.Ordered as LO

-- ************************************************************************************************

class IsSet f where
  type Key f
  type Elem f
  size :: f -> Int
  member :: Key f -> f -> Bool
  insertS :: Elem f -> f -> f
  union :: f -> f -> f
  delete :: Key f -> f -> f
  singleton :: Elem f -> f
  fromList :: [Elem f] -> f
  toList :: f -> [Elem f]

class IsSet f => IsMap f where
  type Val f
  lookup :: Key f -> f -> Maybe (Val f)
  insert :: Key f -> Val f -> f -> f
  insertWith :: (Val f -> Val f -> Val f) -> Key f -> Val f -> f -> f
  unionWith :: (Val f -> Val f -> Val f) -> f -> f -> f
  alter :: (Maybe (Val f) -> Maybe (Val f)) -> Key f -> f -> f
  adjust :: (Val f -> Val f) -> Key f -> f -> f
  toKeys :: f -> [Key f]
  fromListWith :: (Val f -> Val f -> Val f) -> [Elem f] -> f

-- ************************************************************************************************

instance Ord k => IsSet (S.Set k) where
  type Key (S.Set k) = k
  type Elem (S.Set k) = k
  size = S.size
  {-# INLINE size #-}
  member = S.member
  {-# INLINE member #-}
  insertS = S.insert
  {-# INLINE insertS #-}
  union = S.union
  {-# INLINE union #-}
  delete = S.delete
  {-# INLINE delete #-}
  singleton = S.singleton
  {-# INLINE singleton #-}
  fromList = S.fromList
  {-# INLINE fromList #-}
  toList = S.toList
  {-# INLINE toList #-}

instance Hashable k => IsSet (HS.HashSet k) where
  type Key (HS.HashSet k) = k
  type Elem (HS.HashSet k) = k
  size = HS.size
  {-# INLINE size #-}
  member = HS.member
  {-# INLINE member #-}
  insertS = HS.insert
  {-# INLINE insertS #-}
  union = HS.union
  {-# INLINE union #-}
  delete = HS.delete
  {-# INLINE delete #-}
  singleton = HS.singleton
  {-# INLINE singleton #-}
  fromList = HS.fromList
  {-# INLINE fromList #-}
  toList = HS.toList
  {-# INLINE toList #-}

-- ************************************************************************************************

instance Ord k => IsSet (M.Map k v) where
  type Key (M.Map k v) = k
  type Elem (M.Map k v) = (k, v)
  size = M.size
  {-# INLINE size #-}
  member = M.member
  {-# INLINE member #-}
  insertS = uncurry M.insert
  {-# INLINE insertS #-}
  union = M.union
  {-# INLINE union #-}
  delete = M.delete
  {-# INLINE delete #-}
  singleton = uncurry M.singleton
  {-# INLINE singleton #-}
  fromList = M.fromList
  {-# INLINE fromList #-}
  toList = M.toList
  {-# INLINE toList #-}

instance Ord k => IsMap (M.Map k v) where
  type Val (M.Map k v) = v
  lookup = M.lookup
  {-# INLINE lookup #-}
  insert = M.insert
  {-# INLINE insert #-}
  insertWith = M.insertWith
  {-# INLINE insertWith #-}
  unionWith = M.unionWith
  {-# INLINE unionWith #-}
  alter = M.alter
  {-# INLINE alter #-}
  adjust = M.adjust
  {-# INLINE adjust #-}
  toKeys = M.keys
  {-# INLINE toKeys #-}
  fromListWith = M.fromListWith
  {-# INLINE fromListWith #-}

instance Hashable k => IsSet (HM.HashMap k v) where
  type Key (HM.HashMap k v) = k
  type Elem (HM.HashMap k v) = (k, v)
  size = HM.size
  {-# INLINE size #-}
  member = HM.member
  {-# INLINE member #-}
  insertS = uncurry HM.insert
  {-# INLINE insertS #-}
  union = HM.union
  {-# INLINE union #-}
  delete = HM.delete
  {-# INLINE delete #-}
  singleton = uncurry HM.singleton
  {-# INLINE singleton #-}
  fromList = HM.fromList
  {-# INLINE fromList #-}
  toList = HM.toList
  {-# INLINE toList #-}

instance Hashable k => IsMap (HM.HashMap k v) where
  type Val (HM.HashMap k v) = v
  lookup = HM.lookup
  {-# INLINE lookup #-}
  insert = HM.insert
  {-# INLINE insert #-}
  insertWith = HM.insertWith
  {-# INLINE insertWith #-}
  unionWith = HM.unionWith
  {-# INLINE unionWith #-}
  alter = HM.alter
  {-# INLINE alter #-}
  adjust = HM.adjust
  {-# INLINE adjust #-}
  toKeys = HM.keys
  {-# INLINE toKeys #-}
  fromListWith = HM.fromListWith
  {-# INLINE fromListWith #-}

-- ************************************************************************************************

newtype IntSet k = IntSet {intSet :: IS.IntSet}
  deriving newtype (Eq, Ord, NFData, Hashable, Semigroup, Monoid)

instance Coercible Int k => IsSet (IntSet k) where
  type Key (IntSet k) = k
  type Elem (IntSet k) = k
  size = IS.size . intSet
  {-# INLINE size #-}
  member k = IS.member (coerce k) . intSet
  {-# INLINE member #-}
  insertS k = IntSet . IS.insert (coerce k) . intSet
  {-# INLINE insertS #-}
  union a b = IntSet $ IS.union (intSet a) (intSet b)
  {-# INLINE union #-}
  delete k = IntSet . IS.delete (coerce k) . intSet
  {-# INLINE delete #-}
  singleton k = IntSet $ IS.singleton (coerce k)
  {-# INLINE singleton #-}
  fromList = IntSet . IS.fromList . coerce
  {-# INLINE fromList #-}
  toList = coerce . IS.toList . intSet
  {-# INLINE toList #-}

-- ************************************************************************************************

newtype IntMap k v = IntMap {intMap :: IM.IntMap v}
  deriving newtype (Eq, Ord, NFData, Hashable, Semigroup, Monoid) deriving (Functor, Foldable, Traversable)

instance Coercible Int k => IsSet (IntMap k v) where
  type Key (IntMap k v) = k
  type Elem (IntMap k v) = (k, v)
  size = IM.size . intMap
  {-# INLINE size #-}
  member k = IM.member (coerce k) . intMap
  {-# INLINE member #-}
  insertS (k, v) = IntMap . IM.insert (coerce k) v . intMap
  {-# INLINE insertS #-}
  union a b = IntMap $ IM.union (intMap a) (intMap b)
  {-# INLINE union #-}
  delete k = IntMap . IM.delete (coerce k) . intMap
  {-# INLINE delete #-}
  singleton (k, v) = IntMap $ IM.singleton (coerce k) v
  {-# INLINE singleton #-}
  fromList = IntMap . IM.fromList . coerce
  {-# INLINE fromList #-}
  toList = coerce . IM.toList . intMap
  {-# INLINE toList #-}

instance Coercible Int k  => IsMap (IntMap k v) where
  type Val (IntMap k v) = v
  lookup k = IM.lookup (coerce k) . intMap
  {-# INLINE lookup #-}
  insert k v = IntMap . IM.insert (coerce k) v . intMap
  {-# INLINE insert #-}
  insertWith f k v = IntMap . IM.insertWith f (coerce k) v . intMap
  {-# INLINE insertWith #-}
  unionWith f a b = IntMap $ IM.unionWith f (intMap a) (intMap b)
  {-# INLINE unionWith #-}
  alter f k = IntMap . IM.alter f (coerce k) . intMap
  {-# INLINE alter #-}
  adjust f k = IntMap . IM.adjust f (coerce k) . intMap
  {-# INLINE adjust #-}
  toKeys = coerce . IM.keys . intMap
  {-# INLINE toKeys #-}
  fromListWith f = IntMap . IM.fromListWith f . coerce
  {-# INLINE fromListWith #-}

-- ************************************************************************************************

newtype ListSet k = ListSet {listSet :: [k]}
  deriving newtype (Eq, Ord, NFData, Hashable, Monoid, Foldable)

instance Ord k => Semigroup (ListSet k) where
  (<>) = union

instance Ord k => IsSet (ListSet k) where
  type Key (ListSet k) = k
  type Elem (ListSet k) = k
  size = length . listSet
  {-# INLINE size #-}
  member k = LO.member k . listSet
  {-# INLINE member #-}
  insertS k = ListSet . LO.insertSet k . listSet
  {-# INLINE insertS #-}
  union a b = ListSet $ LO.union (listSet a) (listSet b)
  {-# INLINE union #-}
  delete k xs = ListSet $ LO.minus (listSet xs) [k]
  {-# INLINE delete #-}
  singleton = ListSet . L.singleton
  {-# INLINE singleton #-}
  fromList = ListSet . LO.nubSort
  {-# INLINE fromList #-}
  toList = listSet
  {-# INLINE toList #-}

-- ************************************************************************************************

newtype ListMap k v = ListMap {listMap :: [(k, v)]}
  deriving newtype (Eq, Ord, NFData, Hashable, Monoid) deriving (Functor, Foldable, Traversable)

instance Ord k => Semigroup (ListMap k v) where
  (<>) = union

instance Ord k => IsSet (ListMap k v) where
  type Key (ListMap k v) = k
  type Elem (ListMap k v) = (k, v)
  size = length . listMap
  {-# INLINE size #-}
  member k = isJust . lookup k
  {-# INLINE member #-}
  insertS (k, v) = ListMap . insertWithLO const k v . listMap
  {-# INLINE insertS #-}
  union a b = ListMap $ unionWithLO const (listMap a) (listMap b)
  {-# INLINE union #-}
  delete k = ListMap . L.filter ((k /=) . fst) . listMap
  {-# INLINE delete #-}
  singleton = ListMap . L.singleton
  {-# INLINE singleton #-}
  fromList = ListMap . LO.nubSortOn fst
  {-# INLINE fromList #-}
  toList = listMap
  {-# INLINE toList #-}

instance Ord k => IsMap (ListMap k v) where
  type Val (ListMap k v) = v
  lookup k = L.lookup k . listMap
  {-# INLINE lookup #-}
  insert k v = ListMap . insertWithLO const k v . listMap
  {-# INLINE insert #-}
  insertWith f k v = ListMap . insertWithLO f k v . listMap
  {-# INLINE insertWith #-}
  unionWith f a b = ListMap $ unionWithLO f (listMap a) (listMap b)
  {-# INLINE unionWith #-}
  alter f k xs = maybe (delete k xs) (\v -> insert k v xs) $ f $ lookup k xs
  {-# INLINE alter #-}
  adjust f = alter (fmap f)
  {-# INLINE adjust #-}
  toKeys = map fst . listMap
  {-# INLINE toKeys #-}
  fromListWith f = foldr (uncurry $ insertWith f) mempty
  {-# INLINE fromListWith #-}

insertWithLO :: Ord k => (v -> v -> v) -> k -> v -> [(k, v)] -> [(k, v)]
insertWithLO f k v = \case
  [] -> [(k, v)]
  (k', v') : xs -> case compare k k' of
    LT -> (k, v) : (k', v') : xs
    EQ -> (k, f v v') : xs
    GT -> (k', v') : insertWithLO f k v xs
{-# INLINABLE insertWithLO #-}

unionWithLO :: Ord k => (v -> v -> v) -> [(k, v)] -> [(k, v)] -> [(k, v)]
unionWithLO f xs ys = case (xs, ys) of
  ([], _) -> ys
  (_, []) -> xs
  ((k1, v1):xs', (k2, v2):ys') -> case compare k1 k2 of
    LT -> (k1, v1) : unionWithLO f xs' ys
    GT -> (k2, v2) : unionWithLO f xs ys'
    EQ -> (k1, f v1 v2) : unionWithLO f xs' ys'
{-# INLINABLE unionWithLO #-}