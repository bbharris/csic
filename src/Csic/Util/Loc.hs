module Csic.Util.Loc
( Loc(..), nullLoc, showSrcLocError
, HasLocation(..), Located(..), locate, nullLocated
) where
import Csic.Util.Base
import Csic.Util.Container
import Csic.Util.Irrelevant
import Csic.Util.Text
import Text.Megaparsec
import qualified Data.List.NonEmpty as NE

-- | Source code location.
data Loc = Loc
  { locFile   :: FilePath
  , locLine   :: Int
  , locColumn :: Int
  , locOffset :: Int
  , locLength :: Int
  } deriving (Eq, Ord, Generic, NFData)
instance Show Loc where show (Loc f l c _ _) = f ++ ":" ++ show l ++ ":" ++ show c

nullLoc :: Loc
nullLoc = Loc "" 0 0 0 0

class HasLocation t where
  getLoc :: t -> Loc

data Located a = Located {irrLoc :: Irr Loc, unLoc :: a}
  deriving (Eq, Ord, Functor, Foldable, Traversable, Generic, NFData)

instance Hashable a => Hashable (Located a) where
  hashWithSalt salt = hashWithSalt salt . unLoc
  {-# INLINE hashWithSalt #-}

instance (Show a) => Show (Located a) where
  show = show . unLoc
  {-# INLINE show #-}

instance HasLocation (Located a) where
  getLoc = relevant . irrLoc
  {-# INLINE getLoc #-}

locate :: Loc -> a -> Located a
locate = Located . Irr
{-# INLINE locate #-}

nullLocated :: a -> Located a
nullLocated = locate nullLoc

showSrcLocError :: HashMap FilePath Text -> Loc -> String -> Maybe String
showSrcLocError srcMap l s = do
  src <- lookup (locFile l) srcMap
  let errs = FancyError (locOffset l) (fromList [ErrorCustom $ ErrLocStr l s]) NE.:| []
  let pstate = PosState src 0 (SourcePos (locFile l) (mkPos 1) (mkPos 1)) (mkPos 2) ""
  pure $ errorBundlePretty (ParseErrorBundle errs pstate)

data ErrLocStr = ErrLocStr Loc String
  deriving (Eq, Ord)

instance ShowErrorComponent ErrLocStr where
  showErrorComponent (ErrLocStr _ s) = s
  errorComponentLen (ErrLocStr l _) = locLength l