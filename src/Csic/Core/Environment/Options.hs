module Csic.Core.Environment.Options where
import Csic.Util.Prelude

-- ************************************************************************************************

data CoreOptions = CoreOptions
  { evalRuntime   :: Bool     -- ^ Enable runtime evaluation features such as general recursion (infinite loops).
  , evalBackend   :: Bool
  , levelCheckOff :: Bool     -- ^ Disable level checking and assume (Type : Type).
  , multiCheckOff :: Bool     -- ^ Disable multi checking and assume (x : %ω).
  , traceLevels   :: Bool     -- ^ Debug trace level checking.
  , traceMultis   :: Bool     -- ^ Debug trace multi checking.
  , traceMetas    :: Bool     -- ^ Debug trace all declarations after meta-checking.
  , traceOkays    :: Bool     -- ^ Debug trace all declarations after type-checking.
  , coreFuelMax   :: Int      -- ^ Amount of iterations before assuming an infinite loop.
  , printOpts :: PrintOptions -- ^ Pretty-print options.
  } deriving (Generic, NFData)

coreOptionsDefault :: CoreOptions
coreOptionsDefault = CoreOptions
  { evalRuntime = False
  , evalBackend = False
  , levelCheckOff = False
  , multiCheckOff = False
  , traceLevels = False
  , traceMultis = False
  , traceMetas = False
  , traceOkays = False
  , coreFuelMax = 10000
  , printOpts = printOptionsDefault
  }

enableTraceAll :: CoreOptions -> CoreOptions
enableTraceAll x = x {traceMetas = True, traceMultis = True, traceLevels = True, traceOkays = True}

-- | Number of steps allowed before assuming an infinite loop.
type LoopFuel = Int

-- ************************************************************************************************

data PrintOptions = PrintOptions
  { showLocStr       :: Loc -> String -> Maybe String
  , showIndent       :: Int
  , showColor        :: Bool
  , showLocations    :: Bool
  , showImplicits    :: Bool
  , showLevels       :: Bool
  , showMultis       :: Bool
  , showErrorDetails :: Bool
  } deriving (Generic, NFData)

printOptionsDefault :: PrintOptions
printOptionsDefault = PrintOptions
  { showLocStr = \_ _ -> Nothing
  , showIndent = 0
  , showColor = False
  , showLocations = False
  , showImplicits = False
  , showLevels = False
  , showMultis = False
  , showErrorDetails = False
  }

printOptionsVerbose :: PrintOptions
printOptionsVerbose = PrintOptions
  { showLocStr = \_ _ -> Nothing
  , showIndent = 0
  , showColor = True
  , showLocations = True
  , showImplicits = True
  , showLevels = True
  , showMultis = True
  , showErrorDetails = True
  }