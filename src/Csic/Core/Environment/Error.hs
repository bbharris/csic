module Csic.Core.Environment.Error where
import Csic.Util.Prelude
import Csic.Core.Context

type CheckErrorC = MonadError [CheckError]
type CheckResult = Either [CheckError]

-- | Type-checking error.
data CheckError
  = CErrTree Loc String [CheckError]
  | CErrTerm String ErrTerm
  | CErrDecls String ErrDecls
  | CErrUDecls String [UDecl]

-- | Error term at some arbitrary context depth.
data ErrTerm where
  ErrTerm :: (IsMetaId m, KnownNat n) => PCtx n -> Term m n -> ErrTerm

-- | Declarations with any metavariable environment.
data ErrDecls where
  ErrDecls :: IsMetaId m => [DeclF m] -> ErrDecls

instance NFData CheckError where
  rnf = rwhnf

strErr :: Loc -> String -> CheckError
strErr l s = CErrTree l s []
{-# INLINE strErr #-}

-- | Throw a type-checking error.
checkError :: CheckErrorC f => Loc -> String -> f a
checkError l s = throwError [strErr l s]
{-# INLINE checkError #-}

-- | Throw a type-checking error with sub-errors.
checkErrorN :: CheckErrorC f => Loc -> String -> [CheckError] -> f a
checkErrorN l s xs = throwError [CErrTree l s xs]
{-# INLINE checkErrorN #-}

-- | Wrap a computation with a declaration error context.
withErrDecls :: (CheckErrorC f, IsMetaId m) => String -> [DeclF m] -> f a -> f a
withErrDecls s ds = withError (CErrDecls s (ErrDecls ds):)

ctermError :: String -> CTerm 0 -> CheckError
ctermError s t = CErrTerm s $ ErrTerm emptyPCtx t

-- ************************************************************************************************

-- | Verify prenex arguments match a declaration.
verifyPrenexArgs :: (CheckErrorC f, IsMetaId m) => Loc -> LDecl -> PrenexArgs m -> f ()
verifyPrenexArgs l dL (lvls, muls) = do
  let (lvlsE, mulsE) = ldecl dL declPrenex
  when (isLeft dL) $ do
    forM_ (zip [0::Int ..] lvls) $ \(i, v) -> unless (v == levelVar 0 (levelIdUser i)) $
      checkError l $ "invalid local levels " <> show lvls
    forM_ (zip [0::Int ..] muls) $ \(i, v) -> unless (v == multiVar (multiIdUser i)) $
      checkError l $ "invalid local multis " <> show muls
  unless (length lvls == length lvlsE) $
    checkError l $ "invalid " <> show (qname $ ldecl dL declNameQ) <> " level args, got " <>
      show (length lvls) <> ", expected " <> show lvlsE
  unless (length muls == length mulsE) $
    checkError l $ "invalid " <> show (qname $ ldecl dL declNameQ) <> " multi args, got " <>
      show (length muls) <> ", expected " <> show mulsE
{-# INLINABLE verifyPrenexArgs #-}
