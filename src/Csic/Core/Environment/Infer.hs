module Csic.Core.Environment.Infer (inferTermType) where
import Csic.Util.Prelude
import Csic.Core.Context
import Csic.Core.Environment.Error
import Csic.Core.Environment.Checked
import Csic.Core.Environment.Evaluate

inferTermType :: (HasMetaEnv m env, KnownNat n) =>
  TCtx' z m n -> Term m n -> env -> CheckResult (Term m n)
inferTermType ctx t env = case unT t of
  Var _ i -> pure $ snd $ indexTCtx i ctx
  App _ f _ r -> do
    ft <- inferTermType ctx f env
    pit <- fromMaybeM (checkError l "expected application pi-type") (isPiType $ eval ft)
    pure $ substScope (vsingleton r) (bindResult pit)
  Lam (Irr z) pt m b -> mkPit (Irr z) pt m <$> inferTermType (consTCtx z Nothing pt ctx) b env
  Pit _ _ (u, _) _ -> pure $ mkTypeUniv l u
  Cas _ _ _ ty _ -> pure ty
  Con _ c -> case c of
    TypeUniv u -> pure $ mkTypeUniv l (levelPlus 1 u)
    DeclRef s pargs -> shiftTerm . declType <$> instantiateDecl l s pargs env
    Literal v -> lookupTermPrimE l (Name $ literalTypeName v) mempty env
  Met _ m rs -> instantiateMetaVarType l m rs env
  where
  l = getLoc t
  eval = evalToWHNF (initFuel env) env (pCtx ctx)
{-# INLINABLE inferTermType #-}
