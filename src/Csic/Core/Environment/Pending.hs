module Csic.Core.Environment.Pending
( CheckM, PEnv, PEnvResults(..), initPEnv, finishPEnv, penvSpace, penvPrenex
, liftPE, updatePEnv, deferTypeCheck, evalM, consTCtxM, isTypeClassM
, lookupLDeclQ, lookupPDecl, lookupLDecl, instantiateLDecl, registerPDecl
, MetaVarN, MetaVar(..), getMetaVar
, updateMetaVarSubst, deferConstraint, substMetas, findMetaVarRec, metaVarsIn, metaVarArgMap
, genTermMetaVar, genMetaLevelId, genMetaMultiId
, ImplicitPiInfo, implicitPiType
, DiscInfo(..), dataTypeDiscInfo, ctorDataTypeInfo
, withFuelCheck, withConstraintError, constraintError, depsConstraintError, metaVarError, ptermError, ptermErrorM
) where
import Csic.Util.Prelude
import Csic.Core.Context
import Csic.Core.Environment.Error
import Csic.Core.Environment.Checked
import Csic.Core.Environment.Evaluate
import Csic.Core.Environment.Constraint
import Data.List (sortOn)

-- ************************************************************************************************

-- | Type-checking monad.
type CheckM = StateT PEnv CheckResult

-- | Pending type-checking environment.
data PEnv = PEnv
  { penvSpace   :: Namespace
  , penvCEnv    :: CEnv
  , penvDecls   :: HashMap IName PDecl
  , penvNames   :: HashMap QName DeclName
  , penvMetas   :: AllocN (HashMap MetaId MetaVarN) -- ^ Map term metavariables to their substitution states.
  , penvLevels  :: AllocN [Loc]                     -- ^ Universe level meta ids.
  , penvMultis  :: AllocN [Loc]                     -- ^ Multiplicity meta ids.
  , penvCons    :: AllocN (IntMap ConstraintId DepsConstraint)
  , penvConDeps :: IntMap MetaId [ConstraintId]
  , penvLevelC  :: [LevelConstraint MetaId]
  , penvMultiC  :: [MultiConstraint MetaId]
  , penvDefer   :: [CheckM ()]
  , penvPrenex  :: PrenexParams
  , penvFuel    :: Fuel
  }

-- | Final type-checking results for a group of mutual declarations.
data PEnvResults = PEnvResults
  { presDecls         :: [PDecl]
  , presTermCons      :: [DepsConstraint]
  , presTermMetas     :: [MetaVarN]
  , presLevelCons     :: [LevelConstraint MetaId]
  , presLevelMetaLocs :: AllocN [Loc]
  , presMultiCons     :: [MultiConstraint MetaId]
  , presMultiMetaLocs :: AllocN [Loc]
  , presDefer         :: [CheckM ()]
  }

instance HasCEnv PEnv where
  getCEnv = penvCEnv
  {-# INLINE getCEnv #-}
  mapCEnv f e = e {penvCEnv =  f (penvCEnv e)}
  {-# INLINE mapCEnv #-}

instance HasMetaEnv MetaId PEnv where
  instantiateDecl l s pargs e = lookupLDecl' l s e >>= \d -> instantiateLDecl l d pargs
  instantiateMetaVarBody = instantiateMetaVarBody'
  {-# INLINE instantiateMetaVarBody #-}
  instantiateMetaVarType = instantiateMetaVarType'
  {-# INLINE instantiateMetaVarType #-}

-- ************************************************************************************************

-- | Initialize a fresh pending environment.
initPEnv :: Namespace -> PrenexParams -> CEnv -> PEnv
initPEnv ns prenex e =
  PEnv ns e mempty mempty emptyAllocN
    emptyAllocN emptyAllocN emptyAllocN mempty mempty mempty mempty prenex (initFuel e)

-- | Collect final pending environment results.
finishPEnv :: PEnv -> PEnvResults
finishPEnv e = PEnvResults
  { presDecls         = map snd $ sortOn fst $ toList $ penvDecls e
  , presTermCons      = map snd $ sortOn fst $ toList $ allocV (penvCons e)
  , presTermMetas     = unsolved
  , presLevelCons     = penvLevelC e
  , presLevelMetaLocs = penvLevels e
  , presMultiCons     = penvMultiC e
  , presMultiMetaLocs = penvMultis e
  , presDefer         = reverse $ penvDefer e
  }
  where
  allMetas = toList $ allocV $ penvMetas e
  unsolved = mapMaybe (\(_, Sized v) -> maybe (Just (Sized v)) (const Nothing) (metaBody v)) allMetas

-- ************************************************************************************************

-- | Lift an environment error computation into a environment state computation.
liftPE :: (PEnv -> CheckResult a) -> CheckM a
liftPE f = gets f >>= liftEither
{-# INLINE liftPE #-}

-- | Update pending type-check environment.
updatePEnv :: (PEnv -> (a, PEnv)) -> CheckM a
updatePEnv f = get >>= \env -> let (a, env') = f env in put env' $> a
{-# INLINE updatePEnv #-}

-- | Defer a type-checking computation to a later pass (with independent environment).
deferTypeCheck :: CheckM () -> CheckM ()
deferTypeCheck x = modify $ \env -> env {penvDefer = x:penvDefer env}
{-# INLINE deferTypeCheck #-}

-- | Evaluate a term to WHNF in the current environment.
evalM :: (KnownNat n) => PCtx n -> PTerm n -> CheckM (WHNF MetaId n)
evalM ctx x = gets (\env -> evalToWHNF (initFuel env) env ctx x)
{-# INLINE evalM #-}

-- | Bind a parameter into a type context.
consTCtxM :: KnownNat n => ParamInfo -> PTerm n -> TCtx n -> CheckM (TCtx (1 + n))
consTCtxM z pt ctx = do
  -- Check if the parameter type is a class to cache instance lookup info in the context.
  tcM <- fmap (declName . fst . snd) <$> isTypeClassM (pCtx ctx) pt
  pure $ consTCtx z tcM pt ctx

-- | Check if a term is a type class.
isTypeClassM :: (KnownNat n) => PCtx n -> PTerm n -> CheckM (Maybe (TypeClassInfo, (CDecl, Args MetaId n)))
isTypeClassM ctx t = gets $ \env -> isDecl (evalToWHNF (initFuel env) env ctx t) env >>= isTypeClass
{-# INLINE isTypeClassM #-}

-- ************************************************************************************************

-- | Lookup a declaration name.
lookupPDecl :: Loc -> DeclName -> CheckM PDecl
lookupPDecl loc s = get >>= \env -> case lookup (unTag s) (penvDecls env) of
  Just d -> pure d
  Nothing -> checkError loc $ "invalid local reference " <> show s
{-# INLINE lookupPDecl #-}

-- | Lookup a declaration name.
lookupLDecl :: Loc -> DeclName -> CheckM LDecl
lookupLDecl l s = liftPE (lookupLDecl' l s)
{-# INLINE lookupLDecl #-}

-- | Lookup a declaration name.
lookupLDecl' :: Loc -> DeclName -> PEnv -> CheckResult LDecl
lookupLDecl' l s env = case lookup (unTag s) (penvDecls env) of
  Just d -> pure (Left d)
  Nothing -> Right <$> lookupCDeclE l s env
{-# INLINE lookupLDecl' #-}

-- | Lookup a declaration name.
lookupLDeclQ :: Loc -> QName -> CheckM LDecl
lookupLDeclQ loc s = get >>= \env -> case lookup s (penvNames env) of
  Just i -> lookupLDecl loc i
  Nothing -> case lookupCDeclQ s env of
    Just d -> pure $ Right d
    Nothing -> checkError loc $ "invalid local name " <> show s
{-# INLINE lookupLDeclQ #-}

-- | Instantiate a local declaration.
instantiateLDecl :: Loc -> LDecl -> PrenexArgs MetaId -> CheckResult PDecl
instantiateLDecl l dL pargs = do
  verifyPrenexArgs l dL pargs
  pure $ either id (fmap $ instantiateTerm pargs) dL
{-# INLINE instantiateLDecl #-}

-- | Register a pending declaration.
registerPDecl :: PDecl -> CheckM ()
registerPDecl d = modify $ \env -> env
  { penvDecls = insert (unTag $ declName d) d (penvDecls env)
  , penvNames = insert (declNameQ d) (declName d) (penvNames env)
  }
{-# INLINE registerPDecl #-}

-- ************************************************************************************************

-- | Internal constraint identifier.
newtype ConstraintId = ConstraintId Int
  deriving newtype (Eq, Ord, Show, NFData, Hashable)

-- | Metavariable of any depth.
type MetaVarN = Sized MetaVar

-- | State of a metavariable.
data MetaVar n = MetaVar
  { metaCtx  :: TCtx n
  , metaLoc  :: Loc
  , metaName :: MetaId
  , metaBody :: Maybe (PTerm n)
  , metaType :: PTerm n
  }

-- | Lookup a metavariable.
lookupMetaVar :: MetaId -> PEnv -> Maybe MetaVarN
lookupMetaVar k env = lookup k (allocV $ penvMetas env)
{-# INLINE lookupMetaVar #-}

-- | Instantiate a metavariable body, if it has one.
instantiateMetaVarBody' :: KnownNat n => Loc -> MetaId -> [PTerm n] -> PEnv -> Maybe (PTerm n)
instantiateMetaVarBody' _ k args env = case lookupMetaVar k env of
  Just (Sized (v :: MetaVar k)) -> metaBody v >>= \b -> case vfromList @k args of
    Just args' -> Just $ substScope args' (weakenTermTop b)
    _ -> Nothing
  _ -> Nothing

-- | Instantiate a metavariable type.
instantiateMetaVarType' :: KnownNat n => Loc -> MetaId -> [PTerm n] -> PEnv -> CheckResult (PTerm n)
instantiateMetaVarType' l k args env = case lookupMetaVar k env of
  Just (Sized (v :: MetaVar k)) -> case vfromList @k args of
    Just args' -> pure $ substScope args' (weakenTermTop (metaType v))
    _ -> checkError l "invalid instantiateMetaType args"
  _ -> checkError l "invalid instantiateMetaType key"

-- ************************************************************************************************

-- | Lookup a metavariable.
getMetaVar :: Loc -> MetaId -> CheckM MetaVarN
getMetaVar l k = gets (lookupMetaVar k) >>= fromMaybeM (checkError l $ "invalid metavariable state " <> show k)
{-# INLINE getMetaVar #-}

-- | Update a metavariable with a substitution, returning unblocked constraints.
updateMetaVarSubst :: KnownNat n => MetaVar n -> CheckM [UConstraint]
updateMetaVarSubst v = updatePEnv $ \env -> do
  let cks = fromMaybe [] $ lookup (metaName v) (penvConDeps env)
  let cs = mapMaybe (`lookup` allocV (penvCons env)) cks
  let cons' = foldr (fmap . delete) (penvCons env) cks
  (map snd cs, env {penvCons = cons', penvMetas = insert (metaName v) (Sized v) <$> penvMetas env})

-- | Defer a constraint to be solved when any of a set of metavariables are solved.
deferConstraint :: MetaVarSet -> UConstraint -> CheckM ()
deferConstraint deps c = modify $ \env -> case c of
  CLevel x -> env {penvLevelC = x : penvLevelC env}
  CMulti x -> env {penvMultiC = x : penvMultiC env}
  _ -> env {penvCons = insert ck (deps, c) <$> cons', penvConDeps = deps'}
    where
    (ck, cons') = allocate1 ConstraintId (penvCons env)
    deps' = foldr (\mk -> insertWith (<>) mk [ck]) (penvConDeps env) (toList deps)

-- | Substitute all resolved metavariables into a term.
substMetas :: KnownNat n => PEnv -> PTerm n -> PTerm n
substMetas env x = case unT x of
  Met (Irr l) k rs | Just t' <- instantiateMetaVarBody' l k rs env -> substMetas env t'
  _ -> fmapSubTerms id id (substMetas env) x

-- | Determine if a term contains a metavariable that satisfies a predicate.
findMetaVarRec :: PEnv -> (MetaId -> Bool) -> PTerm n -> Bool
findMetaVarRec env f = findMetaVar $ \k -> case lookupMetaVar k env of
  Nothing -> f k
  Just (Sized mv) -> maybe (f k) (findMetaVarRec env f) (metaBody mv)

-- | Find metavars in a term.
metaVarsIn :: PEnv -> MetaVarSet -> PTerm n -> MetaVarSet
metaVarsIn env = frec
  where
  frec :: MetaVarSet -> PTerm k -> MetaVarSet
  frec res t = foldSubTerms (flip frec) direct t
    where
    direct = case unT t of
      Met _ k _ -> case lookupMetaVar k env of
        Nothing -> insertS k res
        Just (Sized mv) -> maybe (insertS k res) (frec res) (metaBody mv)
      _ -> res

-- | Constrauct a substitution map for a closed metavariable's parameter arguments.
metaVarArgMap :: forall p n. (KnownNat n, KnownNat p) => PCtx n -> [PTerm n] -> CheckM (FiniteMap n p)
metaVarArgMap ctx args = gets $ \env -> foldr (fvar env) mempty $ zip args (vtoList $ vindices @p)
  where
  fvar env (arg, j) res = case evalToWHNF (initFuel env) env ctx arg of
    (Var _ i, []) -> insert i j res
    _ -> res

-- ************************************************************************************************

{- | Generate a fresh term metavariable and add it to the environment.
     Also generates explicit arguments to close the metavariable term.
-}
genTermMetaVar :: KnownNat n => TCtx n -> Loc -> PTerm n -> CheckM (MetaId, PTerm n)
genTermMetaVar ctx l ty = updatePEnv $ \env -> do
  let (k, xs) = allocate1 MetaId (penvMetas env)
  let v = Sized $ MetaVar ctx l k Nothing ty
  let vars = map (mkVar (Irr l)) (vtoList vindices)
  let t = mkMet (Irr l) k vars
  ((k, t), env {penvMetas = insert k v <$> xs})

-- | Generate a fresh level metavariable and add it to the environment.
genMetaLevelId :: Loc -> CheckM PLevelId
genMetaLevelId l = updatePEnv $ \env -> do
  let (k, xs) = allocate1 (levelIdMeta . MetaId) (penvLevels env)
  (k, env {penvLevels = (l:) <$> xs})

-- | Generate a fresh multi metavariable and add it to the environment.
genMetaMultiId :: Loc -> CheckM PMultiId
genMetaMultiId l = updatePEnv $ \env -> do
  let (k, xs) = allocate1 (multiIdMeta . MetaId) (penvMultis env)
  (k, env {penvMultis = (l:) <$> xs})

-- ************************************************************************************************

type ImplicitPiInfo n = Maybe (Maybe (Binder MetaId n))

-- | Check if the first parameter of a type is implicit.
implicitPiType :: KnownNat n => PCtx n -> PTerm n -> CheckM (ImplicitPiInfo n)
implicitPiType ctx x = gets $ \env -> case evalToWHNF (initFuel env) env ctx x of
  (Met {}, []) -> Nothing
  (Pit (Irr z) p m r, []) -> Just $ if isJust (paramImplicit z) then Just (Binder z p m r) else Nothing
  _ -> Just Nothing


-- ************************************************************************************************

-- | Pattern-match discriminator info.
data DiscInfo n = DiscInfo
  { discDataInfo   :: DataTypeInfo
  , discDataPrenex :: PrenexArgs MetaId
  , discDataArgs   :: Args MetaId n
  }

-- | Lookup discriminator info.
dataTypeDiscInfo :: KnownNat n => TCtx n -> Loc -> PTerm n -> CheckM (DiscInfo n)
dataTypeDiscInfo ctx l x = evalM (pCtx ctx) x >>= \case
  (Con _ (DeclRef s pargs), args) -> do
    dL <- lookupLDecl l s
    ldecl dL $ \d -> case declImpl d of
      IData dti -> pure $ DiscInfo dti pargs args
      _ -> checkError l "expected applied data type"
  _ -> checkError l "expected applied data type"

-- | Lookup a contructor's data type info.
ctorDataTypeInfo :: Loc -> LDecl -> CheckM (LDecl, DataTypeInfo)
ctorDataTypeInfo l cL = ldecl cL $ \c -> case declImpl c of
  ICtor y -> do
    dL <- lookupLDecl l (ctorDataTypeName y)
    ldecl dL $ \d -> case declImpl d of
      IData dti -> pure (dL, dti)
      _ -> checkError l $ "expected data type reference " <> show (declNameQ c)
  _ -> checkError l $ "expected constructor " <> show (declNameQ c)

-- ************************************************************************************************

-- | Consume infinite loop detection fuel and restore previous fuel after a computation finishes.
withFuelCheck :: (PEnv -> [CheckError]) -> CheckM a -> CheckM a
withFuelCheck ferrs fgo = do
  -- Check we have enough fuel to proceed.
  oldFuel <- get >>= \env -> do
    consumeFuel (gets ferrs >>= throwError) (penvFuel env) $ \fuel -> put $ env {penvFuel = fuel}
    pure (penvFuel env)
  -- Perform action.
  res <- fgo
  -- Restore previous fuel.
  modify $ \env -> env {penvFuel = oldFuel}
  pure res
{-# INLINE withFuelCheck #-}

-- | Wrap errors with a pending constraint annotations.
withConstraintError :: UConstraint -> CheckM a -> CheckM a
withConstraintError c = handleError $ \errs -> do
  e <- gets (\env -> constraintError env "while solving constraint" c)
  throwError $ e : errs
{-# INLINE withConstraintError #-}

-- | Construct a constraint error.
constraintError :: PEnv -> String -> UConstraint -> CheckError
constraintError env msg c = CErrTree nullLoc msg $ case c of
  CStuck (Sized (Stuck ctx t1 _rel t2)) ->
    [ ptermError env ctx "LHS-stuck" t1
    , ptermError env ctx "RHS-stuck" t2
    ]
  CFlexRigid (Sized (FlexRigid ctx f1 _rel r2)) ->
    [ ptermError env ctx "LHS-flex" (unflex f1)
    , ptermError env ctx "RHS-rigid" r2
    ]
  CInstance (Sized (InstanceConstraint ctx l k _ _ ty)) ->
    [ ptermError env (pCtx ctx) "Instance-Term" (mkMet (Irr l) k [])
    , ptermError env (pCtx ctx) "Instance-Type" ty
    ]
  CImplicit (Sized (ImplicitConstraint ctx _ x actType expType)) ->
    [ ptermError env (pCtx ctx) "Implicit-Term" x
    , ptermError env (pCtx ctx) "Implicit-Type-Actual" actType
    , ptermError env (pCtx ctx) "Implicit-Type-Expect" expType
    ]
  CLevel x -> [strErr nullLoc ("CLevel " <> show x)]
  CMulti x -> [strErr nullLoc ("CMulti " <> show x)]

-- | Construct a dependent constraint error.
depsConstraintError :: PEnv -> String -> DepsConstraint -> CheckError
depsConstraintError env prefix (deps, c) = constraintError env (prefix <> " " <> show (toList deps)) c

-- | Construct a metavariable error.
metaVarError :: PEnv -> String -> MetaVarN -> CheckError
metaVarError env prefix (Sized (MetaVar ctx l k _ ty)) = CErrTree l (prefix <> " " <> show k)
  [ptermError env (pCtx ctx) "with type" ty]

-- | Construct a term error, substituting metavariables for better error messages.
ptermError :: KnownNat n => PEnv -> PCtx n -> String -> PTerm n -> CheckError
ptermError env ctx s t = CErrTerm s $ ErrTerm ctx (substMetas env t)

-- | Construct a term error, substituting metavariables for better error messages.
ptermErrorM :: KnownNat n => PCtx n -> String -> PTerm n -> CheckM CheckError
ptermErrorM ctx s t = gets $ \env -> CErrTerm s $ ErrTerm ctx (substMetas env t)
