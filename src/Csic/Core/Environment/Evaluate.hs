module Csic.Core.Environment.Evaluate
( evalToWHNF, evalToNF, evalToCtor
) where
import Csic.Util.Prelude
import Csic.Core.Context
import Csic.Core.Environment.Options
import Csic.Core.Environment.Error
import Csic.Core.Environment.Pretty
import Csic.Core.Environment.Checked
import Control.Applicative ((<|>))

-- ************************************************************************************************

type Eval m e n r = (HasMetaEnv m e, KnownNat n) => Fuel -> e -> PCtx n -> r

-- | Evaluate a term to weak head normal form.
evalToWHNF :: Eval m e n (Term m n -> WHNF m n)
evalToWHNF !fuel env ctx t = evalWithArgs fuel env ctx t []
{-# INLINABLE evalToWHNF #-}

-- | Evaluate a term applied to a list of arguments.
evalWithArgs :: Eval m e n (Term m n -> Args m n -> WHNF m n)
evalWithArgs !fuel0 env ctx t args = fcheck $ \ !fuel -> fromMaybe (unT t, args) $ case unT t of
  Var {} -> Nothing
  App z f m r -> pure $ evalWithArgs fuel env ctx f (Arg (relevant z) m r : args)
  Lam _ _ _ b -> case args of
    [] -> Nothing
    arg:args' -> pure $ evalWithArgs fuel env ctx (substScope (vsingleton $ argTerm arg) b) args'
  Pit {} -> Nothing
  Cas _ d _ _ bs -> matchBranches fuel env ctx (getLoc t) bs d args
  Con (Irr l) c -> case c of
    DeclRef s pargs -> do
      d <- fmap (instantiateTerm pargs) <$> lookupCDecl s env
      let nameArgs = (unName $ qname $ declNameQ d, args)
      case declImpl d of
        IAxiom -> if evalBackend (getOpts env) then Nothing else evalBuiltin fuel env ctx l nameArgs
        IDef y -> if canUnfoldDefinition fuel env ctx d y args
          then (if False then evalOptimized fuel env ctx l nameArgs else Nothing)
               <|> pure (evalWithArgs fuel env ctx (shiftTerm $ defImpl y) args)
          else Nothing
        IData {} -> Nothing
        ICtor {} -> packPrimCtor fuel env ctx l nameArgs
    _ -> Nothing
  Met (Irr l) m rs -> do
    x <- instantiateMetaVarBody l m rs env
    pure $ evalWithArgs fuel env ctx x args
  where
  fcheck = consumeFuel (error $ "OUT OF FUEL: eval " <> show (getLoc t)) fuel0
{-# INLINABLE evalWithArgs #-}

-- | Check if a definition can be unfolded.
canUnfoldDefinition :: Eval m e n (DeclF m -> Definition t -> Args m n -> Bool)
canUnfoldDefinition !fuel env ctx d dx args = case defRec dx of
  -- Non-recursive definitions are always unfolded.
  RecursionNone -> inlinable
  -- General-recursion definitions are only unfolded at runtime.
  RecursionGeneral -> inlinable && runtime
  -- Guarded-recursion definitions are unfolded at runtime or if any guard argument evaluates to a constructor.
  RecursionGuarded guards -> inlinable && (runtime || any fguard guards)
    where
    fguard i = case drop i args of
      [] -> False
      arg:_ -> isJust (evalToCtor fuel env ctx $ argTerm arg)
  where
  backend = evalBackend (getOpts env)
  runtime = evalRuntime (getOpts env)
  inlinable = not backend || declHasAnnot AnnInline d
{-# INLINABLE canUnfoldDefinition #-}

-- | Match a list of branches against a list of arguments.
matchBranches :: Eval m e n (Loc -> [Branch m n] -> Term m n -> Args m n -> Maybe (WHNF m n))
matchBranches !fuel env ctx l branches disc args = frec branches
  where
  errMsg s = prettyErrorLn (PrettyEnv (printOpts $ getOpts env) 0) $
    CErrTree l s [CErrTerm "discriminator" (ErrTerm ctx disc)]
  ctorM = evalToCtor fuel env ctx disc
  frec = \case
    -- If no more branches, then this must be an absurd match.
    [] -> if null branches then Nothing else error (errMsg "non-exhaustive pattern match")
    -- Match the pattern of the next branch.
    Branch _ p body : branches' -> case matchPattern errMsg p ctorM of
      -- If the match succeeds, substitute into the branch body and continue evaluation.
      Right psubs -> pure $ evalWithArgs fuel env ctx (substScope (vreverse $ vcons disc psubs) body) args
      -- Abort if the match is neutral, otherwise try the remaining branches.
      Left neutral -> if neutral then Nothing else frec branches'
{-# INLINABLE matchBranches #-}

-- | Match a pattern against a discriminator term.
matchPattern :: forall k m n. (KnownNat k) =>
  (String -> String) -> Pattern k -> Maybe (DeclInfo m n) -> Either Bool (Vector k (Term m n))
matchPattern errMsg pattern ctorM = case pattern of
  -- Variable pattern always matches.
  PatWild -> pure vempty
  -- Constructor pattern checks if the discriminator is an applied constructor.
  PatCtor sc (Irr (dti, _)) -> case ctorM of
    -- If the discriminator is not yet an applied constructor then the match is neutral.
    Nothing -> Left True
    -- If the discriminator is a different constructor then fail the match.
    Just (cd, _, cargs) -> if declName cd /= sc then Left False else
      -- If the discriminator is the matching constructor then parse the constructor arguments.
      case vfromList @k (drop (dataTypeArity dti) cargs) of
        Just vargs -> pure $ argTerm <$> vargs
        _ -> error (errMsg "invalid number of constructor arguments")
{-# INLINABLE matchPattern #-}

-- ************************************************************************************************

-- | Evaluate a term to an applied constructor.
evalToCtor :: Eval m e n (Term m n -> Maybe (DeclInfo m n))
evalToCtor !fuel env ctx x = case evalToWHNF fuel env ctx x of
  (Con _ (DeclRef s pargs), args) -> case lookupCDecl s env of
    Just d@(Decl {declImpl = ICtor _}) -> pure (d, pargs, args)
    _ -> Nothing
  (Con (Irr l) (Literal v), []) -> unpackPrimCtor env l v
  _ -> Nothing
{-# INLINABLE evalToCtor #-}

-- ************************************************************************************************

packPrimCtor :: Eval m e n (Loc -> (Text, Args m n) -> Maybe (WHNF m n))
packPrimCtor !fuel env ctx l = \case
  ("'Z", []) -> retLit $ Nat 0
  ("'S", [i]) -> isLiteral (evalToWHNF fuel env ctx (argTerm i)) >>= \case
    (Nat i', []) -> retLit $ Nat (i' + 1)
    _ -> Nothing
  ("'NonNeg", [v]) -> isLiteral (evalToWHNF fuel env ctx (argTerm v)) >>= \case
    (Nat v', []) -> retLit $ Int (fromIntegral v')
    _ -> Nothing
  ("'NegMinus1", [v]) -> isLiteral (evalToWHNF fuel env ctx (argTerm v)) >>= \case
    (Nat v', []) -> retLit $ Int (-(fromIntegral v') - 1)
    _ -> Nothing
  ("'str", [v]) -> extractChars (argTerm v) >>= retLit . Str . pack
    where
    extractChars x = isNamed (evalToWHNF fuel env ctx x) >>= \case
      ("'nil", [_]) -> pure []
      ("'cons", [_, h, ts]) -> isLiteral (evalToWHNF fuel env ctx (argTerm h)) >>= \case
        (Char c, []) -> (c:) <$> extractChars (argTerm ts)
        _ -> Nothing
      _ -> Nothing
  _ -> Nothing
  where
  retLit v = Just (Con (Irr l) (Literal v), [])
{-# INLINABLE packPrimCtor #-}

unpackPrimCtor :: (HasCEnv e) => e -> Loc -> Literal -> Maybe (DeclInfo m n)
unpackPrimCtor env l = \case
  Nat i -> if i == 0
    then do
      d <- lookupCDeclPrim (Name "'Z") env
      pure (d, mempty, [])
    else do
      d <- lookupCDeclPrim (Name "'S") env
      pure (d, mempty, mkArgs l [("", multiOne, mkCon (Irr l) (Literal (Nat (i - 1))))])
  Int i -> if i >= 0
    then do
      d <- lookupCDeclPrim (Name "'NonNeg") env
      pure (d, mempty, mkArgs l [("", multiOne, mkCon (Irr l) (Literal (Nat $ fromIntegral i)))])
    else do
      d <- lookupCDeclPrim (Name "'NegMinus1") env
      pure (d, mempty, mkArgs l [("", multiOne, mkCon (Irr l) (Literal (Nat $ fromIntegral (-(i + 1)))))])
  Str s -> do
    str <- lookupCDeclPrim (Name "'str") env
    char <- declRefE <$> lookupCDeclPrim (Name "Char") env
    nil  <- declRef0 <$> lookupCDeclPrim (Name "'nil") env
    cons <- declRef0 <$> lookupCDeclPrim (Name "'cons") env
    let charTypeArg = ("A", multiZero, char)
    let fcons c res = mkApps cons $ mkArgs l
          [charTypeArg, ("", multiOne, mkCon (Irr l) (Literal (Char c))), ("", multiOne, res)]
    let v = foldr fcons (mkApps nil $ mkArgs l [charTypeArg]) (unpack s)
    pure (str, mempty, mkArgs l [("", multiOne, v)])
  _ -> Nothing
  where
  declRefE d = mkDeclRef l (declName d) mempty
  declRef0 d = mkDeclRef l (declName d) ([levelNat 0], mempty)
{-# INLINABLE unpackPrimCtor #-}

-- ************************************************************************************************

-- | Evaluate a builtin operator.
evalBuiltin :: Eval m e n (Loc -> (Text, Args m n) -> Maybe (WHNF m n))
evalBuiltin !fuel env ctx l = \case
  ("J", _:_:f:x:_:p:args') -> if evalRuntime (getOpts env)
    -- Equality proofs are erasible at runtime so we don't need to reduce.
    then pure fx
    -- During type-checking we need to reduce the equality proof to a concrete constructor to ensure it exists.
    else isNamed (evalToWHNF fuel env ctx (argTerm p)) >>= \case
      ("refl", _) -> pure fx
      _ -> Nothing
    where
    fx = evalWithArgs fuel env ctx (argTerm f) (x:args')
  ("showAny", [_, v]) -> do
    let v' = evalToNF fuel env ctx (argTerm v)
    let s = prettyTermS (PrettyEnv (printOpts $ getOpts env) 0) ctx 0 v' ""
    retLit (Str $ pack s)
  _ -> Nothing
  where
  retLit v = Just (Con (Irr l) (Literal v), [])
{-# INLINABLE evalBuiltin #-}

-- | Evaluate an optimized operator.
evalOptimized :: Eval m e n (Loc -> (Text, Args m n) -> Maybe (WHNF m n))
evalOptimized !fuel env ctx l = \case
  ("method__AdditiveMonoid_Nat_add", [x, y]) -> do
    let x' = isLiteral (evalToWHNF fuel env ctx $ argTerm x)
    let y' = isLiteral (evalToWHNF fuel env ctx $ argTerm y)
    case (x', y') of
      (Just (Nat vx, []), Just (Nat vy, [])) -> retLit (Nat (vx + vy))
      _ -> Nothing
  ("method__MultiplicativeMonoid_Nat_multiply", [x, y]) -> do
    let x' = isLiteral (evalToWHNF fuel env ctx $ argTerm x)
    let y' = isLiteral (evalToWHNF fuel env ctx $ argTerm y)
    case (x', y') of
      (Just (Nat vx, []), Just (Nat vy, [])) -> retLit (Nat (vx * vy))
      _ -> Nothing
  _ -> Nothing
  where
  retLit v = Just (Con (Irr l) (Literal v), [])
{-# INLINABLE evalOptimized #-}

-- ************************************************************************************************

-- | Evaluate a term to fully normalized form.
evalToNF :: Eval m e n (Term m n -> Term m n)
evalToNF !fuel env ctx t = unWHNF (h', args')
  where
  (h, args) = evalToWHNF fuel env ctx t
  h' = case h of
    Var {}         -> h
    App z x m y    -> App z (frec x) m (frec y)
    Lam z p m r    -> Lam z (frec p) m (frec1 (relevant z) r)
    Pit z p m r    -> Pit z (frec p) m (frec1 (relevant z) r)
    Cas z d m r bs -> Cas z (frec d) m (frec r) (fbranch <$> bs)
    Con {}         -> h
    Met {}         -> h
  args' = map (mapArg frec) args

  frec = evalToNF fuel env ctx
  frec1 z = evalToNF fuel env (snd $ consPCtx z ctx)
  fbranch (Branch pz@(Irr z) p b) =
    let bctx = snd $ prettyPattern ctx z p
    in Branch pz p (evalToNF fuel env bctx b)
{-# INLINABLE evalToNF #-}