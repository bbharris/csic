module Csic.Core.Environment.Pretty
( PrettyEnv(..)
, prettyErrorsLn, prettyErrorLn, prettyDeclLn, prettyTermS
, prettyPattern, prettyPatternArgs, prettyConst
) where
import Csic.Util.Prelude
import Csic.Core.Context
import Csic.Core.Environment.Options
import Csic.Core.Environment.Error
import Text.Pretty.Simple hiding (Literal)

data PrettyEnv = PrettyEnv
  { prettyOpts   :: PrintOptions
  , prettyIndent :: Int
  }

-- ************************************************************************************************

prettify :: PrettyEnv -> String -> String
prettify env = unpackL . pStringOpt opts'
  where
  opts' = defaultOutputOptionsNoColor
    { outputOptionsInitialIndent = prettyIndent env
    , outputOptionsIndentAmount = 2
    , outputOptionsColorOptions = if showColor (prettyOpts env) then Just defaultColorOptionsDarkBg else Nothing
    }

addIndent :: Int -> PrettyEnv -> PrettyEnv
addIndent i e = e {prettyIndent = prettyIndent e + 2 * i}

indentPrefix :: PrettyEnv -> String
indentPrefix env = replicate (prettyIndent env) ' '

prettyLocStrLn :: PrettyEnv -> Loc -> String -> String
prettyLocStrLn env l s =
  if showLocations opts && l /= nullLoc
    then case showLocStr opts l s of
      Just s' -> prettyStrLn env s'
      Nothing -> prettyStrLn env (show l <> ":") <> prettyStrLn env s
    else prettyStrLn env s
  where
  opts = prettyOpts env

prettyStrLn :: PrettyEnv -> String -> String
prettyStrLn env s = unlines (map (indentPrefix env <>) $ filter (not . null) $ lines s)

-- ************************************************************************************************

prettyErrorsLn :: PrettyEnv -> [CheckError] -> String
prettyErrorsLn env = concatMap (prettyErrorLn env)

prettyErrorLn :: PrettyEnv -> CheckError -> String
prettyErrorLn env = \case
  CErrTree l s xs ->
    prettyStrLn env (s <> ":") <>
    prettyLocStrLn (addIndent 1 env) l "" <>
    prettyErrorsLn (addIndent 1 env) xs
  CErrTerm s t ->
    prettyErrTermLn env s t
  CErrDecls s (ErrDecls ds) ->
    prettyStrLn env (s <> " " <> s0 <> ":") <>
    if not (showErrorDetails opts) then "" else concatMap (prettyDeclLn $ addIndent 1 env) ds
    where
    s0 = case mapMaybe (\d -> case declImpl d of IData {} -> Just (qname $ declNameQ d); _ -> Nothing) ds of
      sd:_ -> show sd
      [] -> case ds of d:_ -> show (qname $ declNameQ d); _ -> ""
  CErrUDecls s ds ->
    prettyStrLn env (s <> " " <> s0 <> ":") <>
    if not (showErrorDetails opts) then "" else concatMap (prettyUDeclLn $ addIndent 1 env) ds
    where
    s0 = case ds of
      UDeclType d:_ -> show (qname $ declNameQ d)
      UDeclImpl _ sd _:_ -> show sd; _ -> "??"
  where
  opts = prettyOpts env

prettyUDeclLn :: PrettyEnv -> UDecl -> String
prettyUDeclLn env d = prettify env $ prettyUDecl env d "\n"

prettyUDecl :: PrettyEnv -> UDecl -> ShowS
prettyUDecl env = \case
  UDeclType d ->
    prettyConst @Void env (DeclRef (declName d) mempty) . showString " : " . prettyTerm env emptyPCtx 0 (declType d)
  UDeclImpl _ s x ->
    prettyConst @Void env (DeclRef s mempty) . showString " = " . prettyImpl env x

prettyDeclLn :: (IsMetaId m) => PrettyEnv -> DeclF m -> String
prettyDeclLn env d = type' <> impl'
  where
  type' = prettify env $ (prettyConst @Void env (DeclRef (declName d) mempty) .
    showString " : " . prettyTerm env emptyPCtx 0 (declType d)) "\n"
  impl' = prettify env $ (prettyConst @Void env (DeclRef (declName d) mempty) .
    showString " = " . prettyImpl env (declImpl d)) "\n"

prettyImpl :: (IsMetaId m) => PrettyEnv -> Impl (Term m 0) -> ShowS
prettyImpl env = \case
  IAxiom -> showString "axiom"
  IDef y -> prettyTerm env emptyPCtx 0 (defImpl y)
  IData {} -> showString "data"
  ICtor {} -> showString "constructor"

-- ************************************************************************************************

prettyErrTermLn :: PrettyEnv -> String -> ErrTerm -> String
prettyErrTermLn env s (ErrTerm ctx x) =
  prettyStrLn env (s <> ":") <>
  prettyLocStrLn (addIndent 1 env) (getLoc x) "" <>
  prettyTermS (addIndent 1 env) ctx 0 x "\n"

-- ************************************************************************************************

prettyTermS :: (IsMetaId m, KnownNat n) => PrettyEnv -> PCtx n -> Int -> Term m n -> ShowS
prettyTermS env ctx p t suf = prettify env $ prettyTerm env ctx p t suf

prettyTerm :: (IsMetaId m, KnownNat n) => PrettyEnv -> PCtx n -> Int -> Term m n -> ShowS
prettyTerm env ctx p t = case unT t of
  Var _ i -> prettyName $ paramName $ indexPCtx i ctx
  App (Irr z) x m y -> if hide
    then prettyTerm env ctx p x
    else showParen (p > 10) $ prettyTerm env ctx 10 x . showString " " . m' . s' . prettyTerm env ctx 11 y
    where
    hide = isJust (applyImplicit z) && not (showImplicits $ prettyOpts env)
    m' = if showMultis (prettyOpts env)
      then showString "%" . showsPrec 10 m . showString " "
      else id
    s' = maybe id (\s -> showString "`" . prettyName s . showString "=") $ applyImplicit z
  Lam (Irr z) pt (_, m) b ->
    showParen (p > 9) $ showString "λ" . p' . arrow . prettyTerm env bctx 0 b
    where
    (s', bctx) = first (prettyParamName z) $ consPCtx z ctx
    p' = if False
      then showString "(" . s' . showString " : " . prettyTerm env ctx 0 pt . showString ")"
      else s'
    arrow = if showMultis (prettyOpts env) && m /= multiAny
      then showString " " . showString "%" . showsPrec 10 m . showString "=> "
      else showString " => "
  Pit (Irr z) pt (_, m) rt ->
    showParen (p > 9) $ pt' . arrow . prettyTerm env bctx 9 rt
    where
    (s', bctx) = first (prettyParamName z) $ consPCtx z ctx
    pt' = if isJust (paramImplicit z) || paramName z /= Name ""
      then showString "(" . s' . showString " : " . prettyTerm env ctx 0 pt . showString ")"
      else prettyTerm env ctx 10 pt
    arrow = if showMultis (prettyOpts env) && m /= multiAny
      then showString " " . showString "%" . showsPrec 10 m . showString "-> "
      else showString " -> "
  Cas _ d _ _ bs -> showParen (p > 9) $ showString "case " . d' . showString " of " . bs'
    where
    d' = prettyTerm env ctx 0 d
    bs' = showListS (prettyBranch env ctx) bs
  Con _ c -> prettyConst env c
  Met _ m _ -> showsPrec p m

prettyConst :: IsMetaId m => PrettyEnv -> Constant m -> ShowS
prettyConst env = \case
  TypeUniv u -> showString "@" . showsPrec 10 u
  DeclRef s (lvls, muls) -> shows s . lvls' . muls'
    where
    lvls' = if showLevels (prettyOpts env) && not (null lvls)
      then showString "@" . showListS shows lvls
      else id
    muls' = if showMultis (prettyOpts env) && not (null muls)
      then showString "%" . showListS shows muls
      else id
  Literal y -> case y of
    Nat n -> shows n
    Int n -> shows n
    Char c -> shows c
    Str s -> shows s

-- ************************************************************************************************

prettyBranch :: (IsMetaId m, KnownNat n) => PrettyEnv -> PCtx n -> Branch m n -> ShowS
prettyBranch env ctx (Branch (Irr z) p b) =
  let (p', bctx) = prettyPattern ctx z p
  in p' . showString " => " . prettyTerm env bctx 0 b

prettyPattern :: (KnownNat k) =>
  PCtx n -> BindInfo -> Pattern k -> (ShowS, PCtx (k + 1 + n))
prettyPattern ctx z = \case
  PatWild -> (prettyName s, pctx)
  PatCtor sc (Irr (_, ps)) -> (s' . shows sc . showString "{" . psFlat . showString "}", bctx)
    where
    s' = if bindName z /= Name "" then prettyName s . showString "@" else id
    (_, ps', bctx) = prettyPatternArgs pctx (bindLoc z) ps
    psFlat = intercalateS ", " (prettyName <$> ps')
  where
  (s, pctx) = consPCtx (bindParamInfo z) ctx

prettyPatternArgs :: forall k n. (KnownNat k) =>
  PCtx n -> Loc -> PatternArgs k -> (Vector k Name, [Name], PCtx (k + n))
prettyPatternArgs ctx l args = (vreverse names, reverse args', bctx)
  where
  argsM = (`lookup` args) <$> vindices @k
  args' = mapMaybe (\(s, m) -> m $> s) $ vtoList $ vzip names argsM
  (names, bctx) = consPCtxN (maybe (nullParamInfo l) (bindParamInfo . snd) <$> argsM) ctx

-- ************************************************************************************************

prettyName :: Name -> ShowS
prettyName (Name s) = showString (if s == "" then "_" else unpack s)

prettyParamName :: ParamInfo -> Name -> ShowS
prettyParamName z s = case paramImplicit z of
  Nothing -> prettyName s
  Just imp -> showString "`" . prettyName imp . if imp == s then id else showString "=" . prettyName s
