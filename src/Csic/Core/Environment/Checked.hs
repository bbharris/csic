{-# LANGUAGE FunctionalDependencies #-}
module Csic.Core.Environment.Checked
( CEnv, HasCEnv(..), HasMetaEnv(..), initCEnv, getOpts, mapOpts
, CEnvState, CEnvError, liftE
, lookupCDecl, lookupCDeclQ, lookupCDeclPrim, isDecl
, lookupCDeclE, lookupCDeclQE, lookupCDeclPrimE, lookupTermPrimE, lookupUTermPrimE
, lookupModuleCDecls, lookupModuleLocalCDecls, lookupClassInstances
, allocDeclName, registerCDecl, registerCDeclImport
, Fuel, initFuel, consumeFuel
) where
import Csic.Util.Prelude
import Csic.Core.Syntax
import Csic.Core.Environment.Options
import Csic.Core.Environment.Error
import Data.List (sortOn)

-- ************************************************************************************************

-- | Core checked declaration environment.
data CEnv = CEnv
  { coreDecls :: AllocN (HashMap IName CDecl) -- ^ Checked declaration environment.
  , coreMods  :: HashMap Namespace CModule    -- ^ Map namespaces to modules.
  , corePrims :: HashMap Name DeclName        -- ^ Primitives provided by axioms or library.
  , coreOpts  :: CoreOptions                  -- ^ Core type-checking options.
  } deriving (Generic, NFData)

-- | Core module.
data CModule = CModule
  { cmodDecls :: HashMap Name CDecl               -- ^ Local scope.
  , cmodInsts :: HashMap IName (ListSet DeclName) -- ^ Map type class names to visible instances of that class.
  } deriving (Generic, NFData)

-- ************************************************************************************************

-- | Environments containing a core environment.
class HasCEnv e where
  getCEnv :: e -> CEnv
  mapCEnv :: (CEnv -> CEnv) -> e -> e

instance HasCEnv CEnv where
  getCEnv = id
  {-# INLINE getCEnv #-}
  mapCEnv = id
  {-# INLINE mapCEnv #-}

-- ************************************************************************************************

-- | Environments able to instantiate a metavariable type.
class (IsMetaId m, HasCEnv e) => HasMetaEnv m e | e -> m where
  instantiateDecl :: Loc -> DeclName -> PrenexArgs m -> e -> CheckResult (DeclF m)
  instantiateMetaVarBody :: KnownNat n => Loc -> m -> [Term m n] -> e -> Maybe (Term m n)
  instantiateMetaVarType :: KnownNat n => Loc -> m -> [Term m n] -> e -> CheckResult (Term m n)

instance HasMetaEnv Void CEnv where
  instantiateDecl l s pargs e = fmap (instantiateTerm pargs) <$> lookupCDeclE l s e
  instantiateMetaVarBody _ = absurd
  instantiateMetaVarType _ = absurd

-- ************************************************************************************************

-- | Checked environment state constraint.
type CEnvState s f = (MonadState s f, CEnvError s f)

-- | Checked environment error constraint.
type CEnvError e f = (HasCEnv e, CheckErrorC f)

-- | Lift an environment error computation into a environment state computation.
liftE :: (CEnvState s f) => (CEnv -> CheckResult a) -> f a
liftE f = gets getCEnv >>= liftEither . f
{-# INLINE liftE #-}

-- ************************************************************************************************

-- | Create an empty core environemnt.
initCEnv :: CoreOptions -> CEnv
initCEnv = CEnv emptyAllocN mempty mempty

-- | Access core options from an environment.
getOpts :: (HasCEnv e) => e -> CoreOptions
getOpts = coreOpts . getCEnv
{-# INLINE getOpts #-}

-- | Modify core options in an environment.
mapOpts :: (HasCEnv e) => (CoreOptions -> CoreOptions) -> e -> e
mapOpts f = mapCEnv (\x -> x {coreOpts = f (coreOpts x)})
{-# INLINE mapOpts #-}

-- ************************************************************************************************

-- | Lookup a checked declaration.
lookupCDecl :: (HasCEnv e) => DeclName -> e -> Maybe CDecl
lookupCDecl s = lookup (unTag s) . allocV . coreDecls . getCEnv
{-# INLINE lookupCDecl #-}

-- | Lookup a checked declaration by textual name.
lookupCDeclQ :: (HasCEnv e) => QName -> e -> Maybe CDecl
lookupCDeclQ s e = lookup (qspace s) (coreMods $ getCEnv e) >>= lookup (qname s) . cmodDecls
{-# INLINE lookupCDeclQ #-}

-- | Lookup a checked declaration by primitive name.
lookupCDeclPrim :: (HasCEnv e) => Name -> e -> Maybe CDecl
lookupCDeclPrim s e = lookup s (corePrims $ getCEnv e) >>= flip lookupCDecl e
{-# INLINE lookupCDeclPrim #-}

-- | Extract declaration info from a WHNF term.
isDecl :: HasCEnv e => WHNF m n -> e -> Maybe (DeclInfo m n)
isDecl x env = case x of
  (Con _ (DeclRef s pargs), args) -> (,pargs,args) <$> lookupCDecl s env
  _ -> Nothing
{-# INLINE isDecl #-}

-- ************************************************************************************************

-- | Lookup a declaration but fail if missing.
lookupCDeclE :: (CEnvError e f) => Loc -> DeclName -> e -> f CDecl
lookupCDeclE l s e = fromMaybeM (checkError l $ "invalid declaration reference: " <> show s) (lookupCDecl s e)
{-# INLINE lookupCDeclE #-}

-- | Lookup a declaration but fail if missing.
lookupCDeclQE :: (CEnvError e f) => Loc -> QName -> e -> f CDecl
lookupCDeclQE l s e = fromMaybeM (checkError l $ "invalid declaration name: " <> show s) (lookupCDeclQ s e)
{-# INLINE lookupCDeclQE #-}

-- | Lookup a primitive declaration but fail if missing.
lookupCDeclPrimE :: (CEnvError e f) => Loc -> Name -> e -> f CDecl
lookupCDeclPrimE l s e = fromMaybeM (checkError l ("missing primitive: " <> show s)) (lookupCDeclPrim s e)
{-# INLINE lookupCDeclPrimE #-}

-- | Lookup a primitive term.
lookupTermPrimE :: (CEnvError e f, IsMetaId m) => Loc -> Name -> PrenexArgs m -> e -> f (Term m n)
lookupTermPrimE l s args e = do
  d <- lookupCDeclPrimE l s e
  verifyPrenexArgs l (Right d) args
  pure $ mkDeclRef l (declName d) args
{-# INLINE lookupTermPrimE #-}

-- | Lookup an unchecked primitive term.
lookupUTermPrimE :: (CEnvError e f) => Loc -> Name -> e -> f (UTerm n)
lookupUTermPrimE l s e = declRefInferPrenex l <$> lookupCDeclPrimE l s e
{-# INLINE lookupUTermPrimE #-}

-- ************************************************************************************************

-- | Lookup all declarations in scope within a module.
lookupModuleCDecls :: (HasCEnv e) => Namespace -> e -> [(Name, CDecl)]
lookupModuleCDecls ns e = maybe [] (toList . cmodDecls) $ lookup ns (coreMods $ getCEnv e)
{-# INLINABLE lookupModuleCDecls #-}

-- | Lookup all local declarations of a module.
lookupModuleLocalCDecls :: (HasCEnv e) => Namespace -> e -> [CDecl]
lookupModuleLocalCDecls ns e = sortOn declNameQ $ mapMaybe filterLocal $ lookupModuleCDecls ns e
  where
  filterLocal (_, d) = if qspace (declNameQ d) == ns then Just d else Nothing
{-# INLINABLE lookupModuleLocalCDecls #-}

-- | Lookup all instance declarations for a class within scope of a module.
lookupClassInstances :: (HasCEnv e) => Namespace -> DeclName -> e -> [DeclName]
lookupClassInstances ns className env =
  maybe [] toList $ lookup ns (coreMods $ getCEnv env) >>= lookup (unTag className) . cmodInsts
{-# INLINABLE lookupClassInstances #-}

-- ************************************************************************************************

-- | Allocate a new declaration name.
allocDeclName :: (HasCEnv e) => QName -> e -> (DeclName, e)
allocDeclName s e =
  (IrrTagged (Irr s) k, mapCEnv (\x -> x {coreDecls = decls}) e)
  where
  (k, decls) = allocate1 IName (coreDecls $ getCEnv e)
{-# INLINABLE allocDeclName #-}

-- | Register a declaration in an environment.
registerCDecl :: (HasCEnv e) => CDecl -> e -> e
registerCDecl d = mapCEnv $ \e -> (registerCDeclImport (declNameQ d) d e)
  { coreDecls = insert (unTag $ declName d) d <$> coreDecls e
  , corePrims = insert (qname $ declNameQ d) (declName d) (corePrims e)
  }
{-# INLINABLE registerCDecl #-}

-- | Register a declaration import in an environment.
registerCDeclImport :: (HasCEnv e) => QName -> CDecl -> e -> e
registerCDeclImport s d = mapCEnv $ \e -> e
  { coreMods = alter (Just . addDecl .  fromMaybe (CModule mempty mempty)) (qspace s) (coreMods e)
  }
  where
  addDecl m = m
    { cmodDecls = insert (qname s) d (cmodDecls m)
    , cmodInsts = maybe id (\c -> insertWith union (unTag c) (singleton $ declName d)) (declIsInstance d) (cmodInsts m)
    }
{-# INLINABLE registerCDeclImport #-}

-- ************************************************************************************************

-- | Remaining fuel for infinite loop detection.
newtype Fuel = Fuel Int

-- | Get the initial fuel for loop detection.
initFuel :: (HasCEnv e) => e -> Fuel
initFuel = Fuel . coreFuelMax . getOpts
{-# INLINE initFuel #-}

-- | Check we still have fuel and consume one fuel.
consumeFuel :: a -> Fuel -> (Fuel -> a) -> a
consumeFuel err (Fuel n) f = if n > 0 then f (Fuel $ n - 1) else err
{-# INLINE consumeFuel #-}
