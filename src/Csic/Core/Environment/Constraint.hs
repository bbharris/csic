module Csic.Core.Environment.Constraint where
import Csic.Util.Prelude
import Csic.Core.Context

-- ************************************************************************************************

type MetaVarSet = ListSet MetaId
type DepsConstraint = (MetaVarSet, UConstraint)

-- ************************************************************************************************

-- | Unresolved unification constraint.
data UConstraint
  = CStuck (Sized StuckConstraint)
  | CFlexRigid (Sized FlexRigid)
  | CInstance (Sized InstanceConstraint)
  | CImplicit (Sized ImplicitConstraint)
  | CLevel (LevelConstraint MetaId)
  | CMulti (MultiConstraint MetaId)

-- | Unrsolved instance constraint.
data InstanceConstraint n = InstanceConstraint (TCtx n) Loc MetaId CDecl (Args MetaId n) (PTerm n)

-- | Unrsolved implicit arguments constraint.
data ImplicitConstraint n = ImplicitConstraint
  { dimpCtx     :: TCtx n
  , dimpMeta    :: PTerm n
  , dimpTerm    :: PTerm n
  , dimpActType :: PTerm n
  , dimpExpType :: PTerm n
  }

-- | Stuck constraint (flex-flex or rigid-rigid form).
data StuckConstraint n = Stuck (PCtx n) (PTerm n) UnifyRelation (PTerm n)

{- | Flex-rigid constraint of the form:
      \X1 ... Xn => (M T1 ... Tp) ==
      \X1 ... Xn => (@ U1 ... Uq)
-}
data FlexRigid n = FlexRigid (PCtx n) (FlexTerm n) UnifyRelation (PTerm n)

-- | WHNF of a term headed by a metavariable.
type FlexTerm n = (Loc, MetaId, [PTerm n], Args MetaId n)

unflex :: FlexTerm n -> PTerm n
unflex (l, m, rs, ts) = mkApps (mkMet (Irr l) m rs) ts
{-# INLINE unflex #-}

-- ************************************************************************************************

-- | Relation to unify (strict equality or subtyping).
data UnifyRelation = UnifyEQU | UnifyLEQ | UnifyGEQ
  deriving (Eq, Ord, Show)

negateRelation :: UnifyRelation -> UnifyRelation
negateRelation = \case
  UnifyEQU -> UnifyEQU
  UnifyLEQ -> UnifyGEQ
  UnifyGEQ -> UnifyLEQ