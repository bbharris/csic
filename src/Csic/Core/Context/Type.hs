{-# OPTIONS_GHC -Wno-redundant-constraints #-}
module Csic.Core.Context.Type
( TCtx, TCtx', emptyTCtx, consTCtx, indexTCtx, pCtx, insertLetBind, typeClassVars
) where
import Csic.Util.Prelude
import Csic.Core.Syntax
import Csic.Core.Context.Pretty

-- ************************************************************************************************

type TCtx = TCtx' DeclName MetaId

data TCtx' z m n = TCtx (FreeVarMap m n) (Bindings z m n)

data Bindings z m n where
  CNil  :: Bindings z m 0
  CCons :: ParamInfo -> Maybe z -> Term m n -> Bindings z m n -> Bindings z m (1 + n)

emptyTCtx :: TCtx' z m 0
emptyTCtx = TCtx mempty CNil

consTCtx :: KnownNat n => ParamInfo -> Maybe z -> Term m n -> TCtx' z m n -> TCtx' z m (1 + n)
consTCtx z a p ctx@(TCtx lets bs) = TCtx (shiftTermN <$> lets) $ CCons z' a p bs
  where
  z' = z {paramName = fst (consPCtx z (pCtx ctx))}

indexTCtx :: forall z m n. KnownNat n => Finite n -> TCtx' z m n -> ((ParamInfo, Maybe z), Term m n)
indexTCtx i0 (TCtx lets binds) = frec (sNat @0) i0 binds
  where
  frec :: forall k i. (KnownNat k, KnownNat i, n ~ k + i) =>
    SNat k -> Finite i -> Bindings z m i -> ((ParamInfo, Maybe z), Term m n)
  frec _ i ctx = case ctx of
    CNil -> absurdFinite i
    CCons z a pt ctx' -> case separateSum @1 i of
      Left _ -> ((z, a), substFreeVars lets $ shiftTerm pt)
      Right i' -> frec (sNat @(1 + k)) i' ctx'

-- ************************************************************************************************

pCtx :: TCtx' z m n -> PCtx n
pCtx (TCtx _ binds) = PCtx (bindsParamInfo binds)

bindsParamInfo :: Bindings z m n -> Vector n ParamInfo
bindsParamInfo = \case
  CNil -> vempty
  CCons z _ _ ctx -> vcons z (bindsParamInfo ctx)

insertLetBind :: KnownNat n => Finite n -> Term m n -> TCtx' z m n -> TCtx' z m n
insertLetBind i t (TCtx lets binds) =
  TCtx (insert (getFinite $ mirror i) (TermN @0 $ substFreeVars lets t) lets) binds

-- ************************************************************************************************

typeClassVars :: forall n. KnownNat n => DeclName -> TCtx n -> [Finite n]
typeClassVars className (TCtx _ binds) = frec (sNat @0) binds
  where
  frec :: forall k i. (KnownNat k, KnownNat i, n ~ k + i) => SNat k -> Bindings DeclName MetaId i -> [Finite n]
  frec _ = \case
    CNil {} -> []
    CCons _ z _ bs -> (if z == Just className then (finite @k :) else id) (frec sNat bs)
