module Csic.Core.Context.Pretty
( PCtx(..), emptyPCtx, consPCtx, consPCtxN, indexPCtx
) where
import Csic.Util.Prelude
import Csic.Core.Syntax

-- ************************************************************************************************

newtype PCtx n = PCtx (Vector n ParamInfo)

emptyPCtx :: PCtx 0
emptyPCtx = PCtx vempty

consPCtx :: ParamInfo -> PCtx n -> (Name, PCtx (1 + n))
consPCtx z (PCtx binds) =
  (s', PCtx $ vcons (z {paramName = s'}) binds)
  where
  (s, n) = if paramName z == Name ""
    then (Name "_", length (vtoList binds))
    else (paramName z, namePSuffix (paramName z) (vtoList binds))
  s' = if paramName z == Name "" || n > 0 then addNameSuffix s (pack (show n)) else s

consPCtxN :: forall k n. (KnownNat k) =>
  Vector k ParamInfo -> PCtx n -> (Vector k Name, PCtx (k + n))
consPCtxN v ctx = case sNat @k of
  Zero -> (vempty, ctx)
  Succ _ -> (vcons s ss, bctx)
    where
    (s, bctx) = consPCtx (vhead v) pctx
    (ss, pctx) = consPCtxN (vtail v) ctx

indexPCtx :: Finite n -> PCtx n -> ParamInfo
indexPCtx i (PCtx ps) = vindex ps i

namePSuffix :: Name -> [ParamInfo] -> Int
namePSuffix s = \case
  [] -> 0
  z:ctx -> if paramName z == s then 1 else
    let n = namePSuffix s ctx in
    if n == 0 then 0 else if paramName z == addNameSuffix s (pack (show n)) then n + 1 else n
