module Csic.Core.Syntax.Multi where
import Csic.Util.Prelude
import Data.List.Ordered as LO

-- ************************************************************************************************

-- | QTT multiplicity literal.
data MultiLit
  = MultiZero -- ^ Exactly zero runtime use (erasable).
  | MultiOne  -- ^ Exactly one runtime use (linear).
  | MultiAny  -- ^ Any number of runtime uses (unrestricted).
  deriving (Eq, Ord, Enum, Bounded, Generic, NFData)

instance Hashable MultiLit where
  hashWithSalt salt = hashWithSalt salt . fromEnum

-- | Add two multi literals.
multiLitPlus :: MultiLit -> MultiLit -> MultiLit
multiLitPlus x y = case (x, y) of
  (MultiZero, _) -> y
  (_, MultiZero) -> x
  _ -> MultiAny

-- | Multiply two multi literals.
multiLitTimes :: MultiLit -> MultiLit -> MultiLit
multiLitTimes x y = case (x, y) of
  (MultiZero, _) -> MultiZero
  (_, MultiZero) -> MultiZero
  (MultiOne, _) -> y
  (_, MultiOne) -> x
  _ -> MultiAny

-- | Test whether one multi literal is convertible to another.
multiLitConvertible :: MultiLit -> MultiLit -> Bool
multiLitConvertible x y = x == y || y == MultiAny

-- ************************************************************************************************

type CMultiId = MultiId Void
type PMultiId = MultiId MetaId

type CMulti = Multi Void
type PMulti = Multi MetaId

-- ************************************************************************************************

-- | Multiplicity identifier.
newtype MultiId m = MultiIdRaw Int
  deriving newtype (Eq, Ord, NFData, Hashable)

multiIdUser :: Int -> MultiId m
multiIdUser i = if i < 0 then error "bad user multi" else MultiIdRaw i

multiIdMeta :: IsMetaId m => m -> MultiId m
multiIdMeta m = let MetaId i = toMetaId m in MultiIdRaw (-i - 1)
{-# INLINE multiIdMeta #-}

-- ************************************************************************************************

data MultiIdView m
  = MultiUser Int
  | MultiMeta m
  deriving (Eq, Ord, Functor, Foldable, Traversable)

viewMultiId :: IsMetaId m => MultiId m -> MultiIdView m
viewMultiId (MultiIdRaw i) = if i < 0 then MultiMeta (fromMetaId (MetaId $ -i - 1)) else MultiUser i
{-# INLINE viewMultiId #-}

isBasisMultiId :: IsMetaId m => MultiId m -> Either m (MultiId m')
isBasisMultiId m = case viewMultiId m of
  MultiUser k -> Right (MultiIdRaw k)
  MultiMeta k -> Left k
{-# INLINE isBasisMultiId #-}

-- ************************************************************************************************

-- | Constraint between two multiplicities.
type MultiConstraint m = Constraint (Multi m)

-- ************************************************************************************************

-- | Generalized multi expression: a*b*... + c*d*... + ...
newtype Multi m = MultiRaw {multiSOP :: MultiSOP m}
  deriving newtype (Eq, Ord, NFData, Hashable)

-- | Multiplicity SOP (sum of products) form.
type MultiSOP m = [MultiP m]

-- | Multiplicity product.
type MultiP m = ([MultiId m], MultiLit)

-- | Test whether one multi is convertible to another.
multiConvertible :: Multi m -> Multi m -> Bool
multiConvertible m1 m2 = case (multiSOP m1, multiSOP m2) of
  -- 0 ⊆ ω*x
  ([], xs2) -> all ((/= MultiOne) . snd) xs2
  -- General case.
  (xs1, xs2) ->
    all (\(x1, n1) -> foldr ((+) . subsetCount (x1, n1)) 0 xs2 > 1) xs1 &&
    all (\(x2, n2) -> n2 == MultiAny || any (\(x1, _) -> LO.subset x1 x2) xs1) xs2
  where
  subsetCount (x1, n1) (x2, n2) = if LO.subset x2 x1
    then if n2 == MultiAny || (x1 == x2 && multiLitConvertible n1 n2) then 2 else 1
    else 0 :: Int

-- ************************************************************************************************

-- | Construct a normalized multiplicity from SOP form.
multiNormalize :: MultiSOP m -> Multi m
multiNormalize = multiNormalizeWith LO.nubSort

-- | Construct a normalized multiplicity from SOP form.
multiNormalizeWith :: ([MultiId m] -> [MultiId m]) -> MultiSOP m -> Multi m
multiNormalizeWith fproduct xs = MultiRaw (mapMaybe fsimplify sop)
  where
  sop = foldr fsum mempty xs

  fsum (ys, n) = case n of
    -- Identity: x*0 = 0
    MultiZero -> id
    -- Identity: x*n + x*m = x*(n + m)
    _ -> insertWithLO multiLitPlus (fproduct ys, n)

  fsimplify (ks1, n1) = case foldr ((+) . subsetCount) (0 :: Int) sop of
    0 -> Just (ks1, n1)
    -- Identity: x + ω*x*y = x*(1 + ω*y) = x*(1 + y) = x + x*y
    1 -> Just (ks1, MultiOne)
    -- Identity: ω*x + x*y = x*(ω + y) = ω*x
    -- Identity: x + y + x*y*z = x + y*(1 + x*z) = x + y
    _ -> Nothing
    where
    subsetCount (ks2, n2) = if LO.subset ks2 ks1 && ks2 /= ks1 then (if n2 == MultiAny then 2 else 1) else 0

-- ************************************************************************************************

-- | Unknown level to be inferred.
multiHole :: Multi ()
multiHole = multiVar (multiIdMeta ())

-- | 0 multi literal.
multiZero :: Multi m
multiZero = multiLit MultiZero

-- | 1 multi literal.
multiOne :: Multi m
multiOne = multiLit MultiOne

-- | ω multi literal.
multiAny :: Multi m
multiAny = multiLit MultiAny

-- | Construct a multiplicity expression from a multi literal.
multiLit :: MultiLit -> Multi m
multiLit v = MultiRaw (case v of MultiZero -> []; _ -> [([], v)])

-- | Construct a multiplicity expression from a multi identifier.
multiVar :: MultiId m -> Multi m
multiVar k = MultiRaw [([k], MultiOne)]

-- | Add two multis.
multiPlus :: Multi m -> Multi m -> Multi m
multiPlus x y = multiNormalizeWith id (multiSOP x <> multiSOP y)

-- | Multiply two multis.
multiTimes :: Multi m -> Multi m -> Multi m
multiTimes x y = multiNormalizeWith id (multiTimes' (multiSOP x) (multiSOP y))

multiTimes' ::  MultiSOP m -> MultiSOP m -> MultiSOP m
multiTimes' x y = [fproduct xp yp | xp <- x, yp <- y]
  where
  fproduct (xs1, n1) (xs2, n2) = (LO.union xs1 xs2, multiLitTimes n1 n2)

-- ************************************************************************************************

instantiateMultiArgs :: [Multi m] -> Multi m -> Maybe (Multi m)
instantiateMultiArgs xs x = if x == x' then Nothing else Just x'
  where
  x' = multiNormalizeWith id $ concatMap fproduct $ multiSOP x
  fproduct (ys, n) = foldr fsubst [([], n)] ys
  fsubst (MultiIdRaw i) res = multiTimes' res $ multiSOP (xs !! i)

type MultiMap a b = HashMap (MultiId a) (Multi b)

substMultiMap :: MultiMap m m -> Multi m -> Multi m
substMultiMap xs = runIdentity . substMultisM (\k -> Identity $ fromMaybe (multiVar k) $ lookup k xs)

-- | Substitute multis into a multi expression and normalize the result.
substMultisM :: Monad f => (MultiId a -> f (Multi b)) -> Multi a -> f (Multi b)
substMultisM f (MultiRaw xs) = multiNormalizeWith id . concat <$> traverse fproduct xs
  where
  fproduct (ys, n) = foldlM fsubst [([], n)] ys
  fsubst res k = multiTimes' res . multiSOP <$> f k
{-# INLINABLE substMultisM #-}

-- | Extract the set of multi identifiers in a multi expression.
multisInMulti :: Multi m -> HashSet (MultiId m)
multisInMulti (MultiRaw xs) = foldMap (fromList . fst) xs

-- ************************************************************************************************

vacuousMultiId :: MultiId Void -> MultiId m
vacuousMultiId = coerce

vacuousMulti :: Multi Void -> Multi m
vacuousMulti = coerce

vacuousMultiMapV :: MultiMap a Void -> MultiMap a b
vacuousMultiMapV = coerce

-- ************************************************************************************************

instance Show MultiLit where
  show MultiZero = "0"
  show MultiOne = "1"
  show MultiAny = "ω"

instance IsMetaId m => Show (MultiId m) where
  show m = case toMetaId <$> viewMultiId m of
    MultiUser k -> [['p'..'z'] !! k]
    MultiMeta k -> show k

instance IsMetaId m => Show (Multi m) where
  showsPrec p x = case multiSOP x of
    [] -> shows MultiZero
    [([], n)] -> shows n
    [([v], MultiOne)] -> shows v
    xs -> showParen (p > 0) $ intercalateS "+" (map ftimes xs)
    where
    ftimes (xs, s) = if null xs then shows s else intercalateS "*" $ case s of
      MultiOne -> map shows xs
      _ -> shows s : map shows xs