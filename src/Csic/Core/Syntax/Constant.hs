module Csic.Core.Syntax.Constant where
import Csic.Util.Prelude
import Csic.Core.Syntax.Info
import Csic.Core.Syntax.Level
import Csic.Core.Syntax.Multi

-- | Constant value.
data Constant m
  = TypeUniv  (Level m)               -- ^ Type universe, indexed by level.
  | DeclRef   DeclName (PrenexArgs m) -- ^ Declaration reference with prenex arguments.
  | Literal   Literal                 -- ^ Literal value.
  deriving (Eq, Ord, Generic, NFData)

deriving instance (IsMetaId m) => Show (Constant m)

-- | Declaration prenex arguments.
type PrenexArgs m = ([Level m], [Multi m])

-- | Declaration prenex argument.
type PrenexArg m = (Level m, Multi m)

-- | Primitive literal.
data Literal
  = Nat     Natural       -- ^ Natural literal.
  | Int     Integer       -- ^ Integer literal.
  | Char    Char          -- ^ Character literal
  | Str     Text          -- ^ String literal.
  deriving (Eq, Ord, Show, Generic, NFData)

-- | Get the standard library type name of a literal value.
literalTypeName :: Literal -> Text
literalTypeName = \case
  Nat {} -> "Nat"
  Int {} -> "Int"
  Char {} -> "Char"
  Str {} -> "Str"

traverseConst :: Monad f =>
  (Level m -> f (Level m')) ->
  (Multi m -> f (Multi m')) ->
  Constant m -> f (Constant m')
traverseConst flevel fmulti = \case
  TypeUniv u      -> TypeUniv <$> flevel u
  DeclRef s pargs -> DeclRef s <$> bitraverse (traverse flevel) (traverse fmulti) pargs
  Literal v       -> pure $ Literal v
{-# INLINE traverseConst #-}
