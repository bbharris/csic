module Csic.Core.Syntax.Declaration where
import Csic.Util.Prelude
import Csic.Core.Syntax.Info
import Csic.Core.Syntax.Level
import Csic.Core.Syntax.Multi
import Csic.Core.Syntax.Constant
import Csic.Core.Syntax.Term

-- ************************************************************************************************

-- | Checked declaration.
type CDecl = DeclF Void

-- | Pending declaration.
type PDecl = DeclF MetaId

-- | Local declaration (either pending or checked).
type LDecl = Either PDecl CDecl

-- | Unchecked declaration (either type or implementation).
data UDecl
  = UDeclType (Decl (Const ()) (UTerm 0))
  | UDeclImpl Loc DeclName (Impl (UTerm 0))

-- | Applied declaration info.
type DeclInfo m n = (CDecl, PrenexArgs m, Args m n)

-- ************************************************************************************************

-- | Top-level typed declaration, parameterized by metavariables.
type DeclF m = Decl Impl (Term m 0)

-- | Top-level typed declaration, parameterized by implementation and terms.
data Decl fimpl t = Decl
  { declLoc    :: Loc
  , declName   :: DeclName
  , declAccess :: Access
  , declAnnots :: [Annotation]
  , declPrenex :: PrenexParams
  , declType   :: t
  , declImpl   :: fimpl t
  } deriving (Functor, Foldable, Traversable, Generic, NFData)

-- | Level and multi parameter names.
type PrenexParams = ([Name], [Name])

-- | Declaration implementation.
data Impl t
  = IAxiom
  | IDef    (Definition t)
  | IData   DataTypeInfo
  | ICtor   DataCtorInfo
  deriving (Eq, Functor, Foldable, Traversable, Generic, NFData)

-- | Top-level value definition.
data Definition t = Def
  { defKind :: DefKind
  , defRec  :: RecursionKind
  , defImpl :: t
  } deriving (Eq, Functor, Foldable, Traversable, Generic, NFData)

-- ************************************************************************************************

declNameQ :: Decl f t -> QName
declNameQ = relevant . irrTag . declName
{-# INLINE declNameQ #-}

-- | Abstract over a local declaration.
ldecl :: LDecl -> (forall m. DeclF m -> a) -> a
ldecl d f = either f f d
{-# INLINE ldecl #-}

-- | Construct a declaration reference.
declRefInferPrenex :: Loc -> Decl f t -> UTerm n
declRefInferPrenex l d = mkCon (Irr l) $ DeclRef (declName d) prenex
  where
  prenex = bimap (map $ const levelHole) (map $ const multiHole) $ declPrenex d
{-# INLINE declRefInferPrenex #-}

-- | Check if a declaration is a type class instance.
declIsInstance :: CDecl -> Maybe DeclName
declIsInstance d = case declImpl d of
  IDef (Def (DefInstance s) _ _) -> pure s
  _ -> Nothing
{-# INLINE declIsInstance #-}

-- | Check if a declaration has an annotation.
declHasAnnot :: Annotation -> Decl f t -> Bool
declHasAnnot s d = s `elem` declAnnots d
{-# INLINE declHasAnnot #-}

-- | Get the parameter infos of a declaration's type.
declParamInfos :: Decl fimpl (Term m 0) -> [ParamInfo]
declParamInfos = piParams . declType
{-# INLINE declParamInfos #-}

-- | Filter a list of arguments to the explicit arguments of a declaration.
explicitDeclArgs :: Decl fimpl (Term m 0) -> Args m' n -> Args m' n
explicitDeclArgs d = explicitArgs . zipWith (\z r -> r {argInfo = z}) (map paramApplyInfo $ declParamInfos d)
{-# INLINE explicitDeclArgs #-}

-- | Compute the number of nodes in a declaration AST.
declAstSize :: Foldable fimpl => Decl fimpl (Term m 0) -> Sum Int
declAstSize = foldMap termAstSize
{-# INLINE declAstSize #-}

-- | Check if a type is an instantiated type-class.
isTypeClass :: DeclInfo m n -> Maybe (TypeClassInfo, (CDecl, Args m n))
isTypeClass x = case x of
  (d@Decl {declImpl = IData (DataTypeInfo {dataClassInfo = Just z})}, _, args) -> Just (z, (d, args))
  _ -> Nothing
{-# INLINE isTypeClass #-}