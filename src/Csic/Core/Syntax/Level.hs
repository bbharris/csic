module Csic.Core.Syntax.Level where
import Csic.Util.Prelude

-- ************************************************************************************************

type CLevelId = LevelId Void
type PLevelId = LevelId MetaId

type CLevel = Level Void
type PLevel = Level MetaId

-- ************************************************************************************************

-- | Universe level identifier.
newtype LevelId m = LevelIdRaw Int
  deriving newtype (Eq, Ord, NFData, Hashable)

levelIdZero :: LevelId m
levelIdZero = LevelIdRaw 0

levelIdUser :: Int -> LevelId m
levelIdUser i = if i < 0 then error "bad user level" else LevelIdRaw (i + 1)

levelIdMeta :: IsMetaId m => m -> LevelId m
levelIdMeta m = let MetaId i = toMetaId m in LevelIdRaw (-i - 1)
{-# INLINE levelIdMeta #-}

-- ************************************************************************************************

data LevelIdView m
  = LevelZero       -- ^ Base of the universe hierarchy.
  | LevelUser Int   -- ^ User-named basis level (prenex level parameter for a module).
  | LevelMeta m     -- ^ Level metavariable, to be substituted by a basis level expression.
  deriving (Eq, Ord, Functor, Foldable, Traversable)

viewLevelId :: IsMetaId m => LevelId m -> LevelIdView m
viewLevelId (LevelIdRaw i)
  | i < 0 = LevelMeta (fromMetaId (MetaId $ -i - 1))
  | i == 0 = LevelZero
  | otherwise = LevelUser (i - 1)
{-# INLINE viewLevelId #-}

-- | Check if a level identifier is a basis level.
isBasisLevelId :: IsMetaId m => LevelId m -> Either m (LevelId m')
isBasisLevelId x = case viewLevelId x of
  LevelZero -> Right levelIdZero
  LevelUser k -> Right $ levelIdUser k
  LevelMeta k -> Left k
{-# INLINE isBasisLevelId #-}

partitionBasisLevelIds :: IsMetaId m => [(LevelId m, b)] -> ([(m, b)], [(LevelId m', b)])
partitionBasisLevelIds = partitionEithers . map (\(k, n) -> bimap (,n) (,n) $ isBasisLevelId k)
{-# INLINE partitionBasisLevelIds #-}

-- ************************************************************************************************

-- | Constraint between two levels.
type LevelConstraint m = Constraint (Level m)

-- | Generalized universe level expression: max(x_i + n_i, ...).
newtype Level m = LevelRaw {levelMaxes :: LevelMaxes m}
  deriving newtype (Eq, Ord, NFData)

type LevelMaxes m = IntMap (LevelId m) Int

-- ************************************************************************************************

-- | Construct a level that is greater than or equal to a set of offset level identifiers.
levelNormalize :: [(LevelId m, Int)] -> Level m
levelNormalize = levelNormalize' . fromListWith max

levelNormalize' :: LevelMaxes m -> Level m
levelNormalize' xs = LevelRaw $ case toList xs of
  [] -> error "null level"
  (LevelIdRaw 0, n):ys | any ((n <=) . snd) ys -> fromListWith max ys
  _ -> xs

-- ************************************************************************************************

-- | Unknown level to be inferred.
levelHole :: Level ()
levelHole = levelVar 0 (levelIdMeta ())

-- | Construct a level offset from 0.
levelNat :: Int -> Level m
levelNat n = levelVar n levelIdZero

-- | Construct a level offset from a level identifier.
levelVar :: Int -> LevelId m -> Level m
levelVar n k = LevelRaw $ singleton (k, n)

-- | Add a number of successors to a universe level.
levelPlus :: Int -> Level m -> Level m
levelPlus n (LevelRaw xs) = LevelRaw ((+n) <$> xs)

-- ************************************************************************************************

instantiateLevelArgs :: [Level m] -> Level m -> Maybe (Level m)
instantiateLevelArgs xs x = if x == x' then Nothing else Just x'
  where
  x' = levelNormalize' $ foldr fsubst mempty $ toList $ levelMaxes x
  fsubst (k@(LevelIdRaw i), n) = if i == 0
    then insertWith max k n
    else unionWith max ((+n) <$> levelMaxes (xs !! (i - 1)))

type LevelMap a b = HashMap (LevelId a) (Level b)

-- | Substitute levels into a level expression and normalize the result.
substLevelsM :: (Monad f) => (LevelId a -> f (Level b)) -> Level a -> f (Level b)
substLevelsM f = fmap levelNormalize' . foldlM fsubst mempty . toList . levelMaxes
  where
  fsubst res (k, n) = unionWith max res . levelMaxes . levelPlus n <$> f k
{-# INLINABLE substLevelsM #-}

-- | Extract the set of level identifiers in a level expression.
levelsInLevel :: Level a -> HashSet (LevelId a)
levelsInLevel (LevelRaw xs) = fromList (toKeys xs)

-- ************************************************************************************************

vacuousLevelId :: LevelId Void -> LevelId m
vacuousLevelId = coerce
{-# INLINE vacuousLevelId #-}

vacuousLevel :: Level Void -> Level m
vacuousLevel = coerce
{-# INLINE vacuousLevel #-}

vacuousLevelMapV :: LevelMap a Void -> LevelMap a b
vacuousLevelMapV = coerce
{-# INLINE vacuousLevelMapV #-}

-- ************************************************************************************************

instance IsMetaId m => Show (LevelId m) where
  show x = case toMetaId <$> viewLevelId x of
    LevelZero -> "0"
    LevelUser s -> "U" <> show s
    LevelMeta i -> show i

instance IsMetaId m => Show (Level m) where
  showsPrec p x0 = case toList $ levelMaxes x0 of
    [(LevelIdRaw 0, n)] -> shows n
    [(v, 0)] -> shows v
    vs -> showParen (p > 0) $ intercalateS "|" (map fone vs)
    where
    fone (x, n) = shows x . (if n == 0 then id else showString "+" . shows n)