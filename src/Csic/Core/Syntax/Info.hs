module Csic.Core.Syntax.Info where
import Csic.Util.Prelude

-- ************************************************************************************************

-- | Binder information.
data BindInfo = BindInfo
  { bindLoc  :: Loc
  , bindName :: Name
  } deriving (Eq, Ord, Show, Generic, NFData)

-- | Function application info.
data ApplyInfo = ApplyInfo
  { applyLoc      :: Loc
  , applyImplicit :: Maybe Name
  } deriving (Eq, Ord, Show, Generic, NFData)

-- | Parameter info.
data ParamInfo = ParamInfo
  { paramLoc      :: Loc
  , paramImplicit :: Maybe Name
  , paramName     :: Name
  } deriving (Eq, Ord, Show, Generic, NFData)

-- ************************************************************************************************

-- | Construct default bind info.
nullBindInfo :: Loc -> BindInfo
nullBindInfo l = BindInfo l (Name "")

-- | Construct default parameter info.
nullParamInfo :: Loc -> ParamInfo
nullParamInfo loc = ParamInfo loc Nothing (Name "")

-- | Construct default application info.
nullApplyInfo :: Loc -> ApplyInfo
nullApplyInfo l = ApplyInfo l Nothing

-- ************************************************************************************************

-- | Construct an explicit parameter info.
explicitParam :: Loc -> Name -> ParamInfo
explicitParam l = ParamInfo l Nothing

-- | Construct an implicit parameter info.
implicitParam :: Loc -> Name -> ParamInfo
implicitParam l s = ParamInfo l (Just s) s

-- ************************************************************************************************

-- | Construct parameter info from pattern info.
bindParamInfo :: BindInfo -> ParamInfo
bindParamInfo (BindInfo l s) = ParamInfo l Nothing s

-- | Construct parameter info from pattern info.
paramBindInfo :: ParamInfo -> BindInfo
paramBindInfo (ParamInfo l _ s) = BindInfo l s

-- | Construct apply info from parameter info.
paramApplyInfo :: ParamInfo -> ApplyInfo
paramApplyInfo (ParamInfo l imp _) = ApplyInfo l imp

-- | Construct parameter info from apply info.
applyParamInfo :: ApplyInfo -> ParamInfo
applyParamInfo (ApplyInfo l imp) = ParamInfo l imp (fromMaybe (Name "") imp)

-- | Merge two parameter infos into one.
mergeParamInfo :: ParamInfo -> ParamInfo -> ParamInfo
mergeParamInfo = const

-- ************************************************************************************************

-- | Check if an application info matches a parameter info.
doesApplyMatchParam :: ApplyInfo -> ParamInfo -> Bool
doesApplyMatchParam rz pz = applyImplicit rz == paramImplicit pz

-- ************************************************************************************************

-- | Access qualifier for use by frontend import/export mechanisms.
data Access = Private | Public deriving (Eq, Ord, Show, Read, Generic, NFData)
-- TODO move Private to an annotation?

-- | Declaration annotations.
data Annotation
  -- Standard feature annotations:
  = AnnErased       -- ^ Declaration is only usable in erased contexts.
  | AnnInline       -- ^ Inline during backend code generation.
  -- FFI specification annotations:
  | AnnLib String   -- ^ FFI linking library name.
  | AnnSym String   -- ^ FFI linking symbol name.
  | AnnVarargs Int  -- ^ FFI varargs signature specification.
  -- Debug and testing annotations:
  | AnnTrace        -- ^ Trace type-checking the declaration.
  | AnnTest         -- ^ Execute the declaration as a unit test?
  | AnnPrint        -- ^ Print the type-checked declaration in test-suites?
  | AnnError        -- ^ Expect a type-checking error.
  | AnnNoLevelCheck -- ^ Disable universe level checking and assume Type : Type.
  | AnnNoCountCheck -- ^ Disable multiplicity checking and assume ω everywhere.
  | AnnNoPosCheck   -- ^ Disable inductive postivity checks.
  deriving (Eq, Ord, Show, Read, Generic, NFData)

-- ************************************************************************************************

type DeclName = IrrTagged QName IName

-- | Top-level data type info.
data DataTypeInfo = DataTypeInfo
  { dataClassInfo :: Maybe TypeClassInfo
  , dataTypeArity :: Int
  , dataCtorInfos :: [(DeclName, Int)]
  } deriving (Eq, Generic, NFData)

-- | Type-class info.
data TypeClassInfo = TypeClassInfo
  { classBases    :: [(DeclName, [Int])]
  , classMethods  :: [MethodInfo]
  , classDefaults :: [([Name], [Name])]
  } deriving (Eq, Generic, NFData)

-- | Top-level data constructor info.
data MethodInfo = MethodInfo
  { methodName   :: Name
  , methodErased :: Bool
  , methodDeps   :: [Name]
  , methodImps   :: [ParamInfo]
  } deriving (Eq, Generic, NFData)

-- | Top-level data constructor info.
newtype DataCtorInfo = DataCtorInfo
  { ctorDataTypeName :: DeclName
  } deriving newtype (Eq, NFData)

-- ************************************************************************************************

-- | Definition annotation to aid type-checking.
data DefKind
  = DefNormal
  | DefInstance DeclName
  deriving (Eq, Generic, NFData)

-- | Definition recursion annotation.
data RecursionKind
  = RecursionNone
  | RecursionGeneral
  | RecursionGuarded [Int]
  deriving (Eq, Show, Generic, NFData)
