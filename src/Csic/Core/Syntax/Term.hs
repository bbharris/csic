module Csic.Core.Syntax.Term where
import Csic.Util.Prelude
import Csic.Core.Syntax.Info
import Csic.Core.Syntax.Level
import Csic.Core.Syntax.Multi
import Csic.Core.Syntax.Constant
import Unsafe.Coerce (unsafeCoerce)

-- ************************************************************************************************

-- | Checked term (no metavariables).
type CTerm = Term Void

-- | Pending term (with metavariables).
type PTerm = Term MetaId

-- | Unchecked term (with holes).
type UTerm = Term ()

-- ************************************************************************************************

-- | Term parameterized by metavariables and bound variable depth.
newtype Term m n = Term (Term' m n)
  deriving (Eq)

-- | Raw term constructors.
data Term' m n
  = Var (Irr Loc)       {-# UNPACK #-} !(Finite n)
  | App (Irr ApplyInfo) (Term m n) (Multi m) (Term m n)
  | Lam (Irr ParamInfo) (Term m n) (PrenexArg m) (Term m (1 + n))
  | Pit (Irr ParamInfo) (Term m n) (PrenexArg m) (Term m (1 + n))
  | Cas (Irr Loc)       (Term m n) (PrenexArgs m, Multi m) (Term m n) [Branch m n]
  | Con (Irr Loc)       (Constant m)
  | Met (Irr Loc)       m [Term m n]
  deriving (Eq)

-- ************************************************************************************************

-- | Branch of a pattern match.
data Branch m n where
  Branch :: KnownNat k => Irr BindInfo -> Pattern k -> Term m (k + 1 + n) -> Branch m n

-- | Pattern to match against.
data Pattern k where
  PatCtor :: DeclName -> Irr (DataTypeInfo, PatternArgs k) -> Pattern k
  PatWild :: Pattern 0

-- | Pattern argument info.
type PatternArgs k = ListMap (Finite k) (ParamInfo, BindInfo)

-- ************************************************************************************************

mkT :: Term' m n -> Term m n
mkT = Term
{-# INLINE mkT #-}

mkVar :: Irr Loc -> Finite n -> Term m n
mkVar z i = mkT $ Var z i
{-# INLINE mkVar #-}

mkApp :: Irr ApplyInfo -> Term m n -> Multi m -> Term m n -> Term m n
mkApp z f m r = mkT $ App z f m r
{-# INLINE mkApp #-}

mkLam :: Irr ParamInfo -> Term m n -> PrenexArg m -> Term m (1 + n) -> Term m n
mkLam z p m r = mkT $ Lam z p m r
{-# INLINE mkLam #-}

mkPit :: Irr ParamInfo -> Term m n -> PrenexArg m -> Term m (1 + n) -> Term m n
mkPit z p m r = mkT $ Pit z p m r
{-# INLINE mkPit #-}

mkCas :: Irr Loc -> Term m n -> (PrenexArgs m, Multi m) -> Term m n -> [Branch m n] -> Term m n
mkCas z d m r bs = mkT $ Cas z d m r bs
{-# INLINE mkCas #-}

mkCon :: Irr Loc -> Constant m -> Term m n
mkCon z c = mkT $ Con z c
{-# INLINE mkCon #-}

mkMet :: Irr Loc -> m -> [Term m n] -> Term m n
mkMet z m rs = mkT $ Met z m rs
{-# INLINE mkMet #-}

-- ************************************************************************************************

unT :: Term m n -> Term' m n
unT (Term x) = x
{-# INLINE unT #-}

-- ************************************************************************************************

weakenTermTop :: Term m n -> Term m (n + i)
weakenTermTop = coerce
{-# INLINE weakenTermTop #-}

shiftTerm :: forall i n m. (KnownNat i, KnownNat n) => Term m n -> Term m (i + n)
shiftTerm = case sNat @n of
  Zero -> coerce
  Succ _ -> fmapVars (\z (j :: Finite (k + n)) -> mkVar z $ either weaken shift $ separateSum @k j)
{-# INLINABLE shiftTerm #-}

substScope :: forall b m n. (KnownNat b, KnownNat n) => Vector b (Term m n) -> Term m (b + n) -> Term m n
substScope subs = fmapVars $ \z (i :: Finite (k + b + n)) -> case separateSum @k i of
  Left k -> mkVar z (weaken k)
  Right bn -> case separateSum @b bn of
    Left b -> shiftTerm $ vindex subs b
    Right n -> mkVar z (shift n)
{-# INLINABLE substScope #-}

-- ************************************************************************************************

type FiniteMap a b = HashMap (Finite a) (Finite b)

mapFreeVars :: forall n1 n2 m. FiniteMap n1 n2 -> Term m n1 -> Maybe (Term m n2)
mapFreeVars varMap = frec @0
  where
  frec :: forall k. KnownNat k => Term m (k + n1) -> Maybe (Term m (k + n2))
  frec = traverseSubTerms pure pure pure
    (either (Just . weaken) (fmap shift . (`lookup` varMap)) . separateSum @k) frec
{-# INLINABLE mapFreeVars #-}

-- ************************************************************************************************

type FreeVarMap m n = HashMap Int (TermN m n)

data TermN m n where
  TermN :: KnownNat k => Term m p -> TermN m (k + p)

unTermN :: KnownNat n => TermN m n -> Term m n
unTermN (TermN t) = shiftTerm t
{-# INLINE unTermN #-}

shiftTermN :: forall i n m. KnownNat i => TermN m n -> TermN m (i + n)
shiftTermN (TermN @k t) = TermN @(i + k) t
{-# INLINE shiftTermN #-}

substFreeVar :: forall m n. KnownNat n => Finite n -> Term m n -> Term m n -> Term m n
substFreeVar j v = fmapVars $ \z (i :: Finite (k + n)) -> case separateSum @k i of
  Right n | n == j -> shiftTerm v
  _ -> mkVar z i
{-# INLINABLE substFreeVar #-}

substFreeVars :: forall m n. KnownNat n => FreeVarMap m n -> Term m n -> Term m n
substFreeVars subs = if null subs then id else fmapVars $ \z (i :: Finite (k + n)) -> case separateSum @k i of
  Right n | Just t <- lookup (getFinite $ mirror n) subs -> unTermN $ shiftTermN t
  _ -> mkVar z i
{-# INLINABLE substFreeVars #-}

-- ************************************************************************************************

-- | Find a free variable in a term.
findFreeVar :: forall m n. (Finite n -> Bool) -> Term m n -> Bool
findFreeVar f = getAny . frec @0
  where
  frec :: forall k. KnownNat k => Term m (k + n) -> Any
  frec t = case unT t of
    Var _ i -> Any $ either (const False) f $ separateSum @k i
    _ -> foldMapSubTerms frec t
{-# INLINABLE findFreeVar #-}

-- | Find a metavar in a term.
findMetaVar :: forall m n. (m -> Bool) -> Term m n -> Bool
findMetaVar f = getAny . frec @0
  where
  frec :: Term m (k + n) -> Any
  frec t = case unT t of
    Met _ k rs -> Any (f k) <> foldMap frec rs
    _ -> foldMapSubTerms frec t
{-# INLINABLE findMetaVar #-}

-- | Compute the number of nodes in a term AST.
termAstSize :: Term m n -> Sum Int
termAstSize = (+1) . foldMapSubTerms termAstSize
{-# INLINABLE termAstSize #-}

-- ************************************************************************************************

-- | Unfold a term into a function and argument list.
unfoldApps :: Term m n -> (Term' m n, Args m n)
unfoldApps = frec []
  where
  frec rs x = case unT x of
    App z f m r -> frec (Arg (relevant z) m r : rs) f
    x' -> (x', rs)

-- | Fold a function and argument list into a term.
mkApps :: Term m n -> Args m n -> Term m n
mkApps f rs = case rs of
  [] -> f
  (Arg z m r):rs' -> mkApps (mkApp (Irr z) f m r) rs'

-- | Extract the untyped parameters of a type.
piParams :: Term m n -> [ParamInfo]
piParams x = case unT x of
  Pit (Irr z) _ _ rt -> z:piParams rt
  _ -> []

-- ************************************************************************************************

-- | Weak head normal form.
type WHNF m n = (Term' m n, Args m n)

-- | Arguments annotated with implicit application info.
type Args m n = [Arg m n]

-- | Arguments annotated with implicit application info.
data Arg m n = Arg
  { argInfo  :: ApplyInfo
  , argMulti :: Multi m
  , argTerm  :: Term m n
  } deriving (Eq, Generic, NFData)

mapArg :: (Term m n1 -> Term m n2) -> Arg m n1 -> Arg m n2
mapArg f (Arg z m t) = Arg z m (f t)

mapArgM :: Functor f => (Term m n1 -> f (Term m n2)) -> Arg m n1 -> f (Arg m n2)
mapArgM f (Arg z m t) = Arg z m <$> f t

-- | Construct an possibly named argument list.
mkArgs :: Loc -> [(Text, Multi m, Term m n)] -> Args m n
mkArgs l = map (\(s, m, r) -> Arg (ApplyInfo l (if s == "" then Nothing else Just $ Name s)) m r)
{-# INLINABLE mkArgs #-}

-- | Filter to explicit arguments.
explicitArgs :: Args m n -> Args m n
explicitArgs = filter (\r -> isNothing (applyImplicit $ argInfo r))
{-# INLINABLE explicitArgs #-}

-- | Fold a function and argument list into a term.
unWHNF :: WHNF m n -> Term m n
unWHNF (f, rs) = mkApps (mkT f) rs
{-# INLINE unWHNF #-}

-- | Check if a WHNF is a literal value applied to some arguments.
isLiteral :: WHNF m n -> Maybe (Literal, Args m n)
isLiteral = \case
  (Con _ (Literal v), rs) -> Just (v, rs)
  _ -> Nothing
{-# INLINE isLiteral #-}

-- | Check if a WHNF is a named declaration reference applied to some arguments.
isNamed :: WHNF m n -> Maybe (Text, Args m n)
isNamed = \case
  (Con _ (DeclRef s _), rs) -> Just (unName $ qname $ relevant $ irrTag s, rs)
  _ -> Nothing
{-# INLINE isNamed #-}

-- ************************************************************************************************

-- | Dependent binder.
data Binder m n = Binder
  { bindInfo   :: ParamInfo
  , bindParam  :: Term m n
  , bindMulti  :: PrenexArg m
  , bindResult :: Term m (1 + n)
  } deriving (Eq)

-- | Check if a WHNF is a pi-type.
isPiType :: WHNF m n -> Maybe (Binder m n)
isPiType = \case
  (Pit (Irr z) p m r, []) -> Just (Binder z p m r)
  _ -> Nothing
{-# INLINE isPiType #-}

-- ************************************************************************************************

termHole :: Loc -> UTerm n
termHole l = mkMet (Irr l) () []

mkULam :: ParamInfo -> UTerm (1 + n) -> UTerm n
mkULam z = mkLam (Irr z) (termHole $ paramLoc z) (levelHole, multiHole)

mkUCas :: Loc -> UTerm n -> [Branch () n] -> UTerm n
mkUCas l d = mkCas (Irr l) d (mempty, multiHole) (termHole l)

-- ************************************************************************************************

-- | Construct a universe type.
mkTypeUniv :: Loc -> Level m -> Term m n
mkTypeUniv l i = mkCon (Irr l) (TypeUniv i)
{-# INLINE mkTypeUniv #-}

mkDeclRef :: Loc -> DeclName -> PrenexArgs m -> Term m n
mkDeclRef l s args = mkCon (Irr l) (DeclRef s args)
{-# INLINE mkDeclRef #-}

-- ************************************************************************************************

uncheckTerm :: CTerm n -> Term m n
uncheckTerm = unsafeCoerce
{-# INLINE uncheckTerm #-}

instantiateTerm :: forall m. PrenexArgs m -> CTerm 0 -> Term m 0
instantiateTerm (lvls, muls) = fgo . uncheckTerm
  where
  flevel x = fromMaybe x $ instantiateLevelArgs lvls x
  fmulti x = fromMaybe x $ instantiateMultiArgs muls x
  fgo :: Term m n -> Term m n
  fgo = fmapSubTerms flevel fmulti fgo

type PrenexMetas m = (LevelMap m Void, MultiMap m Void)

abstractTerm :: forall m. (IsMetaId m) => PrenexMetas m -> Term m 0 -> Maybe (CTerm 0)
abstractTerm (lvls, muls) = frec
  where
  frec :: Term m n -> Maybe (CTerm n)
  frec = traverseSubTerms (const Nothing) flevel fmulti pure frec

  flevel = substLevelsM $ \k -> case isBasisLevelId k of
    Left _ -> lookup k lvls
    Right k' -> pure $ levelVar 0 k'
  fmulti = substMultisM $ \k -> case isBasisMultiId k of
    Left _ -> lookup k muls
    Right k' -> pure $ multiVar k'
{-# INLINABLE abstractTerm #-}

-- ************************************************************************************************

fmapVars ::
  (forall k. KnownNat k => Irr Loc -> Finite (k + n) -> Term m (k + n')) ->
  Term m n -> Term m n'
fmapVars fvar t = case unT t of
  Var z i        -> fvar @0 z i
  App z x m y    -> mkApp z (fmapVars fvar x) m (fmapVars fvar y)
  Lam z p m r    -> mkLam z (fmapVars fvar p) m (fmapVars fvar r)
  Pit z p m r    -> mkPit z (fmapVars fvar p) m (fmapVars fvar r)
  Cas z d m r bs -> mkCas z (fmapVars fvar d) m (fmapVars fvar r) (fbranch <$> bs)
  Con z c        -> mkCon z c
  Met z k r      -> mkMet z k (fmapVars fvar <$> r)
  where
  fbranch (Branch pz pn b) = Branch pz pn (fmapVars fvar b)
{-# INLINABLE fmapVars #-}

fmapSubTerms ::
  (Level m -> Level m) ->
  (Multi m -> Multi m) ->
  (forall k. KnownNat k => Term m (k + n) -> Term m (k + n)) ->
  Term m n -> Term m n
fmapSubTerms flevel fmulti frec t = case unT t of
  Var {}         -> t
  App z x m y    -> mkApp z (frec @0 x) (fmulti m) (frec @0 y)
  Lam z p m r    -> mkLam z (frec @0 p) (bimap flevel fmulti m) (frec r)
  Pit z p m r    -> mkPit z (frec @0 p) (bimap flevel fmulti m) (frec r)
  Cas z d m r bs -> mkCas z (frec @0 d) (fprenex m) (frec @0 r) ((\(Branch pz pn b) -> Branch pz pn (frec b)) <$> bs)
  Con z c        -> mkCon z (fconst c)
  Met z k r      -> mkMet z k (frec @0 <$> r)
  where
  fprenex = bimap (bimap (fmap flevel) (fmap fmulti)) fmulti
  fconst = \case
    TypeUniv u -> TypeUniv (flevel u)
    DeclRef s (us, ms) -> DeclRef s (flevel <$> us, fmulti <$> ms)
    Literal v -> Literal v
{-# INLINE fmapSubTerms #-}

foldSubTerms :: (forall k. Term m (k + n) -> r -> r) -> r -> Term m n -> r
foldSubTerms frec res t = case unT t of
  Var {}         -> res
  App _ x _ y    -> frec @0 x (frec @0 y res)
  Lam _ p _ r    -> frec @0 p (frec r res)
  Pit _ p _ r    -> frec @0 p (frec r res)
  Cas _ d _ r bs -> frec @0 d (frec @0 r (foldr (\(Branch _ _ b) -> frec b) res bs))
  Con {}         -> res
  Met _ _ r      -> foldr (frec @0) res r
{-# INLINE foldSubTerms #-}

foldMapSubTerms :: (Monoid r) => (forall k. KnownNat k => Term m (k + n) -> r) -> Term m n -> r
foldMapSubTerms frec t = case unT t of
  Var {}         -> mempty
  App _ x _ y    -> frec @0 x <> frec @0 y
  Lam _ p _ r    -> frec @0 p <> frec r
  Pit _ p _ r    -> frec @0 p <> frec r
  Cas _ d _ r bs -> frec @0 d <> frec @0 r <> foldMap (\(Branch _ _ b) -> frec b) bs
  Con {}         -> mempty
  Met _ _ r      -> foldMap (frec @0) r
{-# INLINE foldMapSubTerms #-}

traverseSubTerms :: Monad f =>
  (m -> f m') ->
  (Level m -> f (Level m')) ->
  (Multi m -> f (Multi m')) ->
  (Finite n -> f (Finite n')) ->
  (forall k. KnownNat k => Term m (k + n) -> f (Term m' (k + n'))) ->
  Term m n -> f (Term m' n')
traverseSubTerms fmeta flevel fmulti fvar frec t = case unT t of
  Var z i        -> mkVar z <$> fvar i
  App z x m y    -> mkApp z <$> frec @0 x <*> fmulti m <*> frec @0 y
  Lam z p m r    -> mkLam z <$> frec @0 p <*> bitraverse flevel fmulti m <*> frec r
  Pit z p m r    -> mkPit z <$> frec @0 p <*> bitraverse flevel fmulti m <*> frec r
  Cas z d m r bs -> mkCas z <$> frec @0 d <*> fprenex m <*> frec @0 r <*> traverse fbranch bs
  Con z c        -> mkCon z <$> traverseConst flevel fmulti c
  Met z m r      -> mkMet z <$> fmeta m <*> traverse (frec @0) r
  where
  fprenex = bitraverse (bitraverse (traverse flevel) (traverse fmulti)) fmulti
  fbranch (Branch pz pn b) = Branch pz pn <$> frec b
{-# INLINE traverseSubTerms #-}

-- ************************************************************************************************

instance HasLocation (Term m n) where
  getLoc = getLoc . unT

instance HasLocation (Term' m n) where
  getLoc = \case
    Var z _ -> relevant z
    App z _ _ _ -> applyLoc $ relevant z
    Lam z _ _ _ -> paramLoc $ relevant z
    Pit z _ _ _ -> paramLoc $ relevant z
    Cas z _ _ _ _ -> relevant z
    Con z _ -> relevant z
    Met z _ _ -> relevant z

instance (Eq m) => Eq (Branch m n) where
  Branch @k1 _ ps b == Branch @k2 _ ps' b' = case testEquality (sNat @k1) (sNat @k2) of
    Just e -> gcastWith e $ ps == ps' && b == b'
    Nothing -> False
  {-# INLINE (==) #-}

deriving instance Eq (Pattern k)

-- ************************************************************************************************

instance (NFData m) => NFData (Term m n) where
  rnf (Term t) = case t of
    Var z i        -> rnf z `seq` rnf i
    App z x m y    -> rnf z `seq` rnf x `seq` rnf m `seq` rnf y
    Lam z p m r    -> rnf z `seq` rnf p `seq` rnf m `seq` rnf r
    Pit z p m r    -> rnf z `seq` rnf p `seq` rnf m `seq` rnf r
    Cas z d m r bs -> rnf z `seq` rnf d `seq` rnf m `seq` rnf r `seq` rnf1 bs
    Con z c        -> rnf z `seq` rnf c
    Met z m r      -> rnf z `seq` rnf m `seq` rnf1 r

instance (NFData m) => NFData (Branch m n) where
  rnf (Branch z ps b) = rnf z `seq` rnf ps `seq` rnf b

instance NFData (Pattern k) where
  rnf = \case
    PatWild -> ()
    PatCtor s ps -> rnf s `seq` rnf ps