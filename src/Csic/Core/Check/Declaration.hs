module Csic.Core.Check.Declaration
( checkDecl
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Core.Check.Term
import Data.List (nub)
import qualified Data.List as L
import Csic.Core.Unify.Solve

-- ************************************************************************************************

-- | Check a declaration.
checkDecl :: UDecl -> CheckM ()
checkDecl ud = case ud of
  -- Check a declaration type.
  UDeclType d -> wrapError $ do
    -- Verify prenex parameter names match those registered in the environment.
    prenex <- gets penvPrenex
    unless (declPrenex d == prenex) $ checkError (declLoc d) "invalid declaration prenex parameters"
    -- Check the type.
    (ty, _) <- checkType emptyTCtx (declType d)
    -- Register local declaration as an axiom (until implementation is checked).
    registerPDecl d {declType = ty, declImpl = IAxiom}
  -- Check a declaration implementation.
  UDeclImpl l s x -> wrapError $ do
    -- Lookup the previously checked declaration type assumption.
    d <- lookupPDecl l s
    -- Check the implementation.
    x' <- withErrDecls "against type" [d] $ case x of
      -- Check an axiom.
      IAxiom -> pure IAxiom
      -- Check a definition term.
      IDef y -> do
        t' <- checkTerm emptyTCtx (defImpl y) (declType d)
        pure $ IDef y {defImpl = t'}
      -- Check a data type.
      IData (DataTypeInfo classInfo _ ctors) -> do
        -- Check type-class info.
        classInfo' <- mapM (checkClassInfo d) classInfo
        -- Check data type is well-formed.
        (arity, tyUniv) <- checkDataType l d
        -- Check each constructor is well-formed.
        ctors' <- forM ctors $ \(sc, _) -> lookupPDecl l sc >>= \case
          cd@Decl {declImpl = ICtor yc} | s == ctorDataTypeName yc ->
            (sc,) <$> checkCtorType s arity tyUniv (declType cd)
          _ -> checkError l "invalid data type constructor reference"
        pure $ IData $ DataTypeInfo classInfo' arity ctors'
      -- Check a constructor.
      ICtor z -> pure $ ICtor z
    -- Update declaration with implementation.
    registerPDecl d {declImpl = x'}
  where
  wrapError = withError (CErrUDecls "while type-checking" [ud]:)

-- ************************************************************************************************

checkDataType :: Loc -> PDecl -> CheckM (Int, Located PLevel)
checkDataType l d = gets (typeArity emptyPCtx (declType d)) >>= \case
  (n, Just u) -> pure (n, u)
  _ -> checkError l "invalid data type sort"

typeArity :: KnownNat n => PCtx n -> PTerm n -> PEnv -> (Int, Maybe (Located PLevel))
typeArity ctx ty env = case evalToWHNF (initFuel env) env ctx ty of
  (Pit (Irr z) _ _ rt, []) -> first (+1) $ typeArity (snd $ consPCtx z ctx) rt env
  (Con (Irr l) (TypeUniv u), []) -> (0, Just (locate l u))
  _ -> (0, Nothing)

-- | Check a constructor type is syntactically valid.
checkCtorType :: (KnownNat n) => DeclName -> Int -> Located PLevel -> PTerm n -> CheckM Int
checkCtorType dataTypeName skipParams dataTypeUniv ty = case unT ty of
  Pit (Irr z) _ (u, _) rt -> do
    -- Check universe level of all real constructor parameters fits in data type universe.
    when (skipParams == 0) $ solveConstraints typeChecker
      [ CLevel (Constraint (locate (paramLoc z) u) CConv dataTypeUniv)
      ]
    checkCtorType dataTypeName (skipParams - 1) dataTypeUniv rt
  -- Check the result type is the type being defined, instantiated with uniform parameters.
  _ -> case unfoldApps ty of
    (Con _ (DeclRef s _), rs) | s == dataTypeName -> do
      forM_ (zip rs [0..]) $ \(arg, j) -> case unT (argTerm arg) of
        Var _ i | getFinite (mirror i) == j -> pure ()
        _ -> checkError (applyLoc $ argInfo arg) "invalid constructor result type arg"
      pure (-skipParams)
    (f, _) -> checkError (getLoc f) "invalid constructor result type head"

-- ************************************************************************************************

checkClassInfo :: PDecl -> TypeClassInfo -> CheckM TypeClassInfo
checkClassInfo d z = do
  bases <- nub <$> checkBaseClassInfo emptyPCtx [] (declType d)
  pure $ z {classBases = bases}

checkBaseClassInfo :: forall n. (KnownNat n) => PCtx n -> [(Int, Int)] -> PTerm n -> CheckM [(DeclName, [Int])]
checkBaseClassInfo ctx varMap ty = evalM ctx ty >>= \case
  (Pit (Irr z) pt _ rt, []) -> do
    bases <- isTypeClassM ctx pt >>= \case
      Just (baseInfo, (baseDecl, args)) | isJust (paramImplicit z) -> do
        let expArgs = explicitDeclArgs baseDecl args
        args' <- forM expArgs $ \arg -> case unT (argTerm arg) of
          Var _ i -> case L.lookup (getFinite (mirror i)) varMap of
            Just j -> pure j
            Nothing -> checkError (applyLoc $ argInfo arg) "base class argument must reference an explicit parameter"
          _ -> checkError (applyLoc $ argInfo arg) "base class argument must be a variable"
        let baseBases = map (second (map (args' !!))) (classBases baseInfo)
        pure $ baseBases ++ [(declName baseDecl, args')]
      _ -> pure []
    let varMap' = if isJust (paramImplicit z) then varMap else (getFinite (finite @n @(n + 1)), length varMap):varMap
    (bases <>) <$> checkBaseClassInfo (snd $ consPCtx z ctx) varMap' rt
  _ -> pure []
