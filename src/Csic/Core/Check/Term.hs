module Csic.Core.Check.Term
( typeChecker, checkType, checkTerm
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Core.Unify.Solve
import Data.List (partition)

-- ************************************************************************************************

-- | Unification type-checker interface.
typeChecker :: TypeChecker
typeChecker = TypeChecker checkTerm fapply
  where
  fapply ctx x0 actType0 piM expType = do
    let argInfo = MetaArgsImplicit $ piM >>= paramImplicit . bindInfo
    CheckedTerm x actType <- applyMetaArgs ctx argInfo (CheckedTerm x0 actType0)
    unifySubTypeM ctx (getLoc x) " (deferred implicits)" actType expType
    pure x

solveConstraintsC :: [UConstraint] -> CheckM ()
solveConstraintsC = solveConstraints typeChecker

-- ************************************************************************************************

-- | Type-check a type term.
checkType :: (KnownNat n) => TCtx n -> UTerm n -> CheckM (PTerm n, PLevel)
checkType ctx x = do
  let l = getLoc x
  u <- levelVar 0 <$> genMetaLevelId l
  (,u) <$> checkTerm ctx x (mkTypeUniv l u)
{-# INLINABLE checkType #-}

-- | Check that a term has a specific type.
checkTerm :: (KnownNat n) => TCtx n -> UTerm n -> PTerm n -> CheckM (PTerm n)
checkTerm ctx x expectedType = do
  -- Check if the expected type actually is an implicit pi-type.
  x' <- implicitPiType (pCtx ctx) expectedType >>= \case
    Just (Just pit) -> case unT x of
      -- That does NOT match lambda implicit application info.
      Lam (Irr z') _ _ _ | paramImplicit z == paramImplicit z' -> pure x
      -- Wrap the term with the implicit parameter lambda.
      _ -> pure $ mkULam z (shiftTerm x)
      where
      z = bindInfo pit
    _ -> pure x
  -- Check the (potentially implicit-lambda-wrapped) term.
  checkTerm' ctx x' expectedType
{-# INLINABLE checkTerm #-}

-- | Check that a term has a specific type.
checkTerm' :: (KnownNat n) => TCtx n -> UTerm n -> PTerm n -> CheckM (PTerm n)
checkTerm' ctx x expectedType = case unT x of
  -- Check a lambda.
  Lam (Irr z) _ _ b -> do
    -- Ensure the expected type is a pi-type.
    Binder _ pt (u, m) rt <- ensurePiType ctx z expectedType
    -- Check the lambda body.
    bctx <- consTCtxM z pt ctx
    b' <- checkTerm bctx b rt
    pure $ mkLam (Irr z) pt (u, m) b'
  -- Check an applied pattern-matching lambda (e.g. let-binding and/or case split).
  Cas (Irr l) d (_, m) _ bs -> do
    -- Infer the type of the scrutinee.
    m' <- checkMulti l m
    CheckedTerm d' pt' <- inferTerm ctx (MetaArgsImplicit Nothing) d
    -- If the scrutinee is a variable, we can substitute that variable in the dependent result type.
    let discVar = case unT d' of Var _ i -> Just i; _ -> Nothing
    let rt' = expectedType
    -- Check the dependent branches.
    (di, bs') <- checkBranches ctx l (DepCaseType discVar pt' m' rt') bs
    pure $ mkCas (Irr l) d' (maybe mempty discDataPrenex di, m') rt' bs'
  -- Generate metavariable for an inference hole.
  Met (Irr l) () _ -> genMetaVarM ctx l expectedType
  -- Otherwise infer the term's actual type.
  _ -> do
    x0 <- inferTerm' ctx x
    -- Ensure the actual type is a sub-type of the expected type.
    let checkSubTyping x' = unifySubTypeM ctx (getLoc x) "" (ctype x') expectedType >> pure (cterm x')
    -- Check if the actual type is an implicit pi-type.
    implicitPiType (pCtx ctx) (ctype x0) >>= \case
      -- If the actual type is definitely implicit, extract implicit pi-type info from the expected type.
      Just (Just _) -> implicitPiType (pCtx ctx) expectedType >>= \case
        -- If implicit pi-type info of the expected type is available now, apply implicit arguments as needed.
        Just piM -> applyMetaArgs ctx (MetaArgsImplicit $ piM >>= paramImplicit . bindInfo) x0 >>= checkSubTyping
        -- Otherwise we must defer application of implicit args until the expected type is resolved.
        Nothing -> do
          -- Generate a metavariable with the expected type.
          m <- genMetaVarM ctx (getLoc x) expectedType
          -- Generate a deferred implicit argument application constraint.
          solveConstraintsC [CImplicit $ Sized $ ImplicitConstraint ctx m (cterm x0) (ctype x0) expectedType]
          pure m
      -- If the actual type is unknown or an explicit pi-type, do not generate implicit arguments.
      _ -> checkSubTyping x0

-- ************************************************************************************************

-- | Type-checking judgement giving a type to a term.
data CheckedTerm n = CheckedTerm
  { cterm :: PTerm n
  , ctype :: PTerm n
  }

-- | Infer the type of a term, and apply implicit arguments as needed.
inferTerm :: (KnownNat n) => TCtx n -> MetaArgInfo -> UTerm n -> CheckM (CheckedTerm n)
inferTerm ctx metaArgInfo x = inferTerm' ctx x >>= applyMetaArgs ctx metaArgInfo

-- | Infer the type of a term.
inferTerm' :: (KnownNat n) => TCtx n -> UTerm n -> CheckM (CheckedTerm n)
inferTerm' ctx t = case unT t of
  -- Check a free variable.
  Var (Irr l) i -> do
    let (_, pt) = indexTCtx i ctx
    pure $ CheckedTerm (mkVar (Irr l) i) pt
  -- Check a regular function application.
  App (Irr z) func _ arg -> do
    let l = applyLoc z
    -- Infer the type of the function and apply metavariables for implicit arguments that preceed this argument.
    CheckedTerm func' funcType <- inferTerm ctx (MetaArgsImplicit $ applyImplicit z) func
    -- Unpack the function pi-type, generating a fresh metavariable pi-type constraint if needed.
    Binder pz pt (_, m') rt <- ensurePiType ctx (applyParamInfo z) funcType
    -- Ensure implicit argument matches an implicit pi-type parameter.
    when (applyImplicit z /= paramImplicit pz) $
      checkError l $ "invalid application implicitness "
        <> show (applyImplicit z) <> ", expecting " <> show (paramImplicit pz)
    -- Check the argument against the parameter type.
    arg' <- checkTerm ctx arg pt
    -- Substitute the checked argument into the result type.
    pure $ CheckedTerm (mkApp (Irr z) func' m' arg') (substScope (vsingleton arg') rt)
  -- Check a dependent function type.
  Pit (Irr z) pt (u, m) rt -> do
    let l = paramLoc z
    u' <- checkLevel l u
    m' <- checkMulti l m
    (pt', pu) <- checkType ctx pt
    rctx <- consTCtxM z pt' ctx
    (rt', ru) <- checkType rctx rt
    solveConstraintsC
      [ CLevel (Constraint (locate (getLoc pt') pu) CConv (locate l u'))
      , CLevel (Constraint (locate (getLoc rt') ru) CConv (locate l u'))
      ]
    pure $ CheckedTerm (mkPit (Irr z) pt' (u', m') rt') (mkTypeUniv l u')
  -- Check a constant.
  Con (Irr l) c -> case c of
    TypeUniv u -> do
      u' <- checkLevel l u
      pure $ CheckedTerm (mkTypeUniv l u') (mkTypeUniv l (levelPlus 1 u'))
    DeclRef s args -> do
      d <- lookupLDecl l s
      checkPrenexArgs l args >>= instantiateLDeclRef l d
    Literal v -> CheckedTerm (mkCon (Irr l) (Literal v)) <$>
      liftE (lookupTermPrimE l (Name $ literalTypeName v) mempty)
  -- Otherwise check the term against a fresh type metavariable.
  _ -> do
    u <- levelVar 0 <$> genMetaLevelId (getLoc t)
    ty <- genFreshMetaType ctx (getLoc t) u
    t' <- checkTerm ctx t ty
    pure $ CheckedTerm t' ty

-- ************************************************************************************************

checkLevel :: Loc -> Level () -> CheckM PLevel
checkLevel l u = do
  xs <- forM (toList $ levelMaxes u) $ \(k, n) -> case isBasisLevelId k of
    Right k' -> pure (k', n)
    Left () -> (,n) <$> genMetaLevelId l
  pure $ levelNormalize xs

checkMulti :: Loc -> Multi () -> CheckM PMulti
checkMulti l m = do
  xs <- forM (multiSOP m) $ \(vs, n) -> fmap (,n) $ forM vs $ \k -> case isBasisMultiId k of
    Right k' -> pure k'
    Left _ -> genMetaMultiId l
  pure $ multiNormalize xs

-- ************************************************************************************************

-- | Dependent case type.
data DepCaseType n = DepCaseType
  { caseDiscVar  :: Maybe (Finite n)
  , caseDiscType :: PTerm n
  , caseMulti    :: PMulti
  , caseResult   :: PTerm n
  }

-- | Type-check dependent pattern-matching lambda branches.
checkBranches :: (KnownNat n) =>
  TCtx n -> Loc ->  DepCaseType n -> [Branch () n] -> CheckM (Maybe (DiscInfo n), [Branch MetaId n])
checkBranches ctx l dty bs = do
  -- Check the branches.
  case bs of
    [] -> do
      -- Empty branches can only match against an empty data type.
      di <- dataTypeDiscInfo ctx l (caseDiscType dty)
      unless (null (dataCtorInfos $ discDataInfo di)) $
        checkErrorN l "expected empty data type" . (:[]) =<<
          ptermErrorM (pCtx ctx) "discriminator type" (caseDiscType dty)
      pure (Just di, [])
    b0:_ -> do
      -- Check if the first branch has a constructor pattern.
      diE <- case b0 of
        Branch _ (PatWild {}) _ -> pure $ Left True
        Branch _ (PatCtor sc _) _ -> do
          -- Lookup the constructor declaration.
          ctorDecl <- lookupLDecl l sc
          (actualDiscDecl, _) <- ctorDataTypeInfo l ctorDecl
          -- Unify the actual discriminator type with the expected discriminator type.
          actualDiscType <- cterm <$> inferTerm ctx MetaArgsAll (ldecl actualDiscDecl $ declRefInferPrenex l)
          unifySubTypeM ctx l " (discriminator)" actualDiscType (caseDiscType dty)
          -- Extract the data type discriminator info.
          di <- dataTypeDiscInfo ctx l actualDiscType
          pure $ Right (di, map fst $ dataCtorInfos (discDataInfo di))
      -- Check each branch.
      (exhaustion', rbs') <- foldM (checkBranch ctx dty) (diE, []) bs
      -- Check for exhaustion.
      case exhaustion' of
        Left _ -> pure ()
        Right (_, cs) -> unless (null cs) $ checkError l $ "missing branches for " <> show cs
      pure (fst <$> rightToMaybe diE, reverse rbs')

type BranchResults n = (Either Bool (DiscInfo n, [DeclName]), [Branch MetaId n])

-- | Type-check a dependent pattern-matching lambda branch.
checkBranch :: (KnownNat n) =>
  TCtx n -> DepCaseType n -> BranchResults n -> Branch () n -> CheckM (BranchResults n)
checkBranch ctx dty (diE, res) (Branch @k (Irr z) pattern body) = do
  let l = bindLoc z
  -- Bind the full pattern discriminator into the context (e.g. for both variable and @-patterns).
  pctx <- consTCtxM (bindParamInfo z) (caseDiscType dty) ctx
  let rt = shiftTerm @(k + 1) (caseResult dty)
  -- Check the branch pattern.
  (diE', bctx', rt', pattern') <- case pattern of
    -- Wild pattern has nothing left to do.
    PatWild -> do
      -- Check for relevance.
      diE' <- case diE of
        Left pending -> do
          unless pending $ checkError l "useless default branch"
          pure $ Left False
        Right (di, cs) -> do
          when (null cs) $ checkError l "useless default branch"
          pure $ Right (di, [])
      pure (diE', pctx, rt, pattern)
    -- Constructor pattern must match against the discriminator type.
    PatCtor sc (Irr (_, argMap)) -> do
      -- Check for branch relevance.
      (di, csLeft) <- fromMaybeM (checkError l "useless constructor branch") (rightToMaybe diE)
      let (found, csLeft') = partition (== sc) csLeft
      when (null found) $ checkError l $ "constructor " <> show sc <> " not in " <> show csLeft
      -- Instantiate the constructor type with the discriminator type's arguments.
      ctorDecl <- lookupLDecl l sc
      CheckedTerm ctorRef ctorType0 <- instantiateLDeclRef @0 l ctorDecl (discDataPrenex di)
      ctorType <- applyPiType (pCtx ctx) l (shiftTerm ctorType0) (discDataArgs di)
      -- Add constructor type parameters into context.
      let rpats = (\i -> maybe (nullBindInfo l) snd $ lookup i argMap) <$> vindices
      (bctx, rmuls) <- checkCtorDiscParams pctx l (caseMulti dty) (vreverse rpats) (shiftTerm ctorType)
      let
        -- Construct discriminator arguments referencing the constructor parameters.
        discCtorTerms = vtoList $ mkVar (Irr l) . weaken . mirror <$> vindices @k
        discCtorArgs = zipWith (Arg (nullApplyInfo l)) (vtoList rmuls) discCtorTerms
        disc = mkApps (shiftTerm ctorRef) (map (mapArg shiftTerm) (discDataArgs di) <> discCtorArgs)
        -- Construct and apply dependent context and result type substitutions.
        (bctx', rt') = case caseDiscVar dty of
          Nothing -> (bctx, rt)
          Just i -> (insertLetBind (shift i) disc bctx, substFreeVar (shift i) disc rt)
      pure (Right (di, csLeft'), bctx', rt', PatCtor sc (Irr (discDataInfo di, argMap)))
  -- Check the branch body.
  body' <- checkTerm bctx' body rt'
  pure (diE', Branch (Irr z) pattern' body':res)

checkCtorDiscParams :: forall k n. (KnownNat n, KnownNat k) =>
  TCtx n -> Loc -> PMulti -> Vector k BindInfo -> PTerm n -> CheckM (TCtx (k + n), Vector k PMulti)
checkCtorDiscParams ctx l multiMask pats ty = case sNat @k of
  Zero -> evalM (pCtx ctx) ty >>= \case
    (Pit {}, _) -> checkError l "too few constructor parameters"
    _ -> pure (ctx, vempty)
  Succ _ -> evalM (pCtx ctx) ty >>= \case
    (Pit _ pt (_, m) rt, []) -> do
      let m' = multiTimes m multiMask
      let z = bindParamInfo (vhead pats)
      bctx <- consTCtxM z pt ctx
      second (vcons m') <$> checkCtorDiscParams bctx l multiMask (vtail pats) rt
    _ -> checkError l "too many constructor parameters"

-- ************************************************************************************************

checkPrenexArgs :: Loc -> PrenexArgs () -> CheckM (PrenexArgs MetaId)
checkPrenexArgs l = bitraverse (mapM (checkLevel l)) (mapM (checkMulti l))

-- | Instantiate a local declaration reference.
instantiateLDeclRef :: (KnownNat n) => Loc -> LDecl -> PrenexArgs MetaId -> CheckM (CheckedTerm n)
instantiateLDeclRef l dL args = do
  d' <- liftEither $ instantiateLDecl l dL args
  pure $ CheckedTerm (mkDeclRef l (declName d') args) (shiftTerm $ declType d')

-- ************************************************************************************************

-- | Check that a type is a sub-type of another type.
unifySubTypeM :: KnownNat n => TCtx n -> Loc -> String -> PTerm n -> PTerm n -> CheckM ()
unifySubTypeM ctx _l _msg gotType expType =
  solveConstraintsC [CStuck $ Sized $ Stuck (pCtx ctx) gotType UnifyLEQ expType]
  {-
  let err = CErrTerms "type unification failure" l ErrorInfo
        [ ("inferred type" <> msg, ErrTerm env (pCtx ctx) gotType)
        , ("expected type", ErrTerm env (pCtx ctx) expType)
        ]
  env' <- withError (err:) $ liftEither $ do
  -}

-- ************************************************************************************************

genFreshMetaType :: KnownNat n => TCtx n -> Loc -> PLevel -> CheckM (PTerm n)
genFreshMetaType ctx l u = genMetaVarM ctx l (mkTypeUniv l u)

-- | Generate a fresh metavariable with an optionally fixed type.
genMetaVarM :: KnownNat n => TCtx n -> Loc -> PTerm n -> CheckM (PTerm n)
genMetaVarM ctx l ty = do
  ty' <- evalM (pCtx ctx) ty
  if isEqualType ty'
    then do
      refl <- liftE $ lookupUTermPrimE l (Name "refl")
      checkTerm ctx refl ty
    else do
      (k, t) <- genTermMetaVar ctx l ty
      gets (isDecl ty' >=> isTypeClass) >>= \case
        Just (_, (c, cargs)) ->
          solveConstraintsC [CInstance (Sized $ InstanceConstraint ctx l k c (explicitDeclArgs c cargs) ty)]
        _ -> pure ()
      pure t

isEqualType :: WHNF m n -> Bool
isEqualType x = case isNamed x of
  Just ("Equal", [_, _, _]) -> True
  _ -> False

-- ************************************************************************************************

-- | Ensure a type is a pi-type.
ensurePiType :: KnownNat n => TCtx n -> ParamInfo -> PTerm n -> CheckM (Binder MetaId n)
ensurePiType ctx z ty = evalM (pCtx ctx) ty >>= maybe (genPiType ctx z ty) pure . isPiType

-- | Generate a metavariable pi-type.
genPiType :: KnownNat n => TCtx n -> ParamInfo -> PTerm n -> CheckM (Binder MetaId n)
genPiType ctx z expType = do
  let l = paramLoc z
  u <- levelVar 0 <$> genMetaLevelId l
  m <- multiVar <$> genMetaMultiId l
  pt <- genFreshMetaType ctx l u
  rctx <- consTCtxM z pt ctx
  rt <- genFreshMetaType rctx l u
  let pit = Binder z pt (u, m) rt
  unifySubTypeM ctx l " (generated pi-type)" (mkPit (Irr z) pt (u, m) rt) expType
  pure pit

applyPiType :: KnownNat n => PCtx n -> Loc -> PTerm n -> Args MetaId n -> CheckM (PTerm n)
applyPiType ctx l x = \case
  [] -> pure x
  arg:args -> evalM ctx x >>= \case
    (Pit _ _ _ rt, []) -> applyPiType ctx l (substScope (vsingleton $ argTerm arg) rt) args
    _ -> checkError l "invalid pi-type application"

-- ************************************************************************************************

data MetaArgInfo
  = MetaArgsImplicit (Maybe Name)
  | MetaArgsAll

-- | Apply a function term to fresh metavariable arguments.
applyMetaArgs :: forall n. KnownNat n => TCtx n -> MetaArgInfo -> CheckedTerm n -> CheckM (CheckedTerm n)
applyMetaArgs ctx0 info x = frec [] vempty (pCtx ctx0) (ctype x)
  where
  frec :: KnownNat k =>
    Args MetaId n -> Vector k (PTerm n) -> PCtx (k + n) -> PTerm (k + n) -> CheckM (CheckedTerm n)
  frec rargs rsubs ctx ty = evalM ctx ty >>= \case
    (Pit (Irr pz) pt (_, m) rt, []) | shouldGenMetaVarForParam info pz -> do
      let l = getLoc (cterm x)
      meta <- genMetaVarM ctx0 l (substScope rsubs pt)
      let rz = (paramApplyInfo pz) {applyLoc = l}
      frec (Arg rz m meta : rargs) (vcons meta rsubs) (snd $ consPCtx pz ctx) rt
    _ -> pure $ CheckedTerm (mkApps (cterm x) (reverse rargs)) (substScope rsubs ty)

-- | Determine if an implicit argument should be generated.
shouldGenMetaVarForParam :: MetaArgInfo -> ParamInfo -> Bool
shouldGenMetaVarForParam info p = case info of
  MetaArgsImplicit s -> isJust (paramImplicit p) && paramImplicit p /= s
  MetaArgsAll -> True
