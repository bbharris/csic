module Csic.Core.Check.Level
( solveLevelConstraints
) where
import Csic.Util.Prelude
import Csic.Core.Syntax.Level
import qualified Data.HashMap.Strict as M

-- ************************************************************************************************

-- | Level-checking monad.
type LevelM = Either (String, [(Loc, String)])

-- | Solve a set of universe level constraints, producing a set of level metavariable substitutions.
solveLevelConstraints :: Bool ->
  Int -> LevelMetaLocs -> [LevelConstraint MetaId] -> LevelM (LevelMap MetaId Void)
solveLevelConstraints debug numUserIds metaLocs0 constraints0 = do
  traceIf debug (showTable "input" constraints0) pure ()
  -- Eliminate max levels on right side of constraints.
  let (metaLocs, constraints, maxConstraints, maxSubs) = eliminateRightSideMaxLevels metaLocs0 constraints0
  let numMetas = allocNext metaLocs
  -- Build graph from input constraints.
  let graph = buildGraph (maxConstraints <> map (first $ substMaxFactors maxSubs) constraints)
  traceIf debug (showTable "graph" $ toList graph) pure ()
  -- Compute minimum distance from each vertex to each other vertex.
  dists <- stratifyGraph (1 {-LevelZero-} + numUserIds + numMetas) graph
  -- Partition distances into basis and derived levels.
  let (distDerived, distBasis) = first (map $ first levelIdMeta) $ partitionBasisLevelIds $ toList dists
  traceIf debug (showTable "dist-to-basis" distBasis) pure ()
  traceIf debug (showTable "dist-to-derived" distDerived) pure ()
  -- Ensure level zero is unconstrained.
  case lookup levelIdZero dists of
    Nothing -> pure ()
    Just dists0 -> unless (all (== 0) dists0) $
      Left ("invalid level zero constraints", concat $ mapMaybe ferr constraints0)
      where
      ferr c@(Constraint _ _ u2) = if unLoc u2 == levelNat 0
        then Just (constraintErrorMsg c) else Nothing
  -- Check basis levels are independent.
  basisMap <- foldM (checkBasisLevel dists) mempty distBasis
  -- Compute substitutions to express derived levels in terms of basis levels.
  levelSubs <- fromList <$> mapM (checkDerivedLevel basisMap) distDerived
  traceIf debug (showTable "output" $ toList levelSubs) pure ()
  -- Ensure all metavariables were resolved.
  let metaIds = map (levelIdMeta . MetaId) [0.. allocNext metaLocs - 1]
  let metaErrors = mapMaybe (fcheck levelSubs) (zip metaIds $ reverse $ allocV metaLocs)
  unless (null metaErrors) $ Left ("ambiguous level metavariables", metaErrors)
  -- Verify final substitutions preserve the original constraints.
  let validationErrors = mapMaybe (fvalidate levelSubs) constraints0
  unless (null validationErrors) $ Left ("invalid level constraints", concat validationErrors)
  pure levelSubs
  where
  fcheck subs (k, l) = case lookup k subs of
    Just _ -> Nothing
    Nothing -> Just (l, "unresolved " <> show k)
  fvalidate subs c = do
    c' <- traverse (substLevelsM (`lookup` subs)) c
    if isValidLevelConstraint c' then Nothing else Just (constraintErrorMsg' c c')

-- ************************************************************************************************

type Graph = HashMap PLevelId (HashMap PLevelId Int)

type SimpleLevelConstraint = (PLevel, (PLevelId, Int))

buildGraph :: [SimpleLevelConstraint] -> Graph
buildGraph = foldl' (\g -> foldl' addEdge g . constraintEdges) mempty
  where
  addEdge g (v1, e, v2) = alterHMap2 max v1 v2 e g
  constraintEdges (u1, (v2, n2)) = [(v1, n1 - n2, v2) | (v1, n1) <- toList (levelMaxes u1)]

-- ************************************************************************************************

type Edge = (PLevelId, Int, PLevelId)

type LevelMetaLocs = AllocN [Loc]
type MaxFactorSubs = HashMap [(PLevelId, Int)] PLevelId

eliminateRightSideMaxLevels :: LevelMetaLocs -> [LevelConstraint MetaId] ->
  (LevelMetaLocs, [SimpleLevelConstraint], [SimpleLevelConstraint], MaxFactorSubs)
eliminateRightSideMaxLevels = foldl' fcheck . (,[],[],mempty)
  where
  fcheck res (Constraint u1 CEqual u2) = fconv (fconv res u1 u2) u2 u1
  fcheck res (Constraint u1 CConv u2) = fconv res u1 u2

  fconv (metaLocs, resNorm, resMax, subs) u1 u2 = case levelMaxFactor (unLoc u2) of
    Left (k2, n2) -> (metaLocs, (unLoc u1, (k2, n2)) : resNorm, resMax, subs)
    Right (n2, xs2) ->
      (metaLocs', (unLoc u1, (k2, n2)) : resNorm, (unLoc u2, (k2, n2)) : resMax, insert xs2 k2 subs)
      where
      (k2, metaLocs') = case lookup xs2 subs of
        Just k -> (k, metaLocs)
        Nothing -> (levelIdMeta k, (getLoc u2:) <$> metaLocs2)
          where
          (k, metaLocs2) = allocate1 MetaId metaLocs

substMaxFactors :: MaxFactorSubs -> PLevel -> PLevel
substMaxFactors subs u = case levelMaxFactor u of
  Right (n, xs) | Just v <- lookup xs subs -> levelVar n v
  _ -> u

levelMaxFactor :: PLevel -> Either (PLevelId, Int) (Int, [(PLevelId, Int)])
levelMaxFactor x = case toList $ levelMaxes x of
  [(k, n)] -> Left (k, n)
  xs -> let n = minimum (map snd xs) in Right (n, map (second (flip (-) n)) xs)

edgeConstraint :: Edge -> LevelConstraint MetaId
edgeConstraint (v1, w, v2) =
  -- TODO locations
  Constraint (locate nullLoc $ levelVar n1 v1) CConv (locate nullLoc $ levelVar n2 v2)
  where
  (n1, n2) = if w >= 0 then (w, 0) else (0, -w)

edgeError :: Edge -> [(Loc, String)]
edgeError = constraintErrorMsg . edgeConstraint

-- ************************************************************************************************

type Distances = HashMap PLevelId (HashMap PLevelId Int)
type Paths = HashMap PLevelId (HashMap PLevelId (PLevelId, Int))

-- Compute minimum distances from each level to each other level (inverted parallel Bellman-Ford).
stratifyGraph :: Int -> Graph -> LevelM Distances
stratifyGraph numVerts graph = relaxGraph 0 mempty startDists
  where
  -- Initialize 0 distance from each starting vertex to itself.
  startDists = fromList $ map (\v -> (v, singleton (v, 0))) $ toKeys graph

  relaxGraph pass preds dists = do
    (preds', dists', progress) <- M.foldlWithKey (relaxVertexP $ pass > numVerts) (Right (preds, dists, False)) graph
    if progress then relaxGraph (pass + 1) preds' dists' else pure dists'

  relaxVertexP lastPass st v edges = do
    (preds, dists, progress) <- st
    let vdists = fromMaybe mempty (lookup v dists)
    M.foldlWithKey (relaxVertex1 lastPass v edges) (Right (preds, dists, progress)) vdists

  relaxVertex1 lastPass v1 edges st v0 dist1 = M.foldlWithKey (relaxEdge lastPass v0 v1 dist1) st edges

  relaxEdge lastPass v0 v1 dist1 st v2 weight = do
    (preds, dists, progress) <- st
    let dist2Cur = fromMaybe (-1) $ lookup v2 dists >>= lookup v0
    let dist2New = dist1 + weight
    if dist2New > dist2Cur then do
      when lastPass $ Left ("universe cycle", concatMap edgeError (findCycle v0 preds v1 v2))
      pure (insertHMap2 v2 v0 (v1, weight) preds, insertHMap2 v2 v0 dist2New dists, True)
    else
      pure (preds, dists, progress)

findCycle :: PLevelId -> Paths -> PLevelId -> PLevelId -> [Edge]
findCycle v0 preds v1 v2 =
  let v = findCycleNode v0 preds v1 (singleton v2) in
  findCycleEdges v0 preds v v []

findCycleNode :: PLevelId -> Paths -> PLevelId -> HashSet PLevelId -> PLevelId
findCycleNode v0 preds v visited = if member v visited then v else case lookup v preds >>= lookup v0 of
  Nothing -> v
  Just (v', _) -> findCycleNode v0 preds v' (insertS v visited)

findCycleEdges :: PLevelId -> Paths -> PLevelId -> PLevelId -> [Edge] -> [Edge]
findCycleEdges v0 preds v v2 res = case lookup v2 preds >>= lookup v0 of
  Nothing -> res
  Just (v1, ev) -> if v == v1 then e:res else findCycleEdges v0 preds v v1 (e:res)
    where
    e = (v1, ev, v2)

-- ************************************************************************************************

checkBasisLevel :: Distances -> HashMap PLevelId CLevelId -> (CLevelId, HashMap PLevelId Int) ->
  LevelM (HashMap PLevelId CLevelId)
checkBasisLevel dists subs0 (v, vdists) = do
  case mapMaybe checkBasis basis of
    [] -> pure ()
    bad -> Left ("invalid basis-basis constraints", concatMap constraintErrorMsg bad)
  case mapMaybe checkDerived derived of
    [] -> pure ()
    bad -> Left ("invalid derived-basis constraints", concatMap constraintErrorMsg bad)
  foldM fderive subs0 derived
  where
  checkBasis (k, n) = if (k == v || k == levelIdZero) && n == 0
    then Nothing else Just (edgeConstraint (vacuousLevelId k, n, vacuousLevelId v))
  checkDerived (k, n) = if n == 0 then Nothing else Just (edgeConstraint (k, n, vacuousLevelId v))
  (derived, basis) = first (map $ first levelIdMeta) $ partitionBasisLevelIds (toList vdists)
  fderive subs (k, _) = case lookup k subs of
    Nothing -> case lookup k dists >>= lookup levelIdZero of
      Just 0 -> pure subs
      _ -> pure $ insert k v subs
    Just v' -> if v' == levelIdZero then pure subs else
      Left ("cross-constrained basis level " <> show (v, k, v'), [])

checkDerivedLevel :: HashMap PLevelId CLevelId -> (PLevelId, HashMap PLevelId Int) -> LevelM (PLevelId, CLevel)
checkDerivedLevel basisMap (v, dists) = fmap (v,) $ case lookup v basisMap of
  Just v' -> pure $ levelVar 0 v'
  Nothing -> case partitionBasisLevelIds dists' of
    (_, []) -> Left ("under-constrained derived level " <> show v, concatMap constraintErrorMsg cs)
    (_, vs) -> pure $ levelNormalize vs
    where
    dists' = map (first $ \k -> maybe k vacuousLevelId (lookup k basisMap)) $ toList dists
    cs = map (\(v0, n) -> edgeConstraint (v0, n, v)) dists'

-- ************************************************************************************************

isValidLevelConstraint :: LevelConstraint m -> Bool
isValidLevelConstraint (Constraint u1 rel u2) = case rel of
  CEqual -> u1 == u2
  CConv -> all fcheck $ toList $ levelMaxes (unLoc u1)
    where
    vs2 = levelMaxes (unLoc u2)
    fcheck (v1, n1) = case lookup v1 vs2 of
      Just n2 | n1 <= n2 -> True
      _ | v1 == levelIdZero -> all ((n1 <=) . snd) (toList vs2)
      _ -> False
