module Csic.Core.Check.Multi
( solveMultiConstraints, allMultisForUserSet, allUserMultis, allMultisRaw, allMultisUniq
, isValidMultiConstraint
) where
import Csic.Util.Prelude
import Csic.Core.Syntax.Multi
import qualified Data.List.NonEmpty as NE
import Data.List (sortOn)
import qualified Data.Set as Set

-- ************************************************************************************************

-- | Multi-checking monad.
type MultiM = Either (String, [(Loc, String)])

-- | Solve a set of multi constraints, producing a set of multi metavariable substitutions.
solveMultiConstraints :: Bool -> Int -> AllocN [Loc]  -> [MultiConstraint MetaId] -> MultiM (MultiMap MetaId Void)
solveMultiConstraints debug numUserIds metaLocs allConstraints = do
  traceIf debug (showTable "constraints" allConstraints) pure ()
  -- Determine canonical set of user multiplicities.
  multiSpace <- case lookup numUserIds allMultisForUserSet of
    Just xs -> pure xs
    Nothing -> Left ("invalid user multi names " <> show numUserIds, [])
  let multiValids = (NE.fromList multiSpace, length multiSpace)
  -- Iteratively eliminate constraints until an inconsistency is found or no progress is made.
  st <- eliminateConstraints (Solutions debug Stuck 100 [] [] multiValids mempty mempty) allConstraints
  traceIf debug (showTable "valids" $ toList (snd <$> stValids st)) pure ()
  -- Ensure all constraints were resolved.
  let cErrors = concatMap (\c -> constraintErrorMsg' c (substMultiMap (stSubsP st) <$> c)) (stConstraints st)
  unless (null cErrors) $ Left ("unresolved multi constraints", cErrors)
  -- Iteratively pick best solutions for ambiguous metavariables.
  let metaIds = map (multiIdMeta . MetaId) [0.. allocNext metaLocs - 1]
  multiSubs <- eliminateMetaVars st {stProgressN = 0} metaIds
  traceIf debug (showTable "output" $ toList multiSubs) pure ()
  -- Ensure all metavariables were resolved.
  let metaErrors = mapMaybe (fcheck multiSubs) (zip metaIds $ reverse $ allocV metaLocs)
  unless (null metaErrors) $ Left ("ambiguous multi metavariables", metaErrors)
  -- Verify final substitutions preserve the original constraints.
  let validationErrors = mapMaybe (fvalidate multiSubs) allConstraints
  unless (null validationErrors) $ Left ("invalid multi constraints", concat validationErrors)
  pure multiSubs
  where
  fcheck subs (k, l) = case lookup k subs of
    Just _ -> Nothing
    Nothing -> Just (l, "unresolved " <> show k)
  fvalidate subs c = do
    c' <- traverse (substMultisM (`lookup` subs)) c
    if isValidMultiConstraint c' then Nothing else Just (constraintErrorMsg' c c')

-- ************************************************************************************************

data Progress = Stuck | Alive
  deriving (Eq, Ord, Show)

data SolutionState = Solutions
  { stDebug :: Bool
  , stProgress :: Progress
  , stProgressN :: Int
  , stConstraints :: [MultiConstraint MetaId]
  , stMetaVars :: [PMultiId]
  , stSpace :: (NonEmpty CMulti, Int)
  , stValids :: HashMap PMultiId (NonEmpty (MultiMap MetaId Void), Int)
  , stSubs :: MultiMap MetaId Void
  }

stSubsP :: SolutionState -> MultiMap MetaId MetaId
stSubsP = vacuousMultiMapV . stSubs

lookupValids :: PMultiId -> SolutionState -> (NonEmpty (MultiMap MetaId Void), Int)
lookupValids k st = fromMaybe (first (fmap $ curry singleton k) $ stSpace st) (lookup k (stValids st))

eliminateConstraints :: SolutionState -> [MultiConstraint MetaId] -> MultiM SolutionState
eliminateConstraints st cs = case cs of
  [] -> case stProgress st of
    Alive -> eliminateConstraints st {stProgress = Stuck, stConstraints = []} (stConstraints st)
    Stuck -> if stProgressN st < 10000
      then eliminateConstraints st {stProgressN = 10 * stProgressN st, stConstraints = []} (stConstraints st)
      else pure st
  c0:cs' -> if numSubs0 > stProgressN st
    then do
      traceIf (stDebug st) ("DEFER[" <> show numSubs0 <>  "]: " <> show c0 <> "  ==>  " <> show c) pure ()
      eliminateConstraints st {stConstraints = c0:stConstraints st} cs'
    else case allSubs1 of
      ([], _) -> Left ("invalid constraint", constraintErrorMsg' c0 c)
      (v:vs, numSubs1) -> do
        traceIf (stDebug st && not (null v)) msg pure ()
        traceIf (stDebug st) ("ELIM: " <> show c0 <> "  ==>  " <> show c) pure ()
        eliminateConstraints st {stProgress = Alive, stValids = valids', stSubs = subs'} cs'
        where
        valids' = foldr (`insert` (v NE.:| vs, numSubs1)) (stValids st) allKeys1
        subs' = if null vs then v `union` stSubs st else stSubs st
        msg = if null vs
          then "SUBST: " <> show (toList v)
          else "REFINE: " <> show allKeys1 <> ": " <> show numSubs0 <> " ==> " <> show numSubs1
            <> (if numSubs1 > 10 then "" else " " <> show (fst allSubs1))
    where
    c = substMultiMap (stSubsP st) <$> c0

    allKeys1 = toKeys (NE.head allSubs0)
    allSubs1 = filterLength checkSubst (NE.toList allSubs0)
    checkSubst subs = isValidMultiConstraint (substMultiMap (vacuousMultiMapV subs) <$> c)

    (allSubs0, numSubs0) = foldr fmerge (mempty NE.:| [], 1) (foldMap multisInMulti c)
    fmerge k (vs, n) = case isBasisMultiId k of
      Right {} -> (vs, n)
      Left {} -> if member k (NE.head vs)
        then (vs, n)
        else (NE.fromList [v `union` kv | v <- NE.toList vs, kv <- NE.toList kvs], n * nk)
      where
      (kvs, nk) = lookupValids k st

eliminateMetaVars :: SolutionState -> [PMultiId] -> MultiM (MultiMap MetaId Void)
eliminateMetaVars st ks0 = case ks0 of
  [] -> case stProgress st of
    Alive -> eliminateMetaVars st {stProgress = Stuck, stProgressN = 0, stMetaVars = []} (stMetaVars st)
    Stuck -> if stProgressN st < 2
      then eliminateMetaVars st {stProgressN = stProgressN st + 1, stMetaVars = []} (stMetaVars st)
      else pure $ stSubs st
  k:ks' -> if member k (stSubs st) then eliminateMetaVars st ks' else case mapM (lookup k) vs0 of
    Nothing -> Left ("bad multi state " <> show k, [])
    Just vs1 -> if NE.length vs2 == snd (stSpace st)
      then if stProgressN st >= 1 then final multiAny else defer
      else case foldM chooseByMin (NE.head vs2) (NE.tail vs2) of
        Just v -> final v
        Nothing | stProgressN st >= 2, Just v <- chooseBySize (NE.toList vs2) -> final v
        _ -> defer
      where
      -- sort AND remove duplicates
      vs2 = NE.fromList $ Set.toList $ Set.fromList $ NE.toList vs1
    where
    (vs0, _) = lookupValids k st

    defer = eliminateMetaVars st {stMetaVars = k:stMetaVars st} ks'
    final v = do
      let vs1 = first NE.fromList $ filterLength (\subs -> lookup k subs == Just v) (NE.toList vs0)
      let valids' = foldr (`insert` vs1) (stValids st) (toKeys $ NE.head vs0)
      let subs' = insert k v (stSubs st)
      eliminateMetaVars st {stProgress = Alive, stValids = valids', stSubs = subs'} ks'

    chooseByMin x y
      | multiConvertible x y = Just x
      | multiConvertible y x = Just y
      | otherwise = Nothing

    chooseBySize vs = case sortOn multiSize vs of
      v:v1:_ | multiSize v < multiSize v1 -> Just v
      _ -> Nothing
    multiSize m = foldr (\(ks, n) -> (+ length ks * multiLitSize n)) 0 (multiSOP m)
    multiLitSize = \case MultiZero -> 0; MultiOne -> 1; MultiAny -> 2

-- ************************************************************************************************

isValidMultiConstraint :: MultiConstraint m -> Bool
isValidMultiConstraint (Constraint m1 rel m2) = case rel of
  CEqual -> m1 == m2
  CConv -> multiConvertible (unLoc m1) (unLoc m2)

-- ************************************************************************************************

allMultisForUserSet :: HashMap Int [CMulti]
allMultisForUserSet =
  fromList $ map (\n -> let ks = take n allUserMultis in (n, allMultisUniq ks)) [0..3]

allUserMultis :: [CMultiId]
allUserMultis = [multiIdUser i | i <- [0..]]

allMultisUniq :: [MultiId m] -> [Multi m]
allMultisUniq = Set.toList . Set.fromList . fmap (multiNormalize . multiSOP) . allMultisRaw MultiOne

allMultisRaw :: MultiLit -> [MultiId m] -> [Multi m]
allMultisRaw base = fmap MultiRaw .
  foldr (\x y -> foldr (\n -> (fmap ((x, n):) y <>)) y [base..]) [[]] . powerSet

-- ************************************************************************************************

filterLength :: (a -> Bool) -> [a] -> ([a], Int)
filterLength f = \case
  [] -> ([], 0)
  x:xs -> if f x then bimap (x:) (1+) (filterLength f xs) else filterLength f xs
