module Csic.Core.Check.Runner
( runTypeCheck
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Core.Check.Level
import Csic.Core.Check.Multi
import Csic.Core.Check.Termination
import Csic.Core.Check.Usage

-- | Initial a mutual block type-checking state.
runTypeCheck :: Namespace -> CEnv -> PrenexParams -> CheckM () -> CheckResult CEnv
runTypeCheck ns env0 prenex = frec env0 . (:[])
  where
  frec env = \case
    [] -> pure env
    f:fs -> execStateT f (initPEnv ns prenex env) >>= finishTypeCheck >>= uncurry frec . second (<> fs)

-- | Type-check a final mutual block of declarations.
finishTypeCheck :: PEnv -> CheckResult (CEnv, [CheckM ()])
finishTypeCheck env0 = do
  let res = finishPEnv env0
  let env = if any (declHasAnnot AnnTrace) (presDecls res) then mapOpts enableTraceAll env0 else env0
  let opts = getOpts env
  -- Check for orphan constructors.
  forM_ (presDecls res) $ \d -> case declImpl d of
    ICtor y -> case find ((ctorDataTypeName y ==) . declName) (presDecls res) of
      Just (Decl {declImpl = IData dy}) | any ((declName d ==) . fst) (dataCtorInfos dy) -> pure ()
      _ -> checkError (declLoc d) "invalid constructor data type reference"
    _ -> pure ()
  -- Ensure term unification constraints were solved.
  pdecls <- withErrDecls "while unifying" (presDecls res) $ do
    -- Check all metavariables and constraints were resolved.
    let merrs = map (metaVarError env "METAVAR") (presTermMetas res)
    let cerrs = map (depsConstraintError env "CONSTRAINT") (presTermCons res)
    let errs = merrs <> cerrs
    unless (null errs) $ checkErrorN nullLoc "UNSOLVED" errs
    -- Substitute meta terms.
    pure $ fmap (substMetas env) <$> presDecls res
  -- Determine number of prenex parameters.
  let (nlvls, nmuls) = bimap length length (penvPrenex env)
  -- Solve level constraints.
  levelSubs <- withErrDecls "while universe-checking" pdecls $
    let metaLocs = presLevelMetaLocs res in
    if levelCheckOff opts || any (declHasAnnot AnnNoLevelCheck) pdecls
      then pure $ fromList $ map (\i -> (levelIdMeta (MetaId i), levelNat 0)) [0 .. allocNext metaLocs - 1]
      else case solveLevelConstraints (traceLevels opts) nlvls metaLocs (presLevelCons res) of
        Right subs -> pure subs
        Left (msg, ss) -> checkErrorN nullLoc msg (map (uncurry strErr) ss)
  -- Solve multiplicity constraints.
  multiSubs <- withErrDecls "while multiplicity-checking" pdecls $
    let metaLocs0 = presMultiMetaLocs res in
    if multiCheckOff opts || any (declHasAnnot AnnNoCountCheck) pdecls
      then pure $ fromList $ map (\i -> (multiIdMeta (MetaId i), multiAny)) [0 .. allocNext metaLocs0 - 1]
      else do
        (metaLocs, multiCons) <- checkMultiUsage (getCEnv env) (metaLocs0, presMultiCons res) pdecls
        case solveMultiConstraints (traceMultis opts) nmuls metaLocs multiCons of
          Right subs -> pure subs
          Left (msg, ss) -> checkErrorN nullLoc msg (map (uncurry strErr) ss)
  -- Substitute resolved levels and multis.
  cdeclsLoopy <- withErrDecls "while abstracting" pdecls $
    fromMaybeM (checkError nullLoc "unresolved metavariable") $
      mapM (mapM (abstractTerm (levelSubs, multiSubs))) pdecls
  -- Check termination annotations.
  cdeclsOkay <- withErrDecls "while termination-checking" cdeclsLoopy $
    checkTermination cdeclsLoopy
  -- Trace final declarations for debug.
  forM_ cdeclsOkay $ \d -> do
    let sizeMsg = "sizeof " <> show (qname $ declNameQ d) <> " = " <> show (declAstSize d)
    traceIf (traceOkays opts) (prettyDeclLn (PrettyEnv (printOpts opts) 0) d) pure ()
    traceIf (traceOkays opts) sizeMsg pure ()
  -- Add final declarations to the environment.
  let resultEnv = foldr registerCDecl (getCEnv env) cdeclsOkay
  pure (mapOpts (const $ getOpts env0) resultEnv, presDefer res)
