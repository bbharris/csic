module Csic.Core.Check.Usage (MultiUsageState, checkMultiUsage) where
import Csic.Util.Prelude
import Csic.Core.Environment

-- ************************************************************************************************

type MultiUsageState = (AllocN [Loc], [MultiConstraint MetaId])

checkMultiUsage :: CEnv -> MultiUsageState -> [PDecl] -> CheckResult MultiUsageState
checkMultiUsage env (metaLocs, cs) ds = do
  let st0 = UEnv env (fromList $ map (\d -> (unTag $ declName d, d)) ds) metaLocs cs
  (_, st) <- runStateT (mapM_ checkDeclUsage ds) st0
  pure (uenvMetas st, uenvCons st)

-- ************************************************************************************************

-- | Usage-checking monad.
type UsageM = StateT UEnv CheckResult

-- | Usage-checking environment.
data UEnv = UEnv
  { uenvCEnv  :: CEnv
  , uenvDecls :: HashMap IName PDecl
  , uenvMetas :: AllocN [Loc]
  , uenvCons  :: [MultiConstraint MetaId]
  }

addConstraints :: [MultiConstraint MetaId] -> UsageM ()
addConstraints cs = modify $ \st -> st {uenvCons = cs <> uenvCons st}

instance HasCEnv UEnv where
  getCEnv = uenvCEnv
  {-# INLINE getCEnv #-}
  mapCEnv f e = e {uenvCEnv = f (uenvCEnv e)}
  {-# INLINE mapCEnv #-}

instance HasMetaEnv MetaId UEnv where
  instantiateDecl l s pargs env = do
    d <- case lookup (unTag s) (uenvDecls env) of
      Just d -> pure (Left d)
      Nothing -> Right <$> lookupCDeclE l s env
    instantiateLDecl l d pargs
  instantiateMetaVarBody _ _ _ _ = Nothing
  {-# INLINE instantiateMetaVarBody #-}
  instantiateMetaVarType l _ _ _ = checkError l "unexpected metavariable"
  {-# INLINE instantiateMetaVarType #-}

-- ************************************************************************************************

checkDeclUsage :: PDecl -> UsageM ()
checkDeclUsage d = do
  case declImpl d of
    IAxiom -> pure ()
    IData {} -> pure ()
    ICtor {} -> pure ()
    IDef y -> unless (declIsErased d) $ void $ checkTermUsage vempty (locate (declLoc d) multiOne) (defImpl y)

type UsageCtx n = Vector n PMulti

-- | Map De Bruijn levels to usages.
type UsageMap = IntMap Int PMulti

mergeUsage :: UsageMap -> UsageMap -> UsageMap
mergeUsage = unionWith multiPlus

checkArgUsage :: KnownNat n => UsageCtx n -> Located PMulti -> Loc -> PMulti -> PTerm n -> UsageM UsageMap
checkArgUsage uctx umask l m = checkTermUsage (multiTimes m <$> uctx) (locate l $ multiTimes m (unLoc umask))

ensureErased :: Loc -> Located PMulti -> UsageM ()
ensureErased l umask =
  unless (unLoc umask == multiZero) $ addConstraints [Constraint (locate l multiZero) CEqual umask]

dtrace :: forall n. KnownNat n => UsageCtx n -> String -> UsageM ()
dtrace _ s = do
  debug <- gets $ traceMultis . getOpts
  traceIf debug (replicate (2 * natInt @n) ' ' <> s) $ pure ()

checkTermUsage :: forall n. KnownNat n => UsageCtx n -> Located PMulti -> PTerm n -> UsageM UsageMap
checkTermUsage uctx umask t = case unT t of
  Var _ i -> do
    let ilvl = getFinite (mirror i)
    let m = vindex uctx i
    dtrace uctx $ "V" <> show ilvl <> " += " <> show m
    pure (singleton (ilvl, m))
  App _ f m r -> do
    fus <- checkTermUsage uctx umask f
    rus <- checkArgUsage uctx umask l m r
    pure $ mergeUsage fus rus
  Lam _ _ (_, m) b -> do
    let ilvl = natInt @n
    dtrace uctx $ "\\V" <> show ilvl <> " : " <> show m <> " =>"
    bus <- checkTermUsage (vcons multiOne uctx) umask b
    let m' = fromMaybe multiZero $ lookup ilvl bus
    addConstraints [Constraint (locate l m') CConv (locate l m)]
    pure bus
  Cas _ d (pargs, m) _ bs -> do
    dtrace uctx $ "case : " <> show m
    dus <- checkArgUsage uctx umask l m d
    let pctx = vcons multiOne uctx
    let ilvl = natInt @n
    buss <- forM bs $ \(Branch @k (Irr z) p b) -> do
      let varsToCheck0 = [(ilvl, m)]
      (fdisc, bctx, varsToCheck) <- case p of
        PatWild -> pure (id, pctx, varsToCheck0)
        PatCtor sc (Irr (dti, _)) -> do
          cd <- gets (instantiateDecl l sc pargs) >>= liftEither
          let ctorMuls = multiTimes m <$> ctorMultis (dataTypeArity dti) (declType cd)
          let klvls = vtoList $ (+ (ilvl + 1)) . getFinite <$> vindices @k
          let varsToCheck = varsToCheck0 <> zip klvls ctorMuls
          pure (insertWith multiPlus ilvl multiOne, vappend (vindices $> multiOne) pctx, varsToCheck)
      dtrace uctx $ "branch " <> show varsToCheck
      bus <- fdisc <$> checkTermUsage bctx umask b
      forM_ varsToCheck $ \(j, jm) -> do
        let jm' = fromMaybe multiZero $ lookup j bus
        addConstraints [Constraint (locate (bindLoc z) jm') CConv (locate l jm)]
      pure bus
    bus <- mergeBranchUsage l ilvl buss
    pure $ mergeUsage dus bus
  Pit {} -> do
    ensureErased l umask
    pure mempty
  Con _ c -> do
    erased <- case c of
      TypeUniv _ -> pure True
      DeclRef s pargs -> declIsErased <$> (gets (instantiateDecl l s pargs) >>= liftEither)
      Literal _ -> pure False
    when erased $ ensureErased l umask
    pure mempty
  Met {} -> checkError l "unexpected usage metavariable"
  where
  l = getLoc t

type MergeState = IntMap Int (PMulti, Maybe (HashSet PMulti))

mergeBranchUsage :: Loc -> Int -> [UsageMap] -> UsageM UsageMap
mergeBranchUsage l ilimit uss = fmap fst <$> foldM fmerge mempty uss
  where
  ilvls :: HashSet Int = foldMap (fromList . filter (< ilimit) . toKeys) uss
  fmerge res us = foldM (fmerge1 us) res ilvls

  fmerge1 :: UsageMap -> MergeState -> Int -> UsageM MergeState
  fmerge1 us res k = case lookup k res of
    Nothing -> pure $ insert k (new, Nothing) res
    Just (old, Nothing) -> if new == old then pure res else do
      st <- get
      let (metas, cs) = (uenvMetas st, uenvCons st)
      let (v, metas') = allocate1 MetaId metas
      let m = multiVar (multiIdMeta v)
      let cs' = Constraint (locate l old) CConv (locate l m) :
                Constraint (locate l new) CConv (locate l m) : cs
      put st {uenvMetas = (l:) <$> metas', uenvCons = cs'}
      pure $ insert k (m, Just (fromList [old, new])) res
    Just (m, Just lowers) -> if member new lowers then pure res else do
      addConstraints [Constraint (locate l new) CConv (locate l m)]
      pure $ insert k (m, Just (insertS new lowers)) res
    where
    new = fromMaybe multiZero $ lookup k us

declIsErased :: Decl Impl t -> Bool
declIsErased d = declHasAnnot AnnErased d || case declImpl d of
  IAxiom -> False
  IData {} -> True
  ICtor {} -> False
  IDef _ -> False

ctorMultis :: Int -> PTerm n -> [PMulti]
ctorMultis skip t = case unT t of
  Pit _ _ (_, m) rt -> (if skip > 0 then id else (m:)) $ ctorMultis (skip - 1) rt
  _ -> []