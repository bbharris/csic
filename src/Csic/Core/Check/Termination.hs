module Csic.Core.Check.Termination (checkTermination) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Data.List (partition, sortOn)

-- | Check top-level declarations for their annotated termination criteria.
checkTermination :: [CDecl] -> CheckResult [CDecl]
checkTermination ds = do
  -- Collect list of mutual definitions.
  let isDef d = case declImpl d of IDef y -> Just (d, y); _ -> Nothing
  let defs = mapMaybe isDef ds
  let defNames = map (declName . fst) defs
  let callCtx0 = MutualCallCtx (fromList $ map unTag defNames) mempty 0
  -- Collect mutual calls of each definition.
  let calls = fromList $ map (\(d, y) -> (declName d, collectMutualCalls callCtx0 (defImpl y))) defs
  -- Infer recursion annotations for each definition.
  let recAnns = inferRecursionAnnotations calls defNames
  -- Check each declaration and apply final annotations.
  forM ds $ \d -> case declImpl d of
    IAxiom -> pure d
    -- Check definition for valid recursion annotation.
    IDef y -> do
      let recKind = fromMaybe RecursionGeneral $ lookup (declName d) recAnns
      case (defRec y, recKind) of
        (RecursionNone, RecursionNone) -> pure ()
        (RecursionNone, RecursionGuarded _) -> pure ()
        (RecursionGeneral, RecursionGeneral) -> pure ()
        _ -> checkErrorN nullLoc ("invalid annotation: " <> show (defRec y)) $
          strErr (declLoc d) ("inferred annotation: " <> show recKind) : fcallErrors calls
      pure d {declImpl = IDef y {defRec = recKind}}
    IData {} -> pure d
    -- Check constructor type for positivity criteria.
    ICtor {} -> do
      unless (declHasAnnot AnnNoPosCheck d) $ checkConstructorPositivity ds (declType d)
      pure d
  where
  fcallErrors = concatMap fcallErrors' . sortOn fst . toList
  fcallErrors' (src, calls) = map (fcallError src) calls
  fcallError src (CallInfo l tgt args ctx) = CErrTree nullLoc ("recursive call from " <> show src)
    [strErr l $ "to " <> show tgt <> " " <> show args <> "\n" <> strPretty pctx]
    where
    pctx = "with context: " <> showListS fsize (sortOn fst $ toList ctx) ""
    fsize (i, r) = case r of
      ArgUnknown -> shows i . showString "?"
      ArgEqParam j -> shows i . showString " = " . shows j
      ArgLtParam j -> shows i . showString " < " . shows j

-- ************************************************************************************************

type MutualCalls = HashMap DeclName [CallInfo]

data CallInfo = CallInfo
  { _callLoc :: Loc
  , callName :: DeclName
  , callArgs :: [ArgSize]
  , _callCtx :: HashMap Int ArgSize
  }

data ArgSize
  = ArgUnknown
  | ArgEqParam Int
  | ArgLtParam Int
  deriving (Eq, Ord, Show)

inferRecursionAnnotations :: MutualCalls -> [DeclName] -> HashMap DeclName RecursionKind
inferRecursionAnnotations calls names = untilStuck mempty
  where
  untilStuck res =
    let (progress, res') = foldl' fcheck (False, res) names in
    if not progress then res' else untilStuck res'
  fcheck (progress, res) s = if member s res then (progress, res) else case lookup s calls of
    Just cs | othersDone -> (True, insert s recKind res)
      where
      (self, others) = partition (\c -> callName c == s) cs
      recKind
        | null self = RecursionNone
        | null idxs = RecursionGeneral
        | otherwise = RecursionGuarded idxs
      othersDone = all (\c -> member (callName c) res) others
      idxs = case map (\c -> mapMaybe farg (zip (callArgs c) [0..])) self of
        xs:xss -> filter (\i -> all (i `elem`) xss) xs
        _ -> []
      farg (r, i) = case r of ArgLtParam j | j == i -> Just i; _ -> Nothing
    _ -> (progress, res)

-- ************************************************************************************************

data MutualCallCtx = MutualCallCtx
  { mutNames :: HashSet IName
  , paramMap :: HashMap Int ArgSize
  , paramIdx :: Int
  }

collectMutualCalls :: forall n. KnownNat n => MutualCallCtx -> CTerm n -> [CallInfo]
collectMutualCalls ctx x = fCalls <> foldMap (frecNeg . argTerm) rs
  where
  frecNeg :: KnownNat k => CTerm k -> [CallInfo]
  frecNeg = collectMutualCalls ctx {paramIdx = -1}
  (f, rs) = unfoldApps x
  fCalls = case f of
    Var {} -> []
    App _ f' _ r' -> frecNeg f' <> frecNeg r'
    Lam _ pt _ b -> frecNeg pt <> collectMutualCalls (updateCallCtx ctx (sNat @n) PatWild) b
    Pit _ pt _ rt -> frecNeg pt <> frecNeg rt
    Cas _ d _ rt bs -> frecNeg d <> frecNeg rt <> foldMap fbranch bs
      where
      fbranch (Branch _ p b) = collectMutualCalls ctx' b
        where
        ctx' = if paramIdx ctx < 0 then ctx else case unT d of
          Var _ i -> case lookup (getFinite $ mirror i) (paramMap ctx) of
            Just z -> updateCallCtxArg ctx (sNat @n) p z
            Nothing -> ctx {paramIdx = -1}
          _ -> ctx {paramIdx = -1}
    Con _ (DeclRef s _) | member (unTag s) (mutNames ctx) ->
      [CallInfo (getLoc x) s (map fargSize rs) (paramMap ctx)]
      where
      fargSize arg = case unfoldApps (argTerm arg) of
        (Var _ i, _) -> fromMaybe ArgUnknown $ lookup (getFinite $ mirror i) (paramMap ctx)
        _ -> ArgUnknown
    Con {} -> []
    Met _ k _ -> absurd k

updateCallCtx :: forall n k. (KnownNat n, KnownNat k) => MutualCallCtx -> SNat n -> Pattern k -> MutualCallCtx
updateCallCtx ctx _ _ = qctx
  where
  pctx = advanceCtxEq (sNat @n) (sNat @1) ctx
  qctx = addArgSizes (sNat @(n + 1)) (sNat @k) (ArgLtParam $ paramIdx pctx - 1) pctx

updateCallCtxArg :: forall n k. (KnownNat n, KnownNat k) =>
  MutualCallCtx -> SNat n -> Pattern k -> ArgSize -> MutualCallCtx
updateCallCtxArg ctx _ _ = \case
  ArgUnknown -> ctx {paramIdx = -1}
  ArgEqParam j -> ctx2
    where
    ctx1 = addArgSizes (sNat @n) (sNat @1) (ArgEqParam j) ctx
    ctx2 = addArgSizes (sNat @(n + 1)) (sNat @k) (ArgLtParam j) ctx1
  ArgLtParam j -> ctx1
    where
    ctx1 = addArgSizes (sNat @n) (sNat @(1 + k)) (ArgLtParam j) ctx

advanceCtxEq :: forall n s. (KnownNat n, KnownNat s) => SNat n -> SNat s -> MutualCallCtx -> MutualCallCtx
advanceCtxEq _ _ ctx =
  ctx {paramIdx = paramIdx ctx + natInt @s, paramMap = foldr (uncurry insert) (paramMap ctx) varSizes}
  where
  vars = shift @n <$> vindices @s
  varSizes = zipWith (\i j -> (getFinite i, ArgEqParam j)) (vtoList vars) [paramIdx ctx..]

addArgSizes :: forall n s. (KnownNat n, KnownNat s) => SNat n -> SNat s -> ArgSize -> MutualCallCtx -> MutualCallCtx
addArgSizes _ _ z ctx =
  ctx {paramMap = foldr (uncurry insert) (paramMap ctx) varSizes}
  where
  vars = shift @n <$> vindices @s
  varSizes = map (\i -> (getFinite i, z)) (vtoList vars)

-- ************************************************************************************************

-- | Check each constructor field is strictly positive.
checkConstructorPositivity :: (KnownNat n) => [CDecl] -> CTerm n -> CheckResult ()
checkConstructorPositivity mutual ty = case unT ty of
  Pit (Irr z) pt _ bt -> do
    let negErrs = negativeRefs mutual pt
    unless (null negErrs) $ checkErrorN (getLoc pt) ("non-strictly-positive field " <> show (paramName z))
      (map (uncurry strErr) negErrs)
    checkConstructorPositivity mutual bt
  _ -> pure ()

-- | Check a term for strictly positive references to the mutual inductive types being defined.
negativeRefs :: [CDecl] -> CTerm n -> [(Loc, String)]
negativeRefs mutual ty = case unT ty of
  Pit _ pt _ bt -> allInductiveRefs mutual pt <> negativeRefs mutual bt
  _ -> case unfoldApps ty of
    (f, rs) | isJust (isInductiveRef mutual (mkT f)) -> concatMap (allInductiveRefs mutual . argTerm) rs
    _ -> allInductiveRefs mutual ty

allInductiveRefs :: [CDecl] -> CTerm n -> [(Loc, String)]
allInductiveRefs mutual x = case isInductiveRef mutual x of
  Just l -> [l]
  Nothing -> case unfoldApps x of
    {-
      Special-case nested positivity rule for propositional equality type.
      It is safe to ignore the first implicit type argument as it is
      used strictly positively in the refl constructor.
    -}
    (Con _ (DeclRef s _), _:rs) | show s == "Equal" ->
      concatMap (allInductiveRefs mutual . argTerm) rs
    _ -> foldMapSubTerms (allInductiveRefs mutual) x

isInductiveRef :: [CDecl] -> CTerm n -> Maybe (Loc, String)
isInductiveRef mutual t = case unT t of
  Con (Irr l) (DeclRef s _) -> do
    d <- find ((s ==) . declName) mutual
    case declImpl d of
      IData _ -> Just (l, "negative occurence of " <> show s)
      _ -> Nothing
  _ -> Nothing