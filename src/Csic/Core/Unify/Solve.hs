-- | Based on https://www21.in.tum.de/teaching/sar/SS20/5.pdf
module Csic.Core.Unify.Solve
( TypeChecker(..), solveConstraints
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Core.Unify.Simplify

-- ************************************************************************************************

-- | Type-checking callback interface.
data TypeChecker = TypeChecker
  { typeCheckU :: TypeCheckFunc
  , applyImplicits :: ApplyImplicits
  }

type TypeCheckFunc = forall n. KnownNat n =>
  TCtx n -> UTerm n -> PTerm n -> CheckM (PTerm n)

type ApplyImplicits = forall n. KnownNat n =>
  TCtx n -> PTerm n -> PTerm n -> Maybe (Binder MetaId n) -> PTerm n -> CheckM (PTerm n)

-- ************************************************************************************************

{- | Solve a set of higher-order unification constraints using a Huet-like algorithm.

    Huet-like algorithms are semi-decidable at best, so we terminate the search at a certain depth within
    the search space (even if a deeper solution potentially exists).
-}
solveConstraints :: TypeChecker -> [UConstraint] -> CheckM ()
solveConstraints checker cs =
  withFuelCheck ferrs $ mapM_ (solveConstraint checker) cs
  where
  ferrs env = [CErrTree nullLoc "unification fuel exhausted" (map (constraintError env "stuck constraint") cs)]
{-# INLINE solveConstraints #-}

solveConstraint :: TypeChecker -> UConstraint -> CheckM ()
solveConstraint checker c = case c of
  CStuck     x -> solveStuck checker x
  CFlexRigid x -> solveFlexRigid checker x
  CInstance  x -> solveInstance checker x
  CImplicit  x -> solveImplicit checker x
  CLevel (Constraint x1 _ x2) -> unless (unLoc x1 == unLoc x2) $ deferConstraint mempty c
  CMulti (Constraint x1 _ x2) -> unless (unLoc x1 == unLoc x2) $ deferConstraint mempty c

-- ************************************************************************************************

solveStuck :: TypeChecker -> Sized StuckConstraint -> CheckM ()
solveStuck checker c@(Sized (Stuck ctx t1 rel t2)) = do
  env <- get
  -- Simplify the constraint into smaller constraints.
  case simplifyTerms True rel (initFuel env) env ctx t1 t2 of
    -- If successful then solve the smaller constraints.
    Right cs -> solveConstraints checker cs
    -- Empty error indicates stuck at the same term size - defer the constraint for now.
    Left [] -> deferConstraint (metaVarsIn env (metaVarsIn env mempty t1) t2) (CStuck c)
    -- Fail on actual error.
    Left err -> withConstraintError (CStuck c) $ throwError err

-- ************************************************************************************************

solveFlexRigid :: TypeChecker -> Sized FlexRigid -> CheckM ()
solveFlexRigid checker c@(Sized (FlexRigid ctx flex@(l, k, metaArgs, extraArgs) rel rigid)) = do
  -- Lookup the metavariable state.
  Sized @p mv@(MetaVar {metaCtx = mctx}) <- getMetaVar l k
  let ty = metaType mv
  -- Check whether the metavariable has a substitution already.
  case metaBody mv of
    -- If so, reduce to rigid-rigid constraint.
    Just _ -> solveConstraints checker [CStuck $ Sized $ Stuck ctx (unflex flex) rel rigid]
    -- If not, attempt to solve directly.
    Nothing -> do
      -- Run solve attempt in fresh monad stack to backtrack cleanly on failure.
      res <- gets $ runStateT $ do
        {-
          Guess the number of substitution arguments that should be generated.
          Full Huet's algorithm would try all possibilities and backtrack as needed.
          Instead we try the most likely case and defer the constraint if this fails.
          This heuristic appears to capture many practical inference situations.
        -}
        (rigidHead, rigidArgs) <- evalM ctx rigid
        let numSubstArgs = length rigidArgs - length extraArgs
        -- If negative, the rigid term cannnot unify (unless it expands at a later time).
        when (numSubstArgs < 0) $ checkError l "could not guess number of substitution arguments"
        -- Unify extra args (if the guess for numSubstArgs is wrong this will probably fail immediately).
        let (rigidSubstArgs, rigidExtraArgs) = splitAt numSubstArgs rigidArgs
        csExtra <- get >>= \env -> do
          let simplifyArgs r1 r2 = simplifyTerms False UnifyEQU (initFuel env) env ctx (argTerm r1) (argTerm r2)
          liftEither $ concat <$> zipWithM simplifyArgs extraArgs rigidExtraArgs
        solveConstraints checker csExtra
        -- Abort with success if the metavariable has now been solved (transitively) by one of the csExtra.
        Sized mv' <- getMetaVar l k
        if isJust (metaBody mv') then pure [] else do
          -- Construct metvariable argument map.
          varMap <- metaVarArgMap @p ctx metaArgs
          -- Translate the rigid head into the substitution body scope.
          bodyHead <- gets (isSubstitutable k varMap (mkT rigidHead)) >>=
            fromMaybeM (checkError l "rigid head could not be mapped into substitution")
          -- Infer the (function) type of the body head to generate typed metavariable arguments.
          bodyHeadType <- liftPE $ inferTermType mctx bodyHead
          -- Translate the rigid arguments into the substitution body scope.
          (bodyArgsRev, actType0) <- foldM (guessSubstArg mctx l k varMap) ([], bodyHeadType) rigidSubstArgs
          -- Construct the body term and generate any additional subtyping constraints.
          let body0 = mkApps bodyHead (reverse bodyArgsRev)
          (body, actType) <- genMetaSubType checker mctx l rel body0 >>= \case
            Nothing -> pure (body0, actType0)
            Just body -> (body,) <$> liftPE (inferTermType mctx body)
          -- Type-check the substitution body.
          solveConstraints checker [CStuck $ Sized $ Stuck (pCtx mctx) actType UnifyLEQ ty]
          -- Add metavariable substitution to the environment.
          updateMetaVarSubst (MetaVar mctx l k (Just body) ty)
      -- Check final result of solver attempt.
      case res of
        -- If successful, commit the new environment and solve the remaining constraints.
        Right (cs, env) -> put env >> solveConstraints checker (CFlexRigid c : cs)
        -- Otherwise defer the constraint until later.
        Left _ -> do
          deps <- gets $ \env -> metaVarsIn env (metaVarsIn env mempty (unflex flex)) rigid
          deferConstraint deps (CFlexRigid c)

guessSubstArg :: (KnownNat k, KnownNat n) => TCtx n -> Loc -> MetaId -> FiniteMap k n ->
  (Args MetaId n, PTerm n) -> Arg MetaId k -> CheckM (Args MetaId n, PTerm n)
guessSubstArg ctx l k varMap (res, ty) arg = do
  pit <- evalM (pCtx ctx) ty >>= fromMaybeM (checkError l "expected meta-application pi-type") . isPiType
  -- Translate the rigid arguments into the substitution body scope.
  r <- gets (isSubstitutable k varMap $ argTerm arg) >>= \case
    Just r -> pure r
    -- If not possible, generate a typed metavariable arg.
    Nothing -> snd <$> genTermMetaVar ctx l (bindParam pit)
  pure (mapArg (const r) arg : res, substScope (vsingleton r) (bindResult pit))

-- ************************************************************************************************

solveInstance :: TypeChecker -> Sized InstanceConstraint -> CheckM ()
solveInstance checker c@(Sized (InstanceConstraint ctx l k classDecl expArgs ty)) = do
  -- Lookup the metavariable state.
  Sized mv <- getMetaVar l k
  -- If already solved we are done.
  if isJust (metaBody mv) then pure () else do
    -- Check if all explicit class arguments are rigid.
    expArgMetaVars <- gets $ \env -> foldr (flip (metaVarsIn env) . argTerm) mempty expArgs
    if not (null expArgMetaVars)
      -- If not, then defer the constraint on the explicit arg metavars.
      then deferConstraint expArgMetaVars (CInstance c)
      -- If they are rigid we commit to solving the instance now, or failing.
      else do
        -- Collect potential instance variables from the context.
        let localVars = Left <$> typeClassVars className ctx
        -- Collect potential instance declarations from the environment.
        globalNames <- gets $ \env -> map Right $ lookupClassInstances (penvSpace env) className env
        -- Check all potential instances.
        results <- gets $ \env -> map (`checkInst` env) (localVars <> globalNames)
        case partitionEithers results of
          -- If a single instance succeeds, use it.
          (_, [(body, env')]) -> do
            put env'
            cs <- updateMetaVarSubst (MetaVar ctx l k (Just body) ty)
            solveConstraints checker cs
          -- Otherwise error with ambiguity.
          (_, okays) -> withConstraintError (CInstance c) $ do
            err <- ptermErrorM (pCtx ctx) (show k <> " with type") ty
            checkErrorN l emsg $ err : map finstError okays
      where
      emsg = "ambiguous instance metavariable"
      finstError (t, env) = ptermError env (pCtx ctx) "potential instance" t
      className = declName classDecl

      -- Check a potential instance solution.
      checkInst x = runStateT $ do
        b <- case x of
          Left i -> pure $ mkVar (Irr l) i
          Right s -> declRefInferPrenex l <$> liftE (lookupCDeclE l s)
        typeCheckU checker ctx b ty

-- ************************************************************************************************

-- | Attempt to solve a deferred implicit argument application constraint.
solveImplicit :: TypeChecker -> Sized ImplicitConstraint -> CheckM ()
solveImplicit checker c@(Sized (ImplicitConstraint ctx m x actType expType)) = do
  -- Check if the expected type can now be determined to have an implicit parameter or not.
  implicitPiType (pCtx ctx) expType >>= \case
    Nothing -> do
      deps <- gets $ \env -> metaVarsIn env mempty expType
      deferConstraint deps (CImplicit c)
    Just piM -> withConstraintError (CImplicit c) $ do
      x' <- applyImplicits checker ctx x actType piM expType
      solveConstraints checker [CStuck $ Sized $ Stuck (pCtx ctx) m UnifyEQU x']

-- ************************************************************************************************

isSubstitutable :: KnownNat k => MetaId -> FiniteMap k n -> PTerm k -> PEnv -> Maybe (PTerm n)
isSubstitutable k varMap x env = do
  x' <- mapFreeVars varMap (substMetas env x)
  if findMetaVar (k ==) x' then Nothing else Just x'

genMetaSubType :: KnownNat n =>
  TypeChecker -> TCtx n -> Loc -> UnifyRelation -> PTerm n -> CheckM (Maybe (PTerm n))
genMetaSubType checker ctx l rel x = if rel == UnifyEQU then pure Nothing else evalM (pCtx ctx) x >>= \case
  -- If the body is a type universe, generate fresh level metavariable for subtyping relations.
  (Con (Irr l2) (TypeUniv u2), []) -> do
    u1 <- levelVar 0 <$> genMetaLevelId l
    solveConstraints checker $ simplifyLevels (locate l u1) rel (locate l2 u2)
    pure $ Just $ mkCon (Irr l) (TypeUniv u1)
  -- If the body is a pi-type, generate fresh multi metavariable for subtyping relations.
  (Pit (Irr z) pt (u, m) rt, []) -> do
    m' <- multiVar <$> genMetaMultiId l
    solveConstraints checker $ simplifyMultis (locate l m') rel (locate (paramLoc z) m)
    pure $ Just $ mkPit (Irr z) pt (u, m') rt
  _ -> pure Nothing
