-- | Based on https://www21.in.tum.de/teaching/sar/SS20/5.pdf
module Csic.Core.Unify.Simplify (simplifyTerms, simplifyLevels, simplifyMultis) where
import Csic.Util.Prelude
import Csic.Core.Environment

-- | Reduce a single unification constraint into smaller constraints if possible.
simplifyTerms :: KnownNat n =>
  Bool -> UnifyRelation -> Fuel -> PEnv -> PCtx n -> PTerm n -> PTerm n -> CheckResult [UConstraint]
simplifyTerms errorOnStuck rel !fuel0 env ctx t1 t2 =
  -- Check for infinite loops to catch inductive/recursive comparison bugs.
  -- From experience, these most often occur when a recursive definition is compared to itself
  -- and Evaluate.hs unfolds it without a proper guard condition.
  consumeFuel (error $ "OUT OF FUEL: simplify: " <> show (getLoc t1, getLoc t2)) fuel0 $ \ !fuel ->
    simplifyTerms' errorOnStuck rel fuel env ctx t1 t2

-- | Reduce a single unification constraint into smaller constraints if possible.
simplifyTerms' :: KnownNat n =>
  Bool -> UnifyRelation -> Fuel -> PEnv -> PCtx n -> PTerm n -> PTerm n -> CheckResult [UConstraint]
simplifyTerms' errorOnStuck rel !fuel env ctx t1 t2 = case (h1, h2) of
  -- If both heads are metavariables, return a stuck flex-flex constraint.
  (Met {}, Met {}) -> fstuck
  -- Otherwise if either head is a metavariable, return a flex-rigid constraint.
  (Met l m rs, _) ->
    pure [CFlexRigid $ Sized $ FlexRigid ctx (relevant l, m, rs, args1) rel t2']
  (_, Met l m rs) ->
    pure [CFlexRigid $ Sized $ FlexRigid ctx (relevant l, m, rs, args2) (negateRelation rel) t1']
  -- If one side is a literal value, try to unpack both sides to constructors.
  (Con _ (DeclRef {}), Con _ (Literal {})) -> fctorLits
  (Con _ (Literal {}), Con _ (DeclRef {})) -> fctorLits
  -- Otherwise we have a normal rigid-rigid constraint.
  _ -> case simplifyRigidHeads rel fuel env ctx (mkT h1) (mkT h2) of
    -- If the heads match, ensure the arguments match.
    Right cs -> (cs <>) <$> simplifyArgs errors fuel env ctx args1 args2
    -- Otherwise check for a lambda on either side to apply eta-convertiblity.
    Left errs -> case canEtaExpand of
      Just fcheck -> fcheck
      -- Otherwise if at least one side is a neutral case split, defer to a constraint.
      Nothing -> case (h1, h2) of
        (Cas {}, _) -> fstuck
        (_, Cas {}) -> fstuck
        _ -> throwError errs
  where
  (h1, args1) = evalToWHNF fuel env ctx t1
  (h2, args2) = evalToWHNF fuel env ctx t2
  t1' = unWHNF (h1, args1)
  t2' = unWHNF (h2, args2)

  fstuck = if errorOnStuck then throwError [] else pure [CStuck $ Sized $ Stuck ctx t1' rel t2']
  errors =
    [ ptermError env ctx "LHS-prenorm" t1
    , ptermError env ctx "RHS-prenorm" t2
    , ptermError env ctx "LHS-norm" t1'
    , ptermError env ctx "RHS-norm" t2'
    ]

  -- Check function eta-convertability.
  canEtaExpand = case (h1, h2) of
    (Lam (Irr z1) _ _ b1, _) -> checkEtaExpansion fuel env ctx (z1, b1) t2'
    (_, Lam (Irr z2) _ _ b2) -> checkEtaExpansion fuel env ctx (z2, b2) t1'
    _ -> Nothing
  {-# INLINE canEtaExpand #-}

  -- Check if both sides reduce to constructors.
  fctorLits = case (evalToCtor fuel env ctx t1', evalToCtor fuel env ctx t2') of
    -- If so, compare the constructor forms directly.
    (Just (d1, pargs1, args1'), Just (d2, pargs2, args2')) -> do
      unless (declName d1 == declName d2) $ checkErrorN nullLoc "constructor-literal mismatch" errors
      (<>)
        <$> simplifyPrenexArgs (getLoc t1) pargs1 (getLoc t2) pargs2
        <*> simplifyArgs errors fuel env ctx args1' args2'
    -- If this fails, one side might need type-classes to resolve before reducing, etc.
    _ -> fstuck
  {-# INLINE fctorLits #-}

-- Simplify two argument lists.
simplifyArgs :: KnownNat n => [CheckError] ->
  Fuel -> PEnv -> PCtx n -> Args MetaId n -> Args MetaId n -> CheckResult [UConstraint]
simplifyArgs errors !fuel env ctx = simplifyLists "argument" errors $ \r1 r2 ->
  simplifyTerms False UnifyEQU fuel env ctx (argTerm r1) (argTerm r2)

-- Simplify two lists.
simplifyLists :: String -> [CheckError] ->
  (a -> a -> CheckResult [UConstraint]) -> [a] -> [a] -> CheckResult [UConstraint]
simplifyLists errmsg errors f xs1 xs2 = case (xs1, xs2) of
  ([], []) -> pure []
  (x1:xs1', x2:xs2') -> (<>) <$> f x1 x2 <*> simplifyLists errmsg errors f xs1' xs2'
  _ -> checkErrorN nullLoc (errmsg <> " arity mismatch") errors

-- | Check if a lambda body is a potential eta-expansion (does not check used variables in body head).
checkEtaExpansion :: forall n. KnownNat n =>
  Fuel -> PEnv -> PCtx n -> (ParamInfo, PTerm (1 + n)) -> PTerm n -> Maybe (CheckResult [UConstraint])
checkEtaExpansion !fuel env ctx (z, b) f = case reverse args of
  r:rargs -> case evalToWHNF fuel env bctx (argTerm r) of
    (Var _ v, []) | v == finite @0 -> Just $
      simplifyTerms False UnifyEQU fuel env bctx (unWHNF (h, reverse rargs)) (shiftTerm f)
    _ -> Nothing
  _ -> Nothing
  where
  bctx = snd $ consPCtx z ctx
  (h, args) = evalToWHNF fuel env bctx b

-- | Collect the unification constraints needed to make two rigid head terms equal.
simplifyRigidHeads :: KnownNat n =>
  UnifyRelation -> Fuel -> PEnv -> PCtx n -> PTerm n -> PTerm n -> CheckResult [UConstraint]
simplifyRigidHeads rel !fuel env ctx x y = case (unT x, unT y) of
  -- Check literal equality for variables and constants.
  (Var _ v1, Var _ v2) | v1 == v2 -> pure []
  (Con _ c1, Con _ c2) | c1 == c2 -> pure []
  -- Unify declaration references.
  (Con (Irr l1) (DeclRef s1 pargs1), Con (Irr l2) (DeclRef s2 pargs2)) | s1 == s2 ->
    simplifyPrenexArgs l1 pargs1 l2 pargs2
  -- Unify two type universes.
  (Con (Irr l1) (TypeUniv u1), Con (Irr l2) (TypeUniv u2)) ->
    pure $ simplifyLevels (locate l1 u1) rel (locate l2 u2)
  -- Simplify two pi-types structurally.
  (Pit (Irr z1) p1 (_, m1) b1, Pit (Irr z2) p2 (_, m2) b2) -> do
    let bctx = snd $ consPCtx (mergeParamInfo z1 z2) ctx
    let m = simplifyMultis (locate (paramLoc z1) m1) rel (locate (paramLoc z2) m2)
    p <- simplifyTerms False UnifyEQU fuel env ctx p1 p2
    b <- simplifyTerms False rel fuel env bctx b1 b2
    pure $ m <> p <> b
  -- Simplify two lambdas structurally.
  (Lam (Irr z1) _ _ b1, Lam (Irr z2) _ _ b2) -> do
    let bctx = snd $ consPCtx (mergeParamInfo z1 z2) ctx
    simplifyTerms False UnifyEQU fuel env bctx b1 b2
  -- Simplify two case splits structurally.
  (Cas _ d1 _ _ bs1, Cas _ d2 _ _ bs2) -> do
    d <- simplifyTerms False UnifyEQU fuel env ctx d1 d2
    bs <- simplifyLists "branch" errors (simplifyBranches fuel env ctx) bs1 bs2
    pure $ d <> bs
  _ -> checkErrorN nullLoc "rigid-rigid head mismatch" errors
  where
  errors = [ptermError env ctx "LHS" x, ptermError env ctx "RHS" y]

simplifyBranches :: KnownNat n =>
  Fuel -> PEnv -> PCtx n -> Branch MetaId n -> Branch MetaId n -> CheckResult [UConstraint]
simplifyBranches !fuel env ctx (Branch @k1 (Irr z1) p1 b1) (Branch @k2 _ p2 b2) =
  case testEquality (sNat @k1) (sNat @k2) of
    Nothing -> ferror "pattern arity mismatch"
    Just eq -> gcastWith eq $ do
      unless (p1 == p2) $ ferror "pattern mismatch"
      simplifyTerms False UnifyEQU fuel env bctx b1 b2
  where
  bctx = snd $ prettyPattern ctx z1 p1
  ferror :: String -> CheckResult a
  ferror = checkError nullLoc

simplifyPrenexArgs :: Loc -> PrenexArgs MetaId -> Loc -> PrenexArgs MetaId -> CheckResult [UConstraint]
simplifyPrenexArgs l1 args1 l2 args2 = case (args1, args2) of
  ((lvls1, muls1), (lvls2, muls2)) -> do
    unless (length lvls1 == length lvls2) $
      checkErrorN nullLoc "level map arity" [strErr l1 (show lvls1), strErr l2 (show lvls2)]
    unless (length muls1 == length muls2) $
      checkErrorN nullLoc "multi map arity" [strErr l1 (show muls1), strErr l2 (show muls2)]
    let lcs = concat $ zipWith flevel lvls1 lvls2
    let mcs = concat $ zipWith fmulti muls1 muls2
    pure $ lcs <> mcs
  where
  flevel u1 u2 = simplifyLevels (locate l1 u1) UnifyEQU (locate l2 u2)
  fmulti m1 m2 = simplifyMultis (locate l1 m1) UnifyEQU (locate l2 m2)

simplifyLevels :: Located PLevel -> UnifyRelation -> Located PLevel -> [UConstraint]
simplifyLevels x1 rel x2 = CLevel <$> simplifyBasic x1 rel x2

simplifyMultis :: Located PMulti -> UnifyRelation -> Located PMulti -> [UConstraint]
simplifyMultis x1 rel x2 = CMulti <$> simplifyBasic x1 rel x2

simplifyBasic :: Eq a => Located a -> UnifyRelation -> Located a -> [Constraint a]
simplifyBasic x1 rel x2 = if x1 == x2 then [] else case rel of
  UnifyEQU -> [Constraint x1 CEqual x2]
  UnifyLEQ -> [Constraint x1 CConv  x2]
  UnifyGEQ -> [Constraint x2 CConv  x1]
