module Csic.Back.Target.Codegen where
import Csic.Util.Prelude
import Csic.Back.Target.Interface
import System.IO
import Data.Text.Lazy.Builder
import qualified Data.Text.Lazy.IO as T

-- ************************************************************************************************

type CodegenM = StateT CodegenState BackendM

data CodegenState = CodegenState
  { genCfg     :: BackendConfig
  , genNext    :: Int
  , genOut     :: Handle
  , genBuf     :: Builder
  , genInd     :: Int
  , genNewLine :: Bool
  }

-- ************************************************************************************************

runCodegen :: BackendConfig -> Handle -> CodegenM () -> BackendM ()
runCodegen cfg h f = do
  ((), st) <- runStateT f (CodegenState cfg 0 h mempty 0 True)
  liftIO $ T.hPutStr (genOut st) (toLazyText $ genBuf st)

ostr :: String -> CodegenM ()
ostr = ostr' False

ostrln :: String -> CodegenM ()
ostrln = ostr' True

ostr' :: Bool -> String -> CodegenM ()
ostr' nl s = do
  st <- get
  let pre = if genNewLine st then replicate (2 * genInd st) ' ' else ""
  let buf = genBuf st <> fromString (pre <> if nl then s <> "\n" else s)
  put st {genBuf = buf, genNewLine = nl}

withBuf :: CodegenM a -> CodegenM a
withBuf f = do
  st0 <- get
  put st0 {genBuf = mempty, genInd = 0, genNewLine = True}
  res <- f
  st1 <- get
  liftIO $ T.hPutStr (genOut st1) (toLazyText $ genBuf st1)
  put st1 {genBuf = genBuf st0, genInd = genInd st0, genNewLine = genNewLine st0}
  pure res

withInd :: CodegenM a -> CodegenM a
withInd f = do
  st0 <- get
  put st0 {genInd = genInd st0 + 1}
  res <- f
  st1 <- get
  put st1 {genInd = genInd st0}
  pure res

genTempName :: String -> CodegenM String
genTempName prefix = do
  st <- get
  put st {genNext = genNext st + 1}
  pure $ prefix <> show (genNext st)