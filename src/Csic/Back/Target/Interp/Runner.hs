module Csic.Back.Target.Interp.Runner (initBackendInterp) where
import Csic.Util.Prelude
import Csic.Back.MIR.Syntax
import Csic.Core.Environment
import Csic.Back.Target.Interface
import Csic.Back.Target.Interp.Env
import Csic.Back.Target.Interp.FFI
import Csic.Back.Target.Interp.Eval

-- | Initialize the interpreter backend.
initBackendInterp :: IO BackendTargetApi
initBackendInterp = pure $ BackendTargetApi
  { targetClose = pure ()
  , targetCompileProg = compProg
  , targetCompileFile = \_ _ -> checkError nullLoc "interpreter does not support file compilation"
  }

-- | Compile a program to an interpreter execution computation.
compProg :: BackendConfig -> MirProgram -> BackendM (Int -> BackendM Int)
compProg cfg p = do
  -- Preload C symbols.
  syms <- mapM (\x -> (csymName x,) <$> loadCSym x) (mirCSyms p)
  pure $ fexec (newInterpEnv cfg p (fromList syms))
  where
  -- Execute the interpreter with the given environment allocator and input.
  fexec fenv n = (>>=) (liftIO fenv) $ evalStateT $ do
    let n' = MirValLit (Nat (fromIntegral n))
    res <- evalMIR (vsingleton n') (mirEntry p)
    case res of
      MirValLit (Int v) -> pure (fromIntegral v)
      _ -> interpErrorN "invalid interpreter result" (mirEntry p) [res]
