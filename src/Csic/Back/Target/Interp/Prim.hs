module Csic.Back.Target.Interp.Prim where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interp.Env
import Csic.Back.Target.Interp.Effect
import Csic.Back.Target.Interp.FFI

-- | Evaluates a primitive operation in a given interpretation context.
evalPrimOp :: MirTerm n -> MirPrimOp -> [MirValue] -> InterpM MirValue
evalPrimOp t op args = case op of
  -- Undefined value
  PrmUndefined -> interpError "undefined value" t
  -- Erased argument
  PrmErased -> op0 $ pure MirValErased
  -- Coerce operation
  PrmCoerce -> op1 pure
  -- Show any value as a string
  PrmShowAny -> op1 $ \x -> pure $ MirValLit (Str (pack $ show x))
  -- Boolean false
  PrmBoolFalse -> op0 $ pure $ MirValLit (Nat 0)
  -- Boolean true
  PrmBoolTrue -> op0 $ pure $ MirValLit (Nat 1)
  -- Natural number zero
  PrmNatZ -> op0 $ pure $ MirValLit (Nat 0)
  -- Successor of a natural number
  PrmNatS -> op1 $ \a -> case a of
    MirValLit (Nat n) -> pure $ MirValLit (Nat (n + 1))
    _ -> interpErrorN "expected natural number" t [a]
  -- Unpack successor of a natural number
  PrmNatSUnpack -> op1 $ \a -> case a of
    MirValLit (Nat n) -> pure $ MirValLit (Nat (n - 1))
    _ -> interpErrorN "expected natural number" t [a]
  -- Check if a natural number is zero
  PrmNatIsZero -> op1 $ \a -> case a of
    MirValLit (Nat n) -> pure $ MirValLit (Nat (if n == 0 then 1 else 0))
    _ -> interpErrorN "expected natural number" t [a]
  -- Add two natural numbers
  PrmNatAdd -> op2 $ \a b -> case (a, b) of
    (MirValLit (Nat n), MirValLit (Nat m)) -> pure $ MirValLit (Nat (n + m))
    _ -> interpErrorN "expected natural numbers" t [a, b]
  -- Convert non-negative integer to integer
  PrmIntNonNeg -> op1 $ \a -> case a of
    MirValLit (Nat n) -> pure $ MirValLit (Int (fromIntegral n))
    _ -> interpErrorN "expected natural number" t [a]
  -- Unpack non-negative integer to natural number
  PrmIntNonNegUnpack -> op1 $ \a -> case a of
    MirValLit (Int n) -> pure $ MirValLit (Nat (fromIntegral n))
    _ -> interpErrorN "expected integer" t [a]
  -- Negate and subtract one from a natural number
  PrmIntNegMinus1 -> op1 $ \a -> case a of
    MirValLit (Nat n) -> pure $ MirValLit (Int (negate (fromIntegral n) - 1))
    _ -> interpErrorN "expected integer" t [a]
  -- Unpack negated and subtracted one integer to natural number
  PrmIntNegMinus1Unpack -> op1 $ \a -> case a of
    MirValLit (Int n) -> pure $ MirValLit (Nat (negate (fromIntegral n) - 1))
    _ -> interpErrorN "expected integer" t [a]
  -- Pack a list of characters into a string
  PrmStrPack -> op1 $ \a -> case strPack a of
    Just s -> pure $ MirValLit $ Str $ pack s
    Nothing -> interpErrorN "expected list of characters" t [a]
  -- Unpack a string into a list of characters
  PrmStrUnpack -> op1 $ \a -> case a of
    MirValLit (Str s) -> pure $ strUnpack (unpack s)
    _ -> interpErrorN "expected string" t [a]
  -- Handle effect operation
  PrmEffOp s -> handleEffectOp t s args
  -- Handle C FFI call
  PrmCCall f -> handleCCall t f args
  where
  -- Helper for zero-argument operations
  op0 f = case args of
    [] -> f
    _ -> interpError "expected no arguments" t
  -- Helper for one-argument operations
  op1 f = case args of
    [a] -> f a
    _ -> interpError "expected one argument" t
  -- Helper for two-argument operations
  op2 f = case args of
    [a, b] -> f a b
    _ -> interpError "expected two arguments" t

-- | Converts a MIR value representing a list of characters into a Haskell string.
strPack :: MirValue -> Maybe String
strPack = \case
  MirValCtr (MirCtorName _ "_nil") [] -> pure ""
  MirValCtr (MirCtorName _ "_cons") [MirValLit (Char h), ts] -> (h:) <$> strPack ts
  _ -> Nothing

-- | Converts a Haskell string into a MIR value representing a list of characters.
strUnpack :: String -> MirValue
strUnpack = \case
  [] -> MirValCtr (MirCtorName (MirTypeName "Any") "_nil") []
  h:ts -> MirValCtr (MirCtorName (MirTypeName "Any") "_cons") [MirValLit (Char h), strUnpack ts]
