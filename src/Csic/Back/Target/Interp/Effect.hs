module Csic.Back.Target.Interp.Effect where
import Csic.Util.Prelude
import Csic.Core.Syntax
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interface
import Csic.Back.Target.Interp.Env
import System.IO
import qualified GHC.IO.FD as FD
import qualified GHC.IO.Handle.FD as FD

{- | Handle an effect operation.

  An effect operation is a primitive operation that is not part of the MIR language
  but is provided by the interpreter. The effect operation is used to interact with
  the host environment, such as writing to the console or manipulating the box heap.
-}
handleEffectOp :: MirTerm n -> MirEffOpName -> [MirValue] -> InterpM MirValue
handleEffectOp t (MirEffOpName s) args = case s of
  "Console_putStrLn" -> op1 $ \case
    MirValLit (Str v') -> do
      h <- gets $ fromMaybe stdout . backStdout . interpCfg
      liftIO $ hPutStrLn h (unpack v')
      pure unit
    _ -> interpError "invalid putStr arg" t
  "Console_outputFD" -> op0 $ do
    h <- gets $ fromMaybe stdout . backStdout . interpCfg
    fd <- liftIO $ FD.handleToFd h
    pure $ MirValLit (Int $ fromIntegral $ FD.fdFD fd)
  "New_new" -> op1 $ \v -> do
    InterpRef r <- interpNew v
    pure $ MirValLit (Int $ fromIntegral r)
  "Load_load" -> op1 $ \r -> case r of
    MirValLit (Int r') -> interpLoad t (InterpRef $ fromIntegral r')
    _ -> interpErrorN "invalid load arg" t [r]
  "Store_store" -> op2 $ \r v -> case r of
    MirValLit (Int r') -> do
      interpStore t (InterpRef $ fromIntegral r') v
      pure unit
    _ -> interpErrorN "invalid store arg" t [r]
  _ -> interpErrorN "invalid effect operation" t args
  where
  unit = MirValCtr (MirCtorName (MirTypeName "Unit") "_unit") []
  op0 f = case args of
    [] -> f
    _ -> interpErrorN "expected zero arguments" t args
  op1 f = case args of
    [a] -> f a
    _ -> interpErrorN "expected one argument" t args
  op2 f = case args of
    [a, b] -> f a b
    _ -> interpErrorN "expected two arguments" t args
