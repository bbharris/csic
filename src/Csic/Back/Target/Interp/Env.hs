module Csic.Back.Target.Interp.Env where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interface
import Data.IORef
import Foreign.Ptr

-- ************************************************************************************************

-- | The monad used by the interpreter.
type InterpM = StateT InterpEnv BackendM

-- | The environment for the interpreter.
data InterpEnv = InterpEnv
  { interpCfg  :: BackendConfig  -- ^ Configuration for the backend.
  , interpProg :: MirProgram     -- ^ The MIR program being interpreted.
  , interpCSyms :: CCallTable    -- ^ Preloaded C symbol call table.
  , interpHeap :: InterpHeap     -- ^ The heap used by the interpreter.
  }

-- | Map of C symbol names to their corresponding handlers.
type CCallTable = HashMap MirCSymName (Loc -> [MirValue] -> InterpM MirValue)

-- | Create a new interpreter environment.
newInterpEnv :: BackendConfig -> MirProgram -> CCallTable -> IO InterpEnv
newInterpEnv cfg p syms = InterpEnv cfg p syms <$> newIORef (0, mempty)

-- ************************************************************************************************

-- | A value in the MIR interpreter.
data MirValue
  = MirValErased                  -- ^ An erased value.
  | MirValCls MirClosure          -- ^ A closure value.
  | MirValLit Literal             -- ^ A literal value.
  | MirValCtr MirCtorName [MirValue] -- ^ A constructor value with arguments.
  | MirValPtr (Ptr ())               -- ^ A pointer value.
  deriving (Show, Generic, NFData)

-- | A closure in the MIR interpreter.
data MirClosure where
  MirClosure :: InterpCtx n -> MirLambda n -> MirClosure

deriving instance Show MirClosure
instance NFData MirClosure where
  rnf (MirClosure ctx lam) = rnf ctx `seq` rnf lam

-- | The context used by the interpreter, storing values in De Bruijn index order.
type InterpCtx n = Vector n MirValue

-- ************************************************************************************************

-- | The heap used by the interpreter.
type InterpHeap = IORef (Int, HashMap InterpRef MirValue)

-- | A reference in the interpreter heap.
newtype InterpRef = InterpRef Int
  deriving newtype (Eq, Ord, Show, Hashable, NFData)

-- | Allocate a new reference in the interpreter heap.
interpNew :: MirValue -> InterpM InterpRef
interpNew initVal = do
  heap <- gets interpHeap
  (h, hmap) <- liftIO $ readIORef heap
  let ref = InterpRef h
  liftIO $ writeIORef heap (h + 1, insert ref initVal hmap)
  pure ref

-- | Load a value from the interpreter heap.
interpLoad :: MirTerm n -> InterpRef -> InterpM MirValue
interpLoad t ref = do
  heap <- gets interpHeap
  (_, hmap) <- liftIO $ readIORef heap
  fromMaybeM (interpError "invalid reference handle" t) $ lookup ref hmap

-- | Store a value in the interpreter heap.
interpStore :: MirTerm n -> InterpRef -> MirValue -> InterpM ()
interpStore t ref val = do
  heap <- gets interpHeap
  (n, hmap) <- liftIO $ readIORef heap
  unless (member ref hmap) $ interpError "invalid reference handle" t
  liftIO $ writeIORef heap (n, insert ref val hmap)

-- ************************************************************************************************

-- | Report an error in the interpreter.
interpError :: String -> MirTerm n -> InterpM a
interpError s t =  checkErrorN (mloc t) s [strErr nullLoc (showPretty t)]

-- | Report an error in the interpreter with additional values.
interpErrorN :: String -> MirTerm n -> [MirValue] -> InterpM a
interpErrorN s t rs = checkErrorN (mloc t) s $ strErr nullLoc (showPretty t) : map (strErr nullLoc . showPretty) rs
