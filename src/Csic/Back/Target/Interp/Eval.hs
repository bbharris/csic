module Csic.Back.Target.Interp.Eval where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interp.Env
import Csic.Back.Target.Interp.Prim

-- | Evaluates an MIR term in a given interpretation context.
evalMIR :: InterpCtx n -> MirTerm n -> InterpM MirValue
evalMIR ctx t = case mterm t of
  -- Variable case: look up the variable in the context.
  MirVar i -> pure $ vindex ctx i
  -- Application case: evaluate the function and arguments.
  MirApp f rs -> case mterm f of
    -- Constructor case: evaluate arguments and construct the value.
    MirCtr sc -> MirValCtr sc <$> mapM (evalMIR ctx) rs
    -- Primitive operation case: evaluate the primitive operation.
    MirPrm op -> mapM (evalMIR ctx) rs >>= evalPrimOp t op
    -- General function application case: evaluate the function and arguments.
    _ -> do
      f' <- evalMIR ctx f
      rs' <- mapM (evalMIR ctx) rs
      case f' of
        -- Closure case: apply the closure to the arguments.
        MirValCls (MirClosure ctx' (MirLambda @k _ b)) -> do
          case vfromList rs' of
            Just rsV -> evalMIR (vreverseAppend rsV ctx') b
            Nothing -> interpErrorN "mismatched argument arity" t rs'
        -- Error case: expected a lambda function.
        _ -> interpErrorN "expected lambda" t [f']
  -- Lambda case: create a closure with the current context.
  MirLam f -> pure $ MirValCls (MirClosure ctx f)
  -- Let binding case: evaluate the bound expression and extend the context.
  MirLet (_, r) b -> do
    r' <- evalMIR ctx r
    evalMIR (vcons r' ctx) b
  -- Case analysis: evaluate the discriminant and match against branches.
  MirCas disc bs -> do
    disc' <- evalMIR ctx disc
    case msum $ map (matchBranch disc') bs of
      Just fcont -> fcont
      Nothing -> interpErrorN "non-exhaustive pattern match" t [disc']
    where
    -- Match a branch against the discriminant.
    matchBranch disc' (MirBranch @k _ p b) = case p of
      -- Constructor pattern case: match the constructor and bind arguments.
      MirPatCtor sc _ -> case disc' of
        MirValCtr sc' rs | sc == sc' -> Just $ case vfromList @k rs of
          Just rsV -> evalMIR (vreverseAppend rsV pctx) b
          Nothing -> interpErrorN "mismatched constructor argument arity" t [disc']
        _ -> Nothing
      -- Literal pattern case: match the literal value.
      MirPatLit v -> case disc' of
        MirValLit v' | v == v' -> Just $ evalMIR pctx b
        _ -> Nothing
      -- Wildcard pattern case: always match.
      MirPatWild -> Just $ evalMIR pctx b
      where
      -- Extend the context with the discriminant value.
      pctx = vcons disc' ctx
  -- Boolean if-then-else:
  MirCnd cond a b -> do
    cond' <- evalMIR ctx cond
    case cond' of
      MirValLit (Nat 0) -> evalMIR ctx b
      MirValLit (Nat _) -> evalMIR ctx a
      _ -> interpErrorN "expected boolean" t [cond']
  -- Literal case: return the literal value.
  MirLit v -> pure $ MirValLit v
  -- Declaration case: look up the declaration and evaluate its term.
  MirDcl s -> do
    p <- gets interpProg
    case find ((s ==) . mdeclName) (mirDecls p) of
      Just d -> evalMIR ctx $ shiftMirTerm0 $ mdeclTerm d
      Nothing -> interpError "unresolved declaration" t
  -- Constructor case: return the constructor value with no arguments.
  MirCtr sc -> pure $ MirValCtr sc []
  -- Primitive operation case: evaluate the primitive operation with no arguments.
  MirPrm op -> evalPrimOp t op []
