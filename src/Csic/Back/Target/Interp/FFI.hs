module Csic.Back.Target.Interp.FFI
( loadCSym, handleCCall
) where
import Csic.Util.Prelude
import Csic.Core.Syntax
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interface
import Csic.Back.Target.Interp.Env
import Foreign
import System.Posix.DynamicLinker
import qualified Foreign.LibFFI as FFI
import Csic.Core.Check (checkError)
import Foreign.C.String

loadCSym :: MirCSym -> BackendM (Loc -> [MirValue] -> InterpM MirValue)
loadCSym (MirCSym lib (MirCSymName s) _ ps rt) = do
  let objName = libSharedObjectName lib
  dl <- liftIO $ dlopen objName [RTLD_LAZY]
  f <- liftIO $ dlsym dl s
  when (f == nullFunPtr) $ checkError nullLoc $ "invalid C symbol " <> s
  pure $ \l args -> do
    args' <- zipWithM (toFFIArg l) args ps
    case rt of
      MirCVoid -> liftIO (FFI.callFFI f FFI.retVoid args') $> unit
      MirCSize -> liftIO (FFI.callFFI f FFI.retCSize args') <&> MirValLit . Nat . fromIntegral
      MirCInt -> liftIO (FFI.callFFI f FFI.retCInt args') <&> MirValLit . Int . fromIntegral
      MirCStr -> liftIO (FFI.callFFI f (FFI.retPtr FFI.retCChar) args') >>=
        liftIO . peekCString <&> MirValLit . Str . pack
      MirCPtr -> liftIO (FFI.callFFI f (FFI.retPtr FFI.retVoid) args') <&> MirValPtr
  where
  unit = MirValCtr (MirCtorName (MirTypeName "Unit") "_unit") []

handleCCall :: MirTerm n -> MirCSymName -> [MirValue] -> InterpM MirValue
handleCCall t s args = do
  f <- gets (lookup s . interpCSyms) >>= fromMaybeM (interpError ("undefined C symbol: " <> show s) t)
  f (mloc t) args

libSharedObjectName :: MirCLibName -> String
libSharedObjectName (MirCLibName s) = "lib" <> s <> ".so.6"

toFFIArg :: Loc -> MirValue -> MirCType -> InterpM FFI.Arg
toFFIArg l v ty = case (v, ty) of
  (MirValLit (Nat n), MirCSize) -> pure $ FFI.argCSize (fromIntegral n)
  (MirValLit (Int n), MirCInt) -> pure $ FFI.argCInt (fromIntegral n)
  (MirValLit (Str s), MirCStr) -> pure $ FFI.argString (unpack s)
  (MirValPtr p,       MirCPtr) -> pure $ FFI.argPtr p
  _ -> checkError l ("invalid C argument: " <> show (v, ty))
