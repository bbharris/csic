module Csic.Back.Target.Cache where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.Target.Interface
import Csic.Back.MIR.Syntax
import Data.IORef
import System.Directory
import System.IO

-- ************************************************************************************************

data CacheTarget a = CacheTarget
  { targetName    :: String
  , targetSrcExt  :: String
  , targetOutExt  :: String
  , targetCodegen :: BackendConfig -> MirProgram -> String -> Handle -> BackendM ()
  , targetCompile :: BackendConfig -> Bool -> FilePath -> FilePath -> BackendM a
  }

type ProgramCache a = IORef (HashMap QName a)

compileProgCached :: CacheTarget a -> ProgramCache a -> BackendConfig -> MirProgram -> BackendM a
compileProgCached tgt st cfg p = do
  let name = targetName tgt
  let s = declNameQ (mirCDecl p)
  let l = declLoc (mirCDecl p)
  cache <- liftIO $ readIORef st
  case lookup s cache of
    Just v -> pure v
    Nothing -> do
      liftIO $ createDirectoryIfMissing True (backOutRoot cfg)
      let inpath = show (qspace s) <> ".csic"
      rpath <- liftIO $ makeRelativeToCurrentDirectory $ show (qspace s)
      let flatpath = flattenPath rpath <> "-" <> show (qname s)
      let srcpath = backOutRoot cfg <> "/" <> flatpath <> targetSrcExt tgt
      let outpath = backOutRoot cfg <> "/" <> flatpath <> targetOutExt tgt
      needCodegen <- if backCaching cfg then liftIO (isFileNewer inpath srcpath) else pure True
      when needCodegen $ bracket (liftIO $ openFile srcpath WriteMode) (liftIO . hClose) $ \f ->
        withError (strErr l ("while generating " <> name <> " for " <> show s <> " backend"):) $
          targetCodegen tgt cfg p flatpath f
      needCompile <- if backCaching cfg then liftIO (isFileNewer srcpath outpath) else pure True
      res <- withError (strErr l ("while compiling " <> show s <> " via " <> name <> " backend"):) $
        targetCompile tgt cfg needCompile srcpath outpath
      liftIO $ writeIORef st (insert s res cache)
      pure res

compileFileCached :: CacheTarget a -> ProgramCache a -> BackendConfig -> FilePath -> BackendM a
compileFileCached tgt _st cfg basepath = do
  let name = targetName tgt
  let flatpath = flattenPath basepath
  let srcpath = basepath <> targetSrcExt tgt
  let outpath = backOutRoot cfg <> "/" <> flatpath <> targetOutExt tgt
  needCompile <- if backCaching cfg then liftIO (isFileNewer srcpath outpath) else pure True
  withError (strErr nullLoc ("while compiling " <> srcpath <> " via " <> name <> " backend"):) $
    targetCompile tgt cfg needCompile srcpath outpath

-- ************************************************************************************************

flattenPath :: FilePath -> FilePath
flattenPath = map (\c -> if c == '/' then '-' else c)

isFileNewer :: FilePath -> FilePath -> IO Bool
isFileNewer srcpath outpath = do
  srcExist <- doesFileExist srcpath
  outExist <- doesFileExist outpath
  if not (srcExist && outExist) then pure True else do
    srcMod <- getModificationTime srcpath
    outMod <- getModificationTime outpath
    pure (srcMod >= outMod)
