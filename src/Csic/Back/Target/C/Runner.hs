module Csic.Back.Target.C.Runner (initBackendC) where

import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.Target.Interface
import Csic.Back.Target.Cache
import Csic.Back.Target.C.Codegen
import Data.IORef
import Foreign.Ptr
import System.Process.Typed
import System.Posix.DynamicLinker
import Foreign.C
import qualified Data.Text.Lazy.Encoding as T
import System.FilePath

-- ************************************************************************************************

-- | Initialize the backend for the C target.
-- This function sets up the necessary environment and returns an API for interacting with the backend.
initBackendC :: IO BackendTargetApi
initBackendC = do
  cache <- newIORef mempty
  pure BackendTargetApi
    { targetClose = pure ()
    , targetCompileProg = \cfg p -> do
        f <- compileProgCached cacheTarget cache cfg p
        pure $ \n -> liftIO (fromIntegral <$> f (fromIntegral n))
    , targetCompileFile = \cfg p -> do
        f <- compileFileCached cacheTarget cache cfg p
        pure $ \n -> liftIO (fromIntegral <$> f (fromIntegral n))
    }

-- ************************************************************************************************

-- | Type alias for a C function that takes a CUInt and returns an IO CInt.
type CProgFunc = CUInt -> IO CInt

-- | Define the cache target for the C backend.
cacheTarget :: CacheTarget CProgFunc
cacheTarget = CacheTarget
  { targetName    = "C"  -- Name of the target.
  , targetSrcExt  = ".c"  -- Source file extension.
  , targetOutExt  = ".c.so"  -- Output file extension.
  , targetCodegen = gencodeC  -- Code generation function.
  , targetCompile = compFile  -- Compilation function.
  }

-- ************************************************************************************************

-- | Foreign import to dynamically load a C function.
foreign import ccall "dynamic" mkCProgFunc :: FunPtr CProgFunc -> CProgFunc

-- | Compile a C file into a shared object and load the entry point function.
compFile :: BackendConfig -> Bool -> FilePath -> FilePath -> BackendM CProgFunc
compFile cfg needCompile srcpath outpath = do
  let cc = "gcc"  -- Define the C compiler to use.
  when needCompile $ do  -- If compilation is needed, run the compiler.
    frun cc $ flags <> [srcpath, "-shared", "-o", outpath]  -- Compile the source file into a shared object.
    when (backDumpAsm cfg) $  -- If assembly dump is enabled, generate the assembly file.
      frun cc $ flags <> [srcpath, "-S", "-o", replaceExtension outpath ".asm"]
  let s = "csic_entrypoint"
  func <- handleAll (checkError nullLoc . show) $ liftIO $ do
    dl <- dlopen outpath [RTLD_LAZY]
    dlsym dl s
  when (func == nullFunPtr) $ checkError nullLoc $ "invalid symbol " <> s
  pure $ mkCProgFunc func
  where
  flags =
    ["-std=c11", "-Wall", "-Werror", "-fPIC", "-Isrc/runtime/libcsic-c"]  -- Compiler flags.
    <> (if backOptimize cfg then ["-O3"] else ["-O0", "-g"])  -- Optimization flags based on the configuration.

  -- | Run a process and handle errors.
  frun prog args = do
    -- traceShow (prog, args) $ pure ()  -- Uncomment for debugging.
    (code, _, err) <- readProcess $ proc prog args
    case code of
      ExitFailure i -> checkErrorN nullLoc (show (prog, args) <> " failed with code " <> show i)
        [strErr nullLoc (unpackL $ T.decodeUtf8 err)]
      ExitSuccess -> pure ()
