module Csic.Back.Target.C.Codegen (gencodeC) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interface
import Csic.Back.Target.Codegen
import System.IO
import Data.Char
import Numeric
import Data.List (intercalate)

-- ************************************************************************************************

gencodeC :: BackendConfig -> MirProgram -> String -> Handle -> BackendM ()
gencodeC cfg p _ h = runCodegen cfg h $ do
  todo
{-
  withBuf $ ostrln "#include <csic-c.h>"
  mapM_ genTypeDecl (mirTypes p)
  mapM_ genTermDecl (mirDecls p)
  withBuf $ do
    ostrln "csic_Int csic_entrypoint(csic_Nat n) {"
    withInd $ do
      f <- genTerm (mirEntry p)
      ostrln $ "return " <> f <> "(n);"
    ostrln "}"

-- ************************************************************************************************

genTypeDecl :: MirTypeDecl -> CodegenM ()
genTypeDecl (MirTypeDecl sd cs) = withBuf $ do
  ostrln "typedef struct {"
  withInd $ do
    ostrln "int32_t tag;" -- TODO move into union for optimal packing
    unless (null cs) $ do
      ostrln "union {"
      withInd $ forM_ cs $ \(sc, ps) -> do
        ostrln "struct {"
        withInd $ forM_ (zip ps [0 :: Int ..]) $ \(pt, i) ->
          ostrln $ genType ("field" <> show i) pt <> ";"
        ostrln $ "} " <> ctorFieldSym sc <> ";"
      ostrln "};"
  ostrln $ "} " <> sd' <> ";"
  unless (null cs) $ do
    ostrln "enum {"
    withInd $ forM_ cs $ \(sc, _) -> ostrln $ ctorTagSym sc <> ","
    ostrln "};"
  ostrln ""
  where
  sd' = typeNameSym sd

genType :: String -> MirType -> String
genType name = \case
  MirTypePrm s -> primTypeSym s <> " " <> name
  MirTypeRef s -> typeNameSym s <> " " <> name
  MirTypeFun ps rt -> genType "" rt <> "(*" <> name <> ")(" <>
    intercalate ", " (map (genType "") ps) <> ")"

-- ************************************************************************************************

genTermDecl :: MirDecl -> CodegenM ()
genTermDecl (MirDecl s t) = withBuf $ do
  ostrln $ "static " <> genType (sthunk <> "(" <> ps' <> ")") (mtype b) <> " {"
  withInd $ do
    b' <- genTerm b
    ostrln $ "return " <> b' <> ";"
  ostrln "}"
  when (null ps) $ ostrln $ "#define " <> declNameSym s <> " " <> sthunk <> "()"
  ostrln ""
  where
  sthunk = if null ps then declNameSym s <> "_thunk" else declNameSym s
  (ps, b) = case mterm t of
    -- MirLam ps0 b0 -> (ps0, b0)
    _ -> ([], t)
  ps' = if null ps then "void" else intercalate ", " (map (\(sp, pt) -> genType (varNameSym sp) pt) ps)

genTerm :: MirTerm n -> CodegenM String
genTerm t = case mterm t of
  MirVar s _ -> pure $ varNameSym s
  MirApp f rs -> do
    f' <- genTerm f
    rs' <- mapM genTerm rs
    pure $ f' <> "(" <> intercalate ", " rs' <> ")"
  MirLam (MirLambda ps b) -> do
    s <- genTempName "_lambda"
    withBuf $ do
      let ps' = intercalate ", " (map (\(sp, pt) -> genType (varNameSym sp) pt) $ vtoList ps)
      let rt' = genType "" (mtype b)
      ostrln $ "static " <> rt' <> s <> "(" <> ps' <> ") {"
      withInd $ do
        b' <- genTerm b
        ostrln $ "return " <> b' <> ";"
      ostrln "}"
    pure s
  MirLet rs b -> todo {-do
    forM_ rs $ \(s, r) -> do
      r' <- genTerm r
      ostrln $ genType (varNameSym s) (mtype r) <> " = " <> r' <> ";"
    genTerm b
    -}
  MirCas disc bs -> case bs of
    [] -> pure "csic_absurd"
    MirBranch _ p0 _ :_ -> do
      disc' <- genTerm disc
      sdisc <- genTempName "_disc"
      ostrln $ genType sdisc (mtype disc) <> " = " <> disc' <> ";"
      sres <- genTempName "_result"
      ostrln $ genType sres (mtype t) <> ";"
      let discv = case p0 of MirPatCtor {} -> sdisc <> ".tag"; _ -> sdisc
      ostrln $ "switch (" <> discv <> ") {"
      withInd $ mapM_ (genBranch sdisc sres) bs
      ostrln "}"
      pure sres
  MirLit x -> pure (genLit x)
  MirDcl s -> pure $ declNameSym s
  MirCtr _sc -> pure "csic_todo_ctor"
  {-
      let tag = ctorTagSym sd sc
      rs' <- mapM genTerm rs
      pure $ "(" <> typeNameSym sd <> ")" <> "{.tag = " <> tag <> ", ." <>
        ctorFieldSym sc <> " = {" <> intercalate ", " rs' <> "}}"
  -}
  MirPrm s -> do
    {-
    RPrm MBoolIf -> case rs of
      [cond, bT, bF] -> do
        cond' <- genTerm cond
        sres <- genTempName "_result"
        ostrln $ genType sres (mtype t) <> ";"
        ostrln $ "if (" <> cond' <> ") {"
        withInd $ do
          b' <- genTerm bT
          ostrln $ sres <> " = " <> b' <> ";"
        ostrln "} else {"
        withInd $ do
          b' <- genTerm bF
          ostrln $ sres <> " = " <> b' <> ";"
        ostrln "}"
        pure sres
      _ -> checkError (mloc t) "invalid if arguments"
    -}
    pure $ primOpSym s

genBranch :: String -> String -> MirBranch n -> CodegenM ()
genBranch sdisc sres (MirBranch _ p b) = case p of
  MirPatCtor sc ps -> do
    ostrln $ "case " <> ctorTagSym sc <> ": {"
    withInd $ do
      forM_ (zip [0 :: Int ..] $ vtoList ps) $ \(i, (s, pt)) ->
        ostrln $ "csic_unused " <> genType (varNameSym s) pt <> " = " <> sdisc <> "." <>
          ctorFieldSym sc <> ".field" <> show i <> ";"
      b' <- genTerm b
      ostrln $ sres <> " = " <> b' <> ";"
      ostrln "break;"
    ostrln "}"
  MirPatLit v -> do
    ostrln $ "case " <> genLit v <> ": {"
    withInd $ do
      b' <- genTerm b
      ostrln $ sres <> " = " <> b' <> ";"
      ostrln "break;"
    ostrln "}"
  MirPatWild -> do
    ostrln "default: {"
    withInd $ do
      b' <- genTerm b
      ostrln $ sres <> " = " <> b' <> ";"
      ostrln "break;"
    ostrln "}"

genLit :: Literal -> String
genLit = \case
  Nat v -> show v
  Int v -> show v
  Char v -> "'" <> showHex (ord v) "" <> "'"
  Str v -> show v
  Foreign {} -> "csic_foreign_ptr"

-- ************************************************************************************************

typeNameSym :: MirTypeName -> String
typeNameSym (MirTypeName s) = s

declNameSym :: MirDeclName -> String
declNameSym (MirDeclName s) = s

ctorTagSym :: MirCtorName -> String
ctorTagSym (MirCtorName sd sc) = typeNameSym sd <> "_" <> sc

ctorFieldSym :: MirCtorName -> String
ctorFieldSym (MirCtorName _ sc) = sc

primOpSym :: MirPrimOp -> String
primOpSym x = "csic_" <> show x

primTypeSym :: MirPrimType -> String
primTypeSym x = "csic_" <> show x

varNameSym :: MirVarName -> String
varNameSym (MirVarName s) = s
-}