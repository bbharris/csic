module Csic.Back.Target.Interface where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import System.IO

-- ************************************************************************************************

data BackendTargetApi = BackendTargetApi
  { targetClose       :: IO ()
  , targetCompileProg :: BackendCompile MirProgram
  , targetCompileFile :: BackendCompile FilePath
  }

type BackendCompile p = BackendConfig -> p -> BackendM (Int -> BackendM Int)

type BackendM = ExceptT [CheckError] IO

-- ************************************************************************************************

data BackendConfig = BackendConfig
  { backOutRoot  :: FilePath
  , backStdout   :: Maybe Handle
  , backVerbose  :: Bool
  , backOptimize :: Bool
  , backCaching  :: Bool
  , backDumpAsm  :: Bool
  }

backConfigDefault :: BackendConfig
backConfigDefault = BackendConfig
  { backOutRoot = "./.build"
  , backStdout = Just stdout
  , backVerbose = True
  , backOptimize = True
  , backCaching = True
  , backDumpAsm = False
  }
