module Csic.Back.Target.Chez.Codegen (gencodeChez) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.Target.Interface
import Csic.Back.Target.Codegen
import Csic.Back.MIR.Syntax
import System.IO
import Data.Char
import Numeric

-- ************************************************************************************************

-- | Generate Chez Scheme code from a MIR program.
gencodeChez :: BackendConfig -> MirProgram -> String -> Handle -> BackendM ()
gencodeChez cfg p libname h = runCodegen cfg h $ do
  let entryname = libname <> "-entrypoint"
  ostrln $ "(library (" <> libname <> ")"
  withInd $ do
    ostrln $ "(export " <> entryname <> ")"
    ostrln "(import (chezscheme) (csic-runtime))"
    mapM_ genPrettyComment (mirTypes p)
    mapM_ genCSym (mirCSyms p)
    mapM_ genDecl (mirDecls p)
    let (n, ctx) = consPCtx (explicitParam nullLoc (Name "n")) emptyPCtx
    ostrln $ "(define (" <> entryname <> " " <> nameStr n <> ")"
    withInd $ genTerm ctx (mirEntry p)
    ostrln ")"
  ostrln ")"
  ostrln $ "(import (" <> libname <> "))"
  ostrln $ "(define csic-entrypoint " <> entryname <> ")"

-- ************************************************************************************************

-- | Generate a pretty-printed comment for a given type.
genPrettyComment :: Show a => a -> CodegenM ()
genPrettyComment x = forM_ (lines (showPretty x)) $ \line -> ostrln $ "; " <> line

-- | Generate a declaration for a C symbol.
genCSym :: MirCSym -> CodegenM ()
genCSym x = do
  ostrln $ "(define " <> symName (csymName x)
  withInd $ ostrln $ "(foreign-procedure " <> cc <> " \"" <> s <> "\" (" <> ps <> ") " <> rt <> ")"
  ostrln ")"
  where
  MirCSymName s = csymName x
  cc = case csymCConv x of
    MirCConvCDecl -> "#f"
    MirCConvVarArg n -> "(__varargs_after " <> show n <> ")"
  ps = unwords $ ftype <$> csymParams x
  rt = ftype (csymResult x)
  ftype = \case
    MirCVoid -> "void"
    MirCSize -> "size_t"
    MirCInt -> "int"
    MirCStr -> "utf-8"
    MirCPtr -> "uptr"

-- ************************************************************************************************

-- | Generate a declaration for a MIR declaration.
genDecl :: MirDecl -> CodegenM ()
genDecl (MirDecl s t) = do
  genPrettyComment (mtype t)
  ostrln $ "(define " <> declNameSym s
  withInd $ genTerm emptyPCtx t
  ostrln ")"

-- | Generate code for a MIR term.
genTerm :: PCtx n -> MirTerm n -> CodegenM ()
genTerm ctx t = case mterm t of
  MirVar i -> ostrln (nameStr $ paramName $ indexPCtx i ctx)
  MirApp f rs -> do
    ostrln "("
    withInd $ do
      case mterm f of
        MirCtr sc -> ostrln $ "list '" <> ctorNameSym sc
        MirPrm s -> ostrln $ primNameSym s
        _ -> genTerm ctx f
      forM_ rs (genTerm ctx)
    ostrln ")"
  MirLam (MirLambda ps b) -> do
    let (ssRev, bctx) = consPCtxN (vreverse $ explicitParam (mloc t) . fst <$> ps) ctx
    let ss = map nameStr $ vtoList $ vreverse ssRev
    ostrln $ "(lambda (" <> unwords ss <> ")"
    withInd $ genTerm bctx b
    ostrln ")"
  MirLet (s, r) b -> do
    let (s', bctx) = consPCtx (explicitParam (mloc t) s) ctx
    ostrln "(let"
    withInd $ do
      ostrln $ "([" <> nameStr s'
      withInd $ genTerm ctx r
      ostrln "])"
    withInd $ genTerm bctx b
    ostrln ")"
  MirCas disc bs -> case bs of
    [] -> ostrln "'absurd"
    MirBranch _ p0 _:_ -> case mterm disc of
        MirVar i -> fcase $ nameStr $ paramName $ indexPCtx i ctx
        _ -> do
          sdisc <- genTempName "__disc"
          ostrln "(let"
          withInd $ do
            ostrln $ "([" <> sdisc
            withInd $ genTerm ctx disc
            ostrln "])"
            fcase sdisc
          ostrln ")"
      where
      fcase sdisc = do
        ostrln $ (case p0 of MirPatCtor {} -> "(record-case "; _ -> "(case ") <> sdisc
        withInd $ forM_ bs (genBranch (mloc t) ctx sdisc)
        ostrln ")"
  MirCnd cond a b -> do
    ostrln "(if"
    withInd $ do
      genTerm ctx cond
      genTerm ctx a
      genTerm ctx b
    ostrln ")"
  MirLit x -> ostrln $ genLit x
  MirDcl s -> ostrln $ declNameSym s
  MirCtr sc -> ostrln $ "(" <> "list '" <> ctorNameSym sc <> ")"
  MirPrm s -> ostrln $ "(" <> primNameSym s <> ")"

-- | Generate code for a MIR branch.
genBranch :: Loc -> PCtx n -> String -> MirBranch n -> CodegenM ()
genBranch l ctx sdisc (MirBranch s p b) = case p of
  MirPatCtor c ps -> do
    let (ssRev, bctx) = consPCtxN (vreverse $ explicitParam l . fst <$> ps) pctx
    let ss = map nameStr $ vtoList $ vreverse ssRev
    ostrln $ "((" <> ctorNameSym c <> ") (" <> unwords ss <> ")"
    withInd $ wrapDisc $ genTerm bctx b
    ostrln ")"
  MirPatLit v -> do
    ostrln $ "((" <> genLit v <> ")"
    withInd $ wrapDisc $ genTerm pctx b
    ostrln ")"
  MirPatWild -> do
    ostrln "(else"
    withInd $ wrapDisc $ genTerm pctx b
    ostrln ")"
  where
  (s', pctx) = consPCtx (explicitParam l s) ctx
  wrapDisc f = if s == Name "" then f else do
    ostrln $ "(let ([" <> nameStr s' <> " " <> sdisc <> "])"
    withInd f
    ostrln "])"

-- | Generate code for a literal.
genLit :: Literal -> String
genLit = \case
  Nat v -> show v
  Int v -> show v
  Char v -> "#\\x" <> showHex (ord v) ""
  Str v -> show v

-- ************************************************************************************************

-- | Get the symbol name for a MIR declaration name.
declNameSym :: MirDeclName -> String
declNameSym (MirDeclName s) = s

-- | Get the symbol name for a MIR constructor name.
ctorNameSym :: MirCtorName -> String
ctorNameSym (MirCtorName _ s) = s

-- | Get the symbol name for a MIR primitive operation.
primNameSym :: MirPrimOp -> String
primNameSym x = case x of
  PrmEffOp s -> effOpSym s
  PrmCCall s -> symName s
  _ -> "csic-prim-" <> drop 3 (show x)

-- | Get the symbol name for a MIR effect operation name.
effOpSym :: MirEffOpName -> String
effOpSym (MirEffOpName s) = "csic-eff-" <> s

symName :: MirCSymName -> String
symName (MirCSymName s) = "c-ffi-" <> s

-- | Convert a name to a string, replacing single quotes with underscores.
nameStr :: Name -> String
nameStr (Name s) = map (\c -> if c == '\'' then '_' else c) (unpack s)
