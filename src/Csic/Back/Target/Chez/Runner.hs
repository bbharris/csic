module Csic.Back.Target.Chez.Runner (initBackendChez) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.Target.Interface
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Cache
import Csic.Back.Target.Chez.Codegen
import Foreign.Ptr
import Foreign.C
import qualified Foreign as F
import Data.IORef
import Control.Concurrent
import GHC.IO.Handle.FD (handleToFd)
import GHC.IO.FD (fdFD)
import qualified System.Posix as P
import Data.Bits

-- ************************************************************************************************

-- | Represents the state of the Chez Scheme backend.
data ChezBackend = ChezBackend
  { progCache :: IORef (HashMap QName ChezPtr)  -- ^ Cache for compiled programs.
  , errorPtr  :: Ptr CString                    -- ^ Pointer to store error messages.
  }

-- | Initializes the Chez Scheme backend.
-- If the runtime system supports bound threads, it sets up an RPC mechanism.
-- Otherwise, it initializes Chez Scheme directly.
-- Returns the API for interacting with the backend.
initBackendChez :: IO BackendTargetApi
initBackendChez = do
  progCache <- newIORef mempty
  errptr <- F.malloc
  let st = ChezBackend progCache errptr
  if not rtsSupportsBoundThreads
    then do
      c_chezInit
      pure BackendTargetApi
        { targetClose = F.free errptr >> c_chezQuit
        , targetCompileProg = compProg st
        , targetCompileFile = compFile st
        }
    else do
      -- Create a channel for RPC requests.
      chan <- newChan
      let
        -- | Processes RPC requests in a loop.
        processRPCs :: IO ()
        processRPCs = join (readChan chan) >> processRPCs

        -- | Makes a direct RPC call.
        makeDirectRPC :: BackendM a -> BackendM a
        makeDirectRPC f = do
          res <- liftIO newEmptyMVar
          liftIO $ writeChan chan $ runExceptT (handleAll (checkError nullLoc . show) f) >>= putMVar res
          liftIO (takeMVar res) >>= liftEither

        -- | Makes an RPC call for a thunk.
        makeThunkRPC :: BackendM (Int -> BackendM a) -> BackendM (Int -> BackendM a)
        makeThunkRPC f = do
          thunk <- makeDirectRPC f
          pure $ makeDirectRPC . thunk

      -- Start a thread to process RPC requests.
      tid <- forkOS $ bracket c_chezInit (const $ F.free errptr >> c_chezQuit) $ \() -> processRPCs
      -- Return the API for interacting with the backend.
      pure BackendTargetApi
        { targetClose = killThread tid
        , targetCompileProg = \cfg p -> makeThunkRPC $ compProg st cfg p
        , targetCompileFile = \cfg p -> makeThunkRPC $ compFile st cfg p
        }

-- ************************************************************************************************

-- | Creates a cache target for the Chez Scheme backend.
cacheTarget :: ChezBackend -> CacheTarget ChezPtr
cacheTarget st = CacheTarget
  { targetName    = "Chez Scheme"               -- ^ Name of the target.
  , targetSrcExt  = ".ss"                       -- ^ Source file extension.
  , targetOutExt  = ".ss.so"                    -- ^ Output file extension.
  , targetCodegen = gencodeChez                 -- ^ Code generation function.
  , targetCompile = compFile' (errorPtr st)     -- ^ Compilation function.
  }

-- | Compiles a MIR program using the Chez Scheme backend.
-- Returns a function to execute the compiled program.
compProg :: ChezBackend -> BackendConfig -> MirProgram -> BackendM (Int -> BackendM Int)
compProg st cfg p = do
  let d = mirCDecl p
  f <- compileProgCached (cacheTarget st) (progCache st) cfg p
  pure $ execProg' (errorPtr st) cfg (declLoc d) (show $ declName d) f

-- | Compiles a source file using the Chez Scheme backend.
-- Returns a function to execute the compiled program.
compFile :: ChezBackend -> BackendConfig -> FilePath -> BackendM (Int -> BackendM Int)
compFile st cfg p = do
  f <- compileFileCached (cacheTarget st) (progCache st) cfg p
  pure $ execProg' (errorPtr st) cfg nullLoc (show p) f

-- ************************************************************************************************

-- | Represents a Chez Scheme object.
data ChezObj
type ChezPtr = Ptr ChezObj

-- | Initializes the Chez Scheme runtime.
foreign import ccall "chezInit" c_chezInit :: IO ()

-- | Shuts down the Chez Scheme runtime.
foreign import ccall "chezQuit" c_chezQuit :: IO ()

-- | Compiles a source file to a Chez Scheme object.
foreign import ccall "chezCompile" c_chezCompile :: Ptr CString -> CInt -> CString -> CString -> IO ChezPtr

-- | Executes a Chez Scheme object.
foreign import ccall "chezExecute" c_chezExecute :: Ptr CString -> ChezPtr -> CInt -> IO CInt

-- | Opens an output port in the Chez Scheme runtime.
foreign import ccall "chezRtOutputPortOpen" c_chezRtOutputPortOpen :: CInt -> IO ()

-- | Closes an output port in the Chez Scheme runtime.
foreign import ccall "chezRtOutputPortClose" c_chezRtOutputPortClose :: IO ()

-- | Compiles a source file to a Chez Scheme object, with error handling.
compFile' :: Ptr CString -> BackendConfig -> Bool -> FilePath -> FilePath -> BackendM ChezPtr
compFile' errptr cfg newer srcpath outpath = do
  ptr <- liftIO $ withCString srcpath $ \srcpath' -> withCString outpath $ c_chezCompile errptr flags srcpath'
  checkForError [] errptr
  pure ptr
  where
  flags = 0
    .|. (if backCaching  cfg && newer then bit 0 else 0)
    .|. (if backCaching  cfg          then bit 1 else 0)
    .|. (if backOptimize cfg          then bit 2 else 0)
    .|. (if backDumpAsm  cfg          then bit 3 else 0)

-- | Executes a compiled Chez Scheme object, with error handling.
execProg' :: Ptr CString -> BackendConfig -> Loc -> String -> ChezPtr -> Int -> BackendM Int
execProg' errptr cfg loc name f n = do
  forM_ (backStdout cfg) $ \h -> liftIO $ do
    fd <- fdFD <$> handleToFd h
    P.Fd fd' <- P.dup (P.Fd fd)
    c_chezRtOutputPortOpen fd'
  res <- liftIO $ c_chezExecute errptr f (fromIntegral n)
  forM_ (backStdout cfg) $ const $ liftIO c_chezRtOutputPortClose
  checkForError [strErr loc ("while executing " <> name <> " with Chez Scheme")] errptr
  pure $ fromIntegral res

-- | Checks for errors in the Chez Scheme runtime and throws an exception if any are found.
checkForError :: [CheckError] -> Ptr CString -> BackendM ()
checkForError ectx perr = do
  err <- liftIO $ F.peek perr
  unless (err == nullPtr) $ do
    msg <- liftIO $ peekCString err
    liftIO $ F.free err
    throwError $ ectx <> [strErr nullLoc msg]
