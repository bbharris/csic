module Csic.Back.Target.Runner
( Backend, BackendTarget(..), initBackend, closeBackend, compileProg, compileFile
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.Target.Interface
import Csic.Back.Target.Interp.Runner
import Csic.Back.Target.Chez.Runner
import Csic.Back.Target.C.Runner
import System.IO ( hPutStrLn, stdout, hPrint )
import Data.List (nub)

-- ************************************************************************************************

newtype Backend = Backend
  { _targets :: HashMap BackendTarget BackendTargetApi
  }
instance NFData Backend where
  rnf _ = ()

data BackendTarget
  = TargetInterp
  | TargetChez
  | TargetC
  deriving (Eq, Ord, Show, Hashable, Generic, NFData)

-- ************************************************************************************************

initBackend :: [BackendTarget] -> IO Backend
initBackend targets = do
  targets' <- forM (nub targets) $ \target -> fmap (target,) $ case target of
    TargetInterp -> initBackendInterp
    TargetChez -> initBackendChez
    TargetC -> initBackendC
  pure $ Backend (fromList targets')

closeBackend :: Backend -> IO ()
closeBackend (Backend targets) = mapM_ targetClose targets

-- | Lookup a backend target.
lookupTarget :: Backend -> BackendTarget -> BackendM BackendTargetApi
lookupTarget (Backend targets) target = case lookup target targets of
  Just x -> pure x
  Nothing -> checkError nullLoc $ "backend " <> show target <> " not loaded"

-- ************************************************************************************************

compileProg :: Backend -> BackendTarget -> BackendConfig -> MirProgram -> BackendM (Int -> BackendM Int)
compileProg be target cfg program = do
  -- Lookup the backend target.
  x <- lookupTarget be target
  -- Compile the program.
  fexec <- targetCompileProg x cfg program
  -- Return thunk that executes the program.
  if not (backVerbose cfg) then pure fexec else pure $ \n -> do
    -- Wrap execution with verbose logging.
    let h = fromMaybe stdout $ backStdout cfg
    liftIO $ hPutStrLn h $ "RUNNING[" <> show target <> "]: " <> show (declName $ mirCDecl program)
    res <- tryError $ fexec n
    case res of
      Right v -> do
        liftIO $ hPutStrLn h "SUCCESS:"
        liftIO $ hPrint h v
        pure v
      Left errs -> do
        liftIO $ hPutStrLn h "FAILURE:"
        liftIO $ hPutStrLn h $ prettyErrorsLn (PrettyEnv (printOpts $ getOpts $ mirCEnv program) 0) errs
        throwError errs

-- ************************************************************************************************

compileFile :: Backend -> BackendTarget -> BackendConfig -> FilePath -> BackendM (Int -> BackendM Int)
compileFile be target cfg program = do
  -- Lookup the backend target.
  x <- lookupTarget be target
  -- Compile the file.
  targetCompileFile x cfg program
