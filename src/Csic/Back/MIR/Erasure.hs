module Csic.Back.MIR.Erasure
( eraseProgram
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax

-- ************************************************************************************************

-- | Perform erasure on a MIR Program, removing all erased types and terms.
eraseProgram :: MirProgram -> CheckResult MirProgram
eraseProgram p = do
  types <- traverse eraseTypeDecl (mirTypes p)
  decls <- traverse eraseDecl (mirDecls p)
  entry <- eraseTerm (consECtx emptyECtx) (mirEntry p)
  pure p { mirTypes = types, mirDecls = decls, mirEntry = entry }

-- ************************************************************************************************

eraseDecl :: MirDecl -> CheckResult MirDecl
eraseDecl d = do
  term <- eraseTerm emptyECtx (mdeclTerm d)
  pure d { mdeclTerm = term }

eraseTerm :: ErasureCtx n m -> MirTerm n -> CheckResult (MirTerm m)
eraseTerm ctx t = do
  ty' <- eraseType l (mtype t)
  let fret x = pure t {mtype = ty', mterm = x}
  case mterm t of
    MirVar i -> ctx l i >>= fret . MirVar
    MirApp f rs -> do
      f' <- eraseTerm ctx f
      rs' <- forM rs $ \r -> case mtype r of
        MirTypePrm TypErased -> pure Nothing
        _ -> Just <$> eraseTerm ctx r
      fret $ MirApp f' (catMaybes rs')
    MirLam (MirLambda ps b) -> eraseLambda l ctx (Proxy @0) ps b >>= fret . MirLam
    MirLet (s, r) b -> case mtype r of
      MirTypePrm TypErased -> eraseTerm (consECtxErased ctx) b
      _ -> do
        r' <- eraseTerm ctx r
        b' <- eraseTerm (consECtx ctx) b
        fret $ MirLet (s, r') b'
    MirCas d bs -> case mtype d of
      MirTypePrm TypErased -> case bs of
        [] -> fret (MirPrm PrmUndefined)
        _ -> checkError l "erased case expression with branches"
      _ -> (MirCas <$> eraseTerm ctx d <*> traverse (eraseBranch (consECtx ctx)) bs) >>= fret
    MirCnd c a b -> (MirCnd <$> eraseTerm ctx c <*> eraseTerm ctx a <*> eraseTerm ctx b) >>= fret
    MirLit v -> fret (MirLit v)
    MirDcl n -> fret (MirDcl n)
    MirCtr n -> fret (MirCtr n)
    MirPrm p -> case p of
      PrmErased -> checkError l "erased primitive operation in non-erased position"
      _ -> fret (MirPrm p)
  where
  l = mloc t

eraseLambda :: forall i k n m. KnownNat i => Loc ->
  ErasureCtx (k + n) m -> Proxy k -> MirParams i -> MirTerm (i + k + n) -> CheckResult (MirLambda m)
eraseLambda l ctx _ ps b = case sNat @i of
  Zero -> MirLambda ps <$> eraseTerm ctx b
  Succ _ -> case pt of
    MirTypePrm TypErased -> eraseLambda l (consECtxErased ctx) (Proxy @(1 + k)) (vtail ps) b
    _ -> do
      pt' <- eraseType l pt
      MirLambda ps' b' <- eraseLambda l (consECtx ctx) (Proxy @(1 + k)) (vtail ps) b
      pure $ MirLambda (vcons (s, pt') ps') b'
    where
    (s, pt) = vhead ps

eraseBranch :: ErasureCtx (1 + n) (1 + m) -> MirBranch n -> CheckResult (MirBranch m)
eraseBranch ctx (MirBranch s p b) = case p of
  MirPatWild -> MirBranch s p <$> eraseTerm ctx b
  MirPatLit _ -> MirBranch s p <$> eraseTerm ctx b
  MirPatCtor c ps -> do
    MirLambda ps' b' <- eraseLambda (mloc b) ctx (Proxy @0) ps b
    pure $ MirBranch s (MirPatCtor c ps') b'

-- ************************************************************************************************

-- | Map variables from original context to erased context
type ErasureCtx n m = Loc -> Finite n -> CheckResult (Finite m)

emptyECtx :: ErasureCtx 0 0
emptyECtx _ _ = checkError nullLoc "empty context"

consECtx :: ErasureCtx n m -> ErasureCtx (1 + n) (1 + m)
consECtx ctx l i = case separateSum @1 i of
  Left i1 -> pure $ weaken i1
  Right j -> shift <$> ctx l j

consECtxErased :: ErasureCtx n m -> ErasureCtx (1 + n) m
consECtxErased ctx l i = case separateSum @1 i of
  Left _ -> checkError l "erased variable in non-erased position"
  Right j -> ctx l j

-- ************************************************************************************************

eraseTypeDecl :: MirTypeDecl -> CheckResult MirTypeDecl
eraseTypeDecl d = do
  ctors <- forM (mtypeCtors d) $ \(s, ps) -> (s,) . catMaybes <$> traverse (eraseTypeM nullLoc) ps
  pure d { mtypeCtors = ctors }

eraseTypeM :: Loc -> MirType -> CheckResult (Maybe MirType)
eraseTypeM l = \case
  MirTypePrm TypErased -> pure Nothing
  t -> Just <$> eraseType l t

eraseType :: Loc -> MirType -> CheckResult MirType
eraseType l = \case
  MirTypePrm p -> case p of
    TypErased -> checkError l "erased type in non-erased position"
    _ -> pure (MirTypePrm p)
  MirTypeRef r -> pure (MirTypeRef r)
  MirTypeFun ps rt -> do
    psM <- traverse (eraseTypeM l) ps
    rt' <- eraseType l rt
    pure (MirTypeFun (catMaybes psM) rt')
