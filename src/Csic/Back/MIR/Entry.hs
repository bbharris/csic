module Csic.Back.MIR.Entry
( checkEntryPointType
) where
import Csic.Util.Prelude
import Csic.Core.Environment

checkEntryPointType :: CEnv -> CTerm 0 -> CheckResult (CTerm 0, ([DeclInfo Void 0], CTerm 0))
checkEntryPointType env ty = do
  let ctx0 = emptyPCtx
  pit <- expectPiType env ctx0 ty
  case multiSOP (snd $ bindMulti pit) of
    [] -> checkError (paramLoc (bindInfo pit)) "entrypoint Nat arg multiplicity cannot be zero"
    _ -> pure ()
  expectNamedType env ctx0 (bindParam pit) "Nat"
  let ctx1 = snd $ consPCtx (bindInfo pit) ctx0
  rt0 <- nonDependent ctx1 "entrypoint result type" (bindResult pit)
  (bindParam pit,) <$> expectEffect env ctx0 rt0

expectPiType :: KnownNat n => CEnv -> PCtx n -> CTerm n -> CheckResult (Binder Void n)
expectPiType env ctx ty = case isPiType (evalToWHNF (initFuel env) env ctx ty) of
  Just pit -> pure pit
  Nothing -> checkErrorN (getLoc ty) "expected pi-type" [CErrTerm "actual type" $ ErrTerm ctx ty]

expectNamedType :: KnownNat n => CEnv -> PCtx n -> CTerm n -> Text -> CheckResult ()
expectNamedType env ctx ty s = case isNamed (evalToWHNF (initFuel env) env ctx ty) of
  Just (s', []) | s == s' -> pure ()
  _ -> checkErrorN (getLoc ty) ("expected " <> unpack s <> " type") [CErrTerm "actual type" $ ErrTerm ctx ty]

expectEffect :: KnownNat n => CEnv -> PCtx n -> CTerm n -> CheckResult ([DeclInfo Void n], CTerm n)
expectEffect env ctx ty = case isNamed (evalToWHNF (initFuel env) env ctx ty) of
  Just ("Effect", [fs, rt]) -> do
    expectNamedType env ctx (argTerm rt) "Int"
    (,argTerm rt) <$> expectAbilities env ctx (argTerm fs)
  _ -> checkErrorN (getLoc ty) "expected Effect type" [CErrTerm "actual type" $ ErrTerm ctx ty]

expectAbilities :: KnownNat n => CEnv -> PCtx n -> CTerm n -> CheckResult [DeclInfo Void n]
expectAbilities env ctx = frec []
  where
  frec res t = case isNamed (evalToWHNF (initFuel env) env ctx t) of
    Just ("'nil", [_]) -> pure (reverse res)
    Just ("'cons", [_, h, ts]) -> do
      f <- expectAbility env ctx (argTerm h)
      frec (f:res) (argTerm ts)
    _ -> checkErrorN (getLoc t) "expected Ability list" [CErrTerm "actual term" $ ErrTerm ctx t]

expectAbility :: KnownNat n => CEnv -> PCtx n -> CTerm n -> CheckResult (DeclInfo Void n)
expectAbility env ctx t = case evalToWHNF (initFuel env) env ctx t of
  (Con _ (DeclRef s pargs), args) -> do
    d <- lookupCDeclE (getLoc t) s env
    pure (d, pargs, args)
  _ -> checkErrorN (getLoc t) "expected ability" [CErrTerm "actual term" $ ErrTerm ctx t]

nonDependent :: KnownNat n => PCtx n -> String -> CTerm n -> CheckResult (CTerm 0)
nonDependent ctx s t = case mapFreeVars mempty t of
  Just t0 -> pure t0
  Nothing -> checkErrorN (getLoc t) ("unexpected dependent " <> s) [CErrTerm "dependent term" $ ErrTerm ctx t]
