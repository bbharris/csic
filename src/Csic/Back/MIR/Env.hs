module Csic.Back.MIR.Env where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax

-- ************************************************************************************************

type CompileM = StateT CompileEnv CheckResult

data CompileEnv = CompileEnv
  { compCEnv      :: CEnv
  , compDeclsDone :: [MirDecl]
  , compDeclsPend :: HashMap (IName, [MirInstArg], MirType) MirDeclName
  , compTypesRec  :: HashSet IName
  , compTypesHash :: HashMap MirTypeCtors MirDeclName
  , compTypesDone :: [MirTypeDecl]
  , compCSyms     :: [MirCSym]
  , compNext      :: Int
  }

instance HasCEnv CompileEnv where
  getCEnv = compCEnv
  mapCEnv f e = e {compCEnv = mapCEnv f (compCEnv e)}

type MCtx n = TCtx' MirType Void n

data MirInstArg
  = MirInstArg String [MirInstArg]
  deriving (Eq, Ord, Generic, Hashable)

-- ************************************************************************************************

compUniqueName :: String -> String -> CompileM String
compUniqueName prefix suffix = do
  st <- get
  put st {compNext = compNext st + 1}
  pure $ prefix <> show (compNext st) <> "_" <> suffix

nameStr :: Name -> String
nameStr (Name s) = map (\c -> if c == '\'' then '_' else c) (unpack s)

mirCtorName :: MirTypeName -> DeclName -> MirCtorName
mirCtorName sd sc = MirCtorName sd $ nameStr (qname $ relevant $ irrTag sc)

-- | Evaluate a term to WHNF in the current environment.
evalE :: (KnownNat n) => PCtx n -> CTerm n -> CompileM (WHNF Void n)
evalE ctx x = gets getCEnv >>= \env -> pure $ evalToWHNF (initFuel env) env ctx x
{-# INLINE evalE #-}

-- | Evaluate a term to WHNF in the current environment.
evalDefInline :: (KnownNat n) =>
  PCtx n -> (CDecl, Definition (CTerm 0)) -> Args Void n -> CEnv -> Maybe (WHNF Void n)
evalDefInline ctx (d, y) args env = if declHasAnnot AnnInline d
  then case evalTermInline ctx (mkApps (shiftTerm $ defImpl y) args) env of
    (Con _ (DeclRef s' _), _) | declName d == s' -> Nothing
    res -> Just res
  else Nothing
{-# INLINE evalDefInline #-}

-- | Evaluate a term to WHNF in the current environment.
evalTermInline :: (KnownNat n) => PCtx n -> CTerm n -> CEnv -> WHNF Void n
evalTermInline ctx x env = evalToWHNF (initFuel env) (mapOpts (\o -> o {evalBackend = True}) env) ctx x
{-# INLINE evalTermInline #-}
