module Csic.Back.MIR.Check where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.MIR.Env
import Csic.Back.MIR.Type

-- ************************************************************************************************

-- | Check a MIR term variable.
checkMirVar :: KnownNat n => MCtx n -> Loc -> Finite n -> CompileM (MirTerm n)
checkMirVar ctx l i = case indexTCtx i ctx of
  ((_, Just ty), _) -> pure $ MirTermC l ty $ MirVar i
  _ -> checkError l "MIR erased variable"

makeMirVar :: MirParams n -> Loc -> Finite n -> MirTerm n
makeMirVar ctx l i = MirTermC l (snd $ vindex ctx i) $ MirVar i

checkMirApp :: forall n. Loc -> MirTerm n -> [MirTerm n] -> CompileM (MirTerm n)
checkMirApp l f args0 = case mtype f of
  MirTypeFun pts rt -> do
    -- Check argument types subtype the parameter types.
    args <- zipWithM coerceToType args0 pts
    -- Compare the argument and parameter arity.
    let n = length args0
    let m = length pts
    case compare n m of
      -- If equal, return complete function application.
      EQ -> pure $ MirTermC l rt $ MirApp f args
      -- If greater, apply the extra arguments to the result.
      GT -> do
        let args' = drop m args0
        f' <- coerceToType (MirTermC l rt $ MirApp f args) (mkMirTypeFun (mtype <$> args') (MirTypePrm TypAny))
        checkMirApp l f' args'
      -- If less, transform the partial application into a closure of a full application.
      LT -> if null args0 && funcIsAtom
        then pure f
        else withParamVars l Proxy (drop n pts) $ \eps evs ->
          checkMirLam l eps $ MirTermC l rt $ MirApp (shiftMirTerm f) (map shiftMirTerm args <> vtoList evs)
  _ -> do
    unless (null args0) $ checkErrorN l "invalid function application"
      [ strErr nullLoc (showPretty $ mtype f)
      , strErr nullLoc (showPretty $ mtype <$> args0)
      ]
    pure f
  where
  funcIsAtom = case mterm f of
    MirCtr {} -> False
    MirPrm {} -> False
    _ -> True

checkMirLam :: forall k n. KnownNat k => Loc -> MirParams k -> MirTerm (k + n) -> CompileM (MirTerm n)
checkMirLam l ps b = case sNat @k of
  -- If no parameters, return the body.
  Zero -> pure b
  -- If at least one parameter, coallesce parameters with the function body.
  Succ _ -> case mterm b of
    MirLam (MirLambda ps' b') -> checkMirLam l (vappend ps ps') b'
    -- Check if the result type is a function.
    _ -> case mtype b of
      MirTypeFun pts _ -> withParamVars l Proxy pts $ \ps' vs ->
        checkMirApp l (shiftMirTerm b) (vtoList vs) >>= checkMirLam l (vappend ps ps')
      _ -> pure $ MirTermC l (mkMirTypeFun (map snd $ vtoList ps) (mtype b)) (MirLam $ MirLambda ps b)

makeMirLet :: Loc -> (Name, MirTerm n) -> MirTerm (1 + n) -> MirTerm n
makeMirLet l (s, a) b = MirTermC l (mtype b) $ MirLet (s, a) b

checkMirCas :: Loc -> MirType -> MirTerm n -> [MirBranch n] -> CompileM (MirTerm n)
checkMirCas l ty d bs = do
  bs' <- forM bs $ \(MirBranch s p b) -> MirBranch s p <$> coerceToType b ty
  pure $ MirTermC l ty $ MirCas d bs'

checkMirCnd :: Loc -> MirType -> MirTerm n -> MirTerm n -> MirTerm n -> CompileM (MirTerm n)
checkMirCnd l ty c a b = do
  c' <- coerceToType c (MirTypePrm TypBool)
  a' <- coerceToType a ty
  b' <- coerceToType b ty
  pure $ MirTermC l ty $ MirCnd c' a' b'

checkMirLit :: Loc -> Literal -> CompileM (MirTerm n)
checkMirLit l v = do
  ty <- liftE (lookupTermPrimE l (Name $ literalTypeName v) mempty)
  ty' <- monomorphType emptyTCtx ty
  pure $ MirTermC l ty' $ MirLit v

makeMirDcl :: Loc -> MirDeclName -> MirType -> MirTerm n
makeMirDcl l s ty = MirTermC l ty $ MirDcl s

checkMirCtr :: Loc -> DeclName -> MirType -> CompileM (MirTerm n)
checkMirCtr l sc ty = do
  sd <- expectTypeNameRef l (snd $ unMirTypeFun ty)
  pure $ MirTermC l ty $ MirCtr $ mirCtorName sd sc

makeMirPrm :: Loc -> MirPrimOp -> MirType -> MirTerm n
makeMirPrm l op ty = MirTermC l ty $ MirPrm op

-- ************************************************************************************************

-- | Coerce a term's type to the given type.
coerceToType :: MirTerm n -> MirType -> CompileM (MirTerm n)
coerceToType x ty = case (tx, ty) of
  -- If the types are the same, return the original term.
  _ | tx == ty -> pure x
  -- If left side is erased, we cannot coerce.
  (MirTypePrm TypErased, _) -> invalid
  -- If right side is erased, return a new erased term.
  (_, MirTypePrm TypErased) -> pure $ makeMirPrm l PrmErased ty
  -- If either side is the Any type, wrap with an explicit coercion.
  (_, MirTypePrm TypAny) -> checkMirApp l (makeMirPrm l PrmCoerce txy) [x]
  (MirTypePrm TypAny, _) -> checkMirApp l (makeMirPrm l PrmCoerce txy) [x]
  -- If both sides are function types, construct a new lambda to coerce the parameter types and result type.
  (MirTypeFun {}, MirTypeFun pys ry) ->
    withParamVars l Proxy pys $ \ps vs ->
      checkMirApp l (shiftMirTerm x) (vtoList vs) >>= (`coerceToType` ry) >>= checkMirLam l ps
  -- Otherwise we cannot coerce.
  _ -> invalid
  where
  l = mloc x
  tx = mtype x
  txy = MirTypeFun [tx] ty
  invalid = checkErrorN (mloc x) "invalid type coercion"
    [ strErr nullLoc (showPretty tx)
    , strErr nullLoc (showPretty ty)
    ]

-- ************************************************************************************************

withParamVars :: Loc -> Proxy n -> [MirType] ->
  (forall k. KnownNat k => MirParams k -> Vector k (MirTerm (k + n)) -> a) -> a
withParamVars l _ pts f = vwithList pts $ \ptsV -> f
  ((Name "",) <$> ptsV)
  ((\(pt, i) -> MirTermC l pt (MirVar (weaken $ mirror i))) <$> vzip ptsV vindices)
