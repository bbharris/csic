module Csic.Back.MIR.Compile
( compileCDeclToMIR
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.MIR.Env
import Csic.Back.MIR.Type
import Csic.Back.MIR.Check
import Csic.Back.MIR.Entry
import Csic.Back.MIR.Effect
import Csic.Back.MIR.Erasure

-- ************************************************************************************************

-- | Compile a core declaration to a Mid-level Intermediate Representation (MIR) program.
compileCDeclToMIR :: CEnv -> CDecl -> CheckResult MirProgram
compileCDeclToMIR env d = withError (CErrDecls "while compiling" (ErrDecls [d]) :) $ do
  -- Ensure entry-point has no prenex arguments.
  verifyPrenexArgs l (Right d) (mempty :: PrenexArgs Void)
  -- Check entry-point type is valid and extract external ability dependencies.
  (inputType, effectType) <- checkEntryPointType env (declType d)
  -- Compile entry-point definition.
  case declImpl d of
    IDef y -> do
      (entry, st) <- runStateT fgo (CompileEnv env mempty mempty mempty mempty mempty mempty 0)
      let p = MirProgram env d (reverse $ compTypesDone st) (reverse $ compCSyms st) (reverse $ compDeclsDone st) entry
      if True then eraseProgram p else pure p
      where
      fgo = do
        (s', fcomp) <- compileDefApps emptyTCtx (d, y) []
        fhandler <- compileEffectHandler l s' (snd $ unMirTypeFun $ mtype fcomp) effectType
        (_, _, bctx) <- monomorphParam emptyTCtx (nullParamInfo l) multiAny inputType
        n <- checkMirVar bctx l (finite @0)
        effect <- checkMirApp l (shiftMirTerm0 fcomp) [n]
        checkMirApp l (shiftMirTerm0 fhandler) [effect]
    _ -> checkError l "entry-point declaration must be a definition"
  where
  l = declLoc d

-- ************************************************************************************************

compileDeclApps :: KnownNat n => MCtx n -> Loc -> DeclName -> PrenexArgs Void -> Args Void n -> CompileM (MirTerm n)
compileDeclApps ctx l s pargs args = do
  -- Instantiate declaration prenex arguments.
  d <- fmap (instantiateTerm pargs) <$> liftE (lookupCDeclE l s)
  let
    ftype skip = monomorphTypeApps ctx (shiftTerm $ declType d) skip args
    fapply skip f = mapM (compileArg ctx) (drop skip args) >>= checkMirApp l f
  -- Check if declaration has a primitive operation.
  case show s of
    "J" -> do
      args' <- mapM (compileArg ctx) args
      case args' of
        _:_:f:x:_:_:rs -> checkMirApp l f (x:rs)
        _ -> checkError l "unsupported axiom J invocation"
    s' | Just primOp <- declHasPrimOp s' -> ftype 0 >>= fapply 0 . makeMirPrm l primOp
    _ -> case declImpl d of
      IAxiom -> checkError l $ "unsupported axiom " <> show (declName d)
      IData _ -> checkError l "runtime data type"
      ICtor y -> do
        dataDecl <- gets getCEnv >>= lookupCDeclE l (ctorDataTypeName y)
        skip <- dataTypeArity <$> expectDataType l dataDecl
        ftype skip >>= f >>= fapply skip
        where
        f = case ctorHasLit (declName d) of
          Just (_, primOp) -> pure . makeMirPrm l primOp
          Nothing -> checkMirCtr l (declName d)
      IDef y -> gets (evalDefInline (pCtx ctx) (d, y) args . getCEnv) >>= \case
        Just (hE, argsE) -> compileTermApps ctx (hE, argsE)
        Nothing -> snd <$> compileDefApps ctx (d, y) args

compileDefApps :: KnownNat n =>
  MCtx n -> (CDecl, Definition (CTerm 0)) -> Args Void n -> CompileM (MirDeclName, MirTerm n)
compileDefApps ctx (d, y) args0 = do
  ((t, ty), (iargs, args)) <- monomorphDefApps emptyPCtx ((defImpl y, declType d), ([], args0))
  let d' = d {declImpl = IDef y {defImpl = t}, declType = ty}
  ty' <- monomorphTypeApps ctx (shiftTerm ty) 0 args
  let k = (unTag $ declName d, iargs, ty')
  s' <- gets (lookup k . compDeclsPend) >>= \case
    Just s' -> pure s'
    Nothing -> withError (CErrDecls "while compiling" (ErrDecls [d']) :) $ do
      s' <- MirDeclName <$> compUniqueName "D" (nameStr (qname (declNameQ d)))
      modify $ \st -> st {compDeclsPend = insert k s' (compDeclsPend st)}
      t' <- compileTerm emptyTCtx t >>= (`coerceToType` ty')
      modify $ \st -> st {compDeclsDone = MirDecl s' t':compDeclsDone st}
      pure s'
  args' <- mapM (compileArg ctx) args
  (s',) <$> checkMirApp l (makeMirDcl l s' ty') args'
  where
  l = declLoc d

type Monomorph n r = ((CTerm n, CTerm n), ([MirInstArg], Args Void r))

monomorphDefApps :: KnownNat n => PCtx n -> Monomorph n r -> CompileM (Monomorph n r)
monomorphDefApps ctx ((t, ty), (iargs, args)) = case (unT t, args) of
  -- If definition is headed by a lambda and arguments remain, check if argument can be substituted directly.
  (Lam (Irr z) pt (u, m) b, arg:args') -> do
    pit <- expectPiType ctx l "monomorphDefApps" ty
    m' <- checkMultiLit l m
    isInstArg <- evalE ctx (bindParam pit) >>= \ptE -> gets (isDecl ptE . getCEnv) >>= \case
      Just (d, _, _) -> case declImpl d of
        IData y | isJust (dataClassInfo y) -> pure True
        _ -> pure False
      _ -> pure False
    case mapFreeVars mempty (argTerm arg) of
      Just arg0 | m' == MultiZero || isInstArg -> do
        let t' = substScope (vsingleton arg0) b
        let ty' = substScope (vsingleton arg0) (bindResult pit)
        monomorphDefApps ctx ((t', ty'), (maybe id (:) iargM iargs, args'))
        where
        iargM = if isInstArg
          then Just $ MirInstArg (prettyTermS (PrettyEnv printOptionsDefault 0) ctx 0 arg0 "") []
          else Nothing
      _ -> do
        let (_, bctx) = consPCtx (bindInfo pit) ctx
        let ft = mkLam (Irr z) pt (u, m)
        let fty = mkPit (Irr (bindInfo pit)) (bindParam pit) (bindMulti pit)
        bimap (bimap ft fty) (second (arg:)) <$> monomorphDefApps bctx ((b, bindResult pit), (iargs, args'))
  _ -> pure ((t, ty), (reverse iargs, args))
  where
  l = getLoc t

-- ************************************************************************************************

compileArg :: KnownNat n => MCtx n -> Arg Void n -> CompileM (MirTerm n)
compileArg ctx arg = do
  m' <- checkMultiLit l (argMulti arg)
  if m' == MultiZero
    then pure $ makeMirPrm l PrmErased (MirTypePrm TypErased)
    else compileTerm ctx (argTerm arg)
  where
  l = applyLoc $ argInfo arg

compileTerm :: KnownNat n => MCtx n -> CTerm n -> CompileM (MirTerm n)
compileTerm ctx t = compileTermApps ctx (unfoldApps t)

compileTermApps :: forall n. KnownNat n => MCtx n -> (Term' Void n, Args Void n) -> CompileM (MirTerm n)
compileTermApps ctx (f, args) = case f of
  Var _ i -> checkMirVar ctx l i >>= fapply
  App {} -> checkError l "invalid application head"
  Lam (Irr z) pt (_, m) b -> do
    (_, pt', bctx) <- monomorphParam ctx z m pt
    b' <- compileTerm bctx b
    checkMirLam l (vsingleton (paramName z, pt')) b' >>= fapply
  Cas _ d (_, m) ty bs -> do
    ty' <- monomorphType ctx ty
    dt <- gets (inferTermType ctx d . getCEnv) >>= liftEither
    instCtorType <- instantiateCtorType (pCtx ctx) <$> expectDeclRef (pCtx ctx) l dt
    (m', dt', pctx) <- monomorphParam ctx (nullParamInfo l) m dt
    sd' <- expectTypeNameRef l dt'
    d' <- compileTerm ctx d
    bs' <- mapM (compileBranch instCtorType sd' m' pctx) bs
    let
      fbranchLit (Branch _ p _) = case p of PatCtor sc _ -> ctorHasLit sc; _ -> Nothing

      fbranch0 :: String -> MirBranch n -> Maybe (MirTerm (1 + n))
      fbranch0 sc (MirBranch _ p b) = case p of
        MirPatCtor @k (MirCtorName _ sc') _ | sc == sc' -> case sNat @k of
          Zero -> Just b
          Succ _ -> Nothing
        _ -> Nothing
      expectBranch0 sc = case msum $ map (fbranch0 sc) bs' of
        Just b -> pure b
        Nothing -> checkError l $ "missing branch for " <> sc

      fbranch1 :: String -> MirBranch n -> Maybe (Name, MirTerm (2 + n))
      fbranch1 sc (MirBranch _ p b) = case p of
        MirPatCtor @k (MirCtorName _ sc') cps | sc == sc' -> case sNat @k of
          Succ Zero -> Just (fst $ vhead cps, b)
          _ -> Nothing
        _ -> Nothing
      expectBranch1 sc = case msum $ map (fbranch1 sc) bs' of
        Just b -> pure b
        Nothing -> checkError l $ "missing branch for " <> sc

    case msum $ map fbranchLit bs of
      Nothing -> checkMirCas l ty' d' bs' >>= fapply
      Just (litType, _) -> do
        disc <- checkMirVar pctx l (finite @0)
        body <- case (litType, length bs') of
          (TypNat, 2) -> do
            let fcond = makeMirPrm l PrmNatIsZero (mkMirTypeFun [MirTypePrm TypNat] (MirTypePrm TypBool))
            cond <- checkMirApp l fcond [disc]
            b0 <- expectBranch0 "_Z"
            (s1, b1) <- expectBranch1 "_S"
            let funpack = makeMirPrm l PrmNatSUnpack (mkMirTypeFun [MirTypePrm TypNat] (MirTypePrm TypNat))
            r1 <- checkMirApp l funpack [disc]
            checkMirCnd l ty' cond b0 (makeMirLet l (s1, r1) b1)
          (TypStr, 1) -> do
            (p0, b0) <- expectBranch1 "_str"
            let funpack = makeMirPrm l PrmStrUnpack (mkMirTypeFun [MirTypePrm TypStr] (MirTypePrm TypAny))
            r0 <- checkMirApp l funpack [disc]
            pure $ makeMirLet l (p0, r0) b0
          {-
          (MirBool, [(MirPatCtor _ (MirCtorName "_false") [], b0), (MirPatCtor _ (MirCtorName "_true") [], b1)]) -> do
          ("Int", [(MPatCtor (MirCtorName "_NegMinus1") [nv], nb), (MPatCtor (MirCtorName "_NonNeg") [pv], pb)]) -> do
            let cond = mkapps (mkprim "Int_isneg") [disc]
            let nb' = mkapps (mklams [nv] nb) [mkapps (mkprim "Int_NegMinus1_unpack") [disc]]
            let pb' = mkapps (mklams [pv] pb) [mkapps (mkprim "Int_NonNeg_unpack") [disc]]
            pure $ mkapps (mkprim "if") [cond, nb', pb']
          -}
          _ -> checkError l "unsupported literal unpack"
        fapply $ makeMirLet l (Name "disc", d') body
  Pit {} -> checkError l "runtime pi-type"
  Con _ c -> case c of
    TypeUniv {} -> checkError l "runtime type universe"
    DeclRef s pargs -> compileDeclApps ctx l s pargs args
    Literal x -> checkMirLit l x >>= fapply
  Met _ k _ -> absurd k
  where
  l = getLoc f
  fapply f' = mapM (compileArg ctx) args >>= checkMirApp l f'

-- ************************************************************************************************

compileBranch :: (KnownNat n) =>
  (Loc -> DeclName -> CompileM (CDecl, CTerm n)) -> MirTypeName -> MultiLit ->
  MCtx (1 + n) -> Branch Void n -> CompileM (MirBranch n)
compileBranch instCtorType sd' m' pctx (Branch @k (Irr z) p b) = do
  let l = bindLoc z
  (p', bctx) <- case p of
    PatWild -> pure (MirPatWild, pctx)
    PatCtor sc (Irr (_, argMap)) -> do
      -- Instantiate the constructor type with the discriminator's type arguments.
      (_, ctorType) <- instCtorType l sc
      -- Add constructor type parameters into context.
      let rpats = (\i -> maybe (Name "") (bindName . snd) $ lookup i argMap) <$> vindices @k
      (ps', bctx) <- bindCtorDiscParams pctx l m' (vreverse rpats) (shiftTerm ctorType)
      -- Construct the constructor pattern.
      pure (MirPatCtor (mirCtorName sd' sc) ps', bctx)
  b' <- compileTerm bctx b
  pure $ MirBranch (bindName z) p' b'

-- ************************************************************************************************

ctorHasLit :: DeclName -> Maybe (MirPrimType, MirPrimOp)
ctorHasLit s = case show s of
  -- "'false'"     -> Just (MirBool, PrmBoolFalse)
  -- "'true'"      -> Just (MirBool, PrmBoolTrue)
  "'Z'"         -> Just (TypNat, PrmNatZ)
  "'S'"         -> Just (TypNat, PrmNatS)
  "'NonNeg'"    -> Just (TypInt, PrmIntNonNeg)
  "'NegMinus1'" -> Just (TypInt, PrmIntNegMinus1)
  "'str'"       -> Just (TypStr, PrmStrPack)
  _             -> Nothing

declHasPrimOp :: String -> Maybe  MirPrimOp
declHasPrimOp s = case s of
  -- "J" -> Just MirAxiomJ
  "showAny" -> Just PrmShowAny
  "method__AdditiveMonoid_Nat_add" -> Just PrmNatAdd
  _ -> Nothing
