module Csic.Back.MIR.Syntax where
import Csic.Util.Prelude
import Csic.Core.Environment

-- ************************************************************************************************

-- | MIR (Mid-level Intermediate Representation) Program
data MirProgram = MirProgram
  { mirCEnv  :: CEnv  -- ^ Core Environment used to compile the MIR Program
  , mirCDecl :: CDecl -- ^ Core Declaration used to compile the MIR Program
  , mirTypes :: [MirTypeDecl] -- ^ List of MIR Type Declarations
  , mirCSyms :: [MirCSym] -- ^ List of C FFI Symbols
  , mirDecls :: [MirDecl] -- ^ List of MIR Declarations
  , mirEntry :: MirTerm 1 -- ^ Entry point of the MIR Program
  } deriving (Generic, NFData)

-- ************************************************************************************************

-- | Name of a declaration
newtype MirDeclName = MirDeclName String
  deriving newtype (Eq, Ord, NFData, Hashable, Show)

-- | MIR Declaration
data MirDecl = MirDecl
  { mdeclName :: MirDeclName -- ^ Name of the MIR Declaration
  , mdeclTerm :: MirTerm 0 -- ^ Term associated with the MIR Declaration
  } deriving (Generic, NFData)

-- | MIR term
data MirTerm n = MirTermC
  { mloc  :: Loc -- ^ Location information
  , mtype :: MirType -- ^ Type of the term
  , mterm :: MirTerm' n -- ^ Actual term
  } deriving (Generic, NFData)
instance Show (MirTerm n) where showsPrec p = showsPrec p . mterm

-- | Different kinds of MIR Terms
data MirTerm' n
  = MirVar (Finite n) -- ^ Variable
  | MirApp (MirTerm n) [MirTerm n] -- ^ Application of a term to a list of terms
  | MirLam (MirLambda n) -- ^ Lambda abstraction
  | MirLet (Name, MirTerm n) (MirTerm (1 + n)) -- ^ Let binding
  | MirCas (MirTerm n) [MirBranch n] -- ^ Case expression
  | MirCnd (MirTerm n) (MirTerm n) (MirTerm n) -- ^ Conditional if-then-else
  | MirLit Literal -- ^ Literal value
  | MirDcl MirDeclName -- ^ Declaration reference
  | MirCtr MirCtorName -- ^ Constructor reference
  | MirPrm MirPrimOp -- ^ Primitive operation
  deriving (Show, Generic, NFData)

-- | Lambda abstraction in MIR
data MirLambda n where
  MirLambda :: KnownNat k => MirParams k -> MirTerm (k + n) -> MirLambda n

-- | Parameters for a lambda abstraction
type MirParams k = Vector k (Name, MirType)

-- | Branch in a case expression
data MirBranch n where
  MirBranch :: KnownNat k => Name -> MirPattern k -> MirTerm (1 + k + n) -> MirBranch n

-- | Pattern in a case expression
data MirPattern k where
  MirPatWild :: MirPattern 0 -- ^ Wildcard pattern
  MirPatLit  :: Literal -> MirPattern 0 -- ^ Literal pattern
  MirPatCtor :: MirCtorName -> MirParams k -> MirPattern k -- ^ Constructor pattern

instance NFData (MirLambda n) where
  rnf (MirLambda ps t) = rnf ps `seq` rnf t
instance NFData (MirBranch n) where
  rnf (MirBranch x p t) = rnf x `seq` rnf p `seq` rnf t
instance NFData (MirPattern n) where
  rnf MirPatWild = ()
  rnf (MirPatLit l) = rnf l
  rnf (MirPatCtor c ps) = rnf c `seq` rnf ps

deriving instance Show (MirLambda n)
deriving instance Show (MirBranch n)
deriving instance Show (MirPattern n)

-- ************************************************************************************************

-- | Name of a type
newtype MirTypeName = MirTypeName String
  deriving newtype (Eq, Ord, NFData, Hashable, Show)

-- | Runtime abstract data type.
data MirTypeDecl = MirTypeDecl
  { mtypeName  :: MirTypeName -- ^ Name of the type
  , mtypeCtors :: MirTypeCtors -- ^ Constructors of the type
  } deriving (Eq, Ord, Show, Generic, NFData)

-- | Constructors for a type
type MirTypeCtors = [(MirCtorName, [MirType])]

-- | Name of a constructor
data MirCtorName = MirCtorName MirTypeName String
  deriving (Eq, Ord, Generic, NFData, Hashable, Show)

-- | Runtime type.
data MirType
  = MirTypePrm MirPrimType -- ^ Primitive type
  | MirTypeRef MirTypeName -- ^ Reference to a named type
  | MirTypeFun [MirType] MirType -- ^ Function type
  deriving (Eq, Ord, Generic, NFData, Hashable)

-- ************************************************************************************************

-- | Primitive types in MIR
data MirPrimType
  = TypErased -- ^ Erased type
  | TypBool -- ^ Boolean type
  | TypNat -- ^ Natural number type
  | TypInt -- ^ Integer type
  | TypChar -- ^ Character type
  | TypStr -- ^ String type
  | TypAny -- ^ Any type
  deriving (Eq, Ord, Show, Generic, NFData, Hashable)

-- | Primitive operations in MIR
data MirPrimOp
  = PrmUndefined -- ^ Undefined value
  | PrmErased -- ^ Erased value
  | PrmCoerce -- ^ Coercion operation
  | PrmShowAny -- ^ Show any value
  | PrmBoolFalse -- ^ Boolean false
  | PrmBoolTrue -- ^ Boolean true
  | PrmNatZ -- ^ Zero natural number
  | PrmNatS -- ^ Successor of a natural number
  | PrmNatSUnpack -- ^ Unpack successor of a natural number
  | PrmNatIsZero -- ^ Check if a natural number is zero
  | PrmNatAdd -- ^ Addition of natural numbers
  | PrmIntNonNeg -- ^ Non-negative integer
  | PrmIntNonNegUnpack -- ^ Unpack non-negative integer
  | PrmIntNegMinus1 -- ^ Negative integer minus one
  | PrmIntNegMinus1Unpack -- ^ Unpack negative integer minus one
  | PrmStrPack -- ^ Pack a string
  | PrmStrUnpack -- ^ Unpack a string
  | PrmEffOp MirEffOpName -- ^ Effect operation
  | PrmCCall MirCSymName -- ^ C FFI call
  deriving (Eq, Ord, Show, Generic, NFData, Hashable)

-- | Name of an effect operation
newtype MirEffOpName = MirEffOpName String
  deriving newtype (Eq, Ord, NFData, Hashable, Show)

-- ************************************************************************************************

-- | C FFI (Foreign Function Interface) library name
newtype MirCLibName = MirCLibName String
  deriving newtype (Eq, Ord, NFData, Hashable, Show)

-- | C FFI symbol name
newtype MirCSymName = MirCSymName String
  deriving newtype (Eq, Ord, NFData, Hashable, Show)

-- | C FFI symbol
data MirCSym = MirCSym
  { csymLib    :: MirCLibName -- ^ C shared library name to link against
  , csymName   :: MirCSymName -- ^ Name of the symbol
  , csymCConv  :: MirCConv -- ^ Calling convention
  , csymParams :: [MirCType] -- ^ Parameter types
  , csymResult :: MirCType -- ^ Return type
  } deriving (Eq, Ord, Show, Generic, NFData)

-- | C FFI primitive types
data MirCType
  = MirCVoid -- ^ C void (unit)
  | MirCSize -- ^ C size_t
  | MirCInt -- ^ C int (platform-dependent width)
  | MirCStr -- ^ C char* (null-terminated string)
  | MirCPtr -- ^ C void* (pointer)
  deriving (Eq, Ord, Show, Generic, NFData, Hashable)

-- | C FFI calling conventions
data MirCConv
  = MirCConvCDecl -- ^ C declaration calling convention
  | MirCConvVarArg Int -- ^ Variable arguments after a specified number of fixed arguments
  deriving (Eq, Ord, Show, Generic, NFData, Hashable)

-- ************************************************************************************************

-- | Shift a MIR Term with 0 variables to a term with n variables
shiftMirTerm0 :: MirTerm 0 -> MirTerm n
shiftMirTerm0 = coerce

-- | Shift a MIR Term with n variables to a term with k + n variables
shiftMirTerm :: forall k n. KnownNat k => MirTerm n -> MirTerm (k + n)
shiftMirTerm = fgo @0
  where
  fgo :: forall d. KnownNat d => MirTerm (d + n) -> MirTerm (d + k + n)
  fgo (MirTermC l ty t') = MirTermC l ty $ case t' of
    MirVar i -> MirVar $ case separateSum @d i of
      Left d -> weaken d
      Right n -> shift n
    MirApp t ts -> MirApp (fgo t) (map fgo ts)
    MirLam (MirLambda ps t) -> MirLam $ MirLambda ps $ fgo t
    MirLet (x, t) b -> MirLet (x, fgo t) (fgo b)
    MirCas t bs -> MirCas (fgo t) $ map (\(MirBranch s p b) -> MirBranch s p $ fgo b) bs
    MirCnd t1 t2 t3 -> MirCnd (fgo t1) (fgo t2) (fgo t3)
    MirLit x -> MirLit x
    MirDcl x -> MirDcl x
    MirCtr x -> MirCtr x
    MirPrm x -> MirPrm x

-- ************************************************************************************************

instance Show MirType where
  showsPrec :: Int -> MirType -> ShowS
  showsPrec p = \case
    MirTypePrm x -> showString (drop 3 (show x))
    MirTypeRef x -> showsPrec p x
    MirTypeFun ps rt -> showParen (p > 0) $ showList ps . showString " -> " . shows rt
