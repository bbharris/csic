module Csic.Back.MIR.Type where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.MIR.Env

-- ************************************************************************************************

type MirTypedArgs n = [(Arg Void n, MirType)]

-- | Monomorphize type applications by recursively processing the arguments.
-- If there are no arguments left, it calls 'monomorphType' to process the type.
-- Otherwise, it processes the current argument and continues with the rest.
monomorphTypeApps :: KnownNat n => MCtx n -> CTerm n -> Int -> Args Void n -> CompileM MirType
monomorphTypeApps ctx ty skip args = case args of
  [] -> monomorphType ctx ty
  arg:args' -> do
    pit <- expectPiType (pCtx ctx) l "monomorphTypeApps" ty
    let rt' = substScope (vsingleton $ argTerm arg) (bindResult pit)
    (_, pt', _) <- monomorphParam ctx (bindInfo pit) (snd $ bindMulti pit) (bindParam pit)
    let ftype = if skip > 0 then id else mkMirTypeFun [pt']
    ftype <$> monomorphTypeApps ctx rt' (skip - 1) args'
  where
  l = getLoc ty

-- | Monomorphize a parameter by checking its multiplicity and processing its type.
monomorphParam :: KnownNat n => MCtx n -> ParamInfo -> CMulti -> CTerm n -> CompileM (MultiLit, MirType, MCtx (1 + n))
monomorphParam ctx z m pt = do
  m' <- checkMultiLit (paramLoc z) m
  pt' <- if m' == MultiZero then pure (MirTypePrm TypErased) else monomorphType ctx pt
  pure (m', pt', consTCtx z (Just pt') pt ctx)

-- | Monomorphize a type by evaluating it and processing its head and arguments.
monomorphType :: KnownNat n => MCtx n -> CTerm n -> CompileM MirType
monomorphType ctx ty = do
  (h, args) <- evalE (pCtx ctx) ty
  case h of
    Var {} -> pure $ MirTypePrm TypAny
    App {} -> checkError l "unexpected application"
    Lam {} -> pure $ MirTypePrm TypAny -- checkError l "unexpected lambda"
    Cas {} -> pure $ MirTypePrm TypAny -- checkError l "unexpected case"
    Pit (Irr z) pt m rt -> do
      unless (null args) $ checkError l "unexpected applied pi-type"
      (_, pt', bctx) <- monomorphParam ctx z (snd m) pt
      mkMirTypeFun [pt'] <$> monomorphType bctx rt
    Con _ c -> case c of
      TypeUniv {} -> checkError l "unexpected universe" -- pure $ MtPrm $ MPrimName "TypeUniv"
      DeclRef s pargs -> do
        d <- fmap (instantiateTerm pargs) <$> liftE (lookupCDeclE l s)
        case declIsPrimType d of
          Just primType -> pure $ MirTypePrm primType
          Nothing -> case declImpl d of
            IData _di -> pure $ MirTypePrm TypAny -- compileDataType ctx d di pargs args
            _ -> checkError l "unsupported runtime type declaration"
      Literal {} -> checkError l "unexpected literal"
    Met _ k _ -> absurd k
  where
  l = getLoc ty

{-
compileDataType :: (KnownNat n) => MCtx n -> CDecl -> DataTypeInfo -> PrenexArgs Void -> Args Void n -> CompileM MirType
compileDataType ctx d di pargs args = do
  let k = unTag $ declName d
  gets (member k . compTypesRec) >>= \case
    True -> pure MtTop
    False -> do
      modify $ \st -> st {compTypesRec = insertS k (compTypesRec st)}
      cs <- forM (dataCtorInfos di) $ \(sc, _) -> do
        cd <- fmap (instantiateTerm pargs) <$> liftE (lookupCDeclE l sc)
        (cty, _) <- compileAppliedType' False ctx (shiftTerm $ declType cd) args
        let sc' = MirCtorName $ nameStr (qname $ relevant $ irrTag sc)
        fmap (sc',) $ case cty of
          MtFun cps _ -> pure cps
          _ -> pure []
      (s', fhash, fdone) <- gets (lookup cs . compTypesHash) >>= \case
        Just s' -> pure (s', id, id)
        Nothing -> do
          s' <- MirDeclName <$> compUniqueName "T" (show (declName d))
          pure (s', insert cs s', (MirTypeDecl s' cs:))
      modify $ \st -> st
        { compTypesRec = delete k (compTypesRec st)
        , compTypesHash = fhash (compTypesHash st)
        , compTypesDone = fdone (compTypesDone st)
        }
      pure $ MtRef s'
  where
  l = declLoc d
-}

-- | Instantiate a constructor type with its data type arguments.
instantiateCtorType :: KnownNat n => PCtx n -> DeclInfo Void n -> Loc -> DeclName -> CompileM (CDecl, CTerm n)
instantiateCtorType ctx (_, dataTypePrenex, dataTypeArgs) l sc = do
  ctorDecl <- gets getCEnv >>= lookupCDeclE l sc
  let ctorType0 = instantiateTerm dataTypePrenex (declType ctorDecl)
  (ctorDecl,) <$> applyPiType ctx l (shiftTerm ctorType0) dataTypeArgs

-- | Bind constructor discriminant parameters by processing each parameter and its type.
bindCtorDiscParams :: forall k n. (KnownNat n, KnownNat k) =>
  MCtx n -> Loc -> MultiLit -> Vector k Name -> CTerm n ->
  CompileM (MirParams k, MCtx (k + n))
bindCtorDiscParams ctx l multiMask names ty = case sNat @k of
  Zero -> evalE (pCtx ctx) ty >>= \case
    (Pit {}, _) -> checkErrorN l "too few constructor parameters" [CErrTerm "" $ ErrTerm (pCtx ctx) ty]
    _ -> pure (vempty, ctx)
  Succ _ -> evalE (pCtx ctx) ty >>= \case
    (Pit _ pt (_, m) rt, []) -> do
      let s = vhead names
      m' <- multiLitTimes multiMask <$> checkMultiLit l m
      (_, pt', bctx) <- monomorphParam ctx (explicitParam l s) (multiLit m') pt
      first (vcons (s, pt')) <$> bindCtorDiscParams bctx l multiMask (vtail names) rt
    _ -> checkErrorN l "too many constructor parameters" [CErrTerm "" $ ErrTerm (pCtx ctx) ty]

-- ************************************************************************************************

-- | Create a function type from a list of parameter types and a return type.
mkMirTypeFun :: [MirType] -> MirType -> MirType
mkMirTypeFun ps rt = if null ps then rt else case rt of
  MirTypeFun ps' rt' -> MirTypeFun (ps <> ps') rt'
  _ -> MirTypeFun ps rt

-- | Decompose a function type into its parameter types and return type.
unMirTypeFun :: MirType -> ([MirType], MirType)
unMirTypeFun = \case
  MirTypeFun ps rt -> (ps, rt)
  ty -> ([], ty)

-- ************************************************************************************************

-- | Expect a runtime data type reference and return its name.
expectTypeNameRef :: Loc -> MirType -> CompileM MirTypeName
expectTypeNameRef l rt = case rt of
  MirTypePrm p -> pure $ MirTypeName (drop 3 $ show p)
  _ -> checkError l "expected runtime data type reference"

-- ************************************************************************************************

-- | Expect a declaration reference and return its information.
expectDeclRef :: KnownNat n => PCtx n -> Loc -> CTerm n -> CompileM (DeclInfo Void n)
expectDeclRef ctx l ty = evalE ctx ty >>= \x -> gets (isDecl x . getCEnv) >>= \case
  Nothing -> checkErrorN l "expected declaration reference" [CErrTerm "" $ ErrTerm ctx ty]
  Just di -> pure di

-- | Expect a data type declaration and return its information.
expectDataType :: Loc -> CDecl -> CompileM DataTypeInfo
expectDataType l d = case declImpl d of
  IData y -> pure y
  _ -> checkError l "expected data-type"

expectPiType :: KnownNat n => PCtx n -> Loc -> String -> CTerm n -> CompileM (Binder Void n)
expectPiType ctx l msg ty = evalE ctx ty <&> isPiType >>= \case
  Nothing -> checkErrorN l "expected pi-type" [CErrTerm msg $ ErrTerm ctx ty]
  Just pit -> pure pit

-- | Apply a pi-type to a list of arguments.
applyPiType :: KnownNat n => PCtx n -> Loc -> CTerm n -> Args Void n -> CompileM (CTerm n)
applyPiType ctx l x = \case
  [] -> pure x
  arg:args -> evalE ctx x >>= \case
    (Pit _ _ _ rt, []) -> applyPiType ctx l (substScope (vsingleton $ argTerm arg) rt) args
    _ -> checkError l "invalid pi-type application"

checkMultiLit :: Loc -> CMulti -> CompileM MultiLit
checkMultiLit l m = case multiSOP m of
  [] -> pure MultiZero
  [([], n)] -> pure n
  _ -> checkError l "expected multi literal"

-- ************************************************************************************************

-- | Check if a declaration is a primitive type and return the corresponding 'MirPrimType'.
declIsPrimType :: CDecl -> Maybe MirPrimType
declIsPrimType d = case show (declName d) of
  "Bool" -> pure TypBool
  "Nat"  -> pure TypNat
  "Int"  -> pure TypInt
  "Char" -> pure TypChar
  "Str"  -> pure TypStr
  "Ref"  -> pure TypAny
  "CPtr" -> pure TypAny
  _  -> Nothing