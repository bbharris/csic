module Csic.Back.MIR.Effect
( compileEffectHandler
) where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Back.MIR.Syntax
import Csic.Back.MIR.Env
import Csic.Back.MIR.Type
import Csic.Back.MIR.Check

compileEffectHandler :: Loc -> MirDeclName -> MirType -> ([DeclInfo Void 0], CTerm 0) -> CompileM (MirTerm 0)
compileEffectHandler l (MirDeclName baseName) effType (abilities, resultType) = do
  t <- genFuncTerm >>= (`coerceToType` funcType)
  modify $ \st -> st {compDeclsDone = MirDecl funcName t : compDeclsDone st}
  pure funcRef
  where
  natType = MirTypePrm TypNat
  intType = MirTypePrm TypInt
  anyType = MirTypePrm TypAny
  eraType = MirTypePrm TypErased
  any2EffType = MirTypeFun [anyType] effType

  funcName = MirDeclName $ baseName <> "__handler"
  funcType = mkMirTypeFun [effType] intType
  funcRef = makeMirDcl l funcName funcType

  genFuncTerm = genLambda vempty l "eff" effType $ \ctx eff -> do
    genCase ctx l intType eff $ \pctx sdisc -> do
      ret <- do
        let sc = MirCtorName (MirTypeName "Any") "_return"
        let cps = vsingleton (Name "v", intType)
        let bctx = vreverseAppend cps pctx
        MirBranch sdisc (MirPatCtor sc cps) <$> do
          pure $ makeMirVar bctx l (finite @0)
      bind <- do
        let sc = MirCtorName (MirTypeName "Any") "_bindOp"
        let cps = vcons (Name "_", eraType) $ vcons (Name "u", anyType) $ vsingleton (Name "k", any2EffType)
        let bctx = vreverseAppend cps pctx
        MirBranch sdisc (MirPatCtor sc cps) <$> do
          cont <- do
            res <- genCase bctx l anyType (makeMirVar bctx l (finite @1)) genOpUnionBranches
            checkMirApp l (makeMirVar bctx l (finite @0)) [res]
          checkMirApp l (shiftMirTerm funcRef) [cont]
      pure [ret, bind]

  genOpUnionBranches pctx sdisc = do
    let sc = MirCtorName (MirTypeName "Any") "_opUnion"
    let cps = vcons (Name "i", natType) $ vsingleton (Name "op", anyType)
    let bctx = vreverseAppend cps pctx
    b <- genCase bctx l anyType (makeMirVar bctx l (finite @1)) genAbilityBranches
    pure [MirBranch sdisc (MirPatCtor sc cps) b]

  genAbilityBranches pctx sdisc = forM (zip abilities [0..]) $ \(ability, i) ->
    MirBranch sdisc (MirPatLit (Nat i)) <$> genAbilityBody pctx ability

  genAbilityBody ctx (d, pargs, argsF) = do
    -- Extend ability args with the actual result type
    let args = argsF <> [Arg (nullApplyInfo l) multiZero resultType]
    let di = (d, pargs, args)
    -- Get the data type info of the ability
    y <- expectDataType l d
    -- Check if the ability has an FFI library annotation
    let libNameM = fmap MirCLibName $ msum $ map (\case {AnnLib s -> Just s; _ -> Nothing}) (declAnnots d)
    -- Monomorphize the ability type
    ty' <- monomorphType emptyTCtx (mkApps (mkCon (Irr l) (DeclRef (declName d) pargs)) args)
    sd' <- expectTypeNameRef l ty'
    -- Generate case split on the ability operation constructors
    genCase ctx l anyType (makeMirVar ctx l (finite @1)) $ \pctx sdisc ->
      forM (dataCtorInfos y) $ \(sc, k) -> withSNat (fromIntegral k) $ \(_ :: SNat k) -> do
        -- Construct the branch parameters from the constructor type
        (ctorDecl, ctorType) <- instantiateCtorType emptyPCtx di l sc
        let lc = declLoc ctorDecl
        let names = Name "" <$ vindices @k
        (cpsAll, _) <- bindCtorDiscParams emptyTCtx l MultiOne names ctorType
        let bctx = vreverseAppend cpsAll pctx
        -- Discard erased parameters directly (handlers don't expect them, with or without erasure enabled)
        let crsAll = map (makeMirVar bctx lc . weaken . mirror) (vtoList $ vindices @k)
        let (cpsReal, crsReal) = unzip $ filter ((/= eraType) . snd . fst) (zip (vtoList cpsAll) crsAll)
        -- Extract the operation result type from the last constructor parameter (equality proof)
        (cpsCM, crtCM, crt) <- extractOpCTypesAndResultType emptyTCtx ctorType
        -- Generate the branch body
        b <- case libNameM of
          -- If ability has an FFI library annotation, generate a C call
          Just libName -> do
            -- Check if the constructor has an FFI symbol name annotation
            let anns = declAnnots ctorDecl
            let symNameM = msum $ map (\case {AnnSym s -> Just s; _ -> Nothing}) anns
            -- If it doesn't, use the constructor name as the symbol name
            let symName = MirCSymName $ fromMaybe (drop 1 $ nameStr (qname $ declNameQ ctorDecl)) symNameM
            -- Check if the constructor has an FFI calling convention annotation
            let cconvM = msum $ map (\case {AnnVarargs n -> Just (MirCConvVarArg n); _ -> Nothing}) anns
            let cconv = fromMaybe MirCConvCDecl cconvM
            -- Ensure the constructor parameters and result types are C types.
            cpsC <- sequence cpsCM
            crtC <- crtCM
            -- Add C symbol to the environment
            let csym = MirCSym libName symName cconv cpsC crtC
            modify $ \st -> st {compCSyms = csym:compCSyms st}
            -- Generate the FFI call
            let f = makeMirPrm l (PrmCCall symName) (mkMirTypeFun (snd <$> cpsReal) crt)
            checkMirApp lc f crsReal
          -- Otherwise, generate a call to an effect operation handler
          Nothing -> do
            let s = MirEffOpName $ nameStr (qname $ relevant $ irrTag (declName d)) <>
                      nameStr (qname $ relevant $ irrTag sc)
            let f = makeMirPrm lc (PrmEffOp s) (mkMirTypeFun (snd <$> cpsReal) crt)
            checkMirApp lc f crsReal
        -- Construct the branch
        pure $ MirBranch sdisc (MirPatCtor (mirCtorName sd' sc) cpsAll) b

genLambda :: MirParams n -> Loc -> Text -> MirType ->
  (MirParams (1 + n) -> MirTerm (1 + n) -> CompileM (MirTerm (1 + n))) -> CompileM (MirTerm n)
genLambda ctx l s pt f =
  checkMirLam l (vsingleton (Name s, pt)) =<< f bctx (makeMirVar bctx l (finite @0))
  where
  bctx = vcons (Name s, pt) ctx

genCase :: MirParams n -> Loc -> MirType -> MirTerm n ->
  (MirParams (1 + n) -> Name -> CompileM [MirBranch n]) -> CompileM (MirTerm n)
genCase ctx l ty disc f =
  checkMirCas l ty disc =<< f pctx sdisc
  where
  sdisc = Name ""
  pctx = vcons (sdisc, mtype disc) ctx

extractOpCTypesAndResultType :: KnownNat n => MCtx n -> CTerm n ->
  CompileM ([CompileM MirCType], CompileM MirCType, MirType)
extractOpCTypesAndResultType ctx ty = case unT ty of
  Pit (Irr z) pt (_, m) rt -> case unT rt of
    Pit {} -> do
      (m', _, bctx) <- monomorphParam ctx z m pt
      (cps, crt, rt') <- extractOpCTypesAndResultType bctx rt
      pure (if m' == MultiZero then cps else checkCType pt:cps, crt, rt')
    _ -> case unfoldApps pt of
      (Con {}, [_, _, rhs]) -> do
        rhs' <- monomorphType ctx (argTerm rhs)
        pure ([], checkCType (argTerm rhs), rhs')
      _ -> checkErrorN l "expected ability result equality proof" [CErrTerm "" $ ErrTerm (pCtx ctx) pt]
  _ -> checkErrorN l "expected ability operation function type" [CErrTerm "" $ ErrTerm (pCtx ctx) ty]
  where
  l = getLoc ty

checkCType :: CTerm n -> CompileM MirCType
checkCType ty = gets (isDecl (unfoldApps ty) . getCEnv) >>= \case
  Just (d, _, _) -> case show (declName d) of
    "CVoid" -> pure MirCVoid
    "CSize" -> pure MirCSize
    "CInt" -> pure MirCInt
    "CStr" -> pure MirCStr
    "CPtr" -> pure MirCPtr
    _ -> checkError (getLoc ty) "expected C type"
  _ -> checkError (getLoc ty) "expected C type"
