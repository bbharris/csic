#!/bin/bash
set -e

# parse command argument
CMD=""
if [ -n "$1" ]; then
  CMD=$1
  shift
fi

LOCAL_LIBS=./.libs
export LD_LIBRARY_PATH=$LOCAL_LIBS

MAKE_BACKEND_CHEZ="make -f src/runtime/libcsic-chez/Makefile"
MAKE_BACKEND_C="make -f src/runtime/libcsic-c/Makefile"

# execute command
case $CMD in
  run)
    $MAKE_BACKEND_CHEZ
    $MAKE_BACKEND_C
    stack build --bench --no-run-benchmarks --test --no-run-tests
    stack exec csic -- $@
    ;;
  test)
    $MAKE_BACKEND_CHEZ
    $MAKE_BACKEND_C
    stack build --bench --no-run-benchmarks --test --ta "$*"
    ;;
  bench)
    $MAKE_BACKEND_CHEZ
    $MAKE_BACKEND_C
    stack build --bench --ba "$*" --test --no-run-tests
    ;;
  prof)
    $MAKE_BACKEND_CHEZ
    $MAKE_BACKEND_C
    # build with profiling enabled and run benchmark
    mkdir -p .prof
    STACKP="stack --work-dir .prof/.stack-work"
    $STACKP build --profile --ghc-options '-fprof-late' --bench --ba "+RTS -P -RTS -n 30 effect $*"
    # find next output directory number
    NUM=$(
      find .prof -maxdepth 1 -mindepth 1 -type d -name 'run-*' |
      sed -E 's/.prof\/run-([0-9]+).*/\1/' | sort -n | tail -n1
    )
    NUM=$((NUM + 1))
    # move profiling data to output directory and process with profiterole
    OUTDIR=.prof/run-$NUM
    mkdir -p $OUTDIR
    mv csic-bench.prof $OUTDIR
    $STACK exec profiterole $OUTDIR/csic-bench.prof
    ;;
  clobber)
    rm -rf ./.libs
    stack clean --full
    ;;
  *)
    echo "invalid command '$CMD'"
    exit 1
    ;;
esac
