#include <csic-c.h>

static csic_Nat testSum(csic_Nat acc, csic_Nat n) {
  return n == 0 ? acc : testSum(acc + n, n - 1);
}
csic_Int csic_entrypoint(csic_Nat n) {
  return testSum(0, n);
}
