(library (bench-exec-ffi-1)
  (export main)
  (import (chezscheme))

  (define c-malloc (foreign-procedure "malloc" (size_t) uptr))
  (define c-free   (foreign-procedure "free"   (uptr) void))
  (define c-memset (foreign-procedure "memset" (uptr int size_t) uptr))
  (define c-memcmp (foreign-procedure "memcmp" (uptr uptr size_t) int))

  (define (main n)
    (let* (
      [ptr (c-malloc n)]
      [_ (c-memset ptr 0 n)]
      [res (c-memcmp ptr ptr n)]
      [_ (c-free ptr)]
      )
      res
    ))
)
(import (bench-exec-ffi-1))
(define csic-entrypoint main)