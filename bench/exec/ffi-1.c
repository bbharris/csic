#include <stdlib.h>
#include <string.h>

int csic_entrypoint(unsigned n) {
  void *volatile ptr = malloc(n);
  memset(ptr, 0, n);
  int res = memcmp(ptr, ptr, n);
  free(ptr);
  return res;
}
