module Main (main) where
import Csic.Util.Prelude
import Csic.Front.Syntax
import Csic.Core.Environment
import Csic.Front.Check
import Csic.Front.Load
import Csic.Back.Api
import Criterion.Main
import qualified Data.Map.Strict as M
import qualified Data.IntMap.Strict as IM
import qualified Data.HashMap.Strict as HM
import qualified Data.Sequence as S
import qualified System.Random as R

main :: IO ()
main = defaultMain
  [ env (snd <$> loadModule' fenv0 "src/stdlib/prelude") $ \fe -> bgroup "func"
    [ bgroup "check"
      [ benchCheckModule fe "bench/check/" "scale-1"
      , benchCheckModule fe "bench/check/" "scale-2"
      , benchCheckModule fe "bench/check/" "scale-3"
      ]
    , envWithCleanup (initBackend [TargetInterp, TargetChez, TargetC]) closeBackend $ \be -> bgroup "exec"
      [ benchExecModule fe be "bench/exec/" "ffi-1"
          [ ("a", 53000, 0, [TargetChez, TargetInterp])
          ]
      , benchExecModule fe be "bench/exec/" "sum-1"
          [ ("a",   100,     5050, [TargetChez, TargetInterp])
          , ("b", 10000, 50005000, [TargetChez, TargetInterp])
          ]
      ]
    ]
  , env initDataEnv $ \st -> bgroup "data"
    [ benchDataOp st "index" benchDataIndex
    , benchDataOp st "cons" benchDataCons
    , benchDataOp st "insert" benchDataInsert
    ]
  ]
  where
  fenv0 = initFrontEnv $ initCEnv coreOptionsDefault
    { coreFuelMax = 10000000
    , printOpts = printOptionsDefault {showLocations = True}
    }

-- ************************************************************************************************

loadModule' :: FrontEnv -> FilePath -> IO (Namespace, FrontEnv)
loadModule' fenv s = do
  (res, fenv') <- runStateT (runExceptT (loadModule nullLoc s)) fenv
  case res of
    Left err -> error $ prettyErrorsLn (PrettyEnv (printOpts $ getOpts fenv) 0) err
    Right ns -> pure (ns, fenv')

loadDecls' :: FrontEnv -> Namespace -> IO ([EDecl ExprExt], FrontEnv)
loadDecls' fenv ns = do
  (res, fenv') <- runStateT (runExceptT (loadModuleDecls nullLoc ns)) fenv
  case res of
    Left _ -> error $ "failed to load " <> show ns
    Right decls -> pure (decls, fenv')

benchCheckModule :: FrontEnv -> FilePath -> FilePath -> Benchmark
benchCheckModule fenv0 base s = env (loadDecls' fenv ns) $ bench s . nf (benchCheckDecls ns)
  where
  ns = Namespace (pack $ base <> s)
  fenv = mapOpts (\x -> x {levelCheckOff = True, multiCheckOff = True}) fenv0

benchCheckDecls :: Namespace -> ([EDecl ExprExt], FrontEnv) -> [CDecl]
benchCheckDecls ns (decls, fenv) = case checkModule ns decls fenv of
  Left err -> error $ prettyErrorsLn (PrettyEnv (printOpts $ getOpts fenv) 0) err
  Right fenv' -> lookupModuleLocalCDecls ns fenv'
{-# SCC benchCheckDecls #-}

-- ************************************************************************************************

newtype BackendThunk = BackendThunk (Int -> BackendM Int)

instance NFData BackendThunk where
  rnf (BackendThunk f) = rwhnf f

type RunConfig = (String, Int, Int, [BackendTarget])

benchExecModule :: FrontEnv -> Backend -> FilePath -> String -> [RunConfig] -> Benchmark
benchExecModule fe be base s cfgs = env (loadProgram' fe (base <> s)) $ \p -> bgroup s $
  map (mkcfg p) cfgs
  where
  mkcfg p (suf, n, gold, tgts) = bgroup suf
    [ bgroup "ref" $ map mkref [TargetC, TargetChez]
    , bgroup "comp" $ map mkcomp tgts
    ]
    where
    mkref tgt = benchTarget (compileFile be) fe tgt (base <> s) n gold
    mkcomp tgt = benchTarget (compileProg be) fe tgt p n gold

loadProgram' :: FrontEnv -> FilePath -> IO MirProgram
loadProgram' fenv0 s = do
  (ns, fenv) <- loadModule' fenv0 s
  case filter (("main" ==) . show . declName) $ lookupModuleLocalCDecls ns fenv of
    [d] -> case compileCDeclToMIR (getCEnv fenv) d of
      Left err -> error $ prettyErrorsLn (PrettyEnv (printOpts $ getOpts fenv) 0) err
      Right p -> pure p
    _ -> error $ "ambiguous entry point for " <> show s

benchTarget :: (BackendTarget -> BackendConfig -> p -> BackendM (Int -> BackendM Int)) -> FrontEnv ->
  BackendTarget -> p -> Int -> Int -> Benchmark
benchTarget fc fe target p n gold = env fcomp $ bench (show target) . nfIO . fexec
  where
  rtcfg = backConfigDefault {backStdout = Nothing, backVerbose = False, backCaching = False, backDumpAsm = True}
  fcomp = runExceptT (fc target rtcfg p) >>= \case
    Right f -> pure $ BackendThunk f
    Left err -> error $ prettyErrorsLn (PrettyEnv (printOpts $ getOpts fe) 0) err
  fexec (BackendThunk f) = runExceptT (f n) >>= \case
    Right res -> unless (res == gold) $ error $ "bad result " <> show res
    Left err -> error $ prettyErrorsLn (PrettyEnv (printOpts $ getOpts fe) 0) err

-- ************************************************************************************************

data DataValues = DataValues
  { dataSize :: Int
  , dataRepeat :: Int
  , dataKeysOrd :: [Int]
  , dataKeysRep :: [Int]
  , dataKeysRand :: [Int]
  , dataList :: [Int]
  , dataSeq  :: S.Seq Int
  , dataMap  :: M.Map Int Int
  , dataIMap :: IM.IntMap Int
  , dataHMap :: HM.HashMap Int Int
  } deriving (Generic, NFData)

data DataEnv = DataEnv
  { dataScale0 :: DataValues
  , dataScale1 :: DataValues
  , dataScale2 :: DataValues
  , dataScale3 :: DataValues
  } deriving (Generic, NFData)

initDataEnv :: IO DataEnv
initDataEnv = pure $ DataEnv (fscale 10) (fscale 100) (fscale 1000) (fscale 10000)
  where
  fscale n = DataValues n (10000 `div` n) keys keysRep keysRand
    keys
    (S.fromList keys)
    (M.fromList $ zip keys keys)
    (IM.fromList $ zip keys keys)
    (HM.fromList $ zip keys keys)
    where
    keys = [0..n-1]
    keysRep = map (`mod` n) [0..10000-1]
    keysRand = take n $ R.randoms (R.mkStdGen n)

benchDataOp :: DataEnv -> String -> DataOp -> Benchmark
benchDataOp st name f = bgroup name
  [ bgroup "0" $ f (dataScale0 st)
  , bgroup "1" $ f (dataScale1 st)
  , bgroup "2" $ f (dataScale2 st)
  , bgroup "3" $ f (dataScale3 st)
  ]

type DataOp = DataValues -> [Benchmark]

benchDataIndex :: DataValues -> [Benchmark]
benchDataIndex st =
  [ fgo "list" (\i xs -> Just (xs !! i)) dataList
  , fgo "seq" S.lookup dataSeq
  , fgo "map" M.lookup dataMap
  , fgo "imap" IM.lookup dataIMap
  , fgo "hmap" HM.lookup dataHMap
  ]
  where
  fgo :: String -> (Int -> a -> Maybe Int) -> (DataValues -> a) -> Benchmark
  fgo s f proj = bench s (nf (zipWith f (dataKeysRep st)) (repeat $ proj st))

benchDataCons :: DataValues -> [Benchmark]
benchDataCons st =
  [ fgo "list" (:)
  , fgo "seq" (S.<|)
  , fgo "map" (\v -> M.insert v v)
  , fgo "imap" (\v -> IM.insert v v)
  , fgo "hmap" (\v -> HM.insert v v)
  ]
  where
  fgo :: (Monoid a, NFData a) => String -> (Int -> a -> a) -> Benchmark
  fgo s f = bench s (nf (replicateM (dataRepeat st) $ foldr f mempty) (dataKeysOrd st))

benchDataInsert :: DataValues -> [Benchmark]
benchDataInsert st =
  [ fgo "map" M.insert
  , fgo "imap" IM.insert
  , fgo "hmap" HM.insert
  ]
  where
  fgo :: (Monoid a, NFData a) => String -> (Int -> Int -> a -> a) -> Benchmark
  fgo s f = bench s (nf (replicateM (dataRepeat st) $ foldr (\k -> f k k) mempty) (dataKeysRand st))
