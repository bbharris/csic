module Main where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Front.Syntax
import Csic.Front.Load
import Csic.Front.Options
import Csic.Back.Api
import Options.Applicative
import System.Exit
import System.IO

data Options = Options
  { coreOptions :: CoreOptions
  , checkOnly   :: Bool
  , modulePath  :: FilePath
  }

optionsParser :: Parser Options
optionsParser = Options
  <$> coreOptionsParser
  <*> switch (long "check-only" <> short 'c' <> help "Disable module execution.")
  <*> strOption (long "module" <> short 'm' <> metavar "PATH" <> value "test/functional/effects" <>
                 help "Input module path.")

main :: IO ()
main = do
  opts <- execParser $ info (optionsParser <**> helper) $
    header "csic - CLI for the Csic Programming Language" <>
    progDesc "Type-checks and executes Csic module PATH."
  (res, fenv) <- runStateT (runExceptT (loadModule nullLoc (modulePath opts)))
    (initFrontEnv $ initCEnv (coreOptions opts))
  let popts1 = printOptionsVerbose {showLocStr = showSrcLocError (frontSrcMap fenv), showColor = True}
  case res of
    Left e -> do
      putStr $ prettyErrorsLn (PrettyEnv popts1 0) e
      exitFailure
    Right ns -> do
      when (checkOnly opts) exitSuccess
      let decls = lookupModuleLocalCDecls ns fenv
      bracket (initBackend [TargetInterp, TargetChez]) closeBackend $ \be ->
        forM_ decls $ \d -> when (declHasAnnot AnnTest d && qspace (declNameQ d) == ns) $
          case compileCDeclToMIR (getCEnv fenv) d of
            Left e -> putStr $ prettyErrorsLn (PrettyEnv popts1 0) e
            Right p -> do
              hSetBuffering stdout NoBuffering
              let rtcfg = backConfigDefault
              res2 <- runExceptT $ compileProg be TargetChez rtcfg p >>= \f -> f 0
              case res2 of
                Left e -> error $ prettyErrorsLn (PrettyEnv popts1 0) e
                Right _ -> pure ()
              putStrLn ""