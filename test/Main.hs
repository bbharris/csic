{-# OPTIONS_GHC -Wno-orphans #-}
module Main where
import Csic.Util.Prelude
import Csic.Core.Environment
import Csic.Front.Syntax
import Csic.Front.Load
import Csic.Back.Api
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.Options
import Data.IORef
import Test.Tasty.Golden
import System.IO
import Data.List (isPrefixOf)
import Reference.Effect
import Unit.Multi
import Csic.Front.Options
import Options.Applicative
import Data.List.Split (splitOn)

-- ************************************************************************************************

data TestState = TestState
  { cachedEnvs :: HashMap String TestState1
  , testBackend :: Backend
  }

data TestState1 = TestState1
  { testNs :: Namespace
  , testEnv :: FrontEnv
  , testPrograms :: [MirProgram]
  }

getTestState :: IORef TestState -> CoreOptions -> String -> IO TestState1
getTestState ref opts k =
  maybe v (\r -> r {testEnv = mapOpts (const opts) (testEnv r)}) . lookup k . cachedEnvs <$> readIORef ref
  where
  v = TestState1 (Namespace "") (initFrontEnv $ initCEnv opts) []

setTestState :: IORef TestState -> String -> TestState1 -> IO ()
setTestState ref k v = atomicModifyIORef ref $ \st -> (st {cachedEnvs = insert k v (cachedEnvs st)}, ())

-- ************************************************************************************************

targets :: [BackendTarget]
targets =
  [ TargetInterp
  , TargetChez
  -- , TargetC
  ]

main :: IO ()
main = bracket (initBackend targets) closeBackend $ \be -> do
  ref <- newIORef (TestState mempty be)
  defaultMainWithIngredients ings $ askOption $ \opts -> testGroup "all"
    [ testGroup "reference"
      [ testCase "effects" $ assertEqual "testEffects_1" (Nothing, 1) testEffects_1
      ]
    , testGroup "unit"
      [ multiTests
      ]
    , testGroup "functional"
      [ moduleTest opts ref "none"   "stdlib" []
      , moduleTest opts ref "none"   "paradox-hurkens" [ExpectError]
      , moduleTest opts ref "none"   "paradox-curry"   [ExpectError]
      , moduleTest opts ref "stdlib" "syntax" []
      , moduleTest opts ref "stdlib" "type-universes" []
      , moduleTest opts ref "stdlib" "data-basic" []
      , moduleTest opts ref "stdlib" "data-induction" []
      , moduleTest opts ref "stdlib" "data-recursion" []
      , moduleTest opts ref "stdlib" "data-induction-recursion" []
      , moduleTest opts ref "stdlib" "data-induction-induction" []
      , moduleTest opts ref "stdlib" "inference" []
      , moduleTest opts ref "stdlib" "multiplicity" []
      , moduleTest opts ref "stdlib" "codegen-1" [Exec]
      , moduleTest opts ref "stdlib" "codegen-2" [Exec]
      , moduleTest opts ref "stdlib" "primitives" [Exec]
      , moduleTest opts ref "stdlib" "type-classes" [Exec]
      , moduleTest opts ref "stdlib" "effects" [Exec]
      ]
    ]
  where
  ings = includingOptions [Option (Proxy @CoreOptions)] : defaultIngredients

-- ************************************************************************************************

data TestFlag = ExpectError | Exec
  deriving (Eq, Ord, Show)

moduleTest :: CoreOptions -> IORef TestState -> String -> String -> [TestFlag] -> TestTree
moduleTest opts ref prereq name flags = testGroup name $
  [ checkTest
  , outputTest
  ] <> if Exec `notElem` flags then [] else
    [ testGroup "compile" $ compTestBase : map compTestTarget targets
    , testGroup "execute" $ map execTestTarget targets
    ]
  where
  dir = "test/functional/"

  poptsLogError = printOptionsDefault
  poptsLogOkay = printOptionsDefault
    { showImplicits = True
    }
  poptsCmdLine fenv = (printOpts opts)
    { showLocStr = showSrcLocError (frontSrcMap fenv)
    , showLocations = True
    , showColor = False
    , showErrorDetails = True
    }

  diffTest testName suffix fwrite = localOption OnPass $ if False
    then goldenVsFile testName goldPath tempPath (fwrite tempPath)
    else goldenVsFileDiff testName (\old new -> ["diff", "-u", old, new]) goldPath tempPath (fwrite tempPath)
    where
    tempPath = dir <> "output/" <> name <> "." <> suffix <> ".temp"
    goldPath = dir <> "output/" <> name <> "." <> suffix <> ".gold"

  checkTest = after AllSucceed ("functional." <> prereq <> ".check") $ testCase "check" $ do
    fenv <- testEnv <$> getTestState ref opts prereq
    let fgo = handleAll (checkError nullLoc . show) $ loadModule nullLoc (dir <> name)
    (res, fenv') <- processResult <$> runStateT (runExceptT fgo) (fenv {frontErrLog = []})
    case res of
      Left e -> assertFailure $ prettyErrorsLn (PrettyEnv (poptsCmdLine fenv') 0) e
      Right ns -> setTestState ref name (TestState1 ns fenv' [])
    where
    processResult (res, fenv) = case res of
      Left es | ExpectError `elem` flags -> (Right (Namespace ""), fenv {frontErrLog = es' : frontErrLog fenv})
        where
        es' = map prettifyError es
        prettifyError = \case
          CErrTree l s ls -> CErrTree l (omitLoopInfo s) ls
          e -> e
        omitLoopInfo s = if "OUT OF FUEL" `isPrefixOf` s then "OUT OF FUEL: <details-omitted>" else s
      _ -> (res, fenv)

  outputTest = after AllSucceed ("functional." <> name <> ".check") $ diffTest "output" "check" $ \tempPath -> do
    TestState1 ns fenv _ <- getTestState ref opts name
    let decls = lookupModuleLocalCDecls ns fenv
    let okays = filter (declHasAnnot AnnPrint) decls
    let errs = reverse $ frontErrLog fenv
    withFile tempPath WriteMode $ \h -> do
      forM_ okays $ \d -> hPutStrLn h $ prettyDeclLn (PrettyEnv poptsLogOkay 0) d
      forM_ errs $ \err -> hPutStrLn h $ prettyErrorsLn (PrettyEnv poptsLogError 0) err

  compTestBase = after AllSucceed ("functional." <> name <> ".check") $ testCase "base" $ do
    TestState1 ns fenv _ <- getTestState ref opts name
    let decls = filter (declHasAnnot AnnTest) $ lookupModuleLocalCDecls ns fenv
    progs <- forM decls $ \d -> case compileCDeclToMIR (getCEnv fenv) d of
      Left e -> assertFailure $ prettyErrorsLn (PrettyEnv (poptsCmdLine fenv) 0) e
      Right p -> pure p
    setTestState ref name (TestState1 ns fenv progs)

  compTestTarget target =
    after AllSucceed ("functional." <> name <> ".compile.base" ) $ testCase tgt $ do
      be <- testBackend <$> readIORef ref
      TestState1 _ fenv progs <- getTestState ref opts name
      let rtcfg = backConfigDefault {backVerbose = False, backOptimize = False, backCaching = False}
      forM_ progs $ \prog -> do
        res <- runExceptT $ compileProg be target rtcfg prog
        case res of
          Left e -> assertFailure $ prettyErrorsLn (PrettyEnv (poptsCmdLine fenv) 0) e
          Right _ -> pure ()
    where
    tgt = show target

  execTestTarget target =
    after AllSucceed ("functional." <> name <> ".compile." <> tgt) $ diffTest tgt ("exec-" <> tgt) $ \tempPath -> do
      be <- testBackend <$> readIORef ref
      TestState1 _ fenv progs <- getTestState ref opts name
      withFile tempPath WriteMode $ \h -> do
        hSetBuffering h LineBuffering -- to interleave output with C FFI tests
        let rtcfg = backConfigDefault {backStdout = Just h, backOptimize = False, backCaching = False}
        forM_ progs $ \prog -> do
          res <- runExceptT $ do
            _ <- compileProg be target rtcfg prog >>= \f -> f 10
            liftIO $ hPutStrLn h ""
          case res of
            Left e -> error $ prettyErrorsLn (PrettyEnv (poptsCmdLine fenv) 0) e
            Right _ -> pure ()
    where
    tgt = show target

-- ************************************************************************************************

instance IsOption CoreOptions where
  optionName = pure "core"
  optionHelp = pure "Comma-delimited CoreOptions."
  defaultValue = coreOptionsDefault
  parseValue = getParseResult . execParserPure (prefs mempty) (info coreOptionsParser mempty) . splitOn ","
