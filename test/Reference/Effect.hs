{- | Reference implementation of a type-safe extensible effects library.

  Based on ideas from:
  - https://okmij.org/ftp/Haskell/extensible/exteff.pdf
  - https://hackage.haskell.org/package/extensible-effects
-}
-- See package.yaml for default extensions.
{-# LANGUAGE TypeFamilies #-}
module Reference.Effect (testEffects_1) where
import Prelude
import Control.Applicative
import Control.Monad
import Data.Kind
import Data.Type.Equality
import Data.Void

-- ************************************************************************************************
-- Dependency: natural numbers

-- | Natural number. We could use Data.Type.Natural, but this is simpler to use at the type-level.
data Nat where
  Z :: Nat
  S :: Nat -> Nat
  deriving Show

-- | Singleton wrapper for natural numbers (used to transport between Haskell's value and type levels).
data SNat (i :: Nat) where
  SZ :: SNat Z
  SS :: SNat i -> SNat (S i)

-- ************************************************************************************************
-- Dependency: indexed type lists

-- | Type indexed by another type (e.g. an operation result type).
type IndexedType = Type -> Type

-- | Type-level function to index into a list of indexed types, returning void when out of bounds.
type family Index (i :: Nat) (rs :: [IndexedType]) :: IndexedType where
  Index _ '[] = Const Void
  Index Z (x:_) = x
  Index (S i) (_:xs) = Index i xs

-- ************************************************************************************************
-- Dependency: indexed type unions and automated membership proofs

-- | Extensible union of indexed types.
data Union (rs :: [IndexedType]) (a :: Type) where
  Union :: SNat i -> Index i rs a -> Union rs a

-- | Proof that an indexed type is contained in a list of indexed types.
data MemberProof (r :: IndexedType) (rs :: [IndexedType]) where
  MemberProof :: SNat i -> (r :~: Index i rs) -> MemberProof r rs

-- | Type-class wrapper to infer membership proofs.
class Member (r :: IndexedType) (rs :: [IndexedType]) where
  memberProof :: MemberProof r rs

-- | Base case of membership proofs.
instance {-# OVERLAPPING #-} Member r (r:rs) where
  memberProof = MemberProof SZ Refl

-- | Inductive step of membership proofs.
instance {-# OVERLAPPING #-} Member r rs => Member r (y:rs) where
  memberProof = case memberProof @r @rs of
    MemberProof i eq -> MemberProof (SS i) eq

-- | Inject a value into indexed type union.
inject :: forall r rs a. (Member r rs) => r a -> Union rs a
inject op = case memberProof @r @rs of
  MemberProof i eq -> Union i (gcastWith eq op)

-- | Decompose an indexed type union into a head value or the rest of the union.
decomp :: forall r rs a. Union (r:rs) a -> Either (r a) (Union rs a)
decomp (Union i op) = case i of
  SZ -> Left op
  SS j -> Right $ Union j op

-- ************************************************************************************************
-- Library: extensible free effect monad

-- | Effect monad paramerized by a list of services (indexed types).
data Effect rs a where
  Return :: a -> Effect rs a
  BindOp :: Union rs b -> (b -> Effect rs a) -> Effect rs a

instance Functor (Effect rs) where
  fmap f = \case
    Return a -> Return (f a)
    BindOp op k -> BindOp op (fmap f . k)

instance Applicative (Effect rs) where
  pure = Return
  (<*>) = ap

instance Monad (Effect rs) where
  ma >>= fmb = case ma of
    Return a -> fmb a
    BindOp op k -> BindOp op (k >=> fmb)

-- | Convert an effect with no services into a pure value.
runPure :: Effect '[] a -> a
runPure = \case
  Return a -> a
  BindOp (Union _ (Const nope)) _ -> absurd nope

-- ************************************************************************************************
-- Example: failure effect

data Fail a where
  Fail :: Fail a

fail' :: (Member Fail rs) => Effect rs a
fail' = BindOp (inject Fail) Return

runFail :: Effect (Fail:rs) a -> Effect rs (Maybe a)
runFail = \case
  Return a -> Return (Just a)
  BindOp op k -> case decomp op of
    Left Fail -> Return Nothing
    Right op' -> BindOp op' (runFail . k)

-- ************************************************************************************************
-- Example: state effect

data State s a where
  Get :: State s s
  Put :: s -> State s ()

get :: (Member (State s) rs) => Effect rs s
get = BindOp (inject Get) Return

put :: (Member (State s) rs) => s -> Effect rs ()
put s = BindOp (inject (Put s)) Return

runState :: s -> Effect (State s:rs) a -> Effect rs (a, s)
runState s = \case
  Return a -> Return (a, s)
  BindOp op k -> case decomp op of
    Left Get -> runState s (k s)
    Left (Put v) -> runState v (k ())
    Right op' -> BindOp op' (runState s . k)

-- ************************************************************************************************
-- Example: effect composition and handling

increment :: (Member (State Int) rs) => Effect rs Int
increment = get >>= (\n -> put (n + 1) >> pure n)

testEffects_1 :: (Maybe Int, Int)
testEffects_1 = runPure $ runState 0 $ runFail (increment >> fail' >> increment)
