module Unit.Multi (multiTests) where
import Csic.Util.Prelude
import Csic.Core.Syntax.Multi
import Csic.Core.Check.Multi
import Test.Tasty
import Test.Tasty.Falsify
import Data.Default
import qualified Test.Falsify.Generator as Gen
import qualified Test.Falsify.Range as Range
import qualified Data.List.NonEmpty as N
import Test.Tasty.HUnit hiding (assert)

-- ************************************************************************************************

multiTests :: TestTree
multiTests = testGroup "multi"
  [ testGroup "sanity"
    [ sanityTest "norm-0" $ let sp = multiSpace 2 in checkNorms sp (allMultisRaw MultiZero (spaceUVars sp))
    , sanityTest "norm-1" $ let sp = multiSpace 3 in checkNorms sp (allMultisRaw MultiOne  (spaceUVars sp))
    , sanityTest "subst"  $ let sp = multiSpace 3 in checkAll checkSubst sp            (spaceMultis sp)
    , sanityTest "plus"   $ let sp = multiSpace 3 in checkAll checkPlus  sp (allPairs $ spaceMultis sp)
    , sanityTest "times"  $ let sp = multiSpace 3 in checkAll checkTimes sp (allPairs $ spaceMultis sp)
    , sanityTest "conv"   $ let sp = multiSpace 3 in checkAll checkConv  sp (allPairs $ spaceMultis sp)
    ]
  , testGroup "random"
    [ randomTest "norm"     100 $ let sp = multiSpace 4 in checkNorms sp <$> gen (genList 200 $ genMultiRaw sp)
    , randomTest "subst"   1000 $ let sp = multiSpace 5 in checkSubst sp <$> gen           (genMulti sp)
    , randomTest "plus"    1000 $ let sp = multiSpace 5 in checkPlus  sp <$> gen (genPair $ genMulti sp)
    , randomTest "times"   1000 $ let sp = multiSpace 5 in checkTimes sp <$> gen (genPair $ genMulti sp)
    , randomTest "conv"   10000 $ let sp = multiSpace 5 in checkConv  sp <$> gen (genPair $ genMulti sp)
    , randomTest "solver-0" 200 $ let sp = multiSpace' (0, 4) in checkSolver sp <$> gen (genMultiConsts 10 sp)
    , randomTest "solver-1" 200 $ let sp = multiSpace' (1, 3) in checkSolver sp <$> gen (genMultiConsts 10 sp)
    , randomTest "solver-2" 200 $ let sp = multiSpace' (2, 2) in checkSolver sp <$> gen (genMultiConsts 10 sp)
    , randomTest "solver-3" 200 $ let sp = multiSpace' (3, 1) in checkSolver sp <$> gen (genMultiConsts 10 sp)
    ]
  ]
  where
  sanityTest s f = testCase s $ either (`assertBool` False) (const $ pure ()) f
  randomTest s n f = testPropertyWith opts s $ f >>= either testFailed (const $ pure ())
    where
    opts = def {overrideNumTests = Just n}

-- ************************************************************************************************

type TestFunc m a = MultiSpace m -> a -> Either String ()

checkAll :: TestFunc m a -> TestFunc m [a]
checkAll f sp = mapM_ (f sp)

-- ************************************************************************************************

checkNorms :: (Foldable f) => TestFunc Void (f CMulti)
checkNorms sp = void . foldlM (checkNorm sp) mempty

type NormEquivMap = HashMap [MultiLit] CMulti

checkNorm :: MultiSpace Void -> NormEquivMap -> CMulti -> Either String NormEquivMap
checkNorm sp res m0 = do
  unless (ec0 == ec1) $ Left $ "INVALID: " <> show m0 <> " = " <> show m1
  case lookup ec1 res of
    Nothing -> pure $ insert ec1 m1 res
    Just m2 -> do
      unless (m2 == m1) $ Left $ "MISSING: " <> show m2 <> " = " <> show m1
      pure res
  where
  m1 = multiNormalize (multiSOP m0)
  ec0 = multiEquivClass (spaceUSubs sp) m0
  ec1 = multiEquivClass (spaceUSubs sp) m1

-- ************************************************************************************************

checkSubst :: TestFunc Void CMulti
checkSubst sp x = do
  let ec1 = multiEquivClass (spaceUSubs sp) x
  let ec2 = fmap (\sub -> substMultiMap (multiLit <$> sub) x) (spaceUSubs sp)
  unless (fmap multiLit ec1 == ec2) $ Left $ "INVALID: " <> show x

-- ************************************************************************************************

checkBinaryOp :: String -> (CMulti -> CMulti -> CMulti) -> (MultiLit -> MultiLit -> MultiLit) ->
    TestFunc Void (CMulti, CMulti)
checkBinaryOp s f g sp (x, y) = do
  let z = f x y
  let ec1 = zipWith g (multiEquivClass (spaceUSubs sp) x) (multiEquivClass (spaceUSubs sp) y)
  let ec2 = multiEquivClass (spaceUSubs sp) z
  unless (ec1 == ec2) $ Left $ show x <> s <> show y <> " /= " <> show z

checkPlus :: TestFunc Void (CMulti, CMulti)
checkPlus = checkBinaryOp " + " multiPlus multiLitPlus

checkTimes :: TestFunc Void (CMulti, CMulti)
checkTimes = checkBinaryOp " * " multiTimes multiLitTimes

-- ************************************************************************************************

checkConv :: TestFunc Void (CMulti, CMulti)
checkConv sp (x, y) = do
  let c = Constraint (nullLocated x) CConv (nullLocated y)
  let res1 = and $ zipWith multiLitConvertible (multiEquivClass (spaceUSubs sp) x) (multiEquivClass (spaceUSubs sp) y)
  let res2 = multiConvertible x y
  when (res2 && not res1) $ Left $ "INVALID: " <> show c
  when (res1 && not res2) $ Left $ "MISSING: " <> show c

-- ************************************************************************************************

checkSolver :: TestFunc MetaId [MultiConstraint MetaId]
checkSolver sp cs = case foldr filterSolutions (spaceMSubs sp) cs of
  [] -> case solverResult of
    Left ("invalid constraint", _) -> pure ()
    Left e -> Left $ "expected invalid constraint, but got: " <> show e
    Right _ -> Left "expected invalid constraint, but got success"
  [solution] -> case solverResult of
    Left e -> Left $ "expected success, but got: " <> show e
    Right solution' -> unless (solution == solution') $ Left $ "invalid: " <> show solution <> " /= " <> show solution'
  solutions -> case solverResult of
    Left ("ambiguous multi metavariables", _) -> pure ()
    Left e -> Left $ "expected success or ambiguity, but got: " <> show e
    Right solution -> unless (solution `elem` solutions) $ Left $ "invalid solution: " <> show solution
  where
  uvarN = length $ spaceUVars sp
  mvars = AllocN (length (spaceMVars sp)) (map (const nullLoc) $ spaceMVars sp)
  solverResult = solveMultiConstraints False uvarN mvars cs
  filterSolutions c = filter (\sub -> isValidMultiConstraint (substMultiMap (vacuousMultiMapV sub) <$> c))

-- ************************************************************************************************

multiEquivClass :: [CMultiSubst] -> Multi Void -> [MultiLit]
multiEquivClass subs m = fmap (`evalMulti` m) subs

evalMulti :: CMultiSubst -> CMulti -> MultiLit
evalMulti subs = foldr (multiLitPlus . fproduct) MultiZero . multiSOP
  where
  fproduct (vs, n) = foldr (multiLitTimes . fvar) n vs
  fvar v = fromMaybe (error ("bad subst " <> show v)) $ lookup v subs

-- ************************************************************************************************

data MultiSpace m = MultiSpace
  { spaceUVars  :: [CMultiId]
  , spaceUSubs  :: [CMultiSubst]
  , spaceMVars  :: [MultiId m]
  , spaceMSubs  :: [MultiMap m Void]
  , spacePower  :: [[MultiId m]]
  , spaceMultis :: [Multi m]
  }

type CMultiSubst = HashMap CMultiId MultiLit

multiSpace :: Int -> MultiSpace Void
multiSpace n = fromMaybe (error (show n)) (lookup n cache)
  where
  cache :: HashMap Int (MultiSpace Void)
  cache = fromList [(u, fmake u) | u <- [0..5]]
  fmake u = MultiSpace uvars usubs [] [mempty] (powerSet uvars) (allMultisUniq uvars)
    where
    uvars = take u allUserMultis
    usubs = allSubsts uvars [minBound..]

multiSpace' :: (Int, Int) -> MultiSpace MetaId
multiSpace' dims = fromMaybe (error (show dims)) (lookup dims cache)
  where
  cache :: HashMap (Int, Int) (MultiSpace MetaId)
  cache = fromList [let d = (u, m) in (d, fmake d) | u <- [0..5], m <- [1..5]]
  fmake :: (Int, Int) -> MultiSpace MetaId
  fmake (u, m) =
    MultiSpace (spaceUVars sp0) (spaceUSubs sp0) mvars msubs (powerSet vars) (allMultisUniq vars)
    where
    sp0 = multiSpace u
    mvars = take m [multiIdMeta (MetaId i) | i <- [0..]]
    msubs = allSubsts mvars (spaceMultis sp0)
    vars = map vacuousMultiId (spaceUVars sp0) <> mvars

-- ************************************************************************************************

genMultiConsts :: Word -> MultiSpace m -> Gen [MultiConstraint m]
genMultiConsts n sp = Gen.list (Range.between (1, n)) (genMultiConst sp)

genMultiConst :: MultiSpace m -> Gen (MultiConstraint m)
genMultiConst sp = do
  m1 <- genMulti sp
  rel <- Gen.frequency [(1, pure CEqual), (1, pure CConv)]
  m2 <- genMulti sp
  pure $ Constraint (nullLocated m1) rel (nullLocated m2)

genMulti :: MultiSpace m -> Gen (Multi m)
genMulti sp = multiNormalize . multiSOP <$> genMultiRaw sp

genMultiRaw :: MultiSpace m -> Gen (Multi m)
genMultiRaw sp = fmap MultiRaw $ catMaybes <$> mapM (genMaybe . fproduct) (spacePower sp)
  where
  fproduct = \case
    [] -> Gen.frequency [(10, pure ([], MultiOne)), (1, pure ([], MultiAny))]
    vs -> Gen.elem ((vs, MultiOne) N.:| [(vs, MultiAny)])

-- ************************************************************************************************

genPair :: Gen a -> Gen (a, a)
genPair f = (,) <$> f <*> f

genMaybe :: Gen a -> Gen (Maybe a)
genMaybe f = Gen.oneof (pure Nothing N.:| [Just <$> f])

genList :: Word -> Gen a -> Gen [a]
genList n = Gen.list (Range.between (0, n))

-- ************************************************************************************************

allPairs :: [a] -> [(a, a)]
allPairs xs = [(x, y) | x <- xs, y <- xs]

allSubsts :: (Hashable k) => [k] -> [v] -> [HashMap k v]
allSubsts ks vs = fromList <$> foldr (\k -> concatMap (\xs -> fmap (\m -> (k,m):xs) vs)) [[]] ks